
#include <gmm/includedefs.h>
#include <grasp/ptCloudIndividual.h>


void SegmentationUsingGMM();

void twoClassSegmentationUsingGMM( string class_1, string class_2, int num_img );

void SegmentationUsingGMM( string class_1, int start_num, int end_num, int total_number );

void SegmentationUsingGMM( string class_1,  int total_number );

bool classifySegment( std::vector<PointCloudIndividual> &segRegion,
                      const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloud_rgb,
                      string class_1,
                      pcl::PointCloud<pcl::PointXYZRGB>::Ptr &vis_cloud_rgb);
