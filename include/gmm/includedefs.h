#ifndef INCLUDEDEFS_H
#define INCLUDEDEFS_H


#include <ros/ros.h>
#include <stdio.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <ros/ros.h>

// header for math functions
#include <math.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>

#include <omp.h>

// header file for path formation
#include <dirent.h>


using namespace cv;
using namespace std;

#define TRAIN_GMM   1


#endif
