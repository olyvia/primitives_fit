#ifndef SHAPEFIT_H  
#define SHAPEFIT_H 

#include <grasp/includedefs.h>
#include <grasp/ptCloudIndividual.h>

#ifdef BOOST_UBLAS_TYPE_CHECK
#	undef BOOST_UBLAS_TYPE_CHECK
#endif
#define BOOST_UBLAS_TYPE_CHECK 0
#ifndef _USE_MATH_DEFINES
#	define _USE_MATH_DEFINES
#endif

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <vector>
#include <stdexcept>

//Polynomial Fit
#include<iostream>
#include<iomanip>
#include<cmath>
using namespace std;

class ShapeFit
{

public :
/*
    Finds the coefficients of a polynomial p(x) of degree n that fits the data,
    p(x(i)) to y(i), in a least squares sense. The result p is a row vector of
    length n+1 containing the polynomial coefficients in incremental powers.

    param:
        oX				x axis values
        oY				y axis values
        nDegree			polynomial degree including the constant

    return:
        coefficients of a polynomial starting at the constant coefficient and
        ending with the coefficient of power to nDegree. C++0x-compatible
        compilers make returning locally created vectors very efficient.

*/
template<typename T>
std::vector<T> polyfit( const std::vector<T>& oX, const std::vector<T>& oY, int nDegree )
{
    using namespace boost::numeric::ublas;

    if ( oX.size() != oY.size() )
        throw std::invalid_argument( "X and Y vector sizes do not match" );

    // more intuative this way
    nDegree++;

    size_t nCount =  oX.size();
    matrix<T> oXMatrix( nCount, nDegree );
    matrix<T> oYMatrix( nCount, 1 );

    // copy y matrix
    for ( size_t i = 0; i < nCount; i++ )
    {
        oYMatrix(i, 0) = oY[i];
    }

    // create the X matrix
    for ( size_t nRow = 0; nRow < nCount; nRow++ )
    {
        T nVal = 1.0f;
        for ( int nCol = 0; nCol < nDegree; nCol++ )
        {
            oXMatrix(nRow, nCol) = nVal;
            nVal *= oX[nRow];
        }
    }

    // transpose X matrix
    matrix<T> oXtMatrix( trans(oXMatrix) );
    // multiply transposed X matrix with X matrix
    matrix<T> oXtXMatrix( prec_prod(oXtMatrix, oXMatrix) );
    // multiply transposed X matrix with Y matrix
    matrix<T> oXtYMatrix( prec_prod(oXtMatrix, oYMatrix) );

    // lu decomposition
    permutation_matrix<int> pert(oXtXMatrix.size1());
    const std::size_t singular = lu_factorize(oXtXMatrix, pert);
    // must be singular
    BOOST_ASSERT( singular == 0 );

    // backsubstitution
    lu_substitute(oXtXMatrix, pert, oXtYMatrix);

    // copy the result to coeff
    return std::vector<T>( oXtYMatrix.data().begin(), oXtYMatrix.data().end() );
}


template<typename T>
T polyfit_reg( const std::vector<T>& ix, const std::vector<T>& iy, int nDegree )
{

    using namespace std;
    if ( ix.size() != iy.size() )
        throw std::invalid_argument( "X and Y vector sizes do not match" );

    int N = ix.size();
    int i,j,k;

    // n is the degree of Polynomial
    std::vector<double> X(2*nDegree + 1);                        //Array that will store the values of sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
    for( i = 0; i < 2*nDegree + 1; i++ )
    {
        X[i] = 0;
        for( j = 0; j < N; j++ )
            X[i] = X[i] + pow(ix[j],i);        //consecutive positions of the array will store N,sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
    }

    double B[nDegree + 1][nDegree + 2];
    std::vector<double> a(nDegree + 1);            //B is the Normal matrix(augmented) that will store the equations, 'a' is for value of the final coefficients
    for( i = 0; i <= nDegree; i++)
    {
        for ( j = 0; j <= nDegree; j++)
            B[i][j]=X[i+j];
    }

    //Build the Normal matrix by storing the corresponding coefficients at the right positions except the last column of the matrix
    std::vector<double> Y(nDegree + 1);                    //Array to store the values of sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
    for ( i = 0; i < nDegree + 1; i++)
    {
        Y[i] = 0;
        for ( j = 0; j < N; j++)
            Y[i] = Y[i] + pow(ix[j],i) * iy[j];        //consecutive positions will store sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
    }

    for ( i = 0; i <= nDegree; i++)
    {
        B[i][nDegree+1] = Y[i];
    }//load the values of Y as the last column of B(Normal Matrix but augmented)

    nDegree = nDegree+1;                //n is made n+1 because the Gaussian Elimination part below was for n equations, but here n is the degree of polynomial and for n degree we get n+1 equations
    cout<<"\nThe Normal(Augmented Matrix) is as follows:\n";
    for (i=0;i<nDegree;i++)            //print the Normal-augmented matrix
    {
        for (j=0;j<=nDegree;j++)
            cout<<B[i][j]<<setw(16);
        cout<<"\n";
    }


    for( i = 0; i < nDegree; i++)
    {
        //From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
        for( k = i+1; k < nDegree; k++)
         {
            if( B[i][i] < B[k][i] )
            {
                for( j = 0; j <= nDegree; j++)
                {
                    double temp = B[i][j];
                    B[i][j] = B[k][j];
                    B[k][j] = temp;
                }
            }
        }
    }

    for( i = 0; i < nDegree - 1; i++)            //loop to perform the gauss elimination
    {
        for( k = i+1; k < nDegree; k++)
            {
                double t = B[k][i]/B[i][i];
                for( j = 0; j <= nDegree; j++)
                    B[k][j] = B[k][j] - t * B[i][j];    //make the elements below the pivot elements equal to zero or elimnate the variables
            }
    }

    for ( i = nDegree - 1; i >= 0; i--)                //back-substitution
    {                        //x is an array whose values correspond to the values of x,y,z..
        a[i] = B[i][nDegree];                //make the variable to be calculated equal to the rhs of the last equation
        for ( j = 0; j < nDegree; j++)
        {
            if( j != i )            //then subtract all the lhs values except the coefficient of the variable whose value                                   is being calculated
                a[i] = a[i] - B[i][j] * a[j];
        }
        a[i] = a[i]/B[i][i];            //now finally divide the rhs by the coefficient of the variable to be calculated
    }
    cout<<"\nThe values of the coefficients are as follows:\n";
    for( i = 0; i < nDegree; i++ )
        cout<<"x^"<<i<<"="<<a[i]<<endl;            // Print the values of x^0,x^1,x^2,x^3,....
    cout<<"\nHence the fitted Polynomial is given by:\ny=";
    for (i=0;i<nDegree;i++)
        cout<<" + ("<<a[i]<<")"<<"x^"<<i;
    cout<<"\n";
    return 0;

}


template<typename T>
T polyfit_( const std::vector<T>& ix, const std::vector<T>& iy, int n_degree )
{
        int i,j,k,n,N;
        cout.precision(6);                        //set precision
        cout.setf(ios::fixed);
        cout<<"\nEnter the no. of data pairs to be entered:\n";        //To find the size of arrays that will store x,y, and z values
        cin>>N;
        N = 15;
        double x[N],y[N];
        x[0] = 50;
        y[0] = 3.3;
        x[1] = 50;
        y[1] = 2.8;
        x[2] = 50;
        y[2] = 2.9;
        x[3] = 70;
        y[3] = 2.3;
        x[4] = 70;
        y[4] = 2.6;
        x[5] = 70;
        y[5] = 2.1;
        x[6] = 80;
        y[6] = 2.5;
        x[7] = 80;
        y[7] = 2.9;
        x[8] = 80;
        y[8] = 2.4;
        x[9] = 90;
        y[9] = 3.0;
        x[10] = 90;
        y[10] = 3.1;
        x[11] = 90;
        y[11] = 2.8;
        x[12] = 100;
        y[12] = 3.3;
        x[13] = 100;
        y[13] = 3.5;
        x[14] = 100;
        y[14] = 3.0;
    //    cout<<"\nEnter the x-axis values:\n";                //Input x-values
    //    for (i=0;i<N;i++)
    //        cin>>x[i];
    //    cout<<"\nEnter the y-axis values:\n";                //Input y-values
    //    for (i=0;i<N;i++)
    //        cin>>y[i];
        cout<<"\nWhat degree of Polynomial do you want to use for the fit?\n";
        cin>>n;                                // n is the degree of Polynomial
        double X[2*n+1];                        //Array that will store the values of sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
        for (i=0;i<2*n+1;i++)
        {
            X[i]=0;
            for (j=0;j<N;j++)
                X[i]=X[i]+pow(x[j],i);        //consecutive positions of the array will store N,sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
        }
        double B[n+1][n+2],a[n+1];            //B is the Normal matrix(augmented) that will store the equations, 'a' is for value of the final coefficients
        for (i=0;i<=n;i++)
            for (j=0;j<=n;j++)
                B[i][j]=X[i+j];            //Build the Normal matrix by storing the corresponding coefficients at the right positions except the last column of the matrix
        double Y[n+1];                    //Array to store the values of sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
        for (i=0;i<n+1;i++)
        {
            Y[i]=0;
            for (j=0;j<N;j++)
            Y[i]=Y[i]+pow(x[j],i)*y[j];        //consecutive positions will store sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
        }
        for (i=0;i<=n;i++)
            B[i][n+1]=Y[i];                //load the values of Y as the last column of B(Normal Matrix but augmented)
        n=n+1;                //n is made n+1 because the Gaussian Elimination part below was for n equations, but here n is the degree of polynomial and for n degree we get n+1 equations
        cout<<"\nThe Normal(Augmented Matrix) is as follows:\n";
        for (i=0;i<n;i++)            //print the Normal-augmented matrix
        {
            for (j=0;j<=n;j++)
                cout<<B[i][j]<<setw(16);
            cout<<"\n";
        }
        for (i=0;i<n;i++)                    //From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
            for (k=i+1;k<n;k++)
                if (B[i][i]<B[k][i])
                    for (j=0;j<=n;j++)
                    {
                        double temp=B[i][j];
                        B[i][j]=B[k][j];
                        B[k][j]=temp;
                    }

        for (i=0;i<n-1;i++)            //loop to perform the gauss elimination
            for (k=i+1;k<n;k++)
                {
                    double t=B[k][i]/B[i][i];
                    for (j=0;j<=n;j++)
                        B[k][j]=B[k][j]-t*B[i][j];    //make the elements below the pivot elements equal to zero or elimnate the variables
                }
        for (i=n-1;i>=0;i--)                //back-substitution
        {                        //x is an array whose values correspond to the values of x,y,z..
            a[i]=B[i][n];                //make the variable to be calculated equal to the rhs of the last equation
            for (j=0;j<n;j++)
                if (j!=i)            //then subtract all the lhs values except the coefficient of the variable whose value                                   is being calculated
                    a[i]=a[i]-B[i][j]*a[j];
            a[i]=a[i]/B[i][i];            //now finally divide the rhs by the coefficient of the variable to be calculated
        }
        cout<<"\nThe values of the coefficients are as follows:\n";
        for (i=0;i<n;i++)
            cout<<"x^"<<i<<"="<<a[i]<<endl;            // Print the values of x^0,x^1,x^2,x^3,....
        cout<<"\nHence the fitted Polynomial is given by:\ny=";
        for (i=0;i<n;i++)
            cout<<" + ("<<a[i]<<")"<<"x^"<<i;
        cout<<"\n";
        return 0;
    }//output attached as .jpg






void createHSHistogram( std::vector<PointCloudIndividual> segRegion,
                        pcl::PointCloud<pcl::PointXYZRGB>::Ptr pt_cloudRGB )

{



    for( int s_count = 0; s_count < segRegion.size(); s_count++ )
    {
        PointCloudIndividual obj = segRegion[s_count];
        std::vector<int> nnindx = obj.getNNIndices();
        Mat input_array, hsv;
        input_array = Mat( 1, nnindx.size(), CV_8UC3 );

        for( int idx= 0 ; idx < nnindx.size(); idx++ )
        {
            pcl::PointXYZRGB pt_rgb = pt_cloudRGB->points[nnindx[idx]];
            input_array.at<Vec3b>(0,idx)[0] = pt_rgb.b;
            input_array.at<Vec3b>(0,idx)[1] = pt_rgb.g;
            input_array.at<Vec3b>(0,idx)[2] = pt_rgb.r;

//            std::cout << "compare ... ";
//            std::cout << "[" << (int)pt_rgb.b << "," << (int)pt_rgb.g << "," << (int)pt_rgb.r << "]" << std::endl;
//            std::cout << "[" << (int)input_array.at<Vec3b>(0,idx)[0] << "," << (int)input_array.at<Vec3b>(0,idx)[1] << "," << (int)input_array.at<Vec3b>(0,idx)[2] << "]" << std::endl;
//            getchar();
        }
        std::cout << " mat size = " << input_array.size();
        cvtColor(input_array, hsv, CV_BGR2HSV);
        std::cout << " mat size = " << hsv.size();
        getchar();

    }



    // Quantize the hue to 30 levels
    // and the saturation to 32 levels

//    int hbins = 50, sbins = 32;
//    int histSize[] = {hbins, sbins};

//    // hue varies from 0 to 179, see cvtColor
//    float hranges[] = { 0, 180 };

//    // saturation varies from 0 (black-gray-white) to
//    // 255 (pure spectrum color)

//    float sranges[] = { 0, 256 };
//    const float* ranges[] = { hranges, sranges };

//    // we compute the histogram from the 0-th and 1-st channels
//    int channels[] = {0, 1};


//    if(mask.data == NULL)
//{

//        calcHist( &hsv, 1, channels, Mat(), // do not use mask

//                  hist, 2, histSize, ranges,

//                  true, // the histogram is uniform

//                  false );

//    }

//    else

//    {

//        calcHist( &hsv, 1, channels, mask, //  use mask
//                    hist, 2, histSize, ranges,
//                    true, // the histogram is uniform
//                    false );

//    }


//    cv::normalize(hist,hist,1.0,0.0,cv::NORM_L1);
    return;

}





};





#endif
