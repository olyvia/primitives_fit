#ifndef SURFACEPATCH_H
#define SURFACEPATCH_H

#include <grasp/includedefs.h>
#include <grasp/ptCloudIndividual.h>
#include <grasp/ShapeFit.h>
#include <pcl/features/boundary.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

//#include "/usr/local/include/opencv2/ml/ml.hpp"
#include <ml.h>
//#include <grasp/object_detection.h>
//#include <gmm/gmm_model.h>
#include <grasp_localize/cylindrical_shell.h>
#include "curve_fitting/curvature_estimation_taubin.h"
#include "curve_fitting/curvature_estimation_taubin.hpp"
#include "region_growing_modified/rg_full.h"
#include "region_growing_modified/rg_full.hpp"
//#include <grasp_localize/incClustering.h>

using namespace cv;
using namespace std;

#define PI 3.14159265
#define VISOPTION_SURFACE 0
#define FINGER_LENGTH_MAX 0.03
#define PERPENDICULAR_CUTOFF 0.5
#define AXIS_MATCH_BOX 0.68
#define FRACT_AXIS_CUTOFF 0.7

class SurfacePatch
{

    protected:

        std::vector<int>
        isBoundary( const std::vector<int> &pointIdxNKNSearch,
                    const std::vector<float> pointNKNSquaredDistance,
                    const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud,
                    const int cluster_process,
                    const int max_value );

        std::vector<std::vector<int> >
        mergeregions( const std::vector<PointCloudIndividual> &segregion,
                      const std::vector<std::vector<int> > neighbour_matix,
                      std::vector<std::vector<int> > &Neighbours,
                      const float threshold_merge );

        bool
        pca_axis_normal( std::vector<PointCloudIndividual> &segRegion,
                  const PointCloudXYZ::Ptr &pt_cloud );

        bool
        finalRegion_2(  std::vector<PointCloudIndividual> &segRegion,
                        const std::vector<std::vector<int> > neighbour_merge,
                        const std::vector<std::vector<int> > &neighbours );

        bool
        finalRegion_3(  std::vector<PointCloudIndividual> &segRegion,
                        const std::vector<std::vector<int> > neighbour_merge,
                        const std::vector<std::vector<int> > &neighbours );

        bool
        curvatureEstimation( const PointCloudXYZ::Ptr &cloud,
                                 std::vector<PointCloudIndividual> &segRegion );

        double
        curvatureEstimation( const PointCloudXYZ::Ptr &cloud,
                                const std::vector<int> &nnIndices );

//        int
//        classifySegment_gmm( std::vector<PointCloudIndividual> &segRegion,
//                              const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloud_rgb,
//                              std::string obj_cls );

//        std::vector<int>
//        classifySegment_gmm_v1( std::vector<PointCloudIndividual> &segRegion,
//                              const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloud_rgb,
//                              std::string obj_cls );

        std::vector<int>
        classifySegment_gmm_color_curv( std::vector<PointCloudIndividual> &segRegion,
                              const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloud_rgb,
                              std::string obj_cls );

        int
        fit_primitives(std::vector<PointCloudIndividual> &segRegion,
                 std::vector<int> &segnum,
                 const PointCloudXYZ::Ptr &f_cloud,
                 std::vector<Cylinder_Param> &cylind,
                 std::vector<Sphere_Param> &sphere,
                 std::vector<Box_Param> &box ,
                 std::vector<int> &localize_seg);

        int
        fit_primitives_big_segment(std::vector<PointCloudIndividual> &segRegion,
                 std::vector<int> &segnum,
                 const PointCloudXYZ::Ptr &f_cloud,
                 std::vector<Cylinder_Param> &cylind,
                 std::vector<Sphere_Param> &sphere,
                 std::vector<Box_Param> &box ,
                 std::vector<int> &localize_seg);

        int
        fit_primitives_color_curv(std::vector<PointCloudIndividual> &segRegion,
                 std::vector<int> &segnum,
                 const PointCloudXYZ::Ptr &f_cloud,
                 std::vector<Cylinder_Param> &cylind,
                 std::vector<Sphere_Param> &sphere,
                 std::vector<Box_Param> &box ,
                 std::vector<int> &localize_seg);

        // have to check the axis length
        bool
        fit_cylinder_sphere( std::vector<PointCloudIndividual> &segRegion,
                         std::vector<int> &segnum,
                         const PointCloudXYZ::Ptr &f_cloud,
                         const float diff_radius,
                         std::vector<Cylinder_Param> &cylin_final,
                         std::vector<Sphere_Param> &sphere_final,
                         std::vector<int> &localize_seg);

        int
        fit_primitives_color_curv_1(std::vector<PointCloudIndividual> &segRegion,
                 std::vector<int> &segnum,
                 const PointCloudXYZ::Ptr &f_cloud,
                 std::vector<Cylinder_Param> &cylind,
                 std::vector<Sphere_Param> &sphere,
                 std::vector<Box_Param> &box ,
                 std::vector<int> &localize_seg);


        std::vector<Box_Param>
        fit_box_final( std::vector<PointCloudIndividual> &segRegion,
                 std::vector<int> &segnum );

        // fit box final
        std::vector<Box_Param>
        fit_box( std::vector<PointCloudIndividual> &segRegion,
                 std::vector<int> &segnum);


        std::vector<CylindricalShell>
        grasp_localization_box( const std::vector<PointCloudIndividual> &segRegion,
                                const PointCloudXYZ::Ptr &ptCloud,
                                const PointCloudRGB::Ptr &ptCloud_RGB,
                                Box_Param box_all );

        std::vector<CylindricalShell>
        grasp_localization_without_box( const std::vector<PointCloudIndividual> &segRegion,
                                        const PointCloudXYZ::Ptr &ptCloud,
                                        const PointCloudRGB::Ptr &ptCloud_RGB,
                                        const std::vector<int> segnum);

        std::vector<CylindricalShell>
        grasp_localization_box_final( const std::vector<PointCloudIndividual> &segRegion,
                                const PointCloudXYZ::Ptr &ptCloud,
                                const PointCloudRGB::Ptr &ptCloud_RGB,
                                Box_Param box_all );

        std::vector<CylindricalShell>
        grasp_localization_without_box_final( const std::vector<PointCloudIndividual> &segRegion,
                                        const PointCloudXYZ::Ptr &ptCloud,
                                        const PointCloudRGB::Ptr &ptCloud_RGB,
                                        const std::vector<int> segnum);

        CylindricalShell
        grasp_localization_cylinder_bestfit( const std::vector<PointCloudIndividual> &segRegion,
                                 const PointCloudXYZ::Ptr &ptCloud,
                                 Cylinder_Param cylind );
        CylindricalShell
        grasp_localization_cylinder_bestfit( const std::vector<PointCloudIndividual> &segRegion,
                                             const PointCloudXYZ::Ptr &ptCloud,
                                             const std::vector<int> localize_seg,
                                             Cylinder_Param cylind );
        CylindricalShell
        grasp_localization_sphere( const std::vector<PointCloudIndividual> &segRegion,
                            const PointCloudXYZ::Ptr &ptCloud,
                             Sphere_Param sph );
        CylindricalShell
        grasp_localization_sphere( const std::vector<PointCloudIndividual> &segRegion,
                                   const PointCloudXYZ::Ptr &ptCloud,
                                   std::vector<int> localize_seg,
                             Sphere_Param sph );

        Eigen::Vector3d
        basis_direction_e2( const Eigen::Vector3d vec,
                            const Eigen::Vector3d point,
                            const PointCloudXYZ::Ptr &ptCloud);

        bool
        groupRegion(std::vector<PointCloudIndividual> segRegion,
                    const std::vector<int> segnum);

        std::vector<int>
        remove_base(  const PointCloudXYZ::Ptr &ptCloud,
                      std::vector<PointCloudIndividual> &segRegion,
                      const double thr_reject );

public:

        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals;
        std::vector<int> exchangeIndices;


        SurfacePatch();

        bool
        workspaceFilter( PointCloudXYZ::Ptr &ptCloud,
                         PointCloudRGB::Ptr &ptCloud_RGB,
                         const Rect bbox);
        bool
        workspaceFilter( PointCloudXYZ::Ptr &ptCloud,
                         PointCloudRGB::Ptr &ptCloud_RGB,
                         const Rect bbox,
                         PointCloudXYZ::Ptr &ptCloud_new,
                         PointCloudRGB::Ptr &ptCloud_RGB_new );
        int
        getdetectionBox( std::string txt_path,
                             std::vector<Rect> &bbox,
                             std::vector<std::string> &object );

        bool
        remove_walls_bin( PointCloudXYZ::Ptr &ptCloud,
                          std::vector<pcl::PointXYZ> boundary_points_pcl,
                          double thr_reject );

        void validateHandles( std::vector<CylindricalShell> &shells );

        bool isPointInWorkspace( WorkspaceLimits limits, double x, double y, double z );

        void
        limitWorkspace( pcl::PointCloud<pcl::PointXYZ>::Ptr &ptCloud,
                             pcl::PointCloud<pcl::PointXYZRGB>::Ptr &ptCloud_rgb);
        void
        limitWorkspace_bin( pcl::PointCloud<pcl::PointXYZ>::Ptr &ptCloud );

        void
        extractSurface( pcl::PointCloud<pcl::PointXYZ>::Ptr &ptCloud,
                        pcl::PointCloud<pcl::PointXYZ>::Ptr &ptCloud_bg  );

        void
        extractSurface( pcl::PointCloud<pcl::PointXYZ>::Ptr &ptCloud );

        std::vector<std::vector<int> >
        region_growing( const PointCloudXYZ::Ptr &pointCloudTemp,
                        pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloudRGB,
                        std::vector<PointCloudIndividual> &segRegion );

        std::vector<CylindricalShell>
        extract_region_main(
                              PointCloudXYZ:: Ptr &ptCloud,
                             pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloudRGB,
                             std::string current_cls );

        bool
        showCloud( const std::vector<PointCloudIndividual> &segRegion,
                   const pcl::PointCloud<pcl::PointXYZ>::Ptr &f_cloud );

        bool
        showCloud( const std::vector<PointCloudIndividual> &segRegion,
                   const pcl::PointCloud<pcl::PointXYZ>::Ptr &f_cloud,
                   const std::vector<int> segnum);

        bool
        showCloud_gmm( const std::vector<PointCloudIndividual> &segRegion,
                   const pcl::PointCloud<pcl::PointXYZ>::Ptr &f_cloud,
                   std::vector<int> segnum);

        void
        visualizeRegion_pcl( const std::vector<PointCloudIndividual> &segRegion,
                             const PointCloudXYZ::Ptr &pt_cloud,
                             const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloudrgb);


        bool
        visualize_primitives_all(   const std::vector<Cylinder_Param> cylinder,
                                    const std::vector<Sphere_Param> sphere,
                                    const std::vector<Box_Param> box,
                                    const PointCloudRGB::Ptr &f_cloud  );
        bool
        visualize_primitives_handles(   const std::vector<Cylinder_Param> cylinder,
                                        const std::vector<Sphere_Param> sphere,
                                        const std::vector<Box_Param> box,
                                        const PointCloudRGB::Ptr &f_cloud,
                                        const std::vector<CylindricalShell> shells);

        bool
        visualize_primitives_handle(   const std::vector<Cylinder_Param> cylinder,
                                       const std::vector<Sphere_Param> sphere,
                                       const std::vector<Box_Param> box,
                                       const PointCloudRGB::Ptr &f_cloud,
                                       const std::vector<CylindricalShell> shells  );

        //access the private members
        inline float getangle_threshold() const
        {
          return this->region_growing_threshold;
        }

        inline void setangle_threshold(float pos)
        {
              this->region_growing_threshold = pos;
        }

        inline void set_K(int k)
        {
              this->K = k;
        }

        inline int get_K() const
        {
          return this->K;
        }

        inline void set_num_to_be_boundary_point(int number_points)
        {
              this->num_to_be_boundary_point = number_points;
        }

        inline int get_num_to_be_boundary_point() const
        {
          return this->num_to_be_boundary_point;
        }

        inline void set_threshold_merge(float th)
        {
              this->threshold_merge = th;
        }

        inline float get_threshold_merge() const
        {
          return this->threshold_merge;
        }

        inline void set_radius_search(double th)
        {
              this->radius_search = th;
        }

        inline double get_radius_search() const
        {
             return this->radius_search;
        }

        inline void set_gmm_segmentation(bool k)
        {
              this->use_gmm_segmentation = k;
        }

        inline int get_gmm_segmentation() const
        {
          return this->use_gmm_segmentation;
        }

        inline void set_rcnn_bounding_box( cv::Rect bbox )
        {
            this->rcnn_box_ = bbox;
        }

        inline cv::Rect get_rcnn_bounding_box() const
        {
            return this->rcnn_box_;
        }

        inline void set_boundary_pcl( std::vector<pcl::PointXYZ> pcl_points )
        {
            this->boundary_pcl_ = pcl_points;
        }

        inline std::vector<pcl::PointXYZ> get_boundary_pcl() const
        {
            return this->boundary_pcl_;
        }

        std::string model_path;
        float thr_curv;
        float thr_rgb;
        float thr_direction;
        bool use_occlution;

        bool noBox ;
        bool noCylinder ;
        bool noSphere ;

private:

        float region_growing_threshold;
        int K ;
        int num_to_be_boundary_point ;
        float threshold_merge ;
        float radius_search;
        bool use_gmm_segmentation;
        cv::Rect rcnn_box_;

        int points_in_gap_allowed ;
        int point_neg;
        float finger_width;
        float handle_gap;
        float maxHandAperture;

        /** \brief Point labels that tells to which segment each point belongs. */
        std::vector<int> point_labels_rg_;
        std::vector<pcl::PointXYZ> boundary_pcl_;


};

#endif
