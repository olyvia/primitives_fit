
#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <stereo_msgs/DisparityImage.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>

//visualization
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>
//publisher

// header for math functions
#include <math.h>


using namespace cv;
using namespace std;


void colorImagecallBack(const sensor_msgs::ImageConstPtr &image, cv::Mat &drawImage, bool &flag);
void depthImagecallBack(const sensor_msgs::ImageConstPtr &image, cv::Mat &drawImage, bool &flag);
void ptCloudcallBack(const sensor_msgs::PointCloud2::ConstPtr &input, pcl::PointCloud<pcl::PointXYZRGB>::Ptr ptCloud, bool &flag);
void ptCloudcallBackXYZ(const sensor_msgs::PointCloud2::ConstPtr &input, pcl::PointCloud<pcl::PointXYZ>::Ptr ptCloud, bool &flag);
#endif
