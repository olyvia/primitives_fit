#ifndef CONTOURCONTROL_H
#define CONTOURCONTROL_H

#include <grasp/includedefs.h>
#include <grasp/ptCloudIndividual.h>



class ContourControl
{

protected:

           bool flag_ptcloud;

           WorkspaceLimits workspaceLimits;

           double workspace_min_x;
           double workspace_max_x;
           double workspace_min_y;
           double workspace_max_y;
           double workspace_min_z;
           double workspace_max_z;


   public:

           //main processing cloud
           PointCloudXYZ::Ptr ptCloud;
           pcl::PointCloud<pcl::Normal>::Ptr cloud_normals;

           //vector of point cloud
           std::vector<PointCloudXYZ::Ptr> ptCloudObjects;

           //no of points in a point cloud
           std::vector<int> noPointsptCloud;

           //store middle point of each cloud
           vector<Point> middlePoint;


            ContourControl();

            /*
             * function for controlling this class i.e like main function
             *
             */
            bool SegmentedPointCloud( ) ;


            /*
             * function for creating publisher and subscriber
             *
             */
            void createPubSub(std::vector<ros::Publisher> &pub, std::vector<ros::Subscriber> &sub, ros::NodeHandle &node);

            /*
             * check if a point is in the WorkspaceLimits
             *
             */
            bool isPointInWorkspace( WorkspaceLimits limits, double x, double y, double z );

             /*
              * limit the WorkspaceLimits
              *
              */
            void limitWorkspace( );
            void limitWorkspaceImage( );

            /*
             * surface normal calculation for a point cloud
             *
             */
            void calculateNormal( int flagParallel );


            /*
             * edge detection on point clouds (depth image) based on surface normals
             *
             */
            void depthEdgeDetectionSurfaceNormals();

            /*
             * surface extraction based on ransac
             *
             */
            void extractSurface ();

            /*
             * compare the area among two contours
             *  http://www.qtcentre.org/archive/index.php/t-37461.html
             */
            static bool compareContourAreas( const std::vector<Point> contour1, const std::vector<Point> contour2 ) ;

            /*
             * outer contour detection
             *
             */

            void drawContour( Mat &edgeImage );

            /*
             * after contour detection generate point cloud
             * based on individual contour
             *
             */
            void generatePointCloudforObjects();

            /*
             *
             *
             */
            std::vector<int> selectIndicesRandom(const PointCloudXYZ::Ptr &cloud );



            bool showHandle(  std::vector<std::vector<int> > &indices, pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloudVis);
            /*
             * function for checking type of Mat
             *
             */
            string type2str(int type);


            /*
             * function for transfer
             *
             */
            std::vector<PointCloudIndividual> assignVariable( );


            bool  regionGrowing1( std::vector<PointCloudIndividual> &segobj, std::vector<std::vector<int> > &indices );

            /*
             * function for region growing algorithm
             *
             */
            bool regionGrowing( const PointCloudXYZ::Ptr &cloudTemp, std::vector<std::vector<int> > &indices );

            ~ContourControl();


};

#endif // CONTOURCONTROL_H
