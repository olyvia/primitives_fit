#ifndef HELPERS_H
#define HELPERS_H


#include <iostream>
//#include <opencv2/opencv.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/video/video.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/nonfree/nonfree.hpp>
//#include <opencv2/nonfree/features2d.hpp>

#include <ros/package.h>
#include <ros/time.h>
#include <sensor_msgs/PointCloud2.h>

#include <ros/ros.h>
#include <stdio.h>
#include <sstream>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/filter.h>

#include <pcl_conversions/pcl_conversions.h>


#include <fstream>

#include <grasp/lbp.hpp>

typedef pcl::PointXYZ PointXYZ;
typedef pcl::PointCloud<pcl::PointXYZ> PointCloudXYZ;

#define DISTANCE_RATIO                  0.5     /*!< Distance ratio threshold :: If a nearest neighbour searched through kdtree is lower */
#define NO_OF_TEMPLATES 6
#define ORB 0
#define SURF    1
#define MRF_MAP 1
#define NO_OF_PATCHES_TEMPLATES 3
#define NO_OF_BG_TEMPLATES  9

#define NO_OF_GMM_OBJECT_TEMPLATES  17

#define TRAIN_GMM   1

using namespace std;
using namespace cv;
#if(MRF_MAP)
//using namespace dai;
#endif

void FindBlobs(const cv::Mat &binary, std::vector < std::vector<cv::Point2i> > &blobs);
//void findMatch_MRF_MAP(Mat_<int> adjMat, vector<double> trueAngle,
//                       vector< vector<Point2f> > candidates,
//                       vector<Point2f>& outPoints,
//                       vector<int>& partsFound,
//                       vector<int>& matchIndex);
void getConnectedRegions(Mat color_image, vector<cv::Point2f> &patch_centers, vector<cv::Scalar> &patch_colors, vector<double> &patch_area);
void createHSHistogram(cv::Mat img, cv::MatND& hist, Mat mask);


typedef pcl::PointXYZ PointXYZ;
typedef pcl::PointCloud<pcl::PointXYZ> PointCloudXYZ;


std::vector<std::vector<int> > RG( const PointCloudXYZ::Ptr &pointCloudTemp,
                                   const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &input_rgb_cloud,
                                   vector<Rect> &patchesFromSurfaceNormals,cv::Mat img);


bool showHandle( const pcl::PointCloud<pcl::PointXYZ>::Ptr ptCloud,
                 const std::vector<std::vector<int> > &indices,
                 pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloudVis);

/*!
      @brief Finds a random color for a given seed, defualt seed id current timestamp in nanoseconds.
      @param[in]    Input seed for random number generator.
      @return       Random Scalar color value.
      */
Scalar getRandColor(uint64_t seed = ros::Time::now().toNSec());


void showHistogram(Mat& img,vector<double>& array);

///*!
//    @brief Finds graph matching based on MAP solution using BP for a given MRF model of an undirected graph
//    @param[in] adjMat   adjacency matrix of the graph (n x n, n is number of nodes of graph)
//    @param[in] candidates   observed candidates for each node of graph
//    @param[out] outpoints   selected candidates
//    @param[out] partsFound  flags vector for nodes found or not found
//    @param[out] matchIndex  indicies of selected candidates
//*/
//void findMatch_MRF_MAP(Mat_<int> adjMat, vector<double> trueAngle,
//                       vector< vector<Point2f> > candidates,
//                       vector<Point2f>& outPoints,
//                       vector<int>& partsFound,
//                       vector<int>& matchIndex);




void getConnectedRegions(Mat color_image,vector<cv::Point2f> &patch_centers,
                         vector<cv::Scalar> &patch_colors,vector<double> &patch_area);

void createHSHistogram(cv::Mat img,cv::MatND& hist,cv::Mat mask);


void findMinOfArray( vector<double> array, int &minIndex, double &minValue);

template <typename _Tp>
void OLBP_(const Mat& src, Mat& dst,const Mat mask) ;


void lbp_histogram(cv::Mat lbp_img,cv::Mat &hist);

void createH_Histogram(cv::Mat img, cv::MatND &hist,cv::Mat mask);

void createHSVHistogram(cv::Mat img, cv::MatND &hist,cv::Mat mask);

void createRGBHistogram(cv::Mat img, cv::MatND &hist,cv::Mat mask);



#endif // HELPERS_H
