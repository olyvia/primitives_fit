#ifndef INCLUDEDEFS_H
#define INCLUDEDEFS_H


#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int32.h>
#include <ros/publisher.h>
#include <ros/subscriber.h>
#include <std_msgs/String.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <stereo_msgs/DisparityImage.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include <pcl/point_cloud.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/common/pca.h>

#include <pcl/ros/conversions.h>
#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
//#include <pcl/segmentation/region_growing.h>

//visualization
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>
//publisher

// header for math functions
#include <math.h>
//#include <boost/math/special_functions/fpclassify.hpp>
#include <algorithm>
#include <vector>
#include <string.h>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
//
//#include "pca.h"
#include <omp.h>
#include <grasp/callbacks.h>

#include <geometry_msgs/Vector3.h>
#include <pcl/common/centroid.h>

// header file for path formation
#include <dirent.h>



using namespace cv;
using namespace std;

typedef pcl::PointXYZ PointXYZ;
typedef pcl::PointXYZRGB PointXYZRGB;
typedef pcl::PointCloud<pcl::PointXYZ> PointCloudXYZ;
typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloudRGB;

#define DISPOPTIONS 0
#define VISPOPTIONS 0


struct WorkspaceLimits
{
  double min_x;
  double max_x;
  double min_y;
  double max_y;
  double min_z;
  double max_z;
};


struct Box_Param
{
    Eigen::Matrix3f eigDx; // three axis
    Eigen::Vector3f mean_diag ;  // center of the box
    Eigen::Vector3d axis_length ;
    std::pair<int,int> segment_pair;
};

struct Cylinder_Param
{
    float radius;
    PointXYZ center ;  // center of the center
    Eigen::Vector3f axis ;
    float axis_length;
};

struct Sphere_Param
{
    // sphere can be difined by two parameters
    float radius;
    PointXYZ center ;
    Eigen::Vector3f axis ;
};


#endif



