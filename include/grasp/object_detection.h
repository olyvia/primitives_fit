#ifndef OBJECT_DETECTION_H
#define OBJECT_DETECTION_H

#pragma once

#include <grasp/helpers.h>


#define WRITE_PRODUCT_DETAILS   1
#define NO_OF_MASK_TEMPLATES    4
#define NO_OF_OBJECT_TEMPLATES  4

#define HS_HIST 1

class Object_detection
{
private:
    // Feature detector
    cv::Ptr<cv::FeatureDetector> featuredetector;
    // Feature extractor
    cv::Ptr<cv::DescriptorExtractor> descriptorExtractor;
    // Index table for object matching
    std::map<int , vector<int> > descriptorNodeTable;

    // Segmented image of objects in the bin
    cv::Mat segmented_image;

    cv::Rect segmented_region_rect;

    // K-D tree
    cv::flann::Index index_tree;

    vector<cv::Scalar> colorVector;

    // Keypoints of each point detected
    vector< vector<cv::KeyPoint> > objectKeypoint;


    vector<cv::Point2f> patchCentersInImage;
    vector<cv::Scalar> patchColorInImage;
    vector<double> patchAreaInImage;
    vector<bool> patchStatusInImage;

    vector<cv::Rect> patchesFromSurfaceNormals;

    // GMM data
    EM fgModel,bgModel;


    // Training GMM for each object with unmasked templates
    vector<cv::EM> emObject_Model;

    // Training GMM with masked templates
    vector<cv::EM> emObject_mask_model;

    // SVM model
    CvSVM svm[int(NO_OF_MASK_TEMPLATES)];


protected:
public:

    // ROI of the bin
    cv::Rect ROI;

    int imgNumber = 0;
    // Object detection matrix
    vector< vector<int> > obj_detected_mat;

    ros::NodeHandle nh;
    ros::Publisher pub ;
    //    = nh.advertise<sensor_msgs::PointCloud2>("/ptcloud",1);

    vector< vector<cv::Point2f> > templatePatchCenters;
    vector< vector<cv::Scalar> > templatePatchColor;
    vector< vector<double> > templatePatchAngle;
    vector< cv::MatND > templateHistograms;

    // Temporary variables, background image address, test image address
    //    std::string bk_train_image_addr;
    std::string test_image_rgb_addr;
    std::string test_image_pcd_addr;

    cv::Mat test_image_orig;

    vector<cv::Point2f> object_region_points;
    vector<cv::Point2f> bg_region_points;

    // Video Writer
    cv::VideoWriter writer;

    // Multiple histogram for each templates
    vector < vector<cv::MatND> > multiple_tempHists;

    // Multiple LBP histogram for each template
    vector < vector<cv::Mat> > multiple_lbp_temp_hist;

    vector< vector<string> > template_address;

    vector< vector<int> > obj_bin_status;


    // Maximum area contours for each template
    vector< std::vector<cv::Point> > templates_contours;


    Object_detection();
    ~Object_detection();

    void findObjectsUsingPointFeatures(cv::Mat frame);
    void initFeatureTemplates();
    void initColorTemplates();
    void segmentForeGround();
    void findContours();
    void findColorPathcesInImage(Mat img);
    void objectGraphMatchingMRF(Mat img);
    void createSegmentUsingSurfaceNormals();
    void doHistogramMatchingWithTemplates();
    void backgroundSegmentationUsingGMM();
    void initTemplateHistogramUsingGMM();
    void doObjectMatchingUsingGMM();
    void doObjectMatchingMaskedGMM();
    void initGMMUsingMaskTemplates();
    void initHistWithMultipleTemplates();
    void doHistMatchingMaskedMultipleTemplates();
    void doHistMatchingMaskedMultipleTemplates( cv::MatND test_hist);
    void trainSVMUsingMaskTemplatesWithLBP();
    void initLBPHistogramForTemplates();
    void findObjectUsingSVMwithLBP();
    void doObjectMatchingUsingShapes();
    void initContoursForTemplates();
    void generateAccuracyResults();
    void doLBPHistogramMatchingUsingTemplates();
    void readObjectsInEachBin();


};





#endif // OBJECT_DETECTION_H
