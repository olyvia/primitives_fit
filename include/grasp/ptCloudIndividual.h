/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, Andreas ten Pas
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PTCLOUDINDIVIDUAL_H
#define PTCLOUDINDIVIDUAL_H

#include "Eigen/Dense"
#include <pcl_ros/point_cloud.h>
#include <pcl/search/organized.h>
#include <grasp/includedefs.h>

typedef pcl::PointXYZ PointXYZ;
typedef pcl::PointXYZRGB PointXYZRGB;
typedef pcl::PointCloud<pcl::PointXYZ> PointCloudXYZ;
typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloudRGB;

class PointCloudIndividual
{

public:



    inline std::vector<int> getNNIndices() const
    {
         return this->nn_indices_;
    }
    inline void setNNIndices(std::vector<int> indices)
    {
        this->nn_indices_ = indices;
    }

    inline std::vector<int> getNNIndices_unorganized() const
    {
         return this->nn_indices_unorganized;
    }
    inline void setNNIndices_unorganized(std::vector<int> indices)
    {
        this->nn_indices_unorganized = indices;
    }

    inline Eigen::Matrix3f get_axis () const
    {
        return this->all_axis_;
    }

    inline void set_axis( Eigen::Matrix3f axis )
    {
        this->all_axis_ = axis;
    }

    inline Eigen::Vector3f get_axis_seg( ) const
    {
      return this->axis_;
    }

    inline void set_axis_seg( Eigen::Vector3f ax )
    {
          this->axis_= ax;
    }

    inline Eigen::Vector3d getcut_axis() const
    {
      return this->cut_axis_;
    }

    inline void setcut_axis(Eigen::Vector3d cut)
    {
          this->cut_axis_ = cut;
    }

    inline Eigen::Vector3d getrepresentative_normal() const
    {
      return this->representative_normal_;
    }

    inline void setrepresentative_normal(Eigen::Vector3d norm)
    {
          this->representative_normal_ = norm;
    }

    inline void setrepresentative_position(Eigen::Vector3d pos)
    {
          this->representative_position_ = pos;
    }

    inline Eigen::Vector3d getrepresentative_position() const
    {
      return this->representative_position_;
    }

    inline void set_seg_var(Eigen::Vector3d var)
    {
          this->seg_var_ = var;
    }

    inline Eigen::Vector3d get_seg_var() const
    {
      return this->seg_var_;
    }

    inline void setrepresentative_color( int color )
    {
          this->representative_color_ = color;
    }

    inline int getrepresentative_color( )
    {
          return this->representative_color_ ;
    }

    inline std::vector<int> get_neighbour() const
    {
         return this->neighbour_;
    }
    inline void set_neighbour( std::vector<int> neigh)
    {
        this->neighbour_ = neigh;
    }

    inline void set_curvature( float curvature )
    {
          this->seg_curvature_ = curvature;
    }

    inline float get_curvature( ) const
    {
          return this->seg_curvature_ ;
    }

    inline void set_small_diff( float diff )
    {
          this->small_diff_ = diff;
    }

    inline float get_small_diff( )
    {
          return this->small_diff_ ;
    }

    inline void set_gmm_color_final( float diff )
    {
          this->gmm_score_color_final_ = diff;
    }

    inline float get_gmm_color_final( )
    {
          return this->gmm_score_color_final_ ;
    }

    inline std::vector<float> get_gmm_color() const
    {
         return this->nn_indices_color_value_;
    }
    inline void set_gmm_color(std::vector<float> indices)
    {
        this->nn_indices_color_value_ = indices;
    }

    inline bool get_use_the_segment() const
    {
         return this->is_use_the_segment_;
    }
    inline void set_use_the_segment( bool dec)
    {
        this->is_use_the_segment_ = dec;
    }

private:

    std::vector<int> nn_indices_;
    std::vector<int> nn_indices_unorganized;
    Eigen::Matrix3f all_axis_; // ascending order
    Eigen::Vector3d cut_axis_; // cut with the axis above
    Eigen::Vector3d representative_normal_;
    Eigen::Vector3d representative_position_;
    Eigen::Vector3d seg_var_;
    Eigen::Vector3f axis_;
    int index_rep_;
    int representative_color_ ;
    std::vector<int> neighbour_;
    float seg_curvature_;
    float small_diff_;
    float gmm_score_color_final_;
    std::vector<float> nn_indices_color_value_;
    bool is_use_the_segment_;

};

#endif
