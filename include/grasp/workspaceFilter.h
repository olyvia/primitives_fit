#ifndef WORKSPACEfILTER_H
#define WORKSPACEFILTER_H

#include <grasp/includedefs.h>



class workspaceFilter
{

protected:

           bool flag_ptcloud;
           bool flag_ptCloudRGB;
           bool flag_CloudVis;
           bool flag_image;

           WorkspaceLimits workspaceLimits;

           double workspace_min_x;
           double workspace_max_x;
           double workspace_min_y;
           double workspace_max_y;
           double workspace_min_z;
           double workspace_max_z;


   public:

           //main processing cloud
           PointCloudXYZ::Ptr ptCloud_;
           PointCloudRGB::Ptr ptCloudRGB;
           PointCloudRGB::Ptr CloudVis;
           cv::Mat draw_image;

            workspaceFilter();

            /*
             * function for controlling this class i.e like main function
             *
             */
            bool SegmentedPointCloud( ) ;


            /*
             * function for creating publisher and subscriber
             *
             */
            void createPubSub(std::vector<ros::Publisher> &pub, std::vector<ros::Subscriber> &sub, ros::NodeHandle &node);

            /*
             * check if a point is in the WorkspaceLimits
             *
             */
            bool isPointInWorkspace( WorkspaceLimits limits, double x, double y, double z );

             /*
              * limit the WorkspaceLimits
              *
              */
            void limitWorkspace_Organised( );
            void limitWorkspace_Unorganised(   PointCloudXYZ::Ptr &ptCloud_new,
                                               PointCloudRGB::Ptr &ptCloud_RGB_new );

            /*
             * surface normal calculation for a point cloud
             *
             */
            void calculateNormal( int flagParallel );


            /*
             * surface extraction based on ransac
             *
             */
            bool extractSurface( PointCloudXYZ::Ptr &ptCloud, PointCloudRGB::Ptr &ptCloud_RGB );

            /*
             * function for checking type of Mat
             *
             */
            string type2str(int type);

            ~workspaceFilter();


};

#endif
