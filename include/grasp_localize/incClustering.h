#ifndef INCCLUSTERING_H
#define INCCLUSTERING_H

#include "Eigen/Dense"
#include<pcl/PointIndices.h>
using namespace std;
//using namespace eigen;

#define VISOPTION 0

class incClustering
{

private:
    float thr;
    int data_dimension;
    int min_cluster_size;
    bool isUpdateCluster;
public:
    incClustering()
    {

#if(VISOPTION)
        std::cout << " initialing inc Clustering" << std::endl;
#endif
//            cloud_normals = boost::make_shared<pcl::PointCloud<pcl::Normal> >() ;

//            //set default parameters
//            setangle_threshold(4.0);
//            set_K(30); //30
//            set_num_to_be_boundary_point(20); //20
//            set_threshold_merge(0.85);
//            set_radius_search(0.004);

    }

    inline void set_threshold( float thrld )
    {
          this->thr = thrld;
    }

    inline float get_threshold( )
    {
          return this->thr ;
    }

    inline void set_data_dimension( int dim )
    {
          this->data_dimension = dim;
    }

    inline int get_data_dimension( )
    {
          return this->data_dimension ;
    }

    inline void set_minClusterSize( int thrld )
    {
          this->min_cluster_size = thrld;
    }

    inline int get_minClusterSize( )
    {
          return this->min_cluster_size ;
    }

    inline void setisUpdateClusterFlag( bool thrld )
    {
          this->isUpdateCluster = thrld;
    }

    inline int getisUpdateClusterFlag( )
    {
          return this->isUpdateCluster ;
    }


    std::vector<std::vector<float> > cluster_center;
    std::vector<cv::Mat> cluster_points;
    std::vector<int> cluster_size;
//    float thr ;

    int findClosestCluster( std::vector<float> sample )
    {

        if( this->cluster_center.size() == 0 )
            return -1;
        else
        {

        float dist;
        float dist_smallest = this->thr ;
        int cluster_in_which_belong = -1;
        for( int c_count = 0; c_count < this->cluster_center.size(); c_count++ )
        {
            std::vector<float> current_cls = this->cluster_center[c_count];
            dist = 0;
            for( int d = 0; d < this->data_dimension; d++ )
            {
                   dist = dist + ( ( sample[d] * current_cls[d] ) ) ;
            }

            float norm = sqrt(dist);
            dist =  norm;

            if( dist > dist_smallest )
            {
                cluster_in_which_belong = c_count;
                dist_smallest = dist;
            }

#if(VISOPTION)

        std::cout << " dist = " << dist << " dist smallest = " << dist_smallest << std::endl;
        std::cout << " cluster_in_which_belong = " << cluster_in_which_belong  << " " << dist_smallest << std::endl;
        std::cout << " center porcessed ...." << std::endl;
        for( int d = 0; d <this->data_dimension; d++ )
        {
            std::cout << current_cls[d] << "  " << sample[d] << std::endl;
        }
#endif

        }


        return cluster_in_which_belong;
        }

    }

    void update_cluster( int cluster_in_which_belong, std::vector<float> sample )
    {
        if( cluster_in_which_belong == -1 )
        {
            this->cluster_center.push_back(sample);
            this->cluster_size.push_back(1);

#if(VISOPTION)
            std::cout << " adding a new cluster .." << this->cluster_center.size() << std::endl;
#endif
        }
        else
        {
            if( this->isUpdateCluster )
            {
                double cen, sam, cen_new;
                std::vector<float> center = this->cluster_center[cluster_in_which_belong];
                int size =  this->cluster_size[cluster_in_which_belong];
                int size_new = size + 1;
    #if(VISOPTION)
                std::cout << " size " << size << " center updated ...." << std::endl;
                for( int d = 0; d <this->data_dimension; d++ )
                {
                    std::cout << center[d] << " , ";
                }
    #endif

                for( int d = 0; d <this->data_dimension; d++ )
                {
                    cen = center[d];
                    sam = sample[d];
                    cen_new = ( cen * size ) + sam;
                    cen = cen_new / (float)size_new;
                    center[d] =  cen;
                }

                Eigen::Vector3f center_eigen ;
                center_eigen(0) = center[0];
                center_eigen(1) = center[1];
                center_eigen(2) = center[2];

                center_eigen.normalize();
                center[0] = center_eigen(0);
                center[1] = center_eigen(1);
                center[2] = center_eigen(2);

                this->cluster_size[cluster_in_which_belong] = size_new ;
                this->cluster_center[cluster_in_which_belong] = center;

            }
            else
            {
                this->cluster_size[cluster_in_which_belong] = this->cluster_size[cluster_in_which_belong] + 1 ;
            }

        }

        return;

    }


    void merge_cluster( )
    {
        std::vector<std::pair<float,int> > cluster_vect;
        std::pair<float,int> my_pair;

        for( int c = 0; c < this->cluster_center.size(); c++ )
        {
            std::vector<float> cls_center = this->cluster_center[c];
            float dist = 0;
            for( int d = 0; d < this->data_dimension; d++ )
            {
                   dist = dist + ( cls_center[d]  *  cls_center[d] ) ;
            }

            float norm = sqrt(dist);
            dist = norm;

            my_pair = make_pair( dist, c );
            cluster_vect.push_back( my_pair );
        }

        std::sort( cluster_vect.begin(), cluster_vect.end() );
        std::vector<std::vector<float> > temp_center( this->cluster_center );
        std::vector<int> temp_size(this->cluster_size);
        int index = 0;
        for( int c = temp_center.size() - 1; c >= 0 ; c-- )
        {
            my_pair = cluster_vect[c];
//            std::vector<int> cls_center = temp_center[my_pair.second];
            this->cluster_center[index] = temp_center[my_pair.second];
            this->cluster_size[index] = temp_size[my_pair.second];
            index++;
        }
#if(VISOPTION)
        std::cout << " sorted center..." << std::endl;
        for( int iii = 0; iii < this->cluster_center.size(); iii++ )
        {
            std::cout << this->cluster_size[iii] << std::endl;
            for( int d = 0; d < this->data_dimension; d++ )
            {
                  std::cout << this->cluster_center[iii][d] <<  " , ";
            }
           std::cout << std::endl;
        }
        std::cout << " press ENTER to continue ... " << std::endl;
        getchar();
#endif
        //arrange the vector according to distance


        int i = 0;
        int next_cluster;
        do
        {
            next_cluster = i + 1;
            bool parent_cluster_to_be_deleted = false;

            std::vector<float> cls_center_1 = this->cluster_center[i];
            int cls_size_1 = this->cluster_size[i];
            std::vector<int> cluster_to_delete;

            //check with other clusters
            for( int j = i+1; j < this->cluster_center.size(); j++ )
            {
                    std::vector<float> cls_center_2 = this->cluster_center[j];
                    int cls_size_2 = this->cluster_size[j];
                    float dist = 0;
                    for( int d = 0; d < this->data_dimension; d++ )
                    {
                           dist = dist + ( ( cls_center_2[d] - cls_center_1[d] ) * ( cls_center_2[d] - cls_center_1[d] ) ) ;
//                           std::cout << " " << cls_center_2[d] << " " << cls_center_1[d] << " dist " << dist << std::endl;
                    }

                    float norm = sqrt(dist);
                    dist = norm;
#if(VISOPTION)
                    std::cout << " checking data with cluster " << j << std::endl;
                    std::cout << " dist " << dist << std::endl;
                    std::cout << " parent cluster cls_size_1 " << cls_size_1 << std::endl;
                    std::cout << " cls_size_2 " << cls_size_2 << std::endl;
#endif
                    if( dist < this->thr + 1 )
                    {
                        if( cls_size_1 > cls_size_2 )
                        {
                            cluster_to_delete.push_back( j );
                        }
                        else if (  cls_size_2 > cls_size_1 )
                        {
                            cluster_to_delete.push_back( i );
                            parent_cluster_to_be_deleted = true;
                            break;
                        }

                    }

            }


            for( int r = cluster_to_delete.size() -1 ; r >= 0; r-- )
            {
//                std::cout  << cluster_to_delete[r] << std::endl;
                this->cluster_size.erase( this->cluster_size.begin() + cluster_to_delete[r] );
                this->cluster_center.erase( this->cluster_center.begin() + cluster_to_delete[r] );

            }

#if(VISOPTION)
            std::cout << " parent_cluster_to_be_deleted " << parent_cluster_to_be_deleted << std::endl;
#endif

            if( !parent_cluster_to_be_deleted )
            {
                i = next_cluster;
            }

#if(VISOPTION)
            std::cout  << std::endl;
            std::cout  << std::endl;
            for( int iii = 0; iii < this->cluster_center.size(); iii++ )
            {
                std::cout << this->cluster_size[iii] << std::endl;
                for( int d = 0; d < this->data_dimension; d++ )
                {
                      std::cout << this->cluster_center[iii][d] <<  " , ";
                }
               std::cout << std::endl;
            }
            std::cout << " press ENTER to continue ... " << std::endl;
            getchar();
#endif

         }while( i < this->cluster_size.size() );

#if(VISOPTION)
                for( int iii = 0; iii < this->cluster_center.size(); iii++ )
                {
                    std::cout << " cluster _size = "  << this->cluster_size[iii] << std::endl;
                }
                getchar();
#endif

        return;
    }


    void remove_samll_clusters()
    {

        int c = 0;
        do
        {

            if ( this->cluster_size[c] < min_cluster_size )
            {
#if(VISOPTION)
               std::cout << " deleting cluster with size " << this->cluster_size[c] << std::endl;
#endif
               cluster_size.erase( cluster_size.begin()+ c );
               cluster_center.erase( cluster_center.begin()+ c );


            }
            else
            {
                //increase the pointer by one
                c++;
            }



        }while( c < cluster_size.size());

    }

    void remove_samll_clusters( std::vector<pcl::PointIndices> & point_idx )
    {

        if( ( point_idx.size() == 0 ) || ( point_idx.size() != this->cluster_center.size() ) )
            return;

        int c = 0;
        do
        {

            if ( this->cluster_size[c] < min_cluster_size )
            {
#if(VISOPTION)
               std::cout << " deleting cluster with size " << this->cluster_size[c] << std::endl;
#endif
               cluster_size.erase( cluster_size.begin()+ c );
               cluster_center.erase( cluster_center.begin()+ c );
               point_idx.erase( point_idx.begin() + c );

            }
            else
            {
                //increase the pointer by one
                c++;
            }



        }while( c < cluster_size.size());

        return;
    }


};

#endif
