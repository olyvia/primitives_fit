#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#include <grasp/includedefs.h>
#include <grasp/SurfacePatch.h>
#include <grasp/ShapeFit.h>


#include <pcl/io/pcd_io.h>
#include <publisher.h>

struct MouseParams
{
    Vec2i top, bottom;
    Vec2i tl, tr, bl, br;

    bool got_cord = false;
    bool get_rcnn_bbox = false;

    bool get_tl = false;
    bool get_tr = false;
    bool get_bl = false;
    bool get_br = false;
};

class Primitives
{
public:
    Primitives();
    ~Primitives();

    ros::NodeHandle node;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB_ptr;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudXYZ_ptr;
    bool get_cloud = false;
    bool get_img = false;
    bool process_pc = false;

    cv::Mat src_image;

    ros::Publisher cylinder_marker;
    ros::Publisher axis_markerarray;
    ros::Publisher normal_markerarray;
    ros::Publisher sphere_markerarray, bin_corners_markerarray;
    MouseParams mp;
    std::vector<pcl::PointXYZ> bin_corners;
};

#endif // PRIMITIVES_H
