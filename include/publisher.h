#ifndef PUBLISHER_H
#define PUBLISHER_H

#include <tf/transform_listener.h>

#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>

using namespace std;

void addCylinderMarker(double *centroid, geometry_msgs::Quaternion &q, double &cyl_radius, double *color,
                       visualization_msgs::Marker &marker, std::string &frame_id, int id);

void addLineMarker(string &ns, vector<tf::Vector3> &start_pt, vector<tf::Vector3> &end_pt,
                   double *color, visualization_msgs::Marker &marker, std::string &frame_id, int id);

void addArrowMarker(tf::Vector3 &start_pt, tf::Vector3 &end_pt, double *color, visualization_msgs::Marker &marker,
                    std::string &frame_id, int id);

// Function to accept rotation matrix and return Quaternion
geometry_msgs::Quaternion rotationMatrixtoQuaternion(tf::Matrix3x3 &matrix);

// Function to get the end point a vector at distance 7cm from the start point
tf::Vector3 getEndpt(tf::Vector3 vector, tf::Vector3 &start_pt);

void addCylinderMarkerArray(vector<tf::Vector3> &centroids, vector<tf::Matrix3x3> orientation, double &cyl_radius,
                            ros::Publisher &marker_pub);

void addLineMarkerArray(vector<tf::Vector3> &start_points, vector<tf::Vector3> &end_points, ros::Publisher &marker_pub,
                        string ns, double *line_color);

void addArrowMarkerArray(vector<tf::Vector3> &start_points, vector<tf::Vector3> &end_points, ros::Publisher &marker_pub,
                        string ns, double *line_color);

void addSphereMarkerArray(vector<tf::Vector3> &points, ros::Publisher &marker_pub, string frame_id, double radius);

void addSphereMarker(double *centroid, double *color, visualization_msgs::Marker &marker, std::string &frame_id, int id, double radius);
#endif // PUBLISHER_H
