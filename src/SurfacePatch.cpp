#include "grasp/SurfacePatch.h"

#include <ros/package.h>


SurfacePatch::SurfacePatch()
{
    cloud_normals = boost::make_shared<pcl::PointCloud<pcl::Normal> >() ;

    //set default parameters
    setangle_threshold(4.5);
    set_K(30); //30
    set_num_to_be_boundary_point(20); //20
    set_threshold_merge(0.85);
    set_radius_search(0.004);
    set_gmm_segmentation(false);
    string path = ros::package::getPath("fit_primitives");

//    this->model_path = "/home/olyvia/tcs/dataset/Results/ARC/gmm_dump/";
    this->model_path = path ; //"/home/olyvia/tcs/grasping/src/grasp_localize";
    this->model_path.append("/gmm_model_color_curv/");
    this->thr_curv = 0.4;
    this->thr_rgb = 0.4;
    this->thr_direction = 0.7;

    //grasping parameters
    this->points_in_gap_allowed = 10;
    this->point_neg = 10;
    this->finger_width = 0.02; //0.03;
    this->maxHandAperture = 0.05; //error
    this->handle_gap = 0.02;

    this->noBox = false;
    this->noCylinder = false;
    this->noSphere = false;

    this->use_occlution = true;
}

bool SurfacePatch::
isPointInWorkspace( WorkspaceLimits limits, double x, double y, double z )
{

      if (x >= limits.min_x && x <= limits.max_x && y >= limits.min_y && y <= limits.max_y && z >= limits.min_z
          && z <= limits.max_z)
        return true;
      else
        return false;
}

void SurfacePatch::
limitWorkspace( pcl::PointCloud<pcl::PointXYZ>::Ptr &ptCloud,
                     pcl::PointCloud<pcl::PointXYZRGB>::Ptr &ptCloud_rgb)
{

    WorkspaceLimits workspaceLimits;
    workspaceLimits.min_x = -0.25; //-0.2;
    workspaceLimits.max_x = 0.25;//0.3;
    workspaceLimits.min_y = -0.15;
    workspaceLimits.max_y = 0.25;
    workspaceLimits.min_z = 0.0;
    workspaceLimits.max_z = 1.5;

    pcl::PointXYZ p_nan;
    p_nan.x = std::numeric_limits<float>::quiet_NaN();
    p_nan.y = std::numeric_limits<float>::quiet_NaN();
    p_nan.z = std::numeric_limits<float>::quiet_NaN();

    pcl::PointXYZRGB p_nan_rgb;
    p_nan_rgb.x = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.y = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.z = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.r = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.g = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.b = std::numeric_limits<float>::quiet_NaN();


    //limit the point cloud
    for ( int row = 0 ; row < ptCloud->points.size() ; row++ )
    {
          pcl::PointXYZ pt = ptCloud->points[row];

          if ( pcl::isFinite(pt) )
          {
               //check whether the point belong to the workspace or not
               bool istrue =  isPointInWorkspace( workspaceLimits, pt.x, pt.y, pt.z );

               // make the value equal to NaN if the point is not in workspace
               if ( !istrue )
               {
                    ptCloud->points[row] = p_nan ;
                    ptCloud_rgb->points[row] = p_nan_rgb;
               }

          } //end of if


    }


    return;
}


void SurfacePatch::
limitWorkspace_bin( pcl::PointCloud<pcl::PointXYZ>::Ptr &ptCloud )
{

    WorkspaceLimits workspaceLimits;
    workspaceLimits.min_x = -0.17; //-0.2;
    workspaceLimits.max_x = 0.12;//0.3;
    workspaceLimits.min_y = 0.0;
    workspaceLimits.max_y = 0.183;  //0.182;
    workspaceLimits.min_z = 0.5;
    workspaceLimits.max_z = 0.725;

    pcl::PointXYZ p_nan;
    p_nan.x = std::numeric_limits<float>::quiet_NaN();
    p_nan.y = std::numeric_limits<float>::quiet_NaN();
    p_nan.z = std::numeric_limits<float>::quiet_NaN();

    //limit the point cloud
    for ( int row = 0 ; row < ptCloud->points.size() ; row++ )
    {
          pcl::PointXYZ pt = ptCloud->points[row];

          if ( pcl::isFinite(pt) )
          {
               //check whether the point belong to the workspace or not
               bool istrue =  isPointInWorkspace( workspaceLimits, pt.x, pt.y, pt.z );

               // make the value equal to NaN if the point is not in workspace
               if ( !istrue )
               {
                    ptCloud->points[row] = p_nan ;
               }

          } //end of if


    }


    return;
}


bool SurfacePatch::
workspaceFilter( PointCloudXYZ::Ptr &ptCloud,
                 PointCloudRGB::Ptr &ptCloud_RGB,
                 const Rect bbox)
{
    if( ( ptCloud->empty() ) && ( ptCloud_RGB->empty() ) )
        return false;

    PointXYZ p_nan;
    p_nan.x = std::numeric_limits<float>::quiet_NaN();
    p_nan.y = std::numeric_limits<float>::quiet_NaN();
    p_nan.z = std::numeric_limits<float>::quiet_NaN();

    PointXYZRGB p_nan_rgb;
    p_nan_rgb.x = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.y = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.z = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.r = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.g = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.b = std::numeric_limits<float>::quiet_NaN();


    //limit the point cloud
    for( int h = 0; h < ptCloud->height; h++ )
    {
        if( (h > bbox.y) && (h < bbox.height) )
        {
            for( int w = 0; w < ptCloud->width; w++ )
            {
                    if( (w > bbox.x) && (w < bbox.width) )
                    {
                        //do nothing
                    }
                    else
                    {
                        ptCloud->at(w,h) = p_nan;
                        ptCloud_RGB->at(w,h) = p_nan_rgb;
                    }
            }
        }
        else
        {
            for( int w = 0; w < ptCloud->width; w++ )
            {
                ptCloud->at(w,h) = p_nan;
                ptCloud_RGB->at(w,h) = p_nan_rgb;
            }
        }
    }
    return true;

}

bool SurfacePatch::
workspaceFilter( PointCloudXYZ::Ptr &ptCloud,
                 PointCloudRGB::Ptr &ptCloud_RGB,
                 const Rect bbox,
                 PointCloudXYZ::Ptr &ptCloud_new,
                 PointCloudRGB::Ptr &ptCloud_RGB_new)
{

    PointXYZ p_nan;
    p_nan.x = std::numeric_limits<float>::quiet_NaN();
    p_nan.y = std::numeric_limits<float>::quiet_NaN();
    p_nan.z = std::numeric_limits<float>::quiet_NaN();

    PointXYZRGB p_nan_rgb;
    p_nan_rgb.x = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.y = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.z = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.r = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.g = std::numeric_limits<float>::quiet_NaN();
    p_nan_rgb.b = std::numeric_limits<float>::quiet_NaN();


    //limit the point cloud
    for ( int row = bbox.y ; row < bbox.height ; row++ )
    {
        for ( int col = bbox.x ; col < bbox.width ; col++ )
        {

          PointXYZ pt = ptCloud->at(col,row);
          PointXYZRGB pt_rgb = ptCloud_RGB->at(col,row);

          if ( pcl::isFinite(pt) )
          {
                ptCloud_new->points.push_back(pt);
                ptCloud_RGB_new->points.push_back(pt_rgb);
          }


        }

    }
    return true;

}


bool SurfacePatch::
remove_walls_bin( PointCloudXYZ::Ptr &ptCloud,
                  std::vector<pcl::PointXYZ> boundary_points_pcl,
                  double thr_reject )
{

    {
        pcl::PointXYZ pt_1 = boundary_points_pcl[0];
        pcl::PointXYZ pt_2 = boundary_points_pcl[1];
        Eigen::Vector3f point, pt_on_plane;
        point(0) = pt_1.x - pt_2.x;
        point(1) = pt_1.y - pt_2.y;
        point(2) = pt_1.z - pt_2.z;
        point.normalize();

        pcl::PointXYZ pt_3 = boundary_points_pcl[2];
        pt_on_plane(0) = pt_3.x;
        pt_on_plane(1) = pt_3.y;
        pt_on_plane(2) = pt_3.z;

        pcl::ModelCoefficients::Ptr plane_1 (new pcl::ModelCoefficients);
        plane_1->values.resize (4);
        plane_1->values[0] = point(0);
        plane_1->values[1] = point(1);
        plane_1->values[2] = point(2);
        plane_1->values[3] = -point.dot(pt_on_plane);

//         pcl::visualization::PCLVisualizer viewer("object");
//         viewer.addPointCloud(ptCloud);
//         viewer.addLine(pt_1,pt_2, 0, 255, 0);
//         pt_1 = boundary_points_pcl[0];
//         viewer.addSphere( pt_1, 0.01, 255, 0, 0, "1");
//         pt_1 = boundary_points_pcl[1];
//         viewer.addSphere( pt_1, 0.01, 0, 255, 0, "2");
//         pt_1 = boundary_points_pcl[2];
//         viewer.addSphere( pt_1, 0.01, 0, 0, 255, "3");
//         pt_1 = boundary_points_pcl[3];
//         viewer.addSphere( pt_1, 0.01, 255, 255, 0, "4");

//         viewer.addPlane (*plane_1, "plane_1", 0);
//         std::cout << " close the window cloud_viewer to continue......." << std::endl;
//         while(!viewer.wasStopped())
//            viewer.spinOnce();

        PointXYZ p_nan;
        p_nan.x = std::numeric_limits<float>::quiet_NaN();
        p_nan.y = std::numeric_limits<float>::quiet_NaN();
        p_nan.z = std::numeric_limits<float>::quiet_NaN();

        //limit the point cloud
        for( int i = 0; i < ptCloud->points.size(); i++ )
        {
            pcl::PointXYZ pt_xyz = ptCloud->points[i];

            if( pcl::isFinite(pt_xyz) )
            {
                Eigen::Vector3f cropped,v ;
                cropped(0) = pt_xyz.x;
                cropped(1) = pt_xyz.y;
                cropped(2) = pt_xyz.z;

                v = cropped - pt_on_plane;
                double along_axis_Dist = point.dot(v);

                if( abs(along_axis_Dist) < thr_reject )
                {
                            ptCloud->points[i] = p_nan;
                }


            }
        }

//        pcl::visualization::PCLVisualizer viewer1("without base");
//        viewer1.addPointCloud(ptCloud);
//        std::cout << " close the window cloud_viewer to continue......." << std::endl;
//        while(!viewer1.wasStopped())
//           viewer1.spinOnce();
    }


    return true;

}


std::vector<int> SurfacePatch::
isBoundary(const std::vector<int> &pointIdxNKNSearch,
           const std::vector<float> pointNKNSquaredDistance,
           const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud,
           const int cluster_process,
           const int max_value )
{
        int count = 0;
        pcl::PointXYZRGB point_rgb;
        std::vector<int> neighbours;
        int total_color ;
        int color_assign ;

//        point_rgb = cloud->points[pointIdxNKNSearch[0]] ;
//        std::cout << std::endl <<pointIdxNKNSearch[0]<<  " = [" << point_rgb.x <<"," << point_rgb.y << ", " << point_rgb.z << "]" << std::endl;
//        std::cout << std::endl <<pointIdxNKNSearch[0]<<  " = [" << point_rgb.r <<"," << point_rgb.g << ", " << point_rgb.z << "]" << std::endl;
        for( int i = 0 ; i < pointIdxNKNSearch.size() ; i++ )
        {
            point_rgb = cloud->points[pointIdxNKNSearch[i]] ;

            if( pointNKNSquaredDistance[i] < 0.002 )
            {
                total_color = point_rgb.r + point_rgb.g ;
                color_assign = (int)point_rgb.b ;

                if( total_color  == 0 )
                {
                    if( cluster_process == color_assign)
                        count++;
                    else
                    {
                        if( neighbours.size() == 0)
                            neighbours.push_back(color_assign);
                        else
                        {
                            bool isMatchFound = false;
                            for( int j = 0 ; j < neighbours.size() ; j++ )
                            {
                                if(  neighbours[j] ==  color_assign )
                                   { isMatchFound = true;
                                     break;
                                   }

                            }

                            //push the number of the cluster if match does not found
                            if( !isMatchFound )
                                neighbours.push_back(color_assign);

                        }

                    }
                }

            }
            else
            {
               count++;
            }
        }

        //return the result
        if( count <= max_value )
            return neighbours;
        else
        {
            neighbours.resize(0);
            return neighbours;
        }

}


std::vector<std::vector<int> > SurfacePatch::
mergeregions( const std::vector<PointCloudIndividual> &segregion,
              const std::vector<std::vector<int> > neighbour_matix,
              std::vector<std::vector<int> > &Neighbours,
              const float threshold_merge )
{

        std::vector<std::vector<int> > finalResult;
        if( (segregion.size() == 0) || (neighbour_matix.size() == 0) )
            return finalResult;


        std::vector<int> isChecked(segregion.size(),0);
        std::vector<std::vector<int> > neighbours;

        for( int y = 0 ; y < neighbour_matix.size(); y++ )
        {
            std::vector<int> tempArray;
            tempArray.push_back(y);                               //std::cout << " neighbour for " << y << std::endl;
            for( int u = 0 ; u < neighbour_matix.size(); u++)
            {
                if( neighbour_matix[y][u] == 1 )
                 tempArray.push_back(u);                         //std::cout << " " << u; }
            }

            neighbours.push_back(tempArray);

        }

       for( int count = 0; count < segregion.size(); count++ )
        {

            PointCloudIndividual obj;
            obj = segregion[count];
            std::vector<int> neighbour_single;
            neighbour_single = neighbours[count];
            Eigen::Vector3d normal_1 = obj.getrepresentative_normal();

            // if the region is not checked earlier
            if( isChecked[count] == 0 )
            {
                std::vector<int> temp_res;
                temp_res.push_back(count);
                isChecked[count] = 1;

            //compare with neighbour
            for(int i = 1 ; i< neighbour_single.size(); i++ )
            {
                int current_cluster_compare = neighbour_single[i];

                // check whether the two segments are neighbours and second segment already scanned
                if( /*(current_cluster_compare > count) &&*/ (isChecked[current_cluster_compare] == 0) )
                {
                    PointCloudIndividual obj_2;
                    obj_2 = segregion[current_cluster_compare];
                    Eigen::Vector3d normal_2 = obj_2.getrepresentative_normal();

                    //dot product
                    double dot_product = (normal_1(0) * normal_2(0)) + (normal_1(1) * normal_2(1)) + (normal_1(2) * normal_2(2)) ;

                    if( dot_product >= threshold_merge )
                    {
                        //the segment to be merged... save it in the vector and mark it checked
                        temp_res.push_back(current_cluster_compare);
                        isChecked[current_cluster_compare] = 1;

                        // include the neighbour of the merged segment as now the neighbours will be the neighbours
                        //of the merged segment
                        std::vector<int> nei = neighbours[current_cluster_compare];
                        for( int j = 1 ; j < nei.size() ; j++ )
                         {
//                                if(nei[j] > count )  /* changes made by olyvia */
                                 {
                                    bool isMatchFound = false;
                                    for( int inner_loop = 0 ; inner_loop < neighbour_single.size(); inner_loop++ )
                                    {
                                        if( nei[j] == neighbour_single[inner_loop] )
                                        {
                                            isMatchFound = true;
                                            break;
                                        }


                                    }
                                    if( !isMatchFound )
                                    neighbour_single.push_back(nei[j]);
                                }

                         }


                    }

                }
            }

            //save the final merge segment
            finalResult.push_back(temp_res);
            Neighbours.push_back( neighbour_single );
            }

        }

            return finalResult;
}


int SurfacePatch::
getdetectionBox( std::string txt_path,
                     std::vector<Rect> &bbox,
                     std::vector<std::string> &object )
{

    string file = txt_path;
    std::string line;

    ifstream myReadFile;
    myReadFile.open(file.c_str());

//    std::getline(myReadFile, line, '\n');

    while( std::getline(myReadFile, line, '\n') )
    {
        std::cout << "line = " << line << std::endl;
        size_t pos = 0;
        size_t prev_loc = 0;
        pos = line.find_first_of(" ");
        string item, score;
        string obj = line.substr(0,pos);
        prev_loc = pos;


        std::getline(myReadFile, line, '\n'); std::cout << "line = " << line << std::endl;
        pos = 0;
        prev_loc = 0;
        pos = line.find_first_of(" ");
        score = line.substr(0,pos);
        cout << "\n score = " << score;

        std::getline(myReadFile, line, '\n');
        std::vector<string> param;

            pos = 0;
            prev_loc = 0;
            pos = line.find_first_of(" ");
            item = line.substr(0,pos);
            param.push_back(item);
            prev_loc = pos;
            string p_str = line;

            while( pos != string::npos )
            {
                p_str = p_str.substr( pos + 1,string::npos);
                pos = p_str.find_first_of(" ");
                item = p_str.substr(0,pos);
                param.push_back(item);
            }


            if( atof(score.c_str()) > 0.5 )
            {
            Rect box;
            stringstream convert_x(param[0]);
            convert_x >> box.x ;
            stringstream convert_y(param[1]);
            convert_y >> box.y ;
            stringstream convert_w(param[2]);
            convert_w >> box.width ;          // not width.. max x position
            stringstream convert_h(param[3]);
            convert_h >> box.height ;         // not height.. max y position

            //push back
            bbox.push_back(box);
            object.push_back(obj);

            }


    }

    return 0;

}


// main
std::vector<CylindricalShell> SurfacePatch::
extract_region_main( PointCloudXYZ::Ptr &ptCloud,
                     pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloudRGB,
                     std::string current_cls )
{
    //saving a copy of the point cloud
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr rgb_bin_cropped( new pcl::PointCloud<pcl::PointXYZRGB> ());
    rgb_bin_cropped->width = pt_cloudRGB->width;
    rgb_bin_cropped->height = pt_cloudRGB->height;
    rgb_bin_cropped->points.resize ( pt_cloudRGB->width * pt_cloudRGB->height);
    rgb_bin_cropped->is_dense = false;
    //copy data
    *rgb_bin_cropped = *pt_cloudRGB;

    std::vector<CylindricalShell> shells;
    std::vector<Cylinder_Param> cylind_final;
    std::vector<Sphere_Param> sphere_final;
    std::vector<Box_Param> box_final;

    if( ptCloud->empty() )
    {
        std::cout << " empty point cloud inside extract_region_main function......cannot proceed.." << std::endl;
        return shells;
    }

    {

        std::vector<std::vector<int> > indices;
        std::vector<PointCloudIndividual> segRegion;

//        std::cout << " ptCloud->points.size() = " << ptCloud->points.size() << std::endl;
        point_labels_rg_.resize( ptCloud->points.size(), -1 );
        indices.resize(0);
        indices = region_growing( ptCloud, pt_cloudRGB, segRegion );

        if( segRegion.size() > 0 )
        {
            curvatureEstimation( ptCloud, segRegion );

            //do pca
            pca_axis_normal( segRegion, ptCloud );
//          visualizeRegion_pcl(segRegion, ptCloud, rgb_bin_cropped);
//            showCloud(segRegion, ptCloud);

            std::vector<int> segment_without_base = remove_base( ptCloud, segRegion, 0.02 );

            this->set_gmm_segmentation(true);
            std::vector<int> segnum;
            std::vector<int> localize_seg;
            std::vector<Cylinder_Param> cylind;
            std::vector<Sphere_Param> sphere;
            std::vector<Box_Param> box;
            int is_box;
            if( this->get_gmm_segmentation() )
            {
                segnum = classifySegment_gmm_color_curv( segRegion, rgb_bin_cropped, current_cls );

                if( segnum.size() > 0 )
                {
//                    showCloud_gmm( segRegion, ptCloud, segnum );
//                    showCloud( segRegion, ptCloud, segnum );
                    is_box = fit_primitives_color_curv(  segRegion, segnum, ptCloud, cylind, sphere, box, localize_seg );
                }

            }
            else
            {
                for( int c= 0; c<segment_without_base.size(); c++ )
                {
//                    segnum.push_back(c);
                    segnum.push_back(segment_without_base[c]);
                }
                showCloud( segRegion, ptCloud, segnum );
//                visualizeRegion_pcl(segRegion, ptCloud, rgb_bin_cropped);
                is_box = fit_primitives_big_segment( segRegion, segnum, ptCloud, cylind, sphere, box, localize_seg );
            }

            std::cout << " is_box = " << is_box << std::endl;


            CylindricalShell shell;

            if( sphere.size() > 0 )
            {
                Sphere_Param sph = sphere[0];
                shell = grasp_localization_sphere( segRegion, ptCloud, localize_seg , sph );
                sphere_final.push_back(sph);
                if( shell.getRadius() != -1 )
                    shells.push_back(shell);
            }

            if( cylind.size() > 0 )
            {
                Cylinder_Param cylin = cylind[0];
                shell = grasp_localization_cylinder_bestfit( segRegion, ptCloud, localize_seg, cylin );
                cylind_final.push_back(cylin);
                if( shell.getRadius() != -1 )
                {
                    std::cout << shell.getRadius() << std::endl;
                    shells.push_back(shell);
                }
            }

            if( box.size() > 0 )
            {
                Box_Param box_single = box[0];
                std::vector<CylindricalShell> box_shells;
//                box_shells = grasp_localization_box(segRegion,ptCloud,rgb_bin_cropped,box_single);
                box_shells = grasp_localization_box_final(segRegion,ptCloud,rgb_bin_cropped,box_single);
                box_final.push_back(box_single);

                for( int b = 0; b < box_shells.size(); b++ )
                 {
                        shell = box_shells[b];

                        Eigen::Vector3d center, normal;
                        normal = shell.getNormal();
                        center = shell.getCentroid();

                        center(0) = center(0) - ( normal(0) * 0.015 );
                        center(1) = center(1) - ( normal(1) * 0.015 );
                        center(2) = center(2) - ( normal(2) * 0.015 );

                        shell.setCentroid(center);
                        shells.push_back(shell);
                   }

            }
            else if( is_box == 1 )
//            if( is_box == 1 )
            {
                std::cout << " can't fit a box ..... " << std::endl;

                std::vector<CylindricalShell> box_shells;
//                box_shells = grasp_localization_without_box(segRegion,ptCloud,rgb_bin_cropped, segnum );
                box_shells = grasp_localization_without_box_final(segRegion,ptCloud,rgb_bin_cropped, segnum );
                for( int b = 0; b < box_shells.size(); b++ )
                 {
                        shell = box_shells[b];

                        Eigen::Vector3d center, normal;
                        normal = shell.getNormal();
                        center = shell.getCentroid();

                        center(0) = center(0) - ( normal(0) * 0.015 );
                        center(1) = center(1) - ( normal(1) * 0.015 );
                        center(2) = center(2) - ( normal(2) * 0.015 );

                        shell.setCentroid(center);
                        shells.push_back(shell);
                   }

            }

//            visualize_primitives_all( cylind_final, sphere_final, box_final, rgb_bin_cropped);
//            visualize_primitives_handles( cylind_final, sphere_final, box_final,rgb_bin_cropped,shells);
            std::cout << " no of grasping position found  = " << shells.size() << std::endl;
            validateHandles(shells);
            std::cout << " no of grasping position found final = " << shells.size() << std::endl;
//            visualize_primitives_handle( cylind_final, sphere_final, box_final,rgb_bin_cropped,shells );
        }
    }
    return shells;

}


std::vector<int>  SurfacePatch::
remove_base(  const PointCloudXYZ::Ptr &ptCloud,
              std::vector<PointCloudIndividual> &segRegion,
              const double thr_reject )
{
    std::vector<int> segment_without_base;
    std::vector<pcl::PointXYZ> boundary_points_pcl = get_boundary_pcl();
//    std::cout << " boundary_points_pcl.size = " << boundary_points_pcl.size() << std::endl;

    if( (ptCloud->empty()) || ( segRegion.size() == 0 ) || ( boundary_points_pcl.size() != 4 ) )
        return segment_without_base;

    pcl::PointXYZ pt_1,pt_2;
#if(VISOPTION_SURFACE)

//             pcl::visualization::PCLVisualizer viewer("corners");
//             viewer.addPointCloud(ptCloud);
//             pt_1 = boundary_points_pcl[0];
//             viewer.addSphere( pt_1, 0.01, 255, 0, 0, "1");
//             pt_1 = boundary_points_pcl[1];
//             viewer.addSphere( pt_1, 0.01, 0, 255, 0, "2");
//             pt_1 = boundary_points_pcl[2];
//             viewer.addSphere( pt_1, 0.01, 0, 0, 255, "3");
//             pt_1 = boundary_points_pcl[3];
//             viewer.addSphere( pt_1, 0.01, 255, 255, 0, "4");

//             viewer.addPlane (*plane_1, "plane_1", 0);
//             std::cout << " close the window cloud_viewer to continue......." << std::endl;
//             while(!viewer.wasStopped())
//                viewer.spinOnce();
#endif


            double dot_p_cut = 0.9 ;
            double frac_segment_cutoff = 0.5;
            for( int s_count = 0; s_count < segRegion.size(); s_count++ )
            {
                PointCloudIndividual obj = segRegion[s_count];
                Eigen::Vector3d center_box, normal_dir;
                center_box = obj.getrepresentative_position();
                normal_dir = obj.getrepresentative_normal();
                std::vector<int> nnIndices = obj.getNNIndices();

//                std::cout << " segment count = " << s_count << std::endl;
//                std::cout << normal_dir(1) << " size = " << nnIndices.size() << std::endl;
                if( (( normal_dir(1)) < -0.5 ) && ( nnIndices.size() > 100 ))
                {

                    double dot_product = dot_p_cut;
                    bool is_normal_found = false;
                    Eigen::Vector3d normal, pt_on_plane;
                    Eigen::Vector3d normal_final;
                    {
                        //detecting normal
                        pt_1 = boundary_points_pcl[0];
                        pt_2 = boundary_points_pcl[1];
                        if( ( pcl::isFinite(pt_1) ) && ( pcl::isFinite(pt_2) ) )
                        {
                            normal(0) = pt_1.x - pt_2.x;
                            normal(1) = pt_1.y - pt_2.y;
                            normal(2) = pt_1.z - pt_2.z;
                            normal.normalize();

                            double dot_product_2 = normal.dot( normal_dir );
                            if( abs(dot_product_2) > dot_product )
                            {
                                normal_final = normal;
                                dot_product = abs( dot_product_2 );
                                is_normal_found = true;
                            }
                        }

                        // ------------------------------
                        pt_1 = boundary_points_pcl[3];
                        pt_2 = boundary_points_pcl[2];
                        if( ( pcl::isFinite(pt_1) ) && ( pcl::isFinite(pt_2) ) )
                        {
                            normal(0) = pt_1.x - pt_2.x;
                            normal(1) = pt_1.y - pt_2.y;
                            normal(2) = pt_1.z - pt_2.z;
                            normal.normalize();

                            double dot_product_1 = normal.dot( normal_dir );
                            if( (abs(dot_product_1) > dot_product ) )
                            {
                                dot_product = abs(dot_product_1);
                                is_normal_found = true;
                                normal_final = normal;
                            }
                        }

                    }

//                    std::cout << " is_normal_found = " << is_normal_found << std::endl;
                    if( is_normal_found )
                    {
                        pcl::PointXYZ pt_3 = boundary_points_pcl[2];
                        if(  pcl::isFinite(pt_3) )
                        {
                            pt_on_plane(0) = pt_3.x;
                            pt_on_plane(1) = pt_3.y;
                            pt_on_plane(2) = pt_3.z;
                        }

                        pt_3 = boundary_points_pcl[1];
                        if(  pcl::isFinite(pt_3) )
                        {
                            pt_on_plane(0) = pt_3.x;
                            pt_on_plane(1) = pt_3.y;
                            pt_on_plane(2) = pt_3.z;
                        }

                        normal_dir = normal_final;

                        pcl::ModelCoefficients::Ptr plane_1 (new pcl::ModelCoefficients);
                        plane_1->values.resize (4);
                        plane_1->values[0] = normal_dir(0);
                        plane_1->values[1] = normal_dir(1);
                        plane_1->values[2] = normal_dir(2);
                        plane_1->values[3] = -normal_dir.dot(pt_on_plane);

//                        pcl::visualization::PCLVisualizer viewer("corners");
//                        viewer.addPointCloud(ptCloud);
//                        pt_1 = boundary_points_pcl[0];
//                        viewer.addSphere( pt_1, 0.01, 255, 0, 0, "1");
//                        pt_1 = boundary_points_pcl[1];
//                        viewer.addSphere( pt_1, 0.01, 0, 255, 0, "2");
//                        pt_1 = boundary_points_pcl[2];
//                        viewer.addSphere( pt_1, 0.01, 0, 0, 255, "3");
//                        pt_1 = boundary_points_pcl[3];
//                        viewer.addSphere( pt_1, 0.01, 255, 255, 0, "4");

//                        viewer.addPlane (*plane_1, "plane_1", 0);
//                        std::cout << " close the window cloud_viewer to continue......." << std::endl;
//                        while(!viewer.wasStopped())
//                           viewer.spinOnce();

                        int count = 0;
                        for( int idx = 0; idx < nnIndices.size(); idx++ )
                        {
                                pcl::PointXYZ pt_xyz = ptCloud->points[nnIndices[idx]];

                                if( pcl::isFinite(pt_xyz) )
                                {
                                    Eigen::Vector3d cropped,v ;
                                    cropped(0) = pt_xyz.x;
                                    cropped(1) = pt_xyz.y;
                                    cropped(2) = pt_xyz.z;

                                    v = cropped - pt_on_plane;
                                    double along_axis_Dist = normal_dir.dot(v);

                                    if( abs(along_axis_Dist) < thr_reject )
                                    {
                                        count++;
                                    }

                                }

                        }

                        float frac_segment = count / (float)nnIndices.size();
//                        std::cout << nnIndices.size() << " " << count << " fraction = " << frac_segment << std::endl;

                        if( frac_segment < frac_segment_cutoff )
                            segment_without_base.push_back( s_count );
                        else
                        {
                            obj.set_use_the_segment(false);
                            segRegion[s_count] = obj;

                            if( this->use_occlution )
                            {
                                for( int idx = 0; idx < nnIndices.size(); idx++ )
                                    point_labels_rg_[nnIndices[idx]] = -1;
                            }
                        }

                    }
                    else
                    {
                       segment_without_base.push_back( s_count );
#if(VISOPTION_SURFACE)
                       std::cout << is_normal_found <<  " segment number ... " << s_count << std::endl;
                       std::cout << normal_final.dot(normal_dir) << std::endl;
#endif
                    }



                }
                else
                {
                    segment_without_base.push_back( s_count );

 #if(VISOPTION_SURFACE)
                    std::cout << " normal direction is not upside... " << std::endl;
                    std::cout << "normal_dir " << normal_dir(1) << " nnIndices = " << nnIndices.size() << std::endl;
 #endif
                }

//                std::vector<int>segnum;
//                segnum.push_back(s_count);
//                showCloud(segRegion, ptCloud, segnum );
//                getchar();
            }

}

void SurfacePatch::
validateHandles( std::vector<CylindricalShell> &shells )
{
    if(shells.size() > 0)
    {
        CylindricalShell shell;
        int s_count = 0;
        do
        {
            shell = shells[s_count];
            Eigen::Vector3d curvature_axis, normal, center;
            double rad = shell.getRadius();
            normal = shell.getNormal();
            center = shell.getCentroid();
            curvature_axis = shell.getCurvatureAxis();
            if( (abs(normal(1)) > 0.6)  || (abs(normal(0)) > 0.6) || (rad < 0.005) )
            {
                std::cout << " rejecting the direction .... " << normal(1) << " , "
                          << curvature_axis(1) << " rad = " << rad << std::endl;
                shells.erase( shells.begin() + s_count);
            }
            else
            {
                curvature_axis = curvature_axis.cross(normal);
                curvature_axis.normalize();
                shell.setCurvatureAxis( curvature_axis );

                center(0) = center(0) - ( normal(0) * 0.005 );
                center(1) = center(1) - ( normal(1) * 0.005 );
                center(2) = center(2) - ( normal(2) * 0.005 );

                shell.setCentroid(center);

                shells[s_count] = shell;
                s_count++;

            }


        }while(s_count < shells.size() );
    }
    return;
}

int SurfacePatch::
fit_primitives_color_curv_1(std::vector<PointCloudIndividual> &segRegion,
         std::vector<int> &segnum,
         const PointCloudXYZ::Ptr &f_cloud,
         std::vector<Cylinder_Param> &cylind,
         std::vector<Sphere_Param> &sphere,
         std::vector<Box_Param> &box ,
         std::vector<int> &localize_seg)
{
    std::cout << " inside fit primitives color_curv function ... " << std::endl;
    if( ( segRegion.size() == 0 ) || ( segnum.size() == 0 ) || ( f_cloud->empty() ) )
        return -1;

    float curvature_cutoff_low = 0.05;
    float curvature_cutoff_high = 0.1;
    std::pair<int,int> big_segment;
    big_segment.first = 0;
    big_segment.second = -1;
    std::vector<int> segment_num_to_be_fited;

    std::vector<std::pair<float,int> > color_vector;
    std::vector<std::pair<int,int> > size_vector;
    std::vector<int> segment_out;
    float frac_segment = 0.3;

    for( int u = 0; u < segnum.size() ; u++ )
    {
        PointCloudIndividual obj = segRegion[segnum[u]];
        std::vector<float> color_dist = obj.get_gmm_color();

        int no_pos_sample = 0;
        this->thr_rgb = 0.4;
        for( int f = 0; f < color_dist.size(); f++ )
        {
            if( color_dist[f] > thr_rgb )
               no_pos_sample++ ;
        }
        float frac_color = no_pos_sample / (float) color_dist.size() ;
        std::cout << segnum[u] << "frac_color = "  << frac_color << " " << segnum[u] << " " << segRegion[segnum[u]].getNNIndices().size() << std::endl;

        //don't include thoose segment with gmm color prob less than a thr
        if( (frac_color < frac_segment) || ( segRegion[segnum[u]].getNNIndices().size() < 100 ) )
        {
            segment_out.push_back(segnum[u]);
            continue;
        }

        std::pair<int,int> pair_temp_2;
        pair_temp_2.first = color_dist.size();
        pair_temp_2.second = segnum[u];
        size_vector.push_back( pair_temp_2 );

        std::pair<float,int> pair_temp_color;
        pair_temp_color.first = frac_color;
        pair_temp_color.second = segnum[u];
        color_vector.push_back( pair_temp_color );

        segment_num_to_be_fited.push_back(segnum[u]);

    }

    std::cout << " color_vector.size() = " << color_vector.size() << std::endl;
    std::sort( size_vector.begin(), size_vector.end() );
    std::sort( color_vector.begin(), color_vector.end() );
    segnum.clear();


    std::cout << " size segment ..." << std::endl;
    for( int y = size_vector.size() - 1; y >= 0; y-- )
    {
        std::cout  << size_vector[y].first << " ... " << size_vector[y].second << std::endl;
    }
    std::cout << " color segment ..." << std::endl;
    for( int y = size_vector.size() - 1; y >= 0; y-- )
    {
        std::cout << color_vector[y].first << " ... " << color_vector[y].second << std::endl;
    }

//            if( color_vector.size() > 0 )
//            {
//                int loop_count_color = color_vector.size() - 1 ;
//                int loop_count_size = size_vector.size() - 1 ;
//                std::pair<float,int> pair_temp_color;
//                std::pair<int,int> pair_temp_size;

//                float color_prob_highest = color_vector[loop_count_color].first;
//                do
//                {
//                    pair_temp_size = size_vector[loop_count_size];
//                   // std::cout << " current processing segment " << pair_temp_2.second << " size = " << segRegion[pair_temp_2.second].getNNIndices().size() << std::endl;

//                    // looping through curvature value
//                    for( int inner_loop = 0 ; inner_loop < color_vector.size(); inner_loop++ )
//                    {
//                        pair_temp_inner_loop = color_vector[inner_loop];

//                        //std::cout << loop_count_color << "  " << pair_temp_inner_loop.second << " curv " << pair_temp_inner_loop.first <<
//                        //" " << segRegion[pair_temp_2.second].getNNIndices().size() << std::endl;

//                        if( abs( color_prob_highest - pair_temp_inner_loop.first ) < 0.2 )
//                        {
//                             if( pair_temp_inner_loop.second == pair_temp_2.second )
//                            {
//                                 big_segment.second = pair_temp_2.second ;
//                                 break;
//                            }
//                        }

//                      }



//                    loop_count_size--;

//                }while( loop_count_size >= 0 );
//            }


//    if( color_vector.size() == 1 )
//    {
//        std::pair<float,int> pair_temp;
//        pair_temp = color_vector[0];
//        big_segment.first = pair_temp.first;
//        big_segment.second = pair_temp.second;
//    }
//    else if( color_vector.size() > 1 )
//    {
//        std::pair<int,int> pair_temp_2;
//        std::pair<float,int> pair_temp_3;
//        std::pair<float,int> pair_temp_inner_loop;
//        int loop_count_color = color_vector.size() - 1 ;
//        int loop_count_size = size_vector.size() - 1 ;


//        {
//            pair_temp_2 = size_vector[loop_count_size];
//            pair_temp_3 = color_vector[loop_count_color];
//            if( ( pair_temp_3.second == pair_temp_2.second ) )
//            {
//                std::cout << " biggest segment has the highest prob.. Ideal Case. " << std::endl;
//                big_segment.first = pair_temp_3.first;
//                big_segment.second = pair_temp_3.second;
////                segnum.push_back( pair_temp.second);
//            }

//        if( big_segment.second == -1 )
//        {
//            float color_prob_highest = color_vector[loop_count_color].first;
//            do
//            {
//                pair_temp_2 = size_vector[loop_count_size];
//               // std::cout << " current processing segment " << pair_temp_2.second << " size = " << segRegion[pair_temp_2.second].getNNIndices().size() << std::endl;

//                // looping through curvature value
//                for( int inner_loop = 0 ; inner_loop < color_vector.size(); inner_loop++ )
//                {
//                    pair_temp_inner_loop = color_vector[inner_loop];

//                    //std::cout << loop_count_color << "  " << pair_temp_inner_loop.second << " curv " << pair_temp_inner_loop.first <<
//                    //" " << segRegion[pair_temp_2.second].getNNIndices().size() << std::endl;

//                    if( abs( color_prob_highest - pair_temp_inner_loop.first ) < 0.2 )
//                    {
//                            if( pair_temp_inner_loop.second == pair_temp_2.second )
//                        {
//                             big_segment.second = pair_temp_2.second ;
//                             break;
//                        }
//                    }
//                  }

//                if( big_segment.second != -1 )
//                    break;

//                loop_count_size--;

//            }while( loop_count_size >= 0 );
//        }
//        std::cout << " segemnt number on which decision to be made --- " << big_segment.second << std::endl;
//        }
//    }
// // --------------------------------------------------------------------------------------------------------------------------------


//    if( big_segment.second == -1)
//    {
//        std::cout << " big_segment.second = " << big_segment.second << std::endl;
//        return -1;
//    }

////    std::cout << " fit primitives .. number of segment on which to be fitted.." << segnum.size() << std::endl;
//    float rad_curv = segRegion[big_segment.second].get_curvature();
//    Eigen::Vector3d var = segRegion[big_segment.second].get_seg_var();
//    float var_s = sqrt( (var(0) * var(0)) + (var(1) * var(1)) + (var(2) * var(2))  );
//    std::cout << var_s << " big segment -- " << big_segment.second  << std::endl;

//    bool isCylinder ;
//    int is_box ;
//    //decide whether to fit cylinder or box based on curvature and var
//    {
//        if( rad_curv < curvature_cutoff_low )
//        {
//            isCylinder = true;
//        }
//        else if( rad_curv < curvature_cutoff_high )
//        {
//            if( var_s > 0.1 )
//                isCylinder = true;
//            else
//                isCylinder = false;
//        }
//        else
//        {
//            isCylinder = false;
//        }
//    }

//    if( isCylinder )
//    {
//        std::cout << " ready to fit cylinder or sphere ....with curvature " << rad_curv << std::endl;
//        fit_cylinder_sphere( segRegion, segment_num_to_be_fited, f_cloud, 0.02, cylind, sphere, localize_seg );
//        is_box = 0;
//    }
//    else
//    {
//        std::cout << " ready to fit box...with curvature " << rad_curv << std::endl;
//        box = fit_box_final( segRegion, segment_num_to_be_fited );
//        is_box = 1;
//    }

    int is_box = -1;
    return is_box;

}



int SurfacePatch::
fit_primitives_color_curv(std::vector<PointCloudIndividual> &segRegion,
         std::vector<int> &segnum,
         const PointCloudXYZ::Ptr &f_cloud,
         std::vector<Cylinder_Param> &cylind,
         std::vector<Sphere_Param> &sphere,
         std::vector<Box_Param> &box ,
         std::vector<int> &localize_seg)
{
    std::cout << " inside fit primitives color_curv function ... " << std::endl;
    if( ( segRegion.size() == 0 ) || ( segnum.size() == 0 ) || ( f_cloud->empty() ) )
        return -1;

    float curvature_cutoff_low = 0.05;
    float curvature_cutoff_high = 0.1;
    std::pair<int,int> big_segment;
    big_segment.first = 0;
    big_segment.second = -1;
    std::vector<int> segment_num_to_be_fited;

    std::vector<std::pair<float,int> > color_vector;
    std::vector<std::pair<int,int> > size_vector;
    std::vector<int> segment_out;
    float frac_segment = 0.3;
    this->thr_rgb = 0.3;

    for( int u = 0; u < segnum.size() ; u++ )
    {
        PointCloudIndividual obj = segRegion[segnum[u]];
        std::vector<float> color_dist = obj.get_gmm_color();

        int no_pos_sample = 0;
        for( int f = 0; f < color_dist.size(); f++ )
        {
            if( color_dist[f] > thr_rgb )
               no_pos_sample++ ;
        }
        float frac_color = no_pos_sample / (float) color_dist.size() ;
        std::cout << segnum[u] << "frac_color = "  << frac_color << " " << segnum[u] << " " << segRegion[segnum[u]].getNNIndices().size() << std::endl;

        //don't include thoose segment with gmm color prob less than a thr
        if( /*(frac_color < frac_segment ) ||*/ ( segRegion[segnum[u]].getNNIndices().size() < 200 ))
        {
            segment_out.push_back(segnum[u]);
            continue;
        }

        std::pair<int,int> pair_temp_2;
        pair_temp_2.first = color_dist.size();
        pair_temp_2.second = segnum[u];
        size_vector.push_back( pair_temp_2 );

        std::pair<float,int> pair_temp_color;
        pair_temp_color.first = frac_color;
        pair_temp_color.second = segnum[u];
        color_vector.push_back( pair_temp_color );

        segment_num_to_be_fited.push_back(segnum[u]);

        //set the object
        segRegion[segnum[u]].set_gmm_color_final(frac_color);

    }


    std::sort( size_vector.begin(), size_vector.end() );
    std::sort( color_vector.begin(), color_vector.end() );
    segnum.clear();

#if(VISOPTION_SURFACE)
    std::cout << " color_vector.size() = " << color_vector.size() << std::endl;
    std::cout << " size segment ..." << std::endl;
    for( int y = size_vector.size() - 1; y >= 0; y-- )
    {
        std::cout  << size_vector[y].first << " ... " << size_vector[y].second << std::endl;
    }
    std::cout << " color segment ..." << std::endl;
    for( int y = size_vector.size() - 1; y >= 0; y-- )
    {
        std::cout << color_vector[y].first << " ... " << color_vector[y].second << std::endl;
    }
#endif

    if( color_vector.size() == 1 )
    {
        std::pair<float,int> pair_temp;
        pair_temp = color_vector[0];
        big_segment.first = pair_temp.first;
        big_segment.second = pair_temp.second;
    }
    else if( color_vector.size() > 1 )
    {
        std::pair<int,int> pair_temp_2;
        std::pair<float,int> pair_temp_3;
        std::pair<float,int> pair_temp_inner_loop;
        int loop_count_color = color_vector.size() - 1 ;
        int loop_count_size = size_vector.size() - 1 ;


        {
            pair_temp_2 = size_vector[loop_count_size];
            pair_temp_3 = color_vector[loop_count_color];
            if( ( pair_temp_3.second == pair_temp_2.second ) )
            {
#if(VISOPTION_SURFACE)
                std::cout << " biggest segment has the highest prob.. Ideal Case. " << std::endl;
#endif
                big_segment.first = pair_temp_3.first;
                big_segment.second = pair_temp_3.second;
//                segnum.push_back( pair_temp.second);
            }

        if( big_segment.second == -1 )
        {
            float color_prob_highest = color_vector[loop_count_color].first;
            do
            {
                pair_temp_2 = size_vector[loop_count_size];
               // std::cout << " current processing segment " << pair_temp_2.second << " size = " << segRegion[pair_temp_2.second].getNNIndices().size() << std::endl;

                // looping through curvature value
                for( int inner_loop = 0 ; inner_loop < color_vector.size(); inner_loop++ )
                {
                    pair_temp_inner_loop = color_vector[inner_loop];

                    //std::cout << loop_count_color << "  " << pair_temp_inner_loop.second << " curv " << pair_temp_inner_loop.first <<
                    //" " << segRegion[pair_temp_2.second].getNNIndices().size() << std::endl;

                    if( abs( color_prob_highest - pair_temp_inner_loop.first ) < 0.3 )
                    {
                        if( pair_temp_inner_loop.second == pair_temp_2.second )
                        {
                             big_segment.second = pair_temp_2.second ;
                             break;
                        }
                    }

                  }

                if( big_segment.second != -1 )
                    break;

                loop_count_size--;

            }while( loop_count_size >= 0 );
        }
        std::cout << " segemnt number on which decision to be made --- " << big_segment.second << std::endl;
        }
    }
 // ----------------------------------------------------------------------------------------------------------------

    if( big_segment.second == -1)
    {
        std::cout << " big_segment.second = " << big_segment.second << std::endl;
        return -1;
    }

    segnum.push_back( big_segment.second );
    for( int j = 0; j < segment_num_to_be_fited.size(); j++ )
    {
        if( big_segment.second != segment_num_to_be_fited[j] )
            segnum.push_back( segment_num_to_be_fited[j] );
    }


//    std::cout << " fit primitives .. number of segment on which to be fitted.." << segnum.size() << std::endl;
    float rad_curv = segRegion[big_segment.second].get_curvature();
    Eigen::Vector3d var = segRegion[big_segment.second].get_seg_var();
    float var_s = sqrt( (var(0) * var(0)) + (var(1) * var(1)) + (var(2) * var(2))  );

#if(VISOPTION_SURFACE)
    std::cout << " var_s = " << var_s << " big segment -- " << big_segment.second  << std::endl;
#endif

    bool isCylinder ;
    int is_box ;
    //decide whether to fit cylinder or box based on curvature and var
    {
        if( rad_curv < curvature_cutoff_low )
        {
            isCylinder = true;
        }
        else if( rad_curv < curvature_cutoff_high )
        {
            if( var_s > 0.1 )
                isCylinder = true;
            else
                isCylinder = false;
        }
        else
        {
            isCylinder = false;
        }
    }

    if( isCylinder )
    {
        std::cout << " ready to fit cylinder or sphere ....with curvature " << rad_curv << std::endl;
        fit_cylinder_sphere( segRegion, segment_num_to_be_fited, f_cloud, 0.02, cylind, sphere, localize_seg );
        is_box = 0;
    }
    else
    {
        std::cout << " ready to fit_box...with curvature " << rad_curv << std::endl;
//        box = fit_box_final( segRegion, segment_num_to_be_fited );
        box = fit_box( segRegion, segment_num_to_be_fited );
        is_box = 1;
    }

    return is_box;

}



int SurfacePatch::
fit_primitives(std::vector<PointCloudIndividual> &segRegion,
         std::vector<int> &segnum,
         const PointCloudXYZ::Ptr &f_cloud,
         std::vector<Cylinder_Param> &cylind,
         std::vector<Sphere_Param> &sphere,
         std::vector<Box_Param> &box ,
         std::vector<int> &localize_seg)
{
    std::cout << " inside fit primitives function ... " << std::endl;
    if( ( segRegion.size() == 0 ) || ( segnum.size() == 0 ) || ( f_cloud->empty()) )
        return -1;

    float curvature_cutoff_low = 0.05;
    float curvature_cutoff_high = 0.1;
    std::pair<int,int> big_segment;
    big_segment.first = 0;
    big_segment.second = -1;
    std::vector<int> segment_num_to_be_fited;

    std::vector<std::pair<float,int> > curv_vector;
    std::vector<std::pair<float,int> > color_vector;
    std::vector<std::pair<int,int> > size_vector;
    std::vector<int> segment_out;
    float frac_segment = 0.3;
    bool isCurvSame = true;
    float prev_curv_prob = segRegion[segnum[0]].get_gmm_color_final() ;

    for( int u = 0; u < segnum.size() ; u++ )
    {
        PointCloudIndividual obj = segRegion[segnum[u]];
        float curv = obj.get_gmm_color_final();
        std::vector<float> color_dist = obj.get_gmm_color();

        int no_pos_sample = 0;
        this->thr_rgb = 0.4;
        for( int f = 0; f < color_dist.size(); f++ )
        {
            if( color_dist[f] > thr_rgb )
               no_pos_sample++ ;
        }
        float frac_color = no_pos_sample / (float) color_dist.size() ;
        std::cout << curv << " " << frac_color << " " << segnum[u] << " " << segRegion[segnum[u]].getNNIndices().size() << std::endl;

        //don't include thoose segment with gmm color prob less than a thr
        if( frac_color < frac_segment )
        {
            segment_out.push_back(segnum[u]);
            continue;
        }

        //check whether all the segment has same prob----- segment belongs to plannertype object
        if( abs( curv - prev_curv_prob) > 0.01 )
            isCurvSame = false;

        std::pair<float,int> pair_temp;
        pair_temp.first = curv;
        pair_temp.second = segnum[u];
        curv_vector.push_back( pair_temp );

        std::pair<int,int> pair_temp_2;
        pair_temp_2.first = color_dist.size();
        pair_temp_2.second = segnum[u];
        size_vector.push_back( pair_temp_2 );

        std::pair<float,int> pair_temp_color;
        pair_temp_color.first = frac_color;
        pair_temp_color.second = segnum[u];
        color_vector.push_back( pair_temp_color );

        segment_num_to_be_fited.push_back(segnum[u]);

    }

    std::cout << " curv_vector.size() = " << curv_vector.size() << std::endl;
    std::sort( curv_vector.begin(), curv_vector.end() );
    std::sort( size_vector.begin(), size_vector.end() );
    std::sort( color_vector.begin(), color_vector.end() );
    segnum.clear();

//    std::cout << " curv segment ..." << std::endl;
//    for( int y = curv_vector.size() - 1; y >= 0; y-- )
//    {
//        std::cout << curv_vector[y].first << " ... " << curv_vector[y].second << std::endl;
//    }
//    std::cout << " size segment ..." << std::endl;
//    for( int y = curv_vector.size() - 1; y >= 0; y-- )
//    {
//        std::cout  << size_vector[y].first << " ... " << size_vector[y].second << std::endl;
//    }
//    std::cout << " color segment ..." << std::endl;
//    for( int y = curv_vector.size() - 1; y >= 0; y-- )
//    {
//        std::cout << color_vector[y].first << " ... " << color_vector[y].second << std::endl;
//    }
//    std::cout << " curv_vector.size() = " << curv_vector.size() << std::endl;
//    std::cout << " isCurvSame " << isCurvSame << std::endl;

    if( curv_vector.size() == 1 )
    {
        std::pair<float,int> pair_temp;
        pair_temp = curv_vector[0];
        big_segment.first = pair_temp.first;
        big_segment.second = pair_temp.second;
    }
    else if( curv_vector.size() > 1 )
    {
        std::pair<float,int> pair_temp;
        std::pair<int,int> pair_temp_2;
        std::pair<float,int> pair_temp_3;
        std::pair<float,int> pair_temp_inner_loop;
        int loop_count_curv = curv_vector.size() - 1 ;
        int loop_count_size = size_vector.size() - 1 ;

        if( isCurvSame )
        {
            pair_temp = color_vector[ color_vector.size() - 1 ];
            big_segment.first = pair_temp_2.first;
            big_segment.second = pair_temp_2.second;

            for( int s_out = 0; s_out < segment_out.size(); s_out++ )
            {
                std::vector<int> nei = segRegion[segment_out[s_out]].get_neighbour();

                int no_cout = 0;
                for( int n = 0; n < nei.size(); n++ )
                {
                    for( int s = 0; s<color_vector.size(); s++)
                    {
                        if( nei[n] == segnum[s] )
                        {
                            no_cout++;
                            break;
                        }
                    }

                    if( no_cout> 1 )
                    {
                        break;
                    }
                }
                if( no_cout> 1 )
                {
                    segment_num_to_be_fited.push_back(segment_out[s_out]);
                    std::cout << " including segment " << segment_out[s_out] << std::endl;
                }

            }

        }
        else
        {
            pair_temp = curv_vector[loop_count_curv];
            pair_temp_2 = size_vector[loop_count_size];
            pair_temp_3 = color_vector[color_vector.size() - 1];
            if( ( pair_temp.second == pair_temp_2.second ) || ( pair_temp_3.second == pair_temp.second ) )
            {
                std::cout << " biggest segment has the highest prob.. Ideal Case. " << std::endl;
                big_segment.first = pair_temp.first;
                big_segment.second = pair_temp.second;
//                segnum.push_back( pair_temp.second);
            }

        if( big_segment.second == -1 )
        {
            do
            {
                pair_temp_2 = size_vector[loop_count_size];
//                std::cout << " current processing segment " << pair_temp_2.second << " size = " << segRegion[pair_temp_2.second].getNNIndices().size() << std::endl;

                // looping through curvature value
                for( int inner_loop = 0 ; inner_loop < curv_vector.size(); inner_loop++ )
                {
                    pair_temp_inner_loop = curv_vector[inner_loop];

    //                std::cout << loop_count_curv << "  " << pair_temp.second << " curv " << pair_temp.first <<
    //                " " << segRegion[pair_temp.second].getNNIndices().size() << std::endl;

                    if( pair_temp_inner_loop.second == pair_temp_2.second )
                    {
                         big_segment.second = pair_temp_2.second ;
                         break;
                    }
                }

                if( big_segment.second != -1 )
                    break;

                loop_count_size--;

            }while( loop_count_size >= 0 );
        }
        std::cout << " segemnt number on which decision to be made --- " << big_segment.second << std::endl;
        }
    }
 // --------------------------------------------------------------------------------------------------------------------------------


    if( big_segment.second == -1)
        return -1;

//    std::cout << " fit primitives .. number of segment on which to be fitted.." << segnum.size() << std::endl;
    float rad_curv = segRegion[big_segment.second].get_curvature();
    Eigen::Vector3d var = segRegion[big_segment.second].get_seg_var();
    float var_s = sqrt( (var(0) * var(0)) + (var(1) * var(1)) + (var(2) * var(2))  );
    std::cout << " big segment -- " << big_segment.second  << std::endl;

    bool isCylinder ;
    int is_box ;
    //decide whether to fit cylinder or box based on curvature and var
    {
        if( rad_curv < curvature_cutoff_low )
        {
            isCylinder = true;
        }
        else if( rad_curv < curvature_cutoff_high )
        {
            if( var_s > 0.1 )
                isCylinder = true;
            else
                isCylinder = false;
        }
        else
        {
            isCylinder = false;
        }
    }

    if( isCylinder )
    {
//        std::cout << " ready to fit cylinder or sphere ....with curvature " << rad_curv << std::endl;
        fit_cylinder_sphere( segRegion, segment_num_to_be_fited, f_cloud, 0.02, cylind, sphere, localize_seg );
        is_box = 0;
    }
    else
    {
        std::cout << " ready to fit box...with curvature " << rad_curv << std::endl;
        box = fit_box_final( segRegion, segment_num_to_be_fited );
        is_box = 1;
    }

    return isCylinder;

}


int SurfacePatch::
fit_primitives_big_segment(std::vector<PointCloudIndividual> &segRegion,
         std::vector<int> &segnum,
         const PointCloudXYZ::Ptr &f_cloud,
         std::vector<Cylinder_Param> &cylind,
         std::vector<Sphere_Param> &sphere,
         std::vector<Box_Param> &box ,
         std::vector<int> &localize_seg)
{
     std::cout << " inside fit_primitives_big_segment...." << std::endl;
     if( ( segRegion.size() == 0 ) || ( segnum.size() == 0 ) )
     {
         return -1;
     }

     std::cout << " inside fit_primitives_big_segment ready to fit...." << std::endl;

    float curvature_cutoff_low = 0.05;
    float curvature_cutoff_high = 0.1;
    std::pair<int,int> big_segment;
    big_segment.first = 0;
    big_segment.second = -1;
    for( int u = 0; u < segnum.size(); u++ )
    {
        PointCloudIndividual obj = segRegion[segnum[u]];

        // determine whether to fit box or cylinder based on radius of curvature of biggest segment
        if( obj.getNNIndices().size() > big_segment.first )
        {
            big_segment.first = obj.getNNIndices().size();
            big_segment.second = segnum[u];
        }
    }

    if( big_segment.second == -1)
        return -1;

//    std::cout << " fit primitives .. number of segment on which to be fitted.." << segnum.size() << std::endl;
    float rad_curv = segRegion[big_segment.second].get_curvature();
    Eigen::Vector3d var = segRegion[big_segment.second].get_seg_var();
    float var_s = sqrt( (var(0) * var(0)) + (var(1) * var(1)) + (var(2) * var(2))  );
    std::cout << var_s << " big segment -- " << big_segment.second  << std::endl;

    bool isCylinder ;
    int is_box ;
    //decide whether to fit cylinder or box based on curvature and var
    {
        if( rad_curv < curvature_cutoff_low )
        {
            isCylinder = true;
        }
        else if( rad_curv < curvature_cutoff_high )
        {
            if( var_s > 0.1 )
                isCylinder = true;
            else
                isCylinder = false;
        }
        else
        {
            isCylinder = false;
        }
    }


    if( isCylinder )
    {
        std::cout << " ready to fit cylinder or sphere ....with curvature " << rad_curv << std::endl;
        fit_cylinder_sphere( segRegion, segnum, f_cloud, 0.02, cylind, sphere, localize_seg );
        is_box = 0;
    }
    else
    {
        std::cout << " ready to fit box...with curvature " << rad_curv << std::endl;
        box = fit_box_final( segRegion, segnum);
        is_box = 1;
    }


    return is_box;

}



// fit box final
std::vector<Box_Param> SurfacePatch::
fit_box_final( std::vector<PointCloudIndividual> &segRegion,
         std::vector<int> &segnum)
{

    float fract_axis_cutoff = FRACT_AXIS_CUTOFF;
    std::vector<std::vector<int> > seg_pair_checked(2);
    std::vector<Box_Param> boxes;
    // finding maximum sized segment
    std::pair<int,int> big_segment_final;
    big_segment_final.first = 0;
    big_segment_final.second = -1;
    std::pair<int,int> segment_used ;


    //looping over segments
    for( int cls_seg = 0; cls_seg < segnum.size(); cls_seg++ )
    {
        PointCloudIndividual obj_processing = segRegion[segnum[cls_seg]];
        std::vector<int> idx_seg = obj_processing.getNNIndices();

        std::vector<int> neigh_reg = obj_processing.get_neighbour();
        std::vector<int> neigh ;
        //check which neighbours are in the same object
        for ( int n_count = 0; n_count < neigh_reg.size(); n_count++ )
        {
            for( int ii = 0; ii < segnum.size(); ii++ )
            {
                if( ( neigh_reg[n_count] == segnum[ii] ) ) // checking which neighbour is in the same object
                {
                    neigh.push_back( neigh_reg[n_count] );
                    break;
                }

            }
        }

        std::cout << segnum[cls_seg] << " neighbours = " << " === " ;
        for( int ii = 0; ii < neigh_reg.size(); ii++ )
        {
            std::cout << neigh_reg[ii] << " , ";
        }
        std::cout << std::endl;

        std::vector<Box_Param> boxes_temp;
        std::vector<std::vector<float> > check_segment_aligned ;
        std::vector<float> aPair(7) ;
        bool noBoxfitted = true;
        for( int n = 0 ; n < neigh.size(); n++ )
        {
            std::vector<int> idx = segRegion[neigh[n]].getNNIndices();

            PointCloudIndividual obj, obj_nei;
            //check which segment is bigger
            std::pair<int,int> big_segment;      //  set the parameter to outer segment
            big_segment.first = idx_seg.size();
            big_segment.second = segnum[cls_seg];
            obj = segRegion[big_segment.second];
            obj_nei = segRegion[neigh[n]];
            segment_used.first =  segnum[cls_seg];
            segment_used.second = neigh[n];

            if( idx.size() > big_segment.first ) // changed the
            {
                big_segment.first = idx.size();
                big_segment.second = neigh[n];
                obj = segRegion[big_segment.second];
                obj_nei = segRegion[segnum[cls_seg]];
                segment_used.first =  neigh[n];
                segment_used.second = segnum[cls_seg];
            }


            Eigen::Vector3d re_normal = obj.getrepresentative_normal();
            Eigen::Vector3d pt_on_plane = obj.getrepresentative_position();
            Eigen::Vector3d cut = obj.getcut_axis();
            Eigen::Matrix3f eigen_vecs = obj.get_axis();
            Eigen::Vector3f major_axis = eigen_vecs.col(0);
            Eigen::Vector3f minor_axis = eigen_vecs.col(1);
            Eigen::Matrix3f cube_axis = obj.get_axis();
            Eigen::Vector3d axis_length = obj.getcut_axis();

            Eigen::Vector3d pt_on_plane_nei = obj_nei.getrepresentative_position();
            Eigen::Matrix3f eigen_vecs_nei = obj_nei.get_axis();
            Eigen::Vector3d cut_nei = obj_nei.getcut_axis();
            Eigen::Vector3f major_axis_nei = eigen_vecs_nei.col(0);
            Eigen::Vector3f minor_axis_nei = eigen_vecs_nei.col(1);

            Eigen::Vector3d normal_nei = obj_nei.getrepresentative_normal();

            double dot_product = re_normal(0) * normal_nei(0) + re_normal(1) * normal_nei(1) + re_normal(2) * normal_nei(2);

            //calculate centroid
            Eigen::Vector3f mean_diag ;
            mean_diag(0) = (float)pt_on_plane(0);
            mean_diag(1) = (float)pt_on_plane(1);
            mean_diag(2) = (float)pt_on_plane(2);

            if( abs( dot_product ) < PERPENDICULAR_CUTOFF )
            {
                int first_axis = -1;  // 0 - minor , 1 -major
                int second_axis = -1;  // 0 - minor , 1 -major
                double match_value = 0.6;
                float fract_axis ;

                //check on major axis
                double mm_product = major_axis(0) * major_axis_nei(0) +
                                    major_axis(1) * major_axis_nei(1) +
                                    major_axis(2) * major_axis_nei(2);

                if( abs(mm_product) > match_value )
                {
                     first_axis = 1;
                     second_axis = 1;
                     match_value = 0.7;
                     fract_axis = cut(0) / (float)cut_nei(0);
//                     axis_length(1) = ( cut(0) + cut_nei(0) ) / 2.0 ;
                }


                double mn_product = major_axis(0) * minor_axis_nei(0) +
                                    major_axis(1) * minor_axis_nei(1) +
                                    major_axis(2) * minor_axis_nei(2);

                if( abs(mn_product) > match_value )
                {
                    // match with major with minor
                    match_value = abs(mn_product);
                    first_axis = 1;
                    second_axis = 0;
                    fract_axis = cut(0) / (float)cut_nei(1);
//                    axis_length(1) = ( cut(0) + cut_nei(1) ) / 2.0 ;
                }

                //check on minor axis
                double nm_product = minor_axis(0) * major_axis_nei(0) +
                                    minor_axis(1) * major_axis_nei(1) +
                                    minor_axis(2) * major_axis_nei(2);

                if( abs(nm_product) > match_value )
                {
                    // match with minor with major
                    match_value = abs(nm_product);
                    first_axis = 0;
                    second_axis = 1;
                    fract_axis = cut(1) / (float)cut_nei(0);
//                    axis_length(1) = ( cut(1) + cut_nei(0) ) / 2.0 ;
                }

                double nn_product = minor_axis(0) * minor_axis_nei(0) +
                                    minor_axis(1) * minor_axis_nei(1) +
                                    minor_axis(2) * minor_axis_nei(2);

                if( abs(nn_product) > match_value )
                {
                    // match with minor with minor
                    match_value = abs(nn_product);
                    first_axis = 0;
                    second_axis = 0;
                    fract_axis = cut(1) / (float)cut_nei(1);
//                    axis_length(1) = ( cut(1) + cut_nei(1) ) / 2.0 ;

                }

                if( match_value > AXIS_MATCH_BOX )
                {

                    if( second_axis == 0 ) // match found with minor
                    {
                        cube_axis.col(2) = eigen_vecs_nei.col(0);  // push other axis as other direction of box
                        axis_length(2) = cut_nei(0);               // set other axis length as rest axis lnegth of box


                        Eigen::Vector3f v;
                        Eigen::Vector3f dir_axis = eigen_vecs_nei.col(0);

                        // v = point-orig
                        v(0) = (float)( pt_on_plane_nei(0) - pt_on_plane(0) );
                        v(1) = (float)( pt_on_plane_nei(1) - pt_on_plane(1) );
                        v(2) = (float)( pt_on_plane_nei(2) - pt_on_plane(2) );

                        //dist = v * norm ( scalar product )
                        float dist = ( dir_axis(0) * v(0) ) + ( dir_axis(1) * v(1) ) + ( dir_axis(2) * v(2) ) ;

                        v(0) = dist * dir_axis(0);
                        v(1) = dist * dir_axis(1);
                        v(2) = dist * dir_axis(2);

                        float axis_l = v.norm();

//                        axis_length(2) = axis_l ;
//                        axis_length(2) = 0.9 * axis_length(2);   // have to check this line

                    }
                    else if( second_axis == 1 ) // major
                    {
                        cube_axis.col(2) = eigen_vecs_nei.col(1);
                        axis_length(2) = cut_nei(1);

                        Eigen::Vector3f v;
                        Eigen::Vector3f dir_axis = eigen_vecs_nei.col(1); //minor

                        // v = point-orig
                        v(0) = (float)( pt_on_plane_nei(0) - pt_on_plane(0) );
                        v(1) = (float)( pt_on_plane_nei(1) - pt_on_plane(1) );
                        v(2) = (float)( pt_on_plane_nei(2) - pt_on_plane(2) );

                        //dist = v * norm ( scalar product )
                        float dist = ( dir_axis(0) * v(0) ) + ( dir_axis(1) * v(1) ) + ( dir_axis(2) * v(2) ) ;

                        v(0) = dist * dir_axis(0);
                        v(1) = dist * dir_axis(1);
                        v(2) = dist * dir_axis(2);

                        float axis_l = v.norm();

//                        axis_length(2) = axis_l ;
//                        axis_length(2) = 0.9 * axis_length(2);  // have to check this line
                    }


                    //check the axis length
                    if( fract_axis > 1.0 )
                        fract_axis = 1 / fract_axis;

                    //check direction with surface normal
                    Eigen::Vector3f other_axis = cube_axis.col(2);
                    bool isAligned = false;
                    {
                        float dir_check = other_axis(0) * re_normal(0) +
                                            other_axis(1) * re_normal(1) +
                                            other_axis(2) * re_normal(2);

                        if( dir_check < 0.0 )
                            isAligned = true;
                    }

                    //calculate the centriod of the box
                    if( isAligned )
                    {
                        mean_diag(0) = mean_diag(0) + other_axis(0) * axis_length(2);
                        mean_diag(1) = mean_diag(1) + other_axis(1) * axis_length(2);
                        mean_diag(2) = mean_diag(2) + other_axis(2) * axis_length(2);
                    }
                    else
                    {
                        mean_diag(0) = mean_diag(0) - other_axis(0) * axis_length(2);
                        mean_diag(1) = mean_diag(1) - other_axis(1) * axis_length(2);
                        mean_diag(2) = mean_diag(2) - other_axis(2) * axis_length(2);
                    }

                    int total_point = idx_seg.size() + idx.size();
                    if( fract_axis > FRACT_AXIS_CUTOFF )
                    {
                        Box_Param box_fit;
                        box_fit.mean_diag = mean_diag ;
                        box_fit.eigDx = cube_axis ;
                        box_fit.axis_length = axis_length ;
                        box_fit.segment_pair = segment_used;

                        boxes.push_back( box_fit );
                        noBoxfitted = false;

                        if( total_point > big_segment_final.first )
                        {
                            big_segment_final.first = total_point ;
                            big_segment_final.second = boxes.size() - 1 ;
                        }

                    }

                    else
                    {

                        //small seg
                        int axis_num_cmp = -1;
                        if( second_axis == 0 )
                            axis_num_cmp = 1 ;
                        else if ( second_axis == 1 )
                            axis_num_cmp = 0 ;

                        //big seg
                        int axis_num_big = -1;
                        if( first_axis == 0 )
                            axis_num_big = 1 ;
                        else if ( first_axis == 1 )
                            axis_num_big = 0 ;


                        if( big_segment.second == neigh[n] )
                        {
                            aPair[0] = neigh[n];
                            aPair[1] = segnum[cls_seg];
                        }
                        else if( big_segment.second == segnum[cls_seg] )
                        {
                            aPair[0] = segnum[cls_seg];
                            aPair[1] = neigh[n];
                        }

                        aPair[2] = cut_nei(axis_num_cmp)  ;  //axis_to_be_summed
                        aPair[3] = cut(axis_num_big);        //axis_to_be_compared
                        aPair[4] = (float) first_axis  ;
                        aPair[5] = (float) second_axis ;
                        aPair[6] = (float) total_point;

                        check_segment_aligned.push_back( aPair );

                        Box_Param box_fit;
                        box_fit.mean_diag = mean_diag ;
                        box_fit.eigDx = cube_axis ;
                        box_fit.axis_length = axis_length ;

                        boxes_temp.push_back( box_fit );

                    }


                }
                else
                {
#if(VISOPTION_SURFACE)
                        std::cout << match_value << " cannot find two axis that are parallel to each other ... "<< first_axis << " , " << second_axis << std::endl;
 #endif
                }
            }
            else
            {
#if(VISOPTION_SURFACE)
                    std::cout << " the two segments are not perpendicular to each other ... "<< dot_product<< std::endl;
#endif
            }

        }
        if ( ( noBoxfitted ) && ( check_segment_aligned.size() >= 2 ) )
        {
            float frac;
            Eigen::Vector3d normal_nei_1;
            Eigen::Vector3d normal_nei_2 ;
            float total_axis_length ;
            std::vector<float> aPair_i(7) ;

            //to track which segment to be fitted
            std::vector<int> toFit;
            int s_1, s_2 ;
            bool isBoxFitted = false;
            for( int r_chk = 0; r_chk < check_segment_aligned.size(); r_chk++ )
            {
                toFit.clear();
                toFit.push_back( r_chk );

                aPair = check_segment_aligned[ r_chk ] ;
                total_axis_length = aPair[2] ;
                float axis_to_be_matched = aPair[3];
                for( int r_chk_i = r_chk + 1; r_chk_i < check_segment_aligned.size(); r_chk_i++ )
                {
                   aPair_i = check_segment_aligned[ r_chk_i ];

                   //if the bigger segment is same
                   if( aPair[0] == aPair_i[0])
                   {
                        //check surface normal direction of both segment
                       s_1 = (int)aPair[1];
                       s_2 = (int)aPair_i[1];

                       normal_nei_1 = segRegion[s_1].getrepresentative_normal();
                       normal_nei_2 =  segRegion[s_2].getrepresentative_normal();

                       float d_prod = ( normal_nei_1(0) * normal_nei_2(0) ) +
                                      ( normal_nei_1(1) * normal_nei_2(1) ) +
                                      ( normal_nei_1(2) * normal_nei_2(2) );

                       if( d_prod > 0.6 )
                       {
                            total_axis_length = total_axis_length + aPair_i[2];
                            frac = axis_to_be_matched / total_axis_length ;

                            //add the segment
                            toFit.push_back( r_chk_i );

                            //check the axis length
                            if( frac > 1.0 )
                                frac = 1 / frac ;

                            if( frac > FRACT_AXIS_CUTOFF )
                            {
                                isBoxFitted = true;
                                break;
                            }
                            else
                            {
#if(VISOPTION_SURFACE)
                                    std::cout << " the axis length is still not the same "<< frac <<  std::endl;
#endif
                            }

                       }
                       else
                       {
#if(VISOPTION_SURFACE)
                               std::cout << " the surface nornal direction is not the same for both segments "<< d_prod << std::endl;
 #endif
                       }
                   }
                   else
                   {
#if(VISOPTION_SURFACE)
                           std::cout << " the bigger segments is not the same .. "<< std::endl;
#endif
                   }

                }
                break;

            }

            //set up the parameters of the box
            if( isBoxFitted )
            {

                for( int r = 0; r < 1; r++ )
                {
                    aPair = check_segment_aligned[ toFit[1] ] ;
                    Box_Param box_1 = boxes_temp[toFit[1]];

                    boxes.push_back( box_1 );

                    if( int (aPair[6]) > big_segment_final.first )
                    {
                        big_segment_final.first = aPair[6] ;
                        big_segment_final.second = boxes.size() - 1 ;
                    }
                }

            } //end of if


        }

    }

    std::vector<Box_Param> box_final(1);
    if( big_segment_final.second != -1 )
    {
        box_final[0] = boxes[big_segment_final.second];
    }
    else
        box_final.resize(0);

    return box_final;
}


// fit box final
std::vector<Box_Param> SurfacePatch::
fit_box( std::vector<PointCloudIndividual> &segRegion,
         std::vector<int> &segnum)
{

    float fract_axis_cutoff = FRACT_AXIS_CUTOFF;
    std::vector<std::vector<int> > seg_pair_checked(2);
    std::vector<Box_Param> boxes;
    std::vector<std::pair<float,int> > prob_size_box ;
    // finding maximum sized segment
    std::pair<int,int> big_segment_final;
    big_segment_final.first = 0;
    big_segment_final.second = -1;
    std::pair<int,int> segment_used ;
    float gmm_diff_cutoff = 0.2;


    //looping over segments
    for( int cls_seg = 0; cls_seg < segnum.size(); cls_seg++ )
    {
        PointCloudIndividual obj_processing = segRegion[segnum[cls_seg]];
        std::vector<int> idx_seg = obj_processing.getNNIndices();

        std::vector<int> neigh_reg = obj_processing.get_neighbour();
        std::vector<int> neigh ;
        //check which neighbours are in the same object
        for ( int n_count = 0; n_count < neigh_reg.size(); n_count++ )
        {
            for( int ii = 0; ii < segnum.size(); ii++ )
            {
                if( ( neigh_reg[n_count] == segnum[ii] ) ) // checking which neighbour is in the same object
                {
                    neigh.push_back( neigh_reg[n_count] );
                    break;
                }

            }
        }

#if(VISOPTION_SURFACE)
        std::cout << segnum[cls_seg] << " neighbours size = " << neigh.size() << " ind = " ;
        for( int ii = 0; ii < neigh.size(); ii++ )
        {
            std::cout << neigh[ii] << " , ";
        }
        std::cout << std::endl;
#endif

        std::vector<Box_Param> boxes_temp;
        std::vector<std::vector<float> > check_segment_aligned ;
        std::vector<float> aPair(8) ;
        bool noBoxfitted = true;
        for( int n = 0 ; n < neigh.size(); n++ )
        {
            std::vector<int> idx = segRegion[neigh[n]].getNNIndices();

            PointCloudIndividual obj, obj_nei;
            //check which segment is bigger
            std::pair<int,int> big_segment;      //  set the parameter to outer segment
            big_segment.first = idx_seg.size();
            big_segment.second = segnum[cls_seg];
            obj = segRegion[big_segment.second];
            obj_nei = segRegion[neigh[n]];
            segment_used.first =  segnum[cls_seg];
            segment_used.second = neigh[n];

            if( idx.size() > big_segment.first ) // changed the
            {
                big_segment.first = idx.size();
                big_segment.second = neigh[n];
                obj = segRegion[big_segment.second];
                obj_nei = segRegion[segnum[cls_seg]];
                segment_used.first =  neigh[n];
                segment_used.second = segnum[cls_seg];
            }

#if(VISOPTION_SURFACE)
         std::cout << " segment_used = " << segment_used.first  << "  " << segment_used.second << std::endl;
#endif


            Eigen::Vector3d re_normal = obj.getrepresentative_normal();
            Eigen::Vector3d pt_on_plane = obj.getrepresentative_position();
            Eigen::Vector3d cut = obj.getcut_axis();
            Eigen::Matrix3f eigen_vecs = obj.get_axis();
            Eigen::Vector3f major_axis = eigen_vecs.col(0);
            Eigen::Vector3f minor_axis = eigen_vecs.col(1);
            Eigen::Matrix3f cube_axis = obj.get_axis();
            Eigen::Vector3d axis_length = obj.getcut_axis();
            float gmm_score = obj.get_gmm_color_final();

            Eigen::Vector3d pt_on_plane_nei = obj_nei.getrepresentative_position();
            Eigen::Matrix3f eigen_vecs_nei = obj_nei.get_axis();
            Eigen::Vector3d cut_nei = obj_nei.getcut_axis();
            Eigen::Vector3f major_axis_nei = eigen_vecs_nei.col(0);
            Eigen::Vector3f minor_axis_nei = eigen_vecs_nei.col(1);
            float gmm_score_nei = obj_nei.get_gmm_color_final();

            Eigen::Vector3d normal_nei = obj_nei.getrepresentative_normal();

            double dot_product = re_normal(0) * normal_nei(0) + re_normal(1) * normal_nei(1) + re_normal(2) * normal_nei(2);

            //calculate centroid
            Eigen::Vector3f mean_diag ;
            mean_diag(0) = (float)pt_on_plane(0);
            mean_diag(1) = (float)pt_on_plane(1);
            mean_diag(2) = (float)pt_on_plane(2);

            if( abs( dot_product ) < PERPENDICULAR_CUTOFF )
            {
                int first_axis = -1;  // 0 - minor , 1 -major
                int second_axis = -1;  // 0 - minor , 1 -major
                double match_value = 0.6;
                float fract_axis ;

                //check on major axis
                double mm_product = major_axis(0) * major_axis_nei(0) +
                                    major_axis(1) * major_axis_nei(1) +
                                    major_axis(2) * major_axis_nei(2);

                if( abs(mm_product) > match_value )
                {
                     first_axis = 1;
                     second_axis = 1;
                     match_value = 0.7;
                     fract_axis = cut(0) / (float)cut_nei(0);
//                     axis_length(1) = ( cut(0) + cut_nei(0) ) / 2.0 ;
                }


                double mn_product = major_axis(0) * minor_axis_nei(0) +
                                    major_axis(1) * minor_axis_nei(1) +
                                    major_axis(2) * minor_axis_nei(2);

                if( abs(mn_product) > match_value )
                {
                    // match with major with minor
                    match_value = abs(mn_product);
                    first_axis = 1;
                    second_axis = 0;
                    fract_axis = cut(0) / (float)cut_nei(1);
//                    axis_length(1) = ( cut(0) + cut_nei(1) ) / 2.0 ;
                }

                //check on minor axis
                double nm_product = minor_axis(0) * major_axis_nei(0) +
                                    minor_axis(1) * major_axis_nei(1) +
                                    minor_axis(2) * major_axis_nei(2);

                if( abs(nm_product) > match_value )
                {
                    // match with minor with major
                    match_value = abs(nm_product);
                    first_axis = 0;
                    second_axis = 1;
                    fract_axis = cut(1) / (float)cut_nei(0);
//                    axis_length(1) = ( cut(1) + cut_nei(0) ) / 2.0 ;
                }

                double nn_product = minor_axis(0) * minor_axis_nei(0) +
                                    minor_axis(1) * minor_axis_nei(1) +
                                    minor_axis(2) * minor_axis_nei(2);

                if( abs(nn_product) > match_value )
                {
                    // match with minor with minor
                    match_value = abs(nn_product);
                    first_axis = 0;
                    second_axis = 0;
                    fract_axis = cut(1) / (float)cut_nei(1);
//                    axis_length(1) = ( cut(1) + cut_nei(1) ) / 2.0 ;

                }

                if( match_value > AXIS_MATCH_BOX )
                {

                    if( second_axis == 0 ) // match found with minor
                    {
                        cube_axis.col(2) = eigen_vecs_nei.col(0);  // push other axis as other direction of box
                        axis_length(2) = cut_nei(0);               // set other axis length as rest axis lnegth of box


                        Eigen::Vector3f v;
                        Eigen::Vector3f dir_axis = eigen_vecs_nei.col(0);

                        // v = point-orig
                        v(0) = (float)( pt_on_plane_nei(0) - pt_on_plane(0) );
                        v(1) = (float)( pt_on_plane_nei(1) - pt_on_plane(1) );
                        v(2) = (float)( pt_on_plane_nei(2) - pt_on_plane(2) );

                        //dist = v * norm ( scalar product )
                        float dist = ( dir_axis(0) * v(0) ) + ( dir_axis(1) * v(1) ) + ( dir_axis(2) * v(2) ) ;

                        v(0) = dist * dir_axis(0);
                        v(1) = dist * dir_axis(1);
                        v(2) = dist * dir_axis(2);

                        float axis_l = v.norm();

//                        axis_length(2) = axis_l ;
//                        axis_length(2) = 0.9 * axis_length(2);   // have to check this line

                    }
                    else if( second_axis == 1 ) // major
                    {
                        cube_axis.col(2) = eigen_vecs_nei.col(1);
                        axis_length(2) = cut_nei(1);

                        Eigen::Vector3f v;
                        Eigen::Vector3f dir_axis = eigen_vecs_nei.col(1); //minor

                        // v = point-orig
                        v(0) = (float)( pt_on_plane_nei(0) - pt_on_plane(0) );
                        v(1) = (float)( pt_on_plane_nei(1) - pt_on_plane(1) );
                        v(2) = (float)( pt_on_plane_nei(2) - pt_on_plane(2) );

                        //dist = v * norm ( scalar product )
                        float dist = ( dir_axis(0) * v(0) ) + ( dir_axis(1) * v(1) ) + ( dir_axis(2) * v(2) ) ;

                        v(0) = dist * dir_axis(0);
                        v(1) = dist * dir_axis(1);
                        v(2) = dist * dir_axis(2);

                        float axis_l = v.norm();

//                        axis_length(2) = axis_l ;
//                        axis_length(2) = 0.9 * axis_length(2);  // have to check this line
                    }


                    //check the axis length
                    if( fract_axis > 1.0 )
                        fract_axis = 1 / fract_axis;

                    //check direction with surface normal
                    Eigen::Vector3f other_axis = cube_axis.col(2);
                    bool isAligned = false;
                    {
                        float dir_check = other_axis(0) * re_normal(0) +
                                            other_axis(1) * re_normal(1) +
                                            other_axis(2) * re_normal(2);

                        if( dir_check < 0.0 )
                            isAligned = true;
                    }

                    //calculate the centriod of the box
                    if( isAligned )
                    {
                        mean_diag(0) = mean_diag(0) + other_axis(0) * axis_length(2);
                        mean_diag(1) = mean_diag(1) + other_axis(1) * axis_length(2);
                        mean_diag(2) = mean_diag(2) + other_axis(2) * axis_length(2);
                    }
                    else
                    {
                        mean_diag(0) = mean_diag(0) - other_axis(0) * axis_length(2);
                        mean_diag(1) = mean_diag(1) - other_axis(1) * axis_length(2);
                        mean_diag(2) = mean_diag(2) - other_axis(2) * axis_length(2);
                    }

#if(VISOPTION_SURFACE)
         std::cout << " frac = " << fract_axis  << " first_axis " << first_axis
                   << " second_axis " << second_axis << std::endl;
#endif
                    int total_point = idx_seg.size() + idx.size();
                    if( fract_axis > FRACT_AXIS_CUTOFF )
                    {
                        Box_Param box_fit;
                        box_fit.mean_diag = mean_diag ;
                        box_fit.eigDx = cube_axis ;
                        box_fit.axis_length = axis_length ;
                        box_fit.segment_pair = segment_used;

                        boxes.push_back( box_fit );
                        noBoxfitted = false;

                        if( total_point > big_segment_final.first )
                        {
                            big_segment_final.first = total_point ;
                            big_segment_final.second = boxes.size() - 1 ;
                        }

                        std::pair<float,int> pair_temp;
                        pair_temp.first = gmm_score + gmm_score_nei;
                        pair_temp.second = total_point;
                        prob_size_box.push_back( pair_temp );

#if(VISOPTION_SURFACE)
         std::cout << " one box fitted ..." << std::endl;
#endif

                    }

                    else
                    {

                        //small seg
                        int axis_num_cmp = -1;
                        if( second_axis == 0 )
                            axis_num_cmp = 1 ;
                        else if ( second_axis == 1 )
                            axis_num_cmp = 0 ;

                        //big seg
                        int axis_num_big = -1;
                        if( first_axis == 0 )
                            axis_num_big = 1 ;
                        else if ( first_axis == 1 )
                            axis_num_big = 0 ;


                        if( big_segment.second == neigh[n] )
                        {
                            aPair[0] = neigh[n];
                            aPair[1] = segnum[cls_seg];
                        }
                        else if( big_segment.second == segnum[cls_seg] )
                        {
                            aPair[0] = segnum[cls_seg];
                            aPair[1] = neigh[n];
                        }

                        aPair[2] = cut_nei(axis_num_cmp)  ;  //axis_to_be_summed
                        aPair[3] = cut(axis_num_big);        //axis_to_be_compared
                        aPair[4] = (float) first_axis  ;
                        aPair[5] = (float) second_axis ;
                        aPair[6] = (float) total_point;
                        aPair[7] = gmm_score + gmm_score_nei;

                        check_segment_aligned.push_back( aPair );

                        Box_Param box_fit;
                        box_fit.mean_diag = mean_diag ;
                        box_fit.eigDx = cube_axis ;
                        box_fit.axis_length = axis_length ;
                        box_fit.segment_pair = segment_used;

                        boxes_temp.push_back( box_fit );


                    }


                }
                else
                {
#if(VISOPTION_SURFACE)
                        std::cout << match_value << " cannot find two axis that are parallel to each other ... "<< first_axis << " , " << second_axis << std::endl;
 #endif
                }
            }
            else
            {
#if(VISOPTION_SURFACE)
                    std::cout << " the two segments are not perpendicular to each other ... "<< dot_product<< std::endl;
#endif
            }

        }
        if ( ( noBoxfitted ) && ( check_segment_aligned.size() >= 2 ) )
        {
            float frac;
            Eigen::Vector3d normal_nei_1;
            Eigen::Vector3d normal_nei_2 ;
            float total_axis_length ;
            std::vector<float> aPair_i(7) ;

            //to track which segment to be fitted
            std::vector<int> toFit;
            int s_1, s_2 ;
            bool isBoxFitted = false;
            for( int r_chk = 0; r_chk < check_segment_aligned.size(); r_chk++ )
            {
                toFit.clear();
                toFit.push_back( r_chk );

                aPair = check_segment_aligned[ r_chk ] ;
                total_axis_length = aPair[2] ;
                float axis_to_be_matched = aPair[3];
                for( int r_chk_i = r_chk + 1; r_chk_i < check_segment_aligned.size(); r_chk_i++ )
                {
                   aPair_i = check_segment_aligned[ r_chk_i ];

                   //if the bigger segment is same
                   if( aPair[0] == aPair_i[0])
                   {
                        //check surface normal direction of both segment
                       s_1 = (int)aPair[1];
                       s_2 = (int)aPair_i[1];

                       normal_nei_1 = segRegion[s_1].getrepresentative_normal();
                       normal_nei_2 =  segRegion[s_2].getrepresentative_normal();

                       float d_prod = ( normal_nei_1(0) * normal_nei_2(0) ) +
                                      ( normal_nei_1(1) * normal_nei_2(1) ) +
                                      ( normal_nei_1(2) * normal_nei_2(2) );

                       if( d_prod > 0.6 )
                       {
                            total_axis_length = total_axis_length + aPair_i[2];
                            frac = axis_to_be_matched / total_axis_length ;

                            //add the segment
                            toFit.push_back( r_chk_i );

                            //check the axis length
                            if( frac > 1.0 )
                                frac = 1 / frac ;

                            if( frac > FRACT_AXIS_CUTOFF )
                            {
                                isBoxFitted = true;
                                break;
                            }
                            else
                            {
#if(VISOPTION_SURFACE)
                                    std::cout << " the axis length is still not the same "<< frac <<  std::endl;
#endif
                            }

                       }
                       else
                       {
#if(VISOPTION_SURFACE)
                               std::cout << " the surface nornal direction is not the same for both segments "<< d_prod << std::endl;
 #endif
                       }
                   }
                   else
                   {
#if(VISOPTION_SURFACE)
                           std::cout << " the bigger segments is not the same .. "<< std::endl;
#endif
                   }

                }
                if( isBoxFitted)   /* change by olyvia */
                {
                    std::cout << " inside extened box fit ... found " << r_chk << std::endl;
                    break;
                }

            }

            //set up the parameters of the box
            if( isBoxFitted )
            {

                for( int r = 0; r < 1; r++ )
                {
                    aPair = check_segment_aligned[ toFit[1] ] ;
                    Box_Param box_1 = boxes_temp[toFit[1]];

                    boxes.push_back( box_1 );

                    std::pair<float,int> pair_temp;
                    pair_temp.first = aPair[7];
                    pair_temp.second = aPair[6];
                    prob_size_box.push_back( pair_temp );

                    if( int (aPair[6]) > big_segment_final.first )
                    {
                        big_segment_final.first = aPair[6] ;
                        big_segment_final.second = boxes.size() - 1 ;
                    }
                }

            } //end of if


        }

    }


    // boxes
    std::vector<std::pair<float,int> > pair_temp_gmm;
    std::vector<std::pair<float,int> > pair_temp_size;
    for( int b_count = 0; b_count < prob_size_box.size(); b_count++ )
    {
        std::pair<float,int> pair_temp_1;
        std::pair<float,int> pair_temp = prob_size_box[b_count];

        pair_temp_1.first = pair_temp.first; // gmm
        pair_temp_1.second = b_count;
        pair_temp_gmm.push_back( pair_temp_1 );

        pair_temp_1.first = (float)pair_temp.second; // size
        pair_temp_1.second = b_count;
        pair_temp_size.push_back( pair_temp_1 );

#if(VISOPTION_SURFACE)
        Box_Param box_temp = boxes[b_count];
        std::cout << " pair_temp prob = " << pair_temp.first << " size " << pair_temp.second << std::endl;
        std::cout << " box segment " << box_temp.segment_pair.first << " second " << box_temp.segment_pair.second << std::endl;
#endif

    }

    std::sort( pair_temp_gmm.begin(), pair_temp_gmm.end() );
    std::sort( pair_temp_size.begin(), pair_temp_size.end() );

#if(VISOPTION_SURFACE)
    for( int b_count = pair_temp_gmm.size()-1; b_count >= 0 ; b_count-- )
    {
        std::pair<float,int> pair_temp_1 = pair_temp_size[b_count];
        std::pair<float,int> pair_temp = pair_temp_gmm[b_count];

//        Box_Param box_temp = boxes[b_count];
        std::cout << " pair_temp prob = " << pair_temp.first << " size " << pair_temp.second << std::endl;
        std::cout << " box segment " << pair_temp_1.first << " second " << pair_temp_1.second << std::endl;
    }
#endif


    //--------------------------------------------------------
    big_segment_final.second = -1;
    if( pair_temp_size.size() > 0 )
    {
        int loop_count_color = pair_temp_gmm.size() - 1 ;
        int loop_count_size = pair_temp_size.size() - 1 ;
        std::pair<float,int> pair_temp_s = pair_temp_size[loop_count_size];
        std::pair<float,int> pair_temp_c = pair_temp_gmm[loop_count_color];
        std::pair<float,int> pair_temp_inner_loop;

        if( pair_temp_s.second == pair_temp_c.second )
        {
            std::cout << " biggest segment has the highest prob.. Ideal Case. " << std::endl;
            big_segment_final.second = pair_temp_s.second;
        }

        if( big_segment_final.second == -1 )
        {
            float color_prob_highest = pair_temp_gmm[pair_temp_gmm.size() - 1].first;
            do
            {
                pair_temp_s = pair_temp_size[loop_count_size];
               // std::cout << " current processing segment " << pair_temp_2.second << " size = " << segRegion[pair_temp_2.second].getNNIndices().size() << std::endl;

                // looping through curvature value
                for( int inner_loop = pair_temp_gmm.size() -1 ; inner_loop >= 0 ; inner_loop-- )
                {
                    pair_temp_inner_loop = pair_temp_gmm[inner_loop];

                    //std::cout << loop_count_color << "  " << pair_temp_inner_loop.second << " curv " << pair_temp_inner_loop.first <<
                    //" " << segRegion[pair_temp_2.second].getNNIndices().size() << std::endl;

                    if( abs( color_prob_highest - pair_temp_inner_loop.first ) < gmm_diff_cutoff )
                    {
                        if( pair_temp_inner_loop.second == pair_temp_s.second )
                        {
                             big_segment_final.second = pair_temp_s.second ;
                             break;
                        }
                    }

                  }

                if( big_segment_final.second != -1 )
                    break;

                loop_count_size--;

            }while( loop_count_size >= 0 );
        }

        //try with second gmm score
        if( (big_segment_final.second == -1) && ( pair_temp_gmm.size()> 2 ) )
        {
            float color_prob_highest = pair_temp_gmm[pair_temp_gmm.size() - 3].first;
            loop_count_size = pair_temp_size.size() - 1;
            do
            {
                pair_temp_s = pair_temp_size[loop_count_size];
                // looping through curvature value
                for( int inner_loop = pair_temp_gmm.size() -1 ; inner_loop >= 0 ; inner_loop-- )
                {
                    pair_temp_inner_loop = pair_temp_gmm[inner_loop];

                    if( abs( color_prob_highest - pair_temp_inner_loop.first ) < gmm_diff_cutoff )
                    {
                        if( pair_temp_inner_loop.second == pair_temp_s.second )
                        {
                             big_segment_final.second = pair_temp_s.second ;
                             break;
                        }
                    }

                  }

                if( big_segment_final.second != -1 )
                    break;

                loop_count_size--;

            }while( loop_count_size >= 0 );
        }

#if(VISOPTION_SURFACE)
        std::cout << " segemnt number on which decision to be made --- " << big_segment_final.second << std::endl;
#endif

    }

    //---------------------------------------------------------


    std::vector<Box_Param> box_final(1);
    if( big_segment_final.second != -1 )
    {
        box_final[0] = boxes[big_segment_final.second];
    }
    else
        box_final.resize(0);

    return box_final;
}




std::vector<CylindricalShell> SurfacePatch::
grasp_localization_box( const std::vector<PointCloudIndividual> &segRegion,
                        const PointCloudXYZ::Ptr &ptCloud,
                        const PointCloudRGB::Ptr &ptCloud_RGB,
                        Box_Param box_all )
{
    std::vector<CylindricalShell> box_grasp;
    //check inputs
    if( ( ptCloud->points.size() == 0 ) || (segRegion.size() == 0) )
    {
        std::cout << " in function grasp_localization_box (ptCloud->points.size() == 0 ) || (segRegion.size() == 0).." << std::endl;
        box_grasp.resize(0);
        return box_grasp;
    }


    Eigen::Vector3f axis;
    Eigen::Matrix3f three_axis;
    pcl::PointXYZ center1;
    std::pair<int,int> segment_pair;
    const int points_in_gap_allowed = this->points_in_gap_allowed;
    int num_smallest = points_in_gap_allowed;
    const int point_neg = this->point_neg ; // 20;
    const int num_sample = 30;
    const double handle_gap = this->handle_gap;
    float finger_width = this->finger_width;  // original + error_to_be allowed
    const float finger_length_min = 0.015 ;
    const float finger_length_max = FINGER_LENGTH_MAX;
    const float dist_from_center_extended = 0.05;
    const float dot_with_normal_extended = 0.9;


    //fitted box parameters
    Box_Param box = box_all;
    three_axis = box.eigDx;
    segment_pair = box.segment_pair;
    std::vector<int> seg(2);
    seg[0] = segment_pair.first;
    seg[1] = segment_pair.second;
    Eigen::Vector3d length = box.axis_length;
    Eigen::Vector3d curv_axis, center, center_box, normal_dir, rest_dir ;
    Eigen::Vector3d dot_product_vector;

        //set parameter for shell
        CylindricalShell shell, final_shell;
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
        tree->setInputCloud(ptCloud);
        std::vector<int> nn_indices;
        std::vector<float> nn_dists;

        for( int w = 0 ; w < 2; w++ )
        {

            #if(VISOPTION_SURFACE)
            std::cout << " processing segment " << seg[w] << std::endl;
            #endif

              int processing_seg_num =  seg[w];
               center_box = segRegion[processing_seg_num].getrepresentative_position();
               normal_dir = segRegion[processing_seg_num].getrepresentative_normal();
               Eigen::Vector3d axis_length_1 =  segRegion[processing_seg_num].getcut_axis();
               Eigen::Matrix3f axis_seg = segRegion[processing_seg_num].get_axis() ;

               int other_dir = 0;
               bool is_handle_found = false;
               for( int l = 1; l >= 0; l-- )
               {

                   const double axis_length = axis_length_1(other_dir); // major axis dir / length
                   axis = axis_seg.col( l ) ; //set current axis
                   const float rad = (float)axis_length_1(l);  // minor axis dir / length

                   curv_axis(0) = (double)axis(0);
                   curv_axis(1) = (double)axis(1);
                   curv_axis(2) = (double)axis(2);
                   rest_dir = normal_dir.cross(curv_axis);

                    double maxHandAperture = this->maxHandAperture; //rad + 0.015;
                   if( this->maxHandAperture > (rad+0.02) )
                       maxHandAperture = rad + 0.02 ;

                   double outer_sample_radius = ( maxHandAperture + handle_gap ) * ( maxHandAperture + handle_gap) + ( finger_length_max * finger_length_max ) ;
                   outer_sample_radius = sqrt(outer_sample_radius);

   #if(VISOPTION_SURFACE)
                   std::cout << " current axis = " << l << " axis_length = " << other_dir << std::endl;
                   std::cout << " axis_length " << axis_length << " finger_width " << finger_width << " diff " << (axis_length - finger_width/2) <<  std::endl;
   #endif
                   for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                   {

                       center(0) = center_box(0) - ( rest_dir(0) * g);
                       center(1) = center_box(1) - ( rest_dir(1) * g);
                       center(2) = center_box(2) - ( rest_dir(2) * g);

                       center1.x = center(0) ;
                       center1.y = center(1) ;
                       center1.z = center(2) ;

       #if(VISOPTION_SURFACE)
                       std::cout << " center = " << center << std::endl;
       #endif
                       shell.setRadius( (double)rad );
                       shell.setCentroid( center );  //set center
                       shell.setCurvatureAxis( curv_axis );
                       shell.setExtent( finger_width );
                       shell.setNormal( normal_dir );

                       tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );


                           int no_point_surface = 0;
                           std::vector<std::pair<float,int> > dist_from_center_right_dir;
                           std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                           Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                           pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                           pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                           pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                           pcl::PointXYZRGB pt, pt2;

                           for( int u = 0; u < nn_indices.size(); u++ )
                           {
                                    PointXYZ pt_xyz;
                                    pt_xyz = ptCloud->points[nn_indices[u]];
       #if(VISOPTION_SURFACE)
                                    pt.r = 255;
                                    pt.g = 255;
                                    pt.b = 0;
                                    pt.x = pt_xyz.x;
                                    pt.y = pt_xyz.y;
                                    pt.z = pt_xyz.z;
                                    grasp_cloud->points.push_back(pt);
       #endif

                                    Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                    v = cropped - center;
                                    double along_rest_axis_Dist = rest_dir.dot(v);

                                    if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                    {
                                        //project points on a plane with normal along rest dir
                                        temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

       #if(VISOPTION_SURFACE)
                                        pt.r = 255;
                                        pt.g = 0;
                                        pt.b = 255;
                                        grasp_cloud->points.push_back(pt);

                                        pt2.x = temp_restDir(0);
                                        pt2.y = temp_restDir(1);
                                        pt2.z = temp_restDir(2);
       #endif


                                        // project on the plane parallel to normal _dir
                                        v = temp_restDir - center;
                                        double along_axis_Dist = curv_axis.dot(v);
                                        temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                        //calculate the distance of the projected points
                                        temp_axisDir = temp_axisDir - center;

                                        if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                        {
#if(VISOPTION_SURFACE)
                                            pt.r = 0;
                                            pt.g = 255;
                                            pt.b = 255;
                                            grasp_cloud->points.push_back(pt);
#endif
                                            if( (temp_axisDir.norm() > 0.007) )
                                            {
                                               dot_product_vector = temp_axisDir;
                                               dot_product_vector.normalize();
                                               along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                               if( along_axis_Dist > 0.7)
                                               {
                                                   std::pair<float,int> temp_dist;
                                                   temp_dist.first = temp_axisDir.norm();
                                                   temp_dist.second = nn_indices[u];
                                                   dist_from_center_wrong_dir.push_back( temp_dist );
       #if(VISOPTION_SURFACE)
                                                   // points with direction alond surface normal direction
                                                   pt2.r = 0;
                                                   pt2.g = 255;
                                                   pt2.b = 0;
                                                  cloudVis->points.push_back(pt2);
       #endif
                                               }
                                               else if ( along_axis_Dist < -0.7)
                                               {
       //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                   //project on the plane along axis direction
                                                   v = temp_restDir - center;
                                                   along_axis_Dist = normal_dir.dot(v);
                                                   temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
#if(VISOPTION_SURFACE)
                                                   pt2.r = 0;
                                                   pt2.g = 0;
                                                   pt2.b = 255;
                                                  cloudVis->points.push_back(pt2);

                                                   pt2.r = 200;
                                                   pt2.g = 100;
                                                   pt2.b = 0;
                                                   pt2.x = temp_normalDir(0);
                                                   pt2.y = temp_normalDir(1);
                                                   pt2.z = temp_normalDir(2);
                                                   cloudVis2->points.push_back(pt2);
#endif
                                                   temp_normalDir = temp_normalDir - center;

                                                   std::pair<float,int> temp_dist;
                                                   temp_dist.first = temp_normalDir.norm();
                                                   temp_dist.second = nn_indices[u];
                                                   dist_from_center_right_dir.push_back(temp_dist);

                                               }
                                            }
                                            else
                                            {
                                                no_point_surface ++;

                                                v = temp_restDir - center;
                                                along_axis_Dist = normal_dir.dot(v);
                                                temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

//                                                pt2.r = 0;
//                                                pt2.g = 255;
//                                                pt2.b = 0;
//                                                pt2.x = temp_normalDir(0);
//                                                pt2.y = temp_normalDir(1);
//                                                pt2.z = temp_normalDir(2);
//                                                cloudVis2->points.push_back(pt2);

                                                temp_normalDir = temp_normalDir - center;
                                                std::pair<float,int> temp_dist;
                                                temp_dist.first = temp_normalDir.norm();
                                                temp_dist.second = nn_indices[u];
                                                dist_from_center_right_dir.push_back( temp_dist );
       #if(VISOPTION_SURFACE)
                                                pt2.r = 255;
                                                pt2.g = 0;
                                                pt2.b = 0;
                                                cloudVis->points.push_back(pt2);
       #endif
                                            }

                                        }

                                    }

                           }


                           if(  no_point_surface > num_sample  )
                           {

                               if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                               {
                                    if( dist_from_center_right_dir.size() > num_sample )
                                    {
                                        std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );

                                        //check how many number of point is there in the gap and set shell radius to be r
                                        int startPos = -1, prev_start = 0;
                                        int num_of_points_in_gap_positive_points = 0;
                                        int num_of_points_in_gap_negative_points = 0;
//                                        bool isHandleFound = false;
                                        for( float r = rad - 0.01 ; r <= maxHandAperture; r += 0.001 )
                                        {
                                            num_of_points_in_gap_positive_points = 0;
                                            num_of_points_in_gap_negative_points = 0;
                                            startPos = -1;
                                            for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                             {

                                                 if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                 {
                                                     if( startPos == -1 )
                                                         startPos = c_loop;

                                                     if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                        num_of_points_in_gap_negative_points++;
                                                     else
                                                         num_of_points_in_gap_positive_points++;

                                                 }

                                             }

#if(VISOPTION_SURFACE)
                                                std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                                std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
#endif
                                                if( startPos != -1)
                                                  prev_start = startPos ;

                                                // break from the loop is the handle contains negative points
                                                if( num_of_points_in_gap_negative_points > point_neg )
                                                {
                                                    std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                   break;
                                                }

                                            if(  (num_of_points_in_gap_positive_points > num_smallest) || (num_of_points_in_gap_negative_points > num_smallest) )
                                                continue;

                                             if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                             {
                                                 shell.setRadius(r);
                                                 final_shell = shell;
                                                 is_handle_found = true ;
                                                 break;
                                             }

                                             //break from the loop if there is no positive points in gap
                                             if( num_of_points_in_gap_positive_points == 0 )
                                                break;


                                         }


                                    }
                                    else
                                    {
                       #if(VISOPTION_SURFACE)
                                   std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                       #endif

                                    }

                                }
                               else
                               {

                       #if(VISOPTION_SURFACE)
                                   std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                       #endif
                               }
                           }
                           else
                           {
                   #if(VISOPTION_SURFACE)
                               std::cout << " not enough samples to be a handle... " << std::endl;
                   #endif
                           }

//                           if( is_handle_found)
                           {
       #if(VISOPTION_SURFACE)
                               std::cout << " no_point_surface " << no_point_surface << std::endl;
                               std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                               std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                               Eigen::Vector3d center_vis = final_shell.getCentroid();
                               Eigen::Vector3d normal_vis = final_shell.getNormal();
                               pcl::PointXYZ p3,p4;
                               p3.x = center_vis(0);
                               p3.y = center_vis(1);
                               p3.z = center_vis(2);

                               p4.x = center_vis(0) + normal_vis(0) * 0.05;
                               p4.y = center_vis(1) + normal_vis(1) * 0.05;
                               p4.z = center_vis(2) + normal_vis(2) * 0.05;

                               pcl::visualization::PCLVisualizer viewer("handle");
                               viewer.addPointCloud(ptCloud_RGB);
                               viewer.addPointCloud(grasp_cloud, "c2");
                               viewer.addLine(p3, p4, 0, 255, 0);
                                   viewer.addPointCloud(cloudVis, "c1");
                                   viewer.addPointCloud(cloudVis2, "c3");
                               std::cout << " close the window cloud_viewer to continue......." << std::endl;
                               while(!viewer.wasStopped())
                                   viewer.spinOnce();

       #endif
                                break;
                           }

                   }

                   if ( !is_handle_found )
                   {
#if(VISOPTION_SURFACE)
                       std::cout << " checking in the opposite direction ..." << std::endl;
#endif
                       for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                       {

                           center(0) = center_box(0) + ( rest_dir(0) * g);
                           center(1) = center_box(1) + ( rest_dir(1) * g);
                           center(2) = center_box(2) + ( rest_dir(2) * g);

                           center1.x = center(0) ;
                           center1.y = center(1) ;
                           center1.z = center(2) ;
           #if(VISOPTION_SURFACE)
                           std::cout << " center = " << center << std::endl;
           #endif
                           shell.setRadius( (double)rad );
                           shell.setCentroid( center );  //set center
                           shell.setCurvatureAxis( curv_axis );
                           shell.setExtent( finger_width );
                           shell.setNormal( normal_dir );

                           tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );
//                           int num_smallest = 20;

                               int no_point_surface = 0;
                               std::vector<std::pair<float,int> > dist_from_center_right_dir;
                               std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                               Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud_1( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud_2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointXYZRGB pt, pt2;

                               for( int u = 0; u < nn_indices.size(); u++ )
                               {
                                        PointXYZ pt_xyz;
                                        pt_xyz = ptCloud->points[nn_indices[u]];
           #if(VISOPTION_SURFACE)
                                        pt.r = 255;
                                        pt.g = 255;
                                        pt.b = 0;
                                        pt.x = pt_xyz.x;
                                        pt.y = pt_xyz.y;
                                        pt.z = pt_xyz.z;
                                        grasp_cloud->points.push_back(pt);
           #endif

                                        Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                        v = cropped - center;
                                        double along_rest_axis_Dist = rest_dir.dot(v);

                                        if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                        {
                                            //project points on a plane with normal along rest dir
                                            temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

           #if(VISOPTION_SURFACE)
                                            pt.r = 255;
                                            pt.g = 0;
                                            pt.b = 255;
                                            grasp_cloud_1->points.push_back(pt);

                                            pt2.x = temp_restDir(0);
                                            pt2.y = temp_restDir(1);
                                            pt2.z = temp_restDir(2);
           #endif


                                            // project on the plane parallel to normal _dir
                                            v = temp_restDir - center;
                                            double along_axis_Dist = curv_axis.dot(v);
                                            temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                            //calculate the distance of the projected points
                                            temp_axisDir = temp_axisDir - center;

                                            if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                            {
    #if(VISOPTION_SURFACE)
                                                pt.r = 0;
                                                pt.g = 255;
                                                pt.b = 255;
                                                grasp_cloud->points.push_back(pt);
    #endif
                                                if( (temp_axisDir.norm() > 0.007) )
                                                {
                                                   dot_product_vector = temp_axisDir;
                                                   dot_product_vector.normalize();
                                                   along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                                   if( along_axis_Dist > 0.7)
                                                   {
                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_axisDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_wrong_dir.push_back( temp_dist );
           #if(VISOPTION_SURFACE)
                                                       // points with direction alond surface normal direction
                                                       pt2.r = 0;
                                                       pt2.g = 255;
                                                       pt2.b = 0;
                                                      cloudVis->points.push_back(pt2);
           #endif
                                                   }
                                                   else if ( along_axis_Dist < -0.7)
                                                   {
           //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                       //project on the plane along axis direction
                                                       v = temp_restDir - center;
                                                       along_axis_Dist = normal_dir.dot(v);
                                                       temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
    #if(VISOPTION_SURFACE)
                                                       pt2.r = 0;
                                                       pt2.g = 0;
                                                       pt2.b = 255;
                                                      cloudVis->points.push_back(pt2);

                                                       pt2.r = 200;
                                                       pt2.g = 100;
                                                       pt2.b = 0;
                                                       pt2.x = temp_normalDir(0);
                                                       pt2.y = temp_normalDir(1);
                                                       pt2.z = temp_normalDir(2);
                                                       cloudVis2->points.push_back(pt2);
    #endif
                                                       temp_normalDir = temp_normalDir - center;

                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_normalDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_right_dir.push_back(temp_dist);

                                                   }
                                                }
                                                else
                                                {
                                                    no_point_surface ++;

                                                    v = temp_restDir - center;
                                                    along_axis_Dist = normal_dir.dot(v);
                                                    temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

    //                                                pt2.r = 0;
    //                                                pt2.g = 255;
    //                                                pt2.b = 0;
    //                                                pt2.x = temp_normalDir(0);
    //                                                pt2.y = temp_normalDir(1);
    //                                                pt2.z = temp_normalDir(2);
    //                                                cloudVis2->points.push_back(pt2);

                                                    temp_normalDir = temp_normalDir - center;
                                                    std::pair<float,int> temp_dist;
                                                    temp_dist.first = temp_normalDir.norm();
                                                    temp_dist.second = nn_indices[u];
                                                    dist_from_center_right_dir.push_back( temp_dist );
           #if(VISOPTION_SURFACE)
                                                    pt2.r = 255;
                                                    pt2.g = 0;
                                                    pt2.b = 0;
                                                    cloudVis->points.push_back(pt2);
           #endif
                                                }

                                            }

                                        }

                               }


                               if(  no_point_surface > num_sample  )
                               {

                                   if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                                   {
                                        if( dist_from_center_right_dir.size() > num_sample )
                                        {
                                            std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );

                                            //check how many number of point is there in the gap and set shell radius to be r
                                            int startPos = -1, prev_start = 0;
                                            int num_of_points_in_gap_positive_points = 0;
                                            int num_of_points_in_gap_negative_points = 0;
    //                                        bool isHandleFound = false;
                                            for( float r = rad - 0.01; r <= maxHandAperture; r += 0.001 )
                                            {
                                                num_of_points_in_gap_positive_points = 0;
                                                num_of_points_in_gap_negative_points = 0;
                                                startPos = -1;
                                                for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                                 {

                                                     if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                     {
                                                         if( startPos == -1 )
                                                             startPos = c_loop;

                                                         if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                            num_of_points_in_gap_negative_points++;
                                                         else
                                                             num_of_points_in_gap_positive_points++;

                                                     }

                                                 }
#if(VISOPTION_SURFACE)
                                                std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                                std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
#endif
                                                if( startPos != -1)
                                                  prev_start = startPos ;

                                                // break from the loop is the handle contains negative points
                                                if( num_of_points_in_gap_negative_points > point_neg )
                                                {
                                                    std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                   break;
                                                }

                                               if(  (num_of_points_in_gap_positive_points > num_smallest) || (num_of_points_in_gap_negative_points > num_smallest) )
                                                   continue;

                                                if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                                {
                                                    shell.setRadius(r);
                                                    final_shell = shell;
                                                    is_handle_found = true ;
                                                    break;
                                                }

                                                //break from the loop if there is no positive points in gap
                                                if( num_of_points_in_gap_positive_points == 0 )
                                                   break;

                                             }


                                        }
                                        else
                                        {
                           #if(VISOPTION_SURFACE)
                                       std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                           #endif

                                        }

                                    }
                                   else
                                   {

                           #if(VISOPTION_SURFACE)
                                       std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                           #endif
                                   }
                               }
                               else
                               {
                       #if(VISOPTION_SURFACE)
                                   std::cout << " not enough samples to be a handle... " << std::endl;
                       #endif
                               }

//                               if( is_handle_found)
                               {
           #if(VISOPTION_SURFACE)
                                   std::cout << " no_point_surface " << no_point_surface << std::endl;
                                   std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                   std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                   Eigen::Vector3d center_vis = final_shell.getCentroid();
                                   Eigen::Vector3d normal_vis = final_shell.getNormal();
                                   pcl::PointXYZ p3,p4;
                                   p3.x = center_vis(0);
                                   p3.y = center_vis(1);
                                   p3.z = center_vis(2);

                                   p4.x = center_vis(0) + normal_vis(0) * 0.05;
                                   p4.y = center_vis(1) + normal_vis(1) * 0.05;
                                   p4.z = center_vis(2) + normal_vis(2) * 0.05;

                                   pcl::visualization::PCLVisualizer viewer("handle");
                                   viewer.addPointCloud(ptCloud_RGB);
                                   viewer.addPointCloud(grasp_cloud, "c2");
                                   viewer.addLine(p3, p4, 0, 255, 0);
                                       viewer.addPointCloud(cloudVis, "c1");
                                       viewer.addPointCloud(cloudVis2, "c3");
                                   std::cout << " close the window cloud_viewer to continue......." << std::endl;
                                   while(!viewer.wasStopped())
                                       viewer.spinOnce();

           #endif
                                    break;
                               }
                       }


                   }


                   //increment
                   other_dir++;

                   //save handle
                   if( is_handle_found )
                   {
#if(VISOPTION_SURFACE)
                       std::cout << " handle found .. " << std::endl;
#endif
                       box_grasp.push_back(final_shell);
                       break;
                   }
               }

       }

           std::cout << " number of grasp region found in the segment on which box is fitted.. " << box_grasp.size() << std::endl;
           std::cout << std::endl << " checking in  other segments" << std::endl;


        //find out which segment remains
        std::vector<int> seg_remain;
        for( int s_count = 0; s_count < segRegion.size(); s_count++ )
        {
            if( ( s_count == segment_pair.first ) || ( s_count == segment_pair.second ) )
            {
//                std::cout << " segment already tested ... " << std::endl ;
            }
            else
            {
                if( segRegion[s_count].getNNIndices().size() > 100 )
                    seg_remain.push_back(s_count);
            }
        }

#if(VISOPTION_SURFACE)
        for( int r = 0 ; r < seg_remain.size(); r++ )
            std::cout << r << " , " << seg_remain[r] << std::endl;
        std::cout << " pair = " << segment_pair.first << " , " << segment_pair.second ;
        std::cout << " segment size = " << seg_remain.size() << std::endl;
        std::cout << " printing the six centers of the fitted box" << std::endl;
#endif

        Eigen::Vector3f axis_box, center_box_1;
        Eigen::Vector3d center_face;
        pcl::PointXYZ p1, p2;
        center_box_1 = box.mean_diag;

        int f_count = 0;

        p1.x = center_box_1(0);
        p1.y = center_box_1(1);
        p1.z = center_box_1(2);

        int processing_seg_num = -1;
        for( int r = 0; r < seg_remain.size(); r++  )
        {

            int s = seg_remain[r];
            Eigen::Vector3d center_seg = segRegion[s].getrepresentative_position();
            Eigen::Vector3d normal_seg = segRegion[s].getrepresentative_normal();
            bool is_belong_box = false;

            if( normal_seg(2) < -this->thr_direction ) /*  changes made by me   */
            {

    #if(VISOPTION_SURFACE)
                stringstream ss;
                ss << s;
                string file_num = ss.str();
                std::cout << " processing_seg_num = " << s << std::endl;
                std::cout << r << " seg_remain " << seg_remain[r] << std::endl;
                std::cout << " normal_seg = " << normal_seg << std::endl;
                pcl::visualization::PCLVisualizer viewer("center_position");
                viewer.addPointCloud(ptCloud);
                viewer.addSphere(p1, 0.005, 0, 0, 1, file_num) ;
    #endif


                {
                    Box_Param box_fit = box;
                    //fitting a box
                    Eigen::Matrix3f eigDx = box_fit.eigDx;
                    eigDx.col(2) = eigDx.col(0).cross(eigDx.col(1));
                    // final transform
                    const Eigen::Quaternionf qfinal(eigDx);
                    const Eigen::Vector3f tfinal = box_fit.mean_diag;
                    Eigen::Vector3d axis_length = box_fit.axis_length ;
    //                viewer.addCube( tfinal, qfinal, 2*axis_length(0), 2*axis_length(1), 2*axis_length(2)  ); //minor, major, normal
                }


                Eigen::Vector3d axis_length_1 =  segRegion[processing_seg_num].getcut_axis();
                Eigen::Matrix3f axis_seg = segRegion[processing_seg_num].get_axis() ;

                for( int a_count = 0; a_count < 3; a_count++)
                {
                    axis_box = three_axis.col(a_count);
                    center_face(0) = center_box_1(0) - length(a_count) * axis_box(0);
                    center_face(1) = center_box_1(1) - length(a_count) * axis_box(1);
                    center_face(2) = center_box_1(2) - length(a_count) * axis_box(2);

                    p2.x = center_face(0);
                    p2.y = center_face(1);
                    p2.z = center_face(2);

                    center_face = center_seg - center_face;
                    float dist = axis_box(0) * normal_seg(0) + axis_box(1) * normal_seg(1) + axis_box(2) * normal_seg(2) ;

    #if(VISOPTION_SURFACE)
                    std::cout << " dist1  = " << center_face.norm() << std::endl;
                    std::cout << " dot product = " << -dist << std::endl;
                    viewer.addSphere(p2, 0.005, 1, 0, 0, file_num) ;

                    ss << f_count;
                    file_num = ss.str();
                    f_count++;
    #endif


                    if( ( center_face.norm() < dist_from_center_extended ) && ( -dist > dot_with_normal_extended ) )
                    {
    #if(VISOPTION_SURFACE)
                        std::cout << " found the surface 1 .." << std::endl;
    #endif
                        is_belong_box = true;
                        processing_seg_num = s;
                        break;
                    }

                    center_face(0) = center_box_1(0) + length(a_count) * axis_box(0);
                    center_face(1) = center_box_1(1) + length(a_count) * axis_box(1);
                    center_face(2) = center_box_1(2) + length(a_count) * axis_box(2);

                    p2.x = center_face(0);
                    p2.y = center_face(1);
                    p2.z = center_face(2);

                    center_face = center_seg - center_face;
                    dist = axis_box(0) * normal_seg(0) + axis_box(1) * normal_seg(1) + axis_box(2) * normal_seg(2) ;

    #if(VISOPTION_SURFACE)

                    std::cout << " dist2  = " << center_face.norm() << std::endl;
                    std::cout << " dot product = " << dist << std::endl;
                    viewer.addSphere(p2, 0.005, 0, 1, 0, file_num) ;

                    ss << f_count;
                    file_num = ss.str();
                    f_count++;
    #endif


                    if( ( center_face.norm() < dist_from_center_extended ) && ( dist > dot_with_normal_extended ) )
                    {
    #if(VISOPTION_SURFACE)
                        std::cout << " found the surface 2.." << std::endl;
    #endif
                        is_belong_box = true;
                        processing_seg_num = s;
                        break;
                    }

                }

    #if(VISOPTION_SURFACE)
                std::cout << " close the window cloud_viewer to continue......." << std::endl;
                while(!viewer.wasStopped())
                    viewer.spinOnce();
                std::cout << " is_belong_box " << is_belong_box << std::endl;
    #endif

                if( is_belong_box )
            {
    #if(VISOPTION_SURFACE)
                    std::cout << " is_belong_box = " << is_belong_box << " processing segment ..." << processing_seg_num << std::endl;
    #endif
                    center_box = segRegion[processing_seg_num].getrepresentative_position();
                    normal_dir = segRegion[processing_seg_num].getrepresentative_normal();
                    Eigen::Vector3d axis_length_1 =  segRegion[processing_seg_num].getcut_axis();
                    Eigen::Matrix3f axis_seg = segRegion[processing_seg_num].get_axis() ;


                   int other_dir = 0;
                   bool is_handle_found = false;
                   for( int l = 1; l >= 0; l-- )
                   {
                       const double axis_length = axis_length_1(other_dir); // major axis dir / length
                       axis = axis_seg.col( l ) ; //set current axis
                       const float rad = (float)axis_length_1(l);  // minor axis dir / length

                       curv_axis(0) = (double)axis(0);
                       curv_axis(1) = (double)axis(1);
                       curv_axis(2) = (double)axis(2);
                       rest_dir = normal_dir.cross(curv_axis);


                       double maxHandAperture = this->maxHandAperture; //rad + 0.015;
                      if( this->maxHandAperture > (rad+0.02) )
                          maxHandAperture = rad + 0.02 ;

                       double outer_sample_radius = ( maxHandAperture + handle_gap ) * ( maxHandAperture + handle_gap) + ( finger_length_max * finger_length_max ) ;
                       outer_sample_radius = sqrt(outer_sample_radius);
       #if(VISOPTION_SURFACE)
                       std::cout << " current axis = " << l << " axis_length = " << other_dir << std::endl;
                       std::cout << " axis_length " << axis_length << " finger_width " << finger_width << " diff " << (axis_length - finger_width/2) <<  std::endl;
       #endif
                       for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                       {

                           center(0) = center_box(0) - ( rest_dir(0) * g);
                           center(1) = center_box(1) - ( rest_dir(1) * g);
                           center(2) = center_box(2) - ( rest_dir(2) * g);

                           center1.x = center(0) ;
                           center1.y = center(1) ;
                           center1.z = center(2) ;
           #if(VISOPTION_SURFACE)
                           std::cout << " center = " << center << std::endl;
           #endif
                           shell.setRadius( (double)rad );
                           shell.setCentroid( center );  //set center
                           shell.setCurvatureAxis( curv_axis );
                           shell.setExtent( finger_width );
                           shell.setNormal( normal_dir );

                           tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );
    //                       int num_smallest = 20;

                               int no_point_surface = 0;
                               std::vector<std::pair<float,int> > dist_from_center_right_dir;
                               std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                               Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointXYZRGB pt, pt2;

                               for( int u = 0; u < nn_indices.size(); u++ )
                               {
                                        PointXYZ pt_xyz;
                                        pt_xyz = ptCloud->points[nn_indices[u]];
           #if(VISOPTION_SURFACE)
                                        pt.r = 255;
                                        pt.g = 255;
                                        pt.b = 0;
                                        pt.x = pt_xyz.x;
                                        pt.y = pt_xyz.y;
                                        pt.z = pt_xyz.z;
                                        grasp_cloud->points.push_back(pt);
           #endif

                                        Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                        v = cropped - center;
                                        double along_rest_axis_Dist = rest_dir.dot(v);

                                        if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                        {
                                            //project points on a plane with normal along rest dir
                                            temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

           #if(VISOPTION_SURFACE)
                                            pt.r = 255;
                                            pt.g = 0;
                                            pt.b = 255;
                                            grasp_cloud->points.push_back(pt);

                                            pt2.x = temp_restDir(0);
                                            pt2.y = temp_restDir(1);
                                            pt2.z = temp_restDir(2);
           #endif


                                            // project on the plane parallel to normal _dir
                                            v = temp_restDir - center;
                                            double along_axis_Dist = curv_axis.dot(v);
                                            temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                            //calculate the distance of the projected points
                                            temp_axisDir = temp_axisDir - center;

                                            if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                            {
    #if(VISOPTION_SURFACE)
                                                pt.r = 0;
                                                pt.g = 255;
                                                pt.b = 255;
                                                grasp_cloud->points.push_back(pt);
    #endif
                                                if( (temp_axisDir.norm() > 0.007) )
                                                {
                                                   dot_product_vector = temp_axisDir;
                                                   dot_product_vector.normalize();
                                                   along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                                   if( along_axis_Dist > 0.7)
                                                   {
                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_axisDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_wrong_dir.push_back( temp_dist );
           #if(VISOPTION_SURFACE)
                                                       // points with direction alond surface normal direction
                                                       pt2.r = 0;
                                                       pt2.g = 255;
                                                       pt2.b = 0;
                                                      cloudVis->points.push_back(pt2);
           #endif
                                                   }
                                                   else if ( along_axis_Dist < -0.7)
                                                   {
           //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                       //project on the plane along axis direction
                                                       v = temp_restDir - center;
                                                       along_axis_Dist = normal_dir.dot(v);
                                                       temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
    #if(VISOPTION_SURFACE)
                                                       pt2.r = 0;
                                                       pt2.g = 0;
                                                       pt2.b = 255;
                                                      cloudVis->points.push_back(pt2);

                                                       pt2.r = 200;
                                                       pt2.g = 100;
                                                       pt2.b = 0;
                                                       pt2.x = temp_normalDir(0);
                                                       pt2.y = temp_normalDir(1);
                                                       pt2.z = temp_normalDir(2);
                                                       cloudVis2->points.push_back(pt2);
    #endif
                                                       temp_normalDir = temp_normalDir - center;

                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_normalDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_right_dir.push_back(temp_dist);

                                                   }
                                                }
                                                else
                                                {
                                                    no_point_surface ++;

                                                    v = temp_restDir - center;
                                                    along_axis_Dist = normal_dir.dot(v);
                                                    temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

    //                                                pt2.r = 0;
    //                                                pt2.g = 255;
    //                                                pt2.b = 0;
    //                                                pt2.x = temp_normalDir(0);
    //                                                pt2.y = temp_normalDir(1);
    //                                                pt2.z = temp_normalDir(2);
    //                                                cloudVis2->points.push_back(pt2);

                                                    temp_normalDir = temp_normalDir - center;
                                                    std::pair<float,int> temp_dist;
                                                    temp_dist.first = temp_normalDir.norm();
                                                    temp_dist.second = nn_indices[u];
                                                    dist_from_center_right_dir.push_back( temp_dist );
           #if(VISOPTION_SURFACE)
                                                    pt2.r = 255;
                                                    pt2.g = 0;
                                                    pt2.b = 0;
                                                    cloudVis->points.push_back(pt2);
           #endif
                                                }

                                            }

                                        }

                               }


                               if(  no_point_surface > num_sample  )
                               {

                                   if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                                   {
                                        if( dist_from_center_right_dir.size() > num_sample )
                                        {
                                            std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );

                                            //check how many number of point is there in the gap and set shell radius to be r
                                            int startPos = -1, prev_start = 0;
                                            int num_of_points_in_gap_positive_points = 0;
                                            int num_of_points_in_gap_negative_points = 0;
    //                                        bool isHandleFound = false;
                                            for( float r = rad - 0.01; r <= maxHandAperture; r += 0.001 )
                                            {
                                                num_of_points_in_gap_positive_points = 0;
                                                num_of_points_in_gap_negative_points = 0;
                                                startPos = -1;
                                                for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                                 {

                                                     if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                     {
                                                         if( startPos == -1 )
                                                             startPos = c_loop;

                                                         if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                            num_of_points_in_gap_negative_points++;
                                                         else
                                                             num_of_points_in_gap_positive_points++;

                                                     }

                                                 }

    #if(VISOPTION_SURFACE)
                                                    std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                                    std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
    #endif
                                                    if( startPos != -1)
                                                      prev_start = startPos ;

                                                    // break from the loop is the handle contains negative points
                                                    if( num_of_points_in_gap_negative_points > point_neg )
                                                    {
                                                        std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                       break;
                                                    }

                                               if(  (num_of_points_in_gap_positive_points > num_smallest) || (num_of_points_in_gap_negative_points > num_smallest) )
                                                   continue;

                                                if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                                {
                                                    shell.setRadius(r);
                                                    final_shell = shell;
                                                    is_handle_found = true ;
                                                    break;
                                                }

                                                //break from the loop if there is no positive points in gap
                                                if( num_of_points_in_gap_positive_points == 0 )
                                                   break;

                                             }


                                        }
                                        else
                                        {
                           #if(VISOPTION_SURFACE)
                                       std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                           #endif

                                        }

                                    }
                                   else
                                   {

                           #if(VISOPTION_SURFACE)
                                       std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                           #endif
                                   }
                               }
                               else
                               {
                       #if(VISOPTION_SURFACE)
                                   std::cout << " not enough samples to be a handle... " << std::endl;
                       #endif
                               }

    //                           if( is_handle_found)
                               {
           #if(VISOPTION_SURFACE)
                                   std::cout << " no_point_surface " << no_point_surface << std::endl;
                                   std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                   std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                   std::cout << " no_point_surface " << no_point_surface << std::endl;
                                   std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                   std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                   Eigen::Vector3d center_vis = final_shell.getCentroid();
                                   Eigen::Vector3d normal_vis = final_shell.getNormal();
                                   pcl::PointXYZ p3,p4;
                                   p3.x = center_vis(0);
                                   p3.y = center_vis(1);
                                   p3.z = center_vis(2);

                                   p4.x = center_vis(0) + normal_vis(0) * 0.05;
                                   p4.y = center_vis(1) + normal_vis(1) * 0.05;
                                   p4.z = center_vis(2) + normal_vis(2) * 0.05;

                                   pcl::visualization::PCLVisualizer viewer("handle");
                                   viewer.addPointCloud(ptCloud_RGB);
                                   viewer.addPointCloud(grasp_cloud, "c2");
                                   viewer.addLine(p3, p4, 0, 255, 0);
                                       viewer.addPointCloud(cloudVis, "c1");
                                       viewer.addPointCloud(cloudVis2, "c3");
                                   std::cout << " close the window cloud_viewer to continue......." << std::endl;
                                   while(!viewer.wasStopped())
                                       viewer.spinOnce();
           #endif
                                    break;
                               }

                       }

                       if ( !is_handle_found )
                       {
    #if(VISOPTION_SURFACE)
                           std::cout << " checking in the opposite direction ..." << std::endl;
    #endif
                           for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                           {

                               center(0) = center_box(0) + ( rest_dir(0) * g);
                               center(1) = center_box(1) + ( rest_dir(1) * g);
                               center(2) = center_box(2) + ( rest_dir(2) * g);

                               center1.x = center(0) ;
                               center1.y = center(1) ;
                               center1.z = center(2) ;
               #if(VISOPTION_SURFACE)
                               std::cout << " center = " << center << std::endl;
               #endif
                               shell.setRadius( (double)rad );
                               shell.setCentroid( center );  //set center
                               shell.setCurvatureAxis( curv_axis );
                               shell.setExtent( finger_width );
                               shell.setNormal( normal_dir );

                               tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );
    //                           int num_smallest = 20;

                                   int no_point_surface = 0;
                                   std::vector<std::pair<float,int> > dist_from_center_right_dir;
                                   std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                                   Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointXYZRGB pt, pt2;

                                   for( int u = 0; u < nn_indices.size(); u++ )
                                   {
                                            PointXYZ pt_xyz;
                                            pt_xyz = ptCloud->points[nn_indices[u]];
               #if(VISOPTION_SURFACE)
                                            pt.r = 255;
                                            pt.g = 255;
                                            pt.b = 0;
                                            pt.x = pt_xyz.x;
                                            pt.y = pt_xyz.y;
                                            pt.z = pt_xyz.z;
                                            grasp_cloud->points.push_back(pt);
               #endif

                                            Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                            v = cropped - center;
                                            double along_rest_axis_Dist = rest_dir.dot(v);

                                            if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                            {
                                                //project points on a plane with normal along rest dir
                                                temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

               #if(VISOPTION_SURFACE)
                                                pt.r = 255;
                                                pt.g = 0;
                                                pt.b = 255;
                                                grasp_cloud->points.push_back(pt);

                                                pt2.x = temp_restDir(0);
                                                pt2.y = temp_restDir(1);
                                                pt2.z = temp_restDir(2);
               #endif


                                                // project on the plane parallel to normal _dir
                                                v = temp_restDir - center;
                                                double along_axis_Dist = curv_axis.dot(v);
                                                temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                                //calculate the distance of the projected points
                                                temp_axisDir = temp_axisDir - center;

                                                if(  temp_axisDir.norm() < finger_length_min/*finger_length_max*/ )
                                                {
        #if(VISOPTION_SURFACE)
                                                    pt.r = 0;
                                                    pt.g = 255;
                                                    pt.b = 255;
                                                    grasp_cloud->points.push_back(pt);
        #endif
                                                    if( (temp_axisDir.norm() > 0.007) )
                                                    {
                                                       dot_product_vector = temp_axisDir;
                                                       dot_product_vector.normalize();
                                                       along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                                       if( along_axis_Dist > 0.7)
                                                       {
                                                           std::pair<float,int> temp_dist;
                                                           temp_dist.first = temp_axisDir.norm();
                                                           temp_dist.second = nn_indices[u];
                                                           dist_from_center_wrong_dir.push_back( temp_dist );
               #if(VISOPTION_SURFACE)
                                                           // points with direction alond surface normal direction
                                                           pt2.r = 0;
                                                           pt2.g = 255;
                                                           pt2.b = 0;
                                                          cloudVis->points.push_back(pt2);
               #endif
                                                       }
                                                       else if ( along_axis_Dist < -0.7)
                                                       {
               //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                           //project on the plane along axis direction
                                                           v = temp_restDir - center;
                                                           along_axis_Dist = normal_dir.dot(v);
                                                           temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
        #if(VISOPTION_SURFACE)
                                                           pt2.r = 0;
                                                           pt2.g = 0;
                                                           pt2.b = 255;
                                                          cloudVis->points.push_back(pt2);

                                                           pt2.r = 200;
                                                           pt2.g = 100;
                                                           pt2.b = 0;
                                                           pt2.x = temp_normalDir(0);
                                                           pt2.y = temp_normalDir(1);
                                                           pt2.z = temp_normalDir(2);
                                                           cloudVis2->points.push_back(pt2);
        #endif
                                                           temp_normalDir = temp_normalDir - center;

                                                           std::pair<float,int> temp_dist;
                                                           temp_dist.first = temp_normalDir.norm();
                                                           temp_dist.second = nn_indices[u];
                                                           dist_from_center_right_dir.push_back(temp_dist);

                                                       }
                                                    }
                                                    else
                                                    {
                                                        no_point_surface ++;

                                                        v = temp_restDir - center;
                                                        along_axis_Dist = normal_dir.dot(v);
                                                        temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

        //                                                pt2.r = 0;
        //                                                pt2.g = 255;
        //                                                pt2.b = 0;
        //                                                pt2.x = temp_normalDir(0);
        //                                                pt2.y = temp_normalDir(1);
        //                                                pt2.z = temp_normalDir(2);
        //                                                cloudVis2->points.push_back(pt2);

                                                        temp_normalDir = temp_normalDir - center;
                                                        std::pair<float,int> temp_dist;
                                                        temp_dist.first = temp_normalDir.norm();
                                                        temp_dist.second = nn_indices[u];
                                                        dist_from_center_right_dir.push_back( temp_dist );
               #if(VISOPTION_SURFACE)
                                                        pt2.r = 255;
                                                        pt2.g = 0;
                                                        pt2.b = 0;
                                                        cloudVis->points.push_back(pt2);
               #endif
                                                    }

                                                }

                                            }

                                   }


                                   if(  no_point_surface > num_sample  )
                                   {

                                       if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                                       {
                                            if( dist_from_center_right_dir.size() > num_sample )
                                            {
                                                std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );

                                                //check how many number of point is there in the gap and set shell radius to be r
                                                int startPos = -1, prev_start = 0;
                                                int num_of_points_in_gap_positive_points = 0;
                                                int num_of_points_in_gap_negative_points = 0;
        //                                        bool isHandleFound = false;
                                                for( float r = rad - 0.01; r <= maxHandAperture; r += 0.001 )
                                                {
                                                    num_of_points_in_gap_positive_points = 0;
                                                    num_of_points_in_gap_negative_points = 0;
                                                    startPos = -1;
                                                    for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                                     {

                                                         if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                         {
                                                             if( startPos == -1 )
                                                                 startPos = c_loop;

                                                             if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                                num_of_points_in_gap_negative_points++;
                                                             else
                                                                 num_of_points_in_gap_positive_points++;

                                                         }

                                                     }

    #if(VISOPTION_SURFACE)
                                                    std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                                    std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
    #endif
                                                    if( startPos != -1)
                                                      prev_start = startPos ;

                                                    // break from the loop is the handle contains negative points
                                                    if( num_of_points_in_gap_negative_points > point_neg )
                                                    {
                                                        std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                       break;
                                                    }

                                                   if(  (num_of_points_in_gap_positive_points > num_smallest) || (num_of_points_in_gap_negative_points > num_smallest) )
                                                       continue;

                                                    if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                                    {
                                                        shell.setRadius(r);
                                                        final_shell = shell;
                                                        is_handle_found = true ;
                                                        break;
                                                    }

                                                    //break from the loop if there is no positive points in gap
                                                    if( num_of_points_in_gap_positive_points == 0 )
                                                       break;

                                                 }


                                            }
                                            else
                                            {
                               #if(VISOPTION_SURFACE)
                                           std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                               #endif

                                            }

                                        }
                                       else
                                       {

                               #if(VISOPTION_SURFACE)
                                           std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                               #endif
                                       }
                                   }
                                   else
                                   {
                           #if(VISOPTION_SURFACE)
                                       std::cout << " not enough samples to be a handle... " << std::endl;
                           #endif
                                   }
    //                               if( is_handle_found)
                                   {
               #if(VISOPTION_SURFACE)
                                       std::cout << " no_point_surface " << no_point_surface << std::endl;
                                       std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                       std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                       std::cout << " no_point_surface " << no_point_surface << std::endl;
                                       std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                       std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                       Eigen::Vector3d center_vis = final_shell.getCentroid();
                                       Eigen::Vector3d normal_vis = final_shell.getNormal();
                                       pcl::PointXYZ p3,p4;
                                       p3.x = center_vis(0);
                                       p3.y = center_vis(1);
                                       p3.z = center_vis(2);

                                       p4.x = center_vis(0) + normal_vis(0) * 0.05;
                                       p4.y = center_vis(1) + normal_vis(1) * 0.05;
                                       p4.z = center_vis(2) + normal_vis(2) * 0.05;

                                       pcl::visualization::PCLVisualizer viewer("handle");
                                       viewer.addPointCloud(ptCloud_RGB);
                                       viewer.addPointCloud(grasp_cloud, "c2");
                                       viewer.addLine(p3, p4, 0, 255, 0);
                                           viewer.addPointCloud(cloudVis, "c1");
                                           viewer.addPointCloud(cloudVis2, "c3");
                                       std::cout << " close the window cloud_viewer to continue......." << std::endl;
                                       while(!viewer.wasStopped())
                                           viewer.spinOnce();
               #endif
                                        break;
                                   }
                           }


                       }


                       //increment
                       other_dir++;

                       //save handle
                       if( is_handle_found )
                       {
    #if(VISOPTION_SURFACE)
                           std::cout << " handle found .. " << std::endl;
    #endif
                           box_grasp.push_back(final_shell);
                           break;
                       }
                   }

           }

            }

        }

        return box_grasp;

}


std::vector<CylindricalShell> SurfacePatch::
grasp_localization_without_box( const std::vector<PointCloudIndividual> &segRegion,
                                const PointCloudXYZ::Ptr &ptCloud,
                                const PointCloudRGB::Ptr &ptCloud_RGB,
                                const std::vector<int> segnum)
{
    std::vector<CylindricalShell> box_grasp;
    //check inputs
    if( ( ptCloud->points.size() == 0 ) || (segRegion.size() == 0) )
    {
        std::cout << " in function grasp_localization_box (ptCloud->points.size() == 0 ) || (segRegion.size() == 0).." << std::endl;
        box_grasp.resize(0);
        return box_grasp;
    }


    Eigen::Vector3f axis;
    pcl::PointXYZ center1;
    const int points_in_gap_allowed = this->points_in_gap_allowed;
    int num_smallest = points_in_gap_allowed;
    const int point_neg = this->point_neg ; // 20;
    const int num_sample = 30;
    const double handle_gap = this->handle_gap;
    float finger_width = this->finger_width;  // original + error_to_be allowed
    const float finger_length_min = 0.015 ;
    const float finger_length_max = FINGER_LENGTH_MAX;



        //fitted box parameters
        Eigen::Vector3d curv_axis, center, center_box, normal_dir, rest_dir ;
        Eigen::Vector3d dot_product_vector;

        //set parameter for shell
        CylindricalShell shell, final_shell;
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
        tree->setInputCloud(ptCloud);
        std::vector<int> nn_indices;
        std::vector<float> nn_dists;

        for( int w = 0 ; w < segnum.size(); w++ )
        {

   #if(VISOPTION_SURFACE)
           std::cout << " processing segment " << segnum[w] << std::endl;
   #endif

           int processing_seg_num = segnum[w] ;
           center_box = segRegion[processing_seg_num].getrepresentative_position();
           normal_dir = segRegion[processing_seg_num].getrepresentative_normal();
           Eigen::Vector3d axis_length_1 =  segRegion[processing_seg_num].getcut_axis();
           Eigen::Matrix3f axis_seg = segRegion[processing_seg_num].get_axis() ;
           std::vector<int> nnIndices = segRegion[processing_seg_num].getNNIndices();

           float rad_curv = segRegion[processing_seg_num].get_curvature();
           Eigen::Vector3d var = segRegion[processing_seg_num].get_seg_var();
           float var_s = sqrt( (var(0) * var(0)) + (var(1) * var(1)) + (var(2) * var(2))  );

           bool isCylinder ;
           //decide whether to fit cylinder or box based on curvature and var
           {
               if( rad_curv < 0.05 )
               {
                   isCylinder = true;
               }
               else if( rad_curv < 0.1 )
               {
                   if( var_s > 0.1 )
                       isCylinder = true;
                   else
                       isCylinder = false;
               }
               else
               {
                   isCylinder = false;
               }
           }

           if( (segRegion[processing_seg_num].get_use_the_segment()) && (!isCylinder) )
           {

#if(VISOPTION_SURFACE)
               std::cout << normal_dir(2) << " size = " << nnIndices.size() << std::endl;
#endif

               if( ( normal_dir(2) < -this->thr_direction )  && ( nnIndices.size() > 100 ) )
               {
                    std::cout << " processing_seg_num = " << processing_seg_num << std::endl;

                   int other_dir = 0;
                   bool is_handle_found = false;
                   for( int l = 1; l >= 0; l-- )
                   {

                       const double axis_length = axis_length_1(other_dir); // major axis dir / length
                       axis = axis_seg.col( l ) ; //set current axis
                       const float rad = (float)axis_length_1(l);  // minor axis dir / length

                       curv_axis(0) = (double)axis(0);
                       curv_axis(1) = (double)axis(1);
                       curv_axis(2) = (double)axis(2);
                       rest_dir = normal_dir.cross(curv_axis);

                       const double maxHandAperture = this->maxHandAperture; //rad + 0.015;
                       double outer_sample_radius = ( maxHandAperture + handle_gap ) * ( maxHandAperture + handle_gap) + ( finger_length_max * finger_length_max ) ;
                       outer_sample_radius = sqrt(outer_sample_radius);

       #if(VISOPTION_SURFACE)
                       std::cout << " current axis = " << l << " axis_length = " << other_dir << std::endl;
                       std::cout << " axis_length " << axis_length << " finger_width " << finger_width << " diff " << (axis_length - finger_width/2) <<  std::endl;
       #endif
                       for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                       {

                           center(0) = center_box(0) - ( rest_dir(0) * g);
                           center(1) = center_box(1) - ( rest_dir(1) * g);
                           center(2) = center_box(2) - ( rest_dir(2) * g);

                           center1.x = center(0) ;
                           center1.y = center(1) ;
                           center1.z = center(2) ;

       #if(VISOPTION_SURFACE)
                           std::cout << " center = " << center << std::endl;
        #endif
                           shell.setRadius( (double)rad );
                           shell.setCentroid( center );  //set center
                           shell.setCurvatureAxis( curv_axis );
                           shell.setExtent( finger_width );
                           shell.setNormal( normal_dir );

                           tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );


                               int no_point_surface = 0;
                               std::vector<std::pair<float,int> > dist_from_center_right_dir;
                               std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                               Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointXYZRGB pt, pt2;

                               for( int u = 0; u < nn_indices.size(); u++ )
                               {
                                        PointXYZ pt_xyz;
                                        pt_xyz = ptCloud->points[nn_indices[u]];
           #if(VISOPTION_SURFACE)
                                        pt.r = 255;
                                        pt.g = 255;
                                        pt.b = 0;
                                        pt.x = pt_xyz.x;
                                        pt.y = pt_xyz.y;
                                        pt.z = pt_xyz.z;
                                        grasp_cloud->points.push_back(pt);
           #endif

                                        Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                        v = cropped - center;
                                        double along_rest_axis_Dist = rest_dir.dot(v);

                                        if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                        {
                                            //project points on a plane with normal along rest dir
                                            temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

           #if(VISOPTION_SURFACE)
                                            pt.r = 255;
                                            pt.g = 0;
                                            pt.b = 255;
                                            grasp_cloud->points.push_back(pt);

                                            pt2.x = temp_restDir(0);
                                            pt2.y = temp_restDir(1);
                                            pt2.z = temp_restDir(2);
           #endif


                                            // project on the plane parallel to normal _dir
                                            v = temp_restDir - center;
                                            double along_axis_Dist = curv_axis.dot(v);
                                            temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                            //calculate the distance of the projected points
                                            temp_axisDir = temp_axisDir - center;

                                            if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                            {
    #if(VISOPTION_SURFACE)
                                                pt.r = 0;
                                                pt.g = 255;
                                                pt.b = 255;
                                                grasp_cloud->points.push_back(pt);
    #endif
                                                if( (temp_axisDir.norm() > 0.007) )
                                                {
                                                   dot_product_vector = temp_axisDir;
                                                   dot_product_vector.normalize();
                                                   along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                                   if( along_axis_Dist > 0.7)
                                                   {
                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_axisDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_wrong_dir.push_back( temp_dist );
           #if(VISOPTION_SURFACE)
                                                       // points with direction alond surface normal direction
                                                       pt2.r = 0;
                                                       pt2.g = 255;
                                                       pt2.b = 0;
                                                      cloudVis->points.push_back(pt2);
           #endif
                                                   }
                                                   else if ( along_axis_Dist < -0.7)
                                                   {
           //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                       //project on the plane along axis direction
                                                       v = temp_restDir - center;
                                                       along_axis_Dist = normal_dir.dot(v);
                                                       temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
    #if(VISOPTION_SURFACE)
                                                       pt2.r = 0;
                                                       pt2.g = 0;
                                                       pt2.b = 255;
                                                      cloudVis->points.push_back(pt2);

                                                       pt2.r = 200;
                                                       pt2.g = 100;
                                                       pt2.b = 0;
                                                       pt2.x = temp_normalDir(0);
                                                       pt2.y = temp_normalDir(1);
                                                       pt2.z = temp_normalDir(2);
                                                       cloudVis2->points.push_back(pt2);
    #endif
                                                       temp_normalDir = temp_normalDir - center;

                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_normalDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_right_dir.push_back(temp_dist);

                                                   }
                                                }
                                                else
                                                {
                                                    no_point_surface ++;

                                                    v = temp_restDir - center;
                                                    along_axis_Dist = normal_dir.dot(v);
                                                    temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

    //                                                pt2.r = 0;
    //                                                pt2.g = 255;
    //                                                pt2.b = 0;
    //                                                pt2.x = temp_normalDir(0);
    //                                                pt2.y = temp_normalDir(1);
    //                                                pt2.z = temp_normalDir(2);
    //                                                cloudVis2->points.push_back(pt2);

                                                    temp_normalDir = temp_normalDir - center;
                                                    std::pair<float,int> temp_dist;
                                                    temp_dist.first = temp_normalDir.norm();
                                                    temp_dist.second = nn_indices[u];
                                                    dist_from_center_right_dir.push_back( temp_dist );
           #if(VISOPTION_SURFACE)
                                                    pt2.r = 255;
                                                    pt2.g = 0;
                                                    pt2.b = 0;
                                                    cloudVis->points.push_back(pt2);
           #endif
                                                }

                                            }

                                        }

                               }


                               if(  no_point_surface > num_sample  )
                               {

                                   if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                                   {
                                        if( dist_from_center_right_dir.size() > num_sample )
                                        {
                                            std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );

                                            //check how many number of point is there in the gap and set shell radius to be r
                                            int startPos = -1, prev_start = 0;
                                            int num_of_points_in_gap_positive_points = 0;
                                            int num_of_points_in_gap_negative_points = 0;
    //                                        bool isHandleFound = false;
                                            for( float r = rad - 0.01 ; r <= maxHandAperture; r += 0.001 )
                                            {
                                                num_of_points_in_gap_positive_points = 0;
                                                num_of_points_in_gap_negative_points = 0;
                                                startPos = -1;
                                                for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                                 {

                                                     if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                     {
                                                         if( startPos == -1 )
                                                             startPos = c_loop;

                                                         if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                            num_of_points_in_gap_negative_points++;
                                                         else
                                                             num_of_points_in_gap_positive_points++;

                                                     }

                                                 }

    #if(VISOPTION_SURFACE)
                                                    std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                                    std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
    #endif
                                                    if( startPos != -1)
                                                      prev_start = startPos ;

                                                    // break from the loop is the handle contains negative points
                                                    if( num_of_points_in_gap_negative_points > point_neg )
                                                    {
                                                        std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                       break;
                                                    }

                                                if(  (num_of_points_in_gap_positive_points > num_smallest) || (num_of_points_in_gap_negative_points > num_smallest) )
                                                    continue;

                                                 if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                                 {
                                                     shell.setRadius(r);
                                                     final_shell = shell;
                                                     is_handle_found = true ;
                                                     break;
                                                 }

                                                 //break from the loop if there is no positive points in gap
                                                 if( num_of_points_in_gap_positive_points == 0 )
                                                    break;


                                             }


                                        }
                                        else
                                        {
                           #if(VISOPTION_SURFACE)
                                       std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                           #endif

                                        }

                                    }
                                   else
                                   {

                           #if(VISOPTION_SURFACE)
                                       std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                           #endif
                                   }
                               }
                               else
                               {
                       #if(VISOPTION_SURFACE)
                                   std::cout << " not enough samples to be a handle... " << std::endl;
                       #endif
                               }

//                               if( is_handle_found)
                               {
           #if(VISOPTION_SURFACE)
                                   std::cout << is_handle_found << " no_point_surface " << no_point_surface << std::endl;
                                   std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                   std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                   Eigen::Vector3d center_vis = final_shell.getCentroid();
                                   Eigen::Vector3d normal_vis = final_shell.getNormal();
                                   pcl::PointXYZ p3,p4;
                                   p3.x = center_vis(0);
                                   p3.y = center_vis(1);
                                   p3.z = center_vis(2);

                                   p4.x = center_vis(0) + normal_vis(0) * 0.05;
                                   p4.y = center_vis(1) + normal_vis(1) * 0.05;
                                   p4.z = center_vis(2) + normal_vis(2) * 0.05;

                                   pcl::visualization::PCLVisualizer viewer("handle");
                                   viewer.addPointCloud(ptCloud_RGB);
                                   viewer.addPointCloud(grasp_cloud, "c2");
                                   viewer.addLine(p3, p4, 0, 255, 0);
                                       viewer.addPointCloud(cloudVis, "c1");
                                       viewer.addPointCloud(cloudVis2, "c3");
                                   std::cout << " close the window cloud_viewer to continue......." << std::endl;
                                   while(!viewer.wasStopped())
                                       viewer.spinOnce();

           #endif
                                    break;
                               }

                       }

                       if ( !is_handle_found )
                       {
    #if(VISOPTION_SURFACE)
                           std::cout << " checking in the opposite direction ..." << std::endl;
    #endif
                           for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                           {

                               center(0) = center_box(0) + ( rest_dir(0) * g);
                               center(1) = center_box(1) + ( rest_dir(1) * g);
                               center(2) = center_box(2) + ( rest_dir(2) * g);

                               center1.x = center(0) ;
                               center1.y = center(1) ;
                               center1.z = center(2) ;
               #if(VISOPTION_SURFACE)
                               std::cout << " center = " << center << std::endl;
               #endif
                               shell.setRadius( (double)rad );
                               shell.setCentroid( center );  //set center
                               shell.setCurvatureAxis( curv_axis );
                               shell.setExtent( finger_width );
                               shell.setNormal( normal_dir );

                               tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );
    //                           int num_smallest = 20;

                                   int no_point_surface = 0;
                                   std::vector<std::pair<float,int> > dist_from_center_right_dir;
                                   std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                                   Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud_1( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud_2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointXYZRGB pt, pt2;

                                   for( int u = 0; u < nn_indices.size(); u++ )
                                   {
                                            PointXYZ pt_xyz;
                                            pt_xyz = ptCloud->points[nn_indices[u]];
               #if(VISOPTION_SURFACE)
                                            pt.r = 255;
                                            pt.g = 255;
                                            pt.b = 0;
                                            pt.x = pt_xyz.x;
                                            pt.y = pt_xyz.y;
                                            pt.z = pt_xyz.z;
                                            grasp_cloud->points.push_back(pt);
               #endif

                                            Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                            v = cropped - center;
                                            double along_rest_axis_Dist = rest_dir.dot(v);

                                            if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                            {
                                                //project points on a plane with normal along rest dir
                                                temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

               #if(VISOPTION_SURFACE)
                                                pt.r = 255;
                                                pt.g = 0;
                                                pt.b = 255;
                                                grasp_cloud_1->points.push_back(pt);

                                                pt2.x = temp_restDir(0);
                                                pt2.y = temp_restDir(1);
                                                pt2.z = temp_restDir(2);
               #endif


                                                // project on the plane parallel to normal _dir
                                                v = temp_restDir - center;
                                                double along_axis_Dist = curv_axis.dot(v);
                                                temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                                //calculate the distance of the projected points
                                                temp_axisDir = temp_axisDir - center;

                                                if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                                {
        #if(VISOPTION_SURFACE)
                                                    pt.r = 0;
                                                    pt.g = 255;
                                                    pt.b = 255;
                                                    grasp_cloud->points.push_back(pt);
        #endif
                                                    if( (temp_axisDir.norm() > 0.007) )
                                                    {
                                                       dot_product_vector = temp_axisDir;
                                                       dot_product_vector.normalize();
                                                       along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                                       if( along_axis_Dist > 0.7)
                                                       {
                                                           std::pair<float,int> temp_dist;
                                                           temp_dist.first = temp_axisDir.norm();
                                                           temp_dist.second = nn_indices[u];
                                                           dist_from_center_wrong_dir.push_back( temp_dist );
               #if(VISOPTION_SURFACE)
                                                           // points with direction alond surface normal direction
                                                           pt2.r = 0;
                                                           pt2.g = 255;
                                                           pt2.b = 0;
                                                          cloudVis->points.push_back(pt2);
               #endif
                                                       }
                                                       else if ( along_axis_Dist < -0.7)
                                                       {
               //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                           //project on the plane along axis direction
                                                           v = temp_restDir - center;
                                                           along_axis_Dist = normal_dir.dot(v);
                                                           temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
        #if(VISOPTION_SURFACE)
                                                           pt2.r = 0;
                                                           pt2.g = 0;
                                                           pt2.b = 255;
                                                          cloudVis->points.push_back(pt2);

                                                           pt2.r = 200;
                                                           pt2.g = 100;
                                                           pt2.b = 0;
                                                           pt2.x = temp_normalDir(0);
                                                           pt2.y = temp_normalDir(1);
                                                           pt2.z = temp_normalDir(2);
                                                           cloudVis2->points.push_back(pt2);
        #endif
                                                           temp_normalDir = temp_normalDir - center;

                                                           std::pair<float,int> temp_dist;
                                                           temp_dist.first = temp_normalDir.norm();
                                                           temp_dist.second = nn_indices[u];
                                                           dist_from_center_right_dir.push_back(temp_dist);

                                                       }
                                                    }
                                                    else
                                                    {
                                                        no_point_surface ++;

                                                        v = temp_restDir - center;
                                                        along_axis_Dist = normal_dir.dot(v);
                                                        temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

        //                                                pt2.r = 0;
        //                                                pt2.g = 255;
        //                                                pt2.b = 0;
        //                                                pt2.x = temp_normalDir(0);
        //                                                pt2.y = temp_normalDir(1);
        //                                                pt2.z = temp_normalDir(2);
        //                                                cloudVis2->points.push_back(pt2);

                                                        temp_normalDir = temp_normalDir - center;
                                                        std::pair<float,int> temp_dist;
                                                        temp_dist.first = temp_normalDir.norm();
                                                        temp_dist.second = nn_indices[u];
                                                        dist_from_center_right_dir.push_back( temp_dist );
               #if(VISOPTION_SURFACE)
                                                        pt2.r = 255;
                                                        pt2.g = 0;
                                                        pt2.b = 0;
                                                        cloudVis->points.push_back(pt2);
               #endif
                                                    }

                                                }

                                            }

                                   }


                                   if(  no_point_surface > num_sample  )
                                   {

                                       if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                                       {
                                            if( dist_from_center_right_dir.size() > num_sample )
                                            {
                                                std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );

                                                //check how many number of point is there in the gap and set shell radius to be r
                                                int startPos = -1, prev_start = 0;
                                                int num_of_points_in_gap_positive_points = 0;
                                                int num_of_points_in_gap_negative_points = 0;
        //                                        bool isHandleFound = false;
                                                for( float r = rad - 0.01; r <= maxHandAperture; r += 0.001 )
                                                {
                                                    num_of_points_in_gap_positive_points = 0;
                                                    num_of_points_in_gap_negative_points = 0;
                                                    startPos = -1;
                                                    for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                                     {

                                                         if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                         {
                                                             if( startPos == -1 )
                                                                 startPos = c_loop;

                                                             if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                                num_of_points_in_gap_negative_points++;
                                                             else
                                                                 num_of_points_in_gap_positive_points++;

                                                         }

                                                     }
    #if(VISOPTION_SURFACE)
                                                    std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                                    std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
    #endif
                                                    if( startPos != -1)
                                                      prev_start = startPos ;

                                                    // break from the loop is the handle contains negative points
                                                    if( num_of_points_in_gap_negative_points > point_neg )
                                                    {
                                                        std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                       break;
                                                    }

                                                   if(  (num_of_points_in_gap_positive_points > num_smallest) || (num_of_points_in_gap_negative_points > num_smallest) )
                                                       continue;

                                                    if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                                    {
                                                        shell.setRadius(r);
                                                        final_shell = shell;
                                                        is_handle_found = true ;
                                                        break;
                                                    }

                                                    //break from the loop if there is no positive points in gap
                                                    if( num_of_points_in_gap_positive_points == 0 )
                                                       break;

                                                 }


                                            }
                                            else
                                            {
                               #if(VISOPTION_SURFACE)
                                           std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                               #endif

                                            }

                                        }
                                       else
                                       {

                               #if(VISOPTION_SURFACE)
                                           std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                               #endif
                                       }
                                   }
                                   else
                                   {
                           #if(VISOPTION_SURFACE)
                                       std::cout << " not enough samples to be a handle... " << std::endl;
                           #endif
                                   }

//                                   if( is_handle_found)
                                   {
               #if(VISOPTION_SURFACE)
                                       std::cout << is_handle_found << " no_point_surface " << no_point_surface << std::endl;
                                       std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                       std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                       Eigen::Vector3d center_vis = final_shell.getCentroid();
                                       Eigen::Vector3d normal_vis = final_shell.getNormal();
                                       pcl::PointXYZ p3,p4;
                                       p3.x = center_vis(0);
                                       p3.y = center_vis(1);
                                       p3.z = center_vis(2);

                                       p4.x = center_vis(0) + normal_vis(0) * 0.05;
                                       p4.y = center_vis(1) + normal_vis(1) * 0.05;
                                       p4.z = center_vis(2) + normal_vis(2) * 0.05;

                                       pcl::visualization::PCLVisualizer viewer("handle");
                                       viewer.addPointCloud(ptCloud_RGB);
                                       viewer.addPointCloud(grasp_cloud, "c2");
                                       viewer.addLine(p3, p4, 0, 255, 0);
                                           viewer.addPointCloud(cloudVis, "c1");
                                           viewer.addPointCloud(cloudVis2, "c3");
                                       std::cout << " close the window cloud_viewer to continue......." << std::endl;
                                       while(!viewer.wasStopped())
                                           viewer.spinOnce();

               #endif
                                        break;
                                   }
                           }


                       }


                       //increment
                       other_dir++;

                       //save handle
                       if( is_handle_found )
                       {
    #if(VISOPTION_SURFACE)
                           std::cout << " handle found .. " << std::endl;
    #endif
                           box_grasp.push_back(final_shell);
                           break;
                       }
                   }

               }

           }

        }

           std::cout << " number of grasp region found in the segment on which box is fitted.. " << box_grasp.size() << std::endl;
           std::cout << std::endl << " checking in  other segments" << std::endl;

        return box_grasp;

}


std::vector<CylindricalShell> SurfacePatch::
grasp_localization_box_final( const std::vector<PointCloudIndividual> &segRegion,
                        const PointCloudXYZ::Ptr &ptCloud,
                        const PointCloudRGB::Ptr &ptCloud_RGB,
                        Box_Param box_all )
{
    std::vector<CylindricalShell> box_grasp;
    //check inputs
    if( ( ptCloud->points.size() == 0 ) || (segRegion.size() == 0) )
    {
        std::cout << " in function grasp_localization_box (ptCloud->points.size() == 0 ) || (segRegion.size() == 0).." << std::endl;
        box_grasp.resize(0);
        return box_grasp;
    }


    Eigen::Vector3f axis;
    Eigen::Matrix3f three_axis;
    pcl::PointXYZ center1;
    std::pair<int,int> segment_pair;
    const int points_in_gap_allowed = this->points_in_gap_allowed;
    int num_smallest = points_in_gap_allowed;
    const int point_neg = this->point_neg ; // 20;
    const int num_sample = 30;
    const double handle_gap = this->handle_gap;
    float finger_width = this->finger_width;  // original + error_to_be allowed
    const float finger_length_min = 0.015 ;
    const float finger_length_max = FINGER_LENGTH_MAX;
    const float dist_from_center_extended = 0.03;
    const float dot_with_normal_extended = 0.9;


    //fitted box parameters
    Box_Param box = box_all;
    three_axis = box.eigDx;
    segment_pair = box.segment_pair;
    std::vector<int> seg(2);
    seg[0] = segment_pair.first;
    seg[1] = segment_pair.second;
    Eigen::Vector3d length = box.axis_length;
    Eigen::Vector3d curv_axis, center, center_box, normal_dir, rest_dir ;
    Eigen::Vector3d dot_product_vector;

        //set parameter for shell
        CylindricalShell shell, final_shell;
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
        tree->setInputCloud(ptCloud);
        std::vector<int> nn_indices;
        std::vector<float> nn_dists;

        for( int w = 0 ; w < 2; w++ )
        {

            #if(VISOPTION_SURFACE)
            std::cout << " processing segment " << seg[w] << std::endl;
            #endif

              int processing_seg_num =  seg[w];
               center_box = segRegion[processing_seg_num].getrepresentative_position();
               normal_dir = segRegion[processing_seg_num].getrepresentative_normal();
               Eigen::Vector3d axis_length_1 =  segRegion[processing_seg_num].getcut_axis();
               Eigen::Matrix3f axis_seg = segRegion[processing_seg_num].get_axis() ;

               int other_dir = 0;
               bool is_handle_found = false;
               for( int l = 1; l >= 0; l-- )
               {

                   const double axis_length = axis_length_1(other_dir); // major axis dir / length
                   axis = axis_seg.col( l ) ; //set current axis
                   const float rad = (float)axis_length_1(l);  // minor axis dir / length

                   curv_axis(0) = (double)axis(0);
                   curv_axis(1) = (double)axis(1);
                   curv_axis(2) = (double)axis(2);
                   rest_dir = normal_dir.cross(curv_axis);

//                   const double maxHandAperture = this->maxHandAperture; //rad + 0.015;
                   double maxHandAperture;
                   if( this->maxHandAperture > ( rad + 0.02 ) )
                       maxHandAperture = rad + 0.02;
                   else
                       maxHandAperture = this->maxHandAperture;

                   double outer_sample_radius = ( maxHandAperture + handle_gap ) * ( maxHandAperture + handle_gap) + ( finger_length_max * finger_length_max ) ;
                   outer_sample_radius = sqrt(outer_sample_radius);

   #if(VISOPTION_SURFACE)
                   std::cout << " current axis = " << l << " axis_length = " << other_dir << std::endl;
                   std::cout << " axis_length " << axis_length << " finger_width " << finger_width << " diff " << (axis_length - finger_width/2) <<  std::endl;
   #endif
                   for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                   {

                       center(0) = center_box(0) - ( rest_dir(0) * g);
                       center(1) = center_box(1) - ( rest_dir(1) * g);
                       center(2) = center_box(2) - ( rest_dir(2) * g);

                       center1.x = center(0) ;
                       center1.y = center(1) ;
                       center1.z = center(2) ;

       #if(VISOPTION_SURFACE)
                       std::cout << " center = " << center << std::endl;
       #endif
                       shell.setRadius( (double)rad );
                       shell.setCentroid( center );  //set center
                       shell.setCurvatureAxis( curv_axis );
                       shell.setExtent( finger_width );
                       shell.setNormal( normal_dir );

                       tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );


                           int no_point_surface = 0;
                           std::vector<std::pair<float,int> > dist_from_center_right_dir;
                           std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                           Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                           pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                           pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                           pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                           pcl::PointXYZRGB pt, pt2;

                           for( int u = 0; u < nn_indices.size(); u++ )
                           {
                                    PointXYZ pt_xyz;
                                    pt_xyz = ptCloud->points[nn_indices[u]];
       #if(VISOPTION_SURFACE)
                                    pt.r = 255;
                                    pt.g = 255;
                                    pt.b = 0;
                                    pt.x = pt_xyz.x;
                                    pt.y = pt_xyz.y;
                                    pt.z = pt_xyz.z;
//                                        grasp_cloud->points.push_back(pt);
       #endif

                                    Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                    v = cropped - center;
                                    double along_rest_axis_Dist = rest_dir.dot(v);

                                    if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                    {
                                        //project points on a plane with normal along rest dir
                                        temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

       #if(VISOPTION_SURFACE)
                                        pt.r = 255;
                                        pt.g = 0;
                                        pt.b = 255;
                                        grasp_cloud->points.push_back(pt);

                                        pt2.x = temp_restDir(0);
                                        pt2.y = temp_restDir(1);
                                        pt2.z = temp_restDir(2);
       #endif


                                        // project on the plane parallel to normal _dir
                                        v = temp_restDir - center;
                                        double along_axis_Dist = curv_axis.dot(v);
                                        temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                        //calculate the distance of the projected points
                                        temp_axisDir = temp_axisDir - center;

                                        if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                        {
#if(VISOPTION_SURFACE)
                                            pt.r = 0;
                                            pt.g = 255;
                                            pt.b = 255;
                                            grasp_cloud->points.push_back(pt);
#endif
                                            if( (temp_axisDir.norm() > 0.007) )
                                            {
                                               dot_product_vector = temp_axisDir;
                                               dot_product_vector.normalize();
                                               along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                               if( along_axis_Dist > 0.7)
                                               {

                                                   v = temp_restDir - center;
                                                   along_axis_Dist = normal_dir.dot(v);
                                                   temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

#if(VISOPTION_SURFACE)
                                               pt2.r = 0;
                                               pt2.g = 0;
                                               pt2.b = 255;
                                              cloudVis->points.push_back(pt2);

                                               pt2.r = 255;
                                               pt2.g = 255;
                                               pt2.b = 0;
                                               pt2.x = temp_normalDir(0);
                                               pt2.y = temp_normalDir(1);
                                               pt2.z = temp_normalDir(2);
                                               cloudVis2->points.push_back(pt2);
#endif

                                                   temp_normalDir = temp_normalDir - center;
                                                   std::pair<float,int> temp_dist;
                                                   temp_dist.first = temp_normalDir.norm();
                                                   temp_dist.second = nn_indices[u];
                                                   dist_from_center_wrong_dir.push_back(temp_dist);

                                               }
                                               else if ( along_axis_Dist < -0.7)
                                               {
       //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                   //project on the plane along axis direction
                                                   v = temp_restDir - center;
                                                   along_axis_Dist = normal_dir.dot(v);
                                                   temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
#if(VISOPTION_SURFACE)
                                                   pt2.r = 0;
                                                   pt2.g = 0;
                                                   pt2.b = 255;
                                                  cloudVis->points.push_back(pt2);

                                                   pt2.r = 200;
                                                   pt2.g = 100;
                                                   pt2.b = 0;
                                                   pt2.x = temp_normalDir(0);
                                                   pt2.y = temp_normalDir(1);
                                                   pt2.z = temp_normalDir(2);
                                                   cloudVis2->points.push_back(pt2);
#endif
                                                   temp_normalDir = temp_normalDir - center;

                                                   std::pair<float,int> temp_dist;
                                                   temp_dist.first = temp_normalDir.norm();
                                                   temp_dist.second = nn_indices[u];
                                                   dist_from_center_right_dir.push_back(temp_dist);

                                               }
                                            }
                                            else
                                            {
                                                no_point_surface ++;

                                                v = temp_restDir - center;
                                                along_axis_Dist = normal_dir.dot(v);
                                                temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

//                                                pt2.r = 0;
//                                                pt2.g = 255;
//                                                pt2.b = 0;
//                                                pt2.x = temp_normalDir(0);
//                                                pt2.y = temp_normalDir(1);
//                                                pt2.z = temp_normalDir(2);
//                                                cloudVis2->points.push_back(pt2);

                                                temp_normalDir = temp_normalDir - center;
                                                std::pair<float,int> temp_dist;
                                                temp_dist.first = temp_normalDir.norm();
                                                temp_dist.second = nn_indices[u];
                                                dist_from_center_right_dir.push_back( temp_dist );
       #if(VISOPTION_SURFACE)
                                                pt2.r = 255;
                                                pt2.g = 0;
                                                pt2.b = 0;
                                                cloudVis->points.push_back(pt2);
       #endif
                                            }

                                        }

                                    }

                           }


                           if(  no_point_surface > num_sample  )
                           {

//                                   if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                               if(1)
                               {
                                    if( dist_from_center_right_dir.size() > num_sample )
                                    {
                                        std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );
                                        std::sort( dist_from_center_wrong_dir.begin(), dist_from_center_wrong_dir.end() );

                                        //check how many number of point is there in the gap and set shell radius to be r
                                        int startPos = -1, prev_start = 0;
                                        int num_of_points_in_gap_positive_points = 0;
                                        int num_of_points_in_gap_negative_points = 0;
//                                        bool isHandleFound = false;
                                        for( float r = rad - 0.01 ; r <= maxHandAperture; r += 0.001 )
                                        {
                                            num_of_points_in_gap_positive_points = 0;
                                            num_of_points_in_gap_negative_points = 0;
                                            startPos = -1;
                                            for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                             {

                                                 if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                 {
                                                     if( startPos == -1 )
                                                         startPos = c_loop;

                                                     if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                        num_of_points_in_gap_negative_points++;
                                                     else
                                                         num_of_points_in_gap_positive_points++;

                                                 }

                                             }

                                            int num_of_point_wrong_dir = 0;
                                            for( int g_count = 0; g_count < dist_from_center_wrong_dir.size(); g_count++ )
                                            {

                                                if( ( dist_from_center_wrong_dir[g_count].first <= ( r +  handle_gap )  ) )
                                                    num_of_point_wrong_dir++;

                                            }


#if(VISOPTION_SURFACE)
                                      std::cout << " num_of_point_wrong_dir = " << num_of_point_wrong_dir << std::endl;
                                      std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                      std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
#endif
                                                if( startPos != -1)
                                                  prev_start = startPos ;

                                                // break from the loop is the handle contains negative points
                                                if( num_of_points_in_gap_negative_points > point_neg )
                                                {
                                                    std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                   break;
                                                }

                                            if(  (num_of_points_in_gap_positive_points > num_smallest)
                                                 || (num_of_points_in_gap_negative_points > num_smallest)
                                                 || (num_of_point_wrong_dir > num_smallest) )
                                                continue;

                                             if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                             {
                                                 shell.setRadius(r);
                                                 final_shell = shell;
                                                 is_handle_found = true ;
                                                 break;
                                             }

                                             //break from the loop if there is no positive points in gap
                                             if( num_of_points_in_gap_positive_points == 0 )
                                                break;


                                         }


                                    }
                                    else
                                    {
                       #if(VISOPTION_SURFACE)
                                   std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                       #endif

                                    }

                                }
                               else
                               {

                       #if(VISOPTION_SURFACE)
                                   std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                       #endif
                               }
                           }
                           else
                           {
                   #if(VISOPTION_SURFACE)
                               std::cout << " not enough samples to be a handle... " << std::endl;
                   #endif
                           }

//                           if( is_handle_found)
                           {
       #if(VISOPTION_SURFACE)
                               std::cout << " no_point_surface " << no_point_surface << std::endl;
                               std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                               std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                               Eigen::Vector3d center_vis = final_shell.getCentroid();
                               Eigen::Vector3d normal_vis = final_shell.getNormal();
                               pcl::PointXYZ p3,p4;
                               p3.x = center_vis(0);
                               p3.y = center_vis(1);
                               p3.z = center_vis(2);

                               p4.x = center_vis(0) + normal_vis(0) * 0.05;
                               p4.y = center_vis(1) + normal_vis(1) * 0.05;
                               p4.z = center_vis(2) + normal_vis(2) * 0.05;

                               pcl::visualization::PCLVisualizer viewer("handle");
                               viewer.addPointCloud(ptCloud_RGB);
                               viewer.addPointCloud(grasp_cloud, "c2");
                               viewer.addLine(p3, p4, 0, 255, 0);
                                   viewer.addPointCloud(cloudVis, "c1");
                                   viewer.addPointCloud(cloudVis2, "c3");
                               std::cout << " close the window cloud_viewer to continue......." << std::endl;
                               while(!viewer.wasStopped())
                                   viewer.spinOnce();

       #endif
                                break;
                           }

                   }

                   if ( !is_handle_found )
                   {
#if(VISOPTION_SURFACE)
                       std::cout << " checking in the opposite direction ..." << std::endl;
#endif
                       for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                       {

                           center(0) = center_box(0) + ( rest_dir(0) * g);
                           center(1) = center_box(1) + ( rest_dir(1) * g);
                           center(2) = center_box(2) + ( rest_dir(2) * g);

                           center1.x = center(0) ;
                           center1.y = center(1) ;
                           center1.z = center(2) ;
           #if(VISOPTION_SURFACE)
                           std::cout << " center = " << center << std::endl;
           #endif
                           shell.setRadius( (double)rad );
                           shell.setCentroid( center );  //set center
                           shell.setCurvatureAxis( curv_axis );
                           shell.setExtent( finger_width );
                           shell.setNormal( normal_dir );

                           tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );
//                           int num_smallest = 20;

                               int no_point_surface = 0;
                               std::vector<std::pair<float,int> > dist_from_center_right_dir;
                               std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                               Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud_1( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud_2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointXYZRGB pt, pt2;

                               for( int u = 0; u < nn_indices.size(); u++ )
                               {
                                        PointXYZ pt_xyz;
                                        pt_xyz = ptCloud->points[nn_indices[u]];
           #if(VISOPTION_SURFACE)
                                        pt.r = 255;
                                        pt.g = 255;
                                        pt.b = 0;
                                        pt.x = pt_xyz.x;
                                        pt.y = pt_xyz.y;
                                        pt.z = pt_xyz.z;
//                                        grasp_cloud->points.push_back(pt);
           #endif

                                        Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                        v = cropped - center;
                                        double along_rest_axis_Dist = rest_dir.dot(v);

                                        if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                        {
                                            //project points on a plane with normal along rest dir
                                            temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

           #if(VISOPTION_SURFACE)
                                            pt.r = 255;
                                            pt.g = 0;
                                            pt.b = 255;
                                            grasp_cloud->points.push_back(pt);

                                            pt2.x = temp_restDir(0);
                                            pt2.y = temp_restDir(1);
                                            pt2.z = temp_restDir(2);
           #endif


                                            // project on the plane parallel to normal _dir
                                            v = temp_restDir - center;
                                            double along_axis_Dist = curv_axis.dot(v);
                                            temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                            //calculate the distance of the projected points
                                            temp_axisDir = temp_axisDir - center;

                                            if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                            {
    #if(VISOPTION_SURFACE)
                                                pt.r = 0;
                                                pt.g = 255;
                                                pt.b = 255;
                                                grasp_cloud->points.push_back(pt);
    #endif
                                                if( (temp_axisDir.norm() > 0.007) )
                                                {
                                                   dot_product_vector = temp_axisDir;
                                                   dot_product_vector.normalize();
                                                   along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                                   if( along_axis_Dist > 0.7)
                                                   {

                                                       v = temp_restDir - center;
                                                       along_axis_Dist = normal_dir.dot(v);
                                                       temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

#if(VISOPTION_SURFACE)
                                                   pt2.r = 0;
                                                   pt2.g = 0;
                                                   pt2.b = 255;
                                                  cloudVis->points.push_back(pt2);

                                                   pt2.r = 255;
                                                   pt2.g = 255;
                                                   pt2.b = 0;
                                                   pt2.x = temp_normalDir(0);
                                                   pt2.y = temp_normalDir(1);
                                                   pt2.z = temp_normalDir(2);
                                                   cloudVis2->points.push_back(pt2);
#endif

                                                       temp_normalDir = temp_normalDir - center;
                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_normalDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_wrong_dir.push_back(temp_dist);

                                                   }
                                                   else if ( along_axis_Dist < -0.7)
                                                   {
           //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                       //project on the plane along axis direction
                                                       v = temp_restDir - center;
                                                       along_axis_Dist = normal_dir.dot(v);
                                                       temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
    #if(VISOPTION_SURFACE)
                                                       pt2.r = 0;
                                                       pt2.g = 0;
                                                       pt2.b = 255;
                                                      cloudVis->points.push_back(pt2);

                                                       pt2.r = 200;
                                                       pt2.g = 100;
                                                       pt2.b = 0;
                                                       pt2.x = temp_normalDir(0);
                                                       pt2.y = temp_normalDir(1);
                                                       pt2.z = temp_normalDir(2);
                                                       cloudVis2->points.push_back(pt2);
    #endif
                                                       temp_normalDir = temp_normalDir - center;

                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_normalDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_right_dir.push_back(temp_dist);

                                                   }
                                                }
                                                else
                                                {
                                                    no_point_surface ++;

                                                    v = temp_restDir - center;
                                                    along_axis_Dist = normal_dir.dot(v);
                                                    temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

    //                                                pt2.r = 0;
    //                                                pt2.g = 255;
    //                                                pt2.b = 0;
    //                                                pt2.x = temp_normalDir(0);
    //                                                pt2.y = temp_normalDir(1);
    //                                                pt2.z = temp_normalDir(2);
    //                                                cloudVis2->points.push_back(pt2);

                                                    temp_normalDir = temp_normalDir - center;
                                                    std::pair<float,int> temp_dist;
                                                    temp_dist.first = temp_normalDir.norm();
                                                    temp_dist.second = nn_indices[u];
                                                    dist_from_center_right_dir.push_back( temp_dist );
           #if(VISOPTION_SURFACE)
                                                    pt2.r = 255;
                                                    pt2.g = 0;
                                                    pt2.b = 0;
                                                    cloudVis->points.push_back(pt2);
           #endif
                                                }

                                            }

                                        }

                               }


                               if(  no_point_surface > num_sample  )
                               {

//                                   if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                                   if(1)
                                   {
                                        if( dist_from_center_right_dir.size() > num_sample )
                                        {
                                            std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );
                                            std::sort( dist_from_center_wrong_dir.begin(), dist_from_center_wrong_dir.end() );

                                            //check how many number of point is there in the gap and set shell radius to be r
                                            int startPos = -1, prev_start = 0;
                                            int num_of_points_in_gap_positive_points = 0;
                                            int num_of_points_in_gap_negative_points = 0;
    //                                        bool isHandleFound = false;
                                            for( float r = rad - 0.01 ; r <= maxHandAperture; r += 0.001 )
                                            {
                                                num_of_points_in_gap_positive_points = 0;
                                                num_of_points_in_gap_negative_points = 0;
                                                startPos = -1;
                                                for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                                 {

                                                     if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                     {
                                                         if( startPos == -1 )
                                                             startPos = c_loop;

                                                         if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                            num_of_points_in_gap_negative_points++;
                                                         else
                                                             num_of_points_in_gap_positive_points++;

                                                     }

                                                 }

                                                int num_of_point_wrong_dir = 0;
                                                for( int g_count = 0; g_count < dist_from_center_wrong_dir.size(); g_count++ )
                                                {

                                                    if(  ( dist_from_center_wrong_dir[g_count].first <= ( r +  handle_gap )  ) )
                                                        num_of_point_wrong_dir++;

                                                }


    #if(VISOPTION_SURFACE)
                                          std::cout << " num_of_point_wrong_dir = " << num_of_point_wrong_dir << std::endl;
                                          std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                          std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
    #endif
                                                    if( startPos != -1)
                                                      prev_start = startPos ;

                                                    // break from the loop is the handle contains negative points
                                                    if( num_of_points_in_gap_negative_points > point_neg )
                                                    {
                                                        std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                       break;
                                                    }

                                                if(  (num_of_points_in_gap_positive_points > num_smallest)
                                                     || (num_of_points_in_gap_negative_points > num_smallest)
                                                     || (num_of_point_wrong_dir > num_smallest) )
                                                    continue;

                                                 if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                                 {
                                                     shell.setRadius(r);
                                                     final_shell = shell;
                                                     is_handle_found = true ;
                                                     break;
                                                 }

                                                 //break from the loop if there is no positive points in gap
                                                 if( num_of_points_in_gap_positive_points == 0 )
                                                    break;


                                             }


                                        }
                                        else
                                        {
                           #if(VISOPTION_SURFACE)
                                       std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                           #endif

                                        }

                                    }
                                   else
                                   {

                           #if(VISOPTION_SURFACE)
                                       std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                           #endif
                                   }
                               }
                               else
                               {
                       #if(VISOPTION_SURFACE)
                                   std::cout << " not enough samples to be a handle... " << std::endl;
                       #endif
                               }

//                               if( is_handle_found)
                               {
           #if(VISOPTION_SURFACE)
                                   std::cout << " no_point_surface " << no_point_surface << std::endl;
                                   std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                   std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                   Eigen::Vector3d center_vis = final_shell.getCentroid();
                                   Eigen::Vector3d normal_vis = final_shell.getNormal();
                                   pcl::PointXYZ p3,p4;
                                   p3.x = center_vis(0);
                                   p3.y = center_vis(1);
                                   p3.z = center_vis(2);

                                   p4.x = center_vis(0) + normal_vis(0) * 0.05;
                                   p4.y = center_vis(1) + normal_vis(1) * 0.05;
                                   p4.z = center_vis(2) + normal_vis(2) * 0.05;

                                   pcl::visualization::PCLVisualizer viewer("handle");
                                   viewer.addPointCloud(ptCloud_RGB);
                                   viewer.addPointCloud(grasp_cloud, "c2");
                                   viewer.addLine(p3, p4, 0, 255, 0);
                                       viewer.addPointCloud(cloudVis, "c1");
                                       viewer.addPointCloud(cloudVis2, "c3");
                                   std::cout << " close the window cloud_viewer to continue......." << std::endl;
                                   while(!viewer.wasStopped())
                                       viewer.spinOnce();

           #endif
                                    break;
                               }
                       }


                   }


                   //increment
                   other_dir++;

                   //save handle
                   if( is_handle_found )
                   {
#if(VISOPTION_SURFACE)
                       std::cout << " handle found .. " << std::endl;
#endif
                       box_grasp.push_back(final_shell);
                       break;
                   }
               }

       }

           std::cout << " number of grasp region found in the segment on which box is fitted.. " << box_grasp.size() << std::endl;
           std::cout << std::endl << " checking in  other segments" << std::endl;


        //find out which segment remains
        std::vector<int> seg_remain;
        for( int s_count = 0; s_count < segRegion.size(); s_count++ )
        {
            if( ( s_count == segment_pair.first ) || ( s_count == segment_pair.second ) )
            {
//                std::cout << " segment already tested ... " << std::endl ;
            }
            else
            {
                if( segRegion[s_count].getNNIndices().size() > 100 )
                    seg_remain.push_back(s_count);
            }
        }

#if(VISOPTION_SURFACE)
        for( int r = 0 ; r < seg_remain.size(); r++ )
            std::cout << r << " , " << seg_remain[r] << std::endl;
        std::cout << " pair = " << segment_pair.first << " , " << segment_pair.second ;
        std::cout << " segment size = " << seg_remain.size() << std::endl;
        std::cout << " printing the six centers of the fitted box" << std::endl;
#endif

        Eigen::Vector3f axis_box, center_box_1;
        Eigen::Vector3d center_face;
        pcl::PointXYZ p1, p2;
        center_box_1 = box.mean_diag;

        int f_count = 0;

        p1.x = center_box_1(0);
        p1.y = center_box_1(1);
        p1.z = center_box_1(2);

        int processing_seg_num = -1;
        for( int r = 0; r < seg_remain.size(); r++  )
        {

            int s = seg_remain[r];
            Eigen::Vector3d center_seg = segRegion[s].getrepresentative_position();
            Eigen::Vector3d normal_seg = segRegion[s].getrepresentative_normal();
            bool is_belong_box = false;

            if( normal_seg(2) < -this->thr_direction ) /*  changes made by me   */
            {

    #if(VISOPTION_SURFACE)
                stringstream ss;
                ss << s;
                string file_num = ss.str();
                std::cout << " processing_seg_num = " << s << std::endl;
                std::cout << r << " seg_remain " << seg_remain[r] << std::endl;
                std::cout << " normal_seg = " << normal_seg << std::endl;
                pcl::visualization::PCLVisualizer viewer("center_position");
                viewer.addPointCloud(ptCloud);
                viewer.addSphere(p1, 0.005, 0, 0, 1, file_num) ;
    #endif


                {
                    Box_Param box_fit = box;
                    //fitting a box
                    Eigen::Matrix3f eigDx = box_fit.eigDx;
                    eigDx.col(2) = eigDx.col(0).cross(eigDx.col(1));
                    // final transform
                    const Eigen::Quaternionf qfinal(eigDx);
                    const Eigen::Vector3f tfinal = box_fit.mean_diag;
                    Eigen::Vector3d axis_length = box_fit.axis_length ;
    //                viewer.addCube( tfinal, qfinal, 2*axis_length(0), 2*axis_length(1), 2*axis_length(2)  ); //minor, major, normal
                }


                Eigen::Vector3d axis_length_1 =  segRegion[processing_seg_num].getcut_axis();
                Eigen::Matrix3f axis_seg = segRegion[processing_seg_num].get_axis() ;

                for( int a_count = 0; a_count < 3; a_count++)
                {
                    axis_box = three_axis.col(a_count);
                    center_face(0) = center_box_1(0) - length(a_count) * axis_box(0);
                    center_face(1) = center_box_1(1) - length(a_count) * axis_box(1);
                    center_face(2) = center_box_1(2) - length(a_count) * axis_box(2);

                    p2.x = center_face(0);
                    p2.y = center_face(1);
                    p2.z = center_face(2);

                    center_face = center_seg - center_face;
                    float dist = axis_box(0) * normal_seg(0) + axis_box(1) * normal_seg(1) + axis_box(2) * normal_seg(2) ;

    #if(VISOPTION_SURFACE)
                    std::cout << " dist1  = " << center_face.norm() << std::endl;
                    std::cout << " dot product = " << -dist << std::endl;
                    viewer.addSphere(p2, 0.005, 1, 0, 0, file_num) ;

                    ss << f_count;
                    file_num = ss.str();
                    f_count++;
    #endif


                    if( ( center_face.norm() < dist_from_center_extended ) && ( -dist > dot_with_normal_extended ) )
                    {
    #if(VISOPTION_SURFACE)
                        std::cout << " found the surface 1 .." << std::endl;
    #endif
                        is_belong_box = true;
                        processing_seg_num = s;
                        break;
                    }

                    center_face(0) = center_box_1(0) + length(a_count) * axis_box(0);
                    center_face(1) = center_box_1(1) + length(a_count) * axis_box(1);
                    center_face(2) = center_box_1(2) + length(a_count) * axis_box(2);

                    p2.x = center_face(0);
                    p2.y = center_face(1);
                    p2.z = center_face(2);

                    center_face = center_seg - center_face;
                    dist = axis_box(0) * normal_seg(0) + axis_box(1) * normal_seg(1) + axis_box(2) * normal_seg(2) ;

    #if(VISOPTION_SURFACE)

                    std::cout << " dist2  = " << center_face.norm() << std::endl;
                    std::cout << " dot product = " << dist << std::endl;
                    viewer.addSphere(p2, 0.005, 0, 1, 0, file_num) ;

                    ss << f_count;
                    file_num = ss.str();
                    f_count++;
    #endif


                    if( ( center_face.norm() < dist_from_center_extended ) && ( dist > dot_with_normal_extended ) )
                    {
    #if(VISOPTION_SURFACE)
                        std::cout << " found the surface 2.." << std::endl;
    #endif
                        is_belong_box = true;
                        processing_seg_num = s;
                        break;
                    }

                }

    #if(VISOPTION_SURFACE)
                std::cout << " close the window cloud_viewer to continue......." << std::endl;
                while(!viewer.wasStopped())
                    viewer.spinOnce();
                std::cout << " is_belong_box " << is_belong_box << std::endl;
    #endif

                if( is_belong_box )
            {
    #if(VISOPTION_SURFACE)
                    std::cout << " is_belong_box = " << is_belong_box << " processing segment ..." << processing_seg_num << std::endl;
    #endif
                    center_box = segRegion[processing_seg_num].getrepresentative_position();
                    normal_dir = segRegion[processing_seg_num].getrepresentative_normal();
                    Eigen::Vector3d axis_length_1 =  segRegion[processing_seg_num].getcut_axis();
                    Eigen::Matrix3f axis_seg = segRegion[processing_seg_num].get_axis() ;


                   int other_dir = 0;
                   bool is_handle_found = false;
                   for( int l = 1; l >= 0; l-- )
                   {
                       const double axis_length = axis_length_1(other_dir); // major axis dir / length
                       axis = axis_seg.col( l ) ; //set current axis
                       const float rad = (float)axis_length_1(l);  // minor axis dir / length

                       curv_axis(0) = (double)axis(0);
                       curv_axis(1) = (double)axis(1);
                       curv_axis(2) = (double)axis(2);
                       rest_dir = normal_dir.cross(curv_axis);

//                       const double maxHandAperture = this->maxHandAperture; //rad + 0.02;
                       double maxHandAperture;
                       if( this->maxHandAperture > ( rad + 0.02 ) )
                           maxHandAperture = rad + 0.02;
                       else
                           maxHandAperture = this->maxHandAperture;

                       double outer_sample_radius = ( maxHandAperture + handle_gap ) * ( maxHandAperture + handle_gap) + ( finger_length_max * finger_length_max ) ;
                       outer_sample_radius = sqrt(outer_sample_radius);
       #if(VISOPTION_SURFACE)
                       std::cout << " current axis = " << l << " axis_length = " << other_dir << std::endl;
                       std::cout << " axis_length " << axis_length << " finger_width " << finger_width << " diff " << (axis_length - finger_width/2) <<  std::endl;
       #endif
                       for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                       {

                           center(0) = center_box(0) - ( rest_dir(0) * g);
                           center(1) = center_box(1) - ( rest_dir(1) * g);
                           center(2) = center_box(2) - ( rest_dir(2) * g);

                           center1.x = center(0) ;
                           center1.y = center(1) ;
                           center1.z = center(2) ;
           #if(VISOPTION_SURFACE)
                           std::cout << " center = " << center << std::endl;
           #endif
                           shell.setRadius( (double)rad );
                           shell.setCentroid( center );  //set center
                           shell.setCurvatureAxis( curv_axis );
                           shell.setExtent( finger_width );
                           shell.setNormal( normal_dir );

                           tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );
    //                       int num_smallest = 20;

                               int no_point_surface = 0;
                               std::vector<std::pair<float,int> > dist_from_center_right_dir;
                               std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                               Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointXYZRGB pt, pt2;

                               for( int u = 0; u < nn_indices.size(); u++ )
                               {
                                        PointXYZ pt_xyz;
                                        pt_xyz = ptCloud->points[nn_indices[u]];
           #if(VISOPTION_SURFACE)
                                        pt.r = 255;
                                        pt.g = 255;
                                        pt.b = 0;
                                        pt.x = pt_xyz.x;
                                        pt.y = pt_xyz.y;
                                        pt.z = pt_xyz.z;
//                                        grasp_cloud->points.push_back(pt);
           #endif

                                        Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                        v = cropped - center;
                                        double along_rest_axis_Dist = rest_dir.dot(v);

                                        if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                        {
                                            //project points on a plane with normal along rest dir
                                            temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

           #if(VISOPTION_SURFACE)
                                            pt.r = 255;
                                            pt.g = 0;
                                            pt.b = 255;
                                            grasp_cloud->points.push_back(pt);

                                            pt2.x = temp_restDir(0);
                                            pt2.y = temp_restDir(1);
                                            pt2.z = temp_restDir(2);
           #endif


                                            // project on the plane parallel to normal _dir
                                            v = temp_restDir - center;
                                            double along_axis_Dist = curv_axis.dot(v);
                                            temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                            //calculate the distance of the projected points
                                            temp_axisDir = temp_axisDir - center;

                                            if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                            {
    #if(VISOPTION_SURFACE)
                                                pt.r = 0;
                                                pt.g = 255;
                                                pt.b = 255;
                                                grasp_cloud->points.push_back(pt);
    #endif
                                                if( (temp_axisDir.norm() > 0.007) )
                                                {
                                                   dot_product_vector = temp_axisDir;
                                                   dot_product_vector.normalize();
                                                   along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                                   if( along_axis_Dist > 0.7)
                                                   {

                                                       v = temp_restDir - center;
                                                       along_axis_Dist = normal_dir.dot(v);
                                                       temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

#if(VISOPTION_SURFACE)
                                                   pt2.r = 0;
                                                   pt2.g = 0;
                                                   pt2.b = 255;
                                                  cloudVis->points.push_back(pt2);

                                                   pt2.r = 255;
                                                   pt2.g = 255;
                                                   pt2.b = 0;
                                                   pt2.x = temp_normalDir(0);
                                                   pt2.y = temp_normalDir(1);
                                                   pt2.z = temp_normalDir(2);
                                                   cloudVis2->points.push_back(pt2);
#endif

                                                       temp_normalDir = temp_normalDir - center;
                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_normalDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_wrong_dir.push_back(temp_dist);

                                                   }
                                                   else if ( along_axis_Dist < -0.7)
                                                   {
           //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                       //project on the plane along axis direction
                                                       v = temp_restDir - center;
                                                       along_axis_Dist = normal_dir.dot(v);
                                                       temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
    #if(VISOPTION_SURFACE)
                                                       pt2.r = 0;
                                                       pt2.g = 0;
                                                       pt2.b = 255;
                                                      cloudVis->points.push_back(pt2);

                                                       pt2.r = 200;
                                                       pt2.g = 100;
                                                       pt2.b = 0;
                                                       pt2.x = temp_normalDir(0);
                                                       pt2.y = temp_normalDir(1);
                                                       pt2.z = temp_normalDir(2);
                                                       cloudVis2->points.push_back(pt2);
    #endif
                                                       temp_normalDir = temp_normalDir - center;

                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_normalDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_right_dir.push_back(temp_dist);

                                                   }
                                                }
                                                else
                                                {
                                                    no_point_surface ++;

                                                    v = temp_restDir - center;
                                                    along_axis_Dist = normal_dir.dot(v);
                                                    temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

    //                                                pt2.r = 0;
    //                                                pt2.g = 255;
    //                                                pt2.b = 0;
    //                                                pt2.x = temp_normalDir(0);
    //                                                pt2.y = temp_normalDir(1);
    //                                                pt2.z = temp_normalDir(2);
    //                                                cloudVis2->points.push_back(pt2);

                                                    temp_normalDir = temp_normalDir - center;
                                                    std::pair<float,int> temp_dist;
                                                    temp_dist.first = temp_normalDir.norm();
                                                    temp_dist.second = nn_indices[u];
                                                    dist_from_center_right_dir.push_back( temp_dist );
           #if(VISOPTION_SURFACE)
                                                    pt2.r = 255;
                                                    pt2.g = 0;
                                                    pt2.b = 0;
                                                    cloudVis->points.push_back(pt2);
           #endif
                                                }

                                            }

                                        }

                               }


                               if(  no_point_surface > num_sample  )
                               {

//                                   if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                                   if(1)
                                   {
                                        if( dist_from_center_right_dir.size() > num_sample )
                                        {
                                            std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );
                                            std::sort( dist_from_center_wrong_dir.begin(), dist_from_center_wrong_dir.end() );

                                            //check how many number of point is there in the gap and set shell radius to be r
                                            int startPos = -1, prev_start = 0;
                                            int num_of_points_in_gap_positive_points = 0;
                                            int num_of_points_in_gap_negative_points = 0;
    //                                        bool isHandleFound = false;
                                            for( float r = rad - 0.01 ; r <= maxHandAperture; r += 0.001 )
                                            {
                                                num_of_points_in_gap_positive_points = 0;
                                                num_of_points_in_gap_negative_points = 0;
                                                startPos = -1;
                                                for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                                 {

                                                     if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                     {
                                                         if( startPos == -1 )
                                                             startPos = c_loop;

                                                         if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                            num_of_points_in_gap_negative_points++;
                                                         else
                                                             num_of_points_in_gap_positive_points++;

                                                     }

                                                 }

                                                int num_of_point_wrong_dir = 0;
                                                for( int g_count = 0; g_count < dist_from_center_wrong_dir.size(); g_count++ )
                                                {

                                                    if( ( dist_from_center_wrong_dir[g_count].first <= ( r +  handle_gap )  ) )
                                                        num_of_point_wrong_dir++;

                                                }


    #if(VISOPTION_SURFACE)
                                          std::cout << " num_of_point_wrong_dir = " << num_of_point_wrong_dir << std::endl;
                                          std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                          std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
    #endif
                                                    if( startPos != -1)
                                                      prev_start = startPos ;

                                                    // break from the loop is the handle contains negative points
                                                    if( num_of_points_in_gap_negative_points > point_neg )
                                                    {
                                                        std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                       break;
                                                    }

                                                if(  (num_of_points_in_gap_positive_points > num_smallest)
                                                     || (num_of_points_in_gap_negative_points > num_smallest)
                                                     || (num_of_point_wrong_dir > num_smallest ) )
                                                    continue;

                                                 if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                                 {
                                                     shell.setRadius(r);
                                                     final_shell = shell;
                                                     is_handle_found = true ;
                                                     break;
                                                 }

                                                 //break from the loop if there is no positive points in gap
                                                 if( num_of_points_in_gap_positive_points == 0 )
                                                    break;


                                             }


                                        }
                                        else
                                        {
                           #if(VISOPTION_SURFACE)
                                       std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                           #endif

                                        }

                                    }
                                   else
                                   {

                           #if(VISOPTION_SURFACE)
                                       std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                           #endif
                                   }
                               }
                               else
                               {
                       #if(VISOPTION_SURFACE)
                                   std::cout << " not enough samples to be a handle... " << std::endl;
                       #endif
                               }

    //                           if( is_handle_found)
                               {
           #if(VISOPTION_SURFACE)
                                   std::cout << " no_point_surface " << no_point_surface << std::endl;
                                   std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                   std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                   std::cout << " no_point_surface " << no_point_surface << std::endl;
                                   std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                   std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                   Eigen::Vector3d center_vis = final_shell.getCentroid();
                                   Eigen::Vector3d normal_vis = final_shell.getNormal();
                                   pcl::PointXYZ p3,p4;
                                   p3.x = center_vis(0);
                                   p3.y = center_vis(1);
                                   p3.z = center_vis(2);

                                   p4.x = center_vis(0) + normal_vis(0) * 0.05;
                                   p4.y = center_vis(1) + normal_vis(1) * 0.05;
                                   p4.z = center_vis(2) + normal_vis(2) * 0.05;

                                   pcl::visualization::PCLVisualizer viewer("handle");
                                   viewer.addPointCloud(ptCloud_RGB);
                                   viewer.addPointCloud(grasp_cloud, "c2");
                                   viewer.addLine(p3, p4, 0, 255, 0);
                                       viewer.addPointCloud(cloudVis, "c1");
                                       viewer.addPointCloud(cloudVis2, "c3");
                                   std::cout << " close the window cloud_viewer to continue......." << std::endl;
                                   while(!viewer.wasStopped())
                                       viewer.spinOnce();
           #endif
                                    break;
                               }

                       }

                       if ( !is_handle_found )
                       {
    #if(VISOPTION_SURFACE)
                           std::cout << " checking in the opposite direction ..." << std::endl;
    #endif
                           for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                           {

                               center(0) = center_box(0) + ( rest_dir(0) * g);
                               center(1) = center_box(1) + ( rest_dir(1) * g);
                               center(2) = center_box(2) + ( rest_dir(2) * g);

                               center1.x = center(0) ;
                               center1.y = center(1) ;
                               center1.z = center(2) ;
               #if(VISOPTION_SURFACE)
                               std::cout << " center = " << center << std::endl;
               #endif
                               shell.setRadius( (double)rad );
                               shell.setCentroid( center );  //set center
                               shell.setCurvatureAxis( curv_axis );
                               shell.setExtent( finger_width );
                               shell.setNormal( normal_dir );

                               tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );
    //                           int num_smallest = 20;

                                   int no_point_surface = 0;
                                   std::vector<std::pair<float,int> > dist_from_center_right_dir;
                                   std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                                   Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointXYZRGB pt, pt2;

                                   for( int u = 0; u < nn_indices.size(); u++ )
                                   {
                                            PointXYZ pt_xyz;
                                            pt_xyz = ptCloud->points[nn_indices[u]];
               #if(VISOPTION_SURFACE)
                                            pt.r = 255;
                                            pt.g = 255;
                                            pt.b = 0;
                                            pt.x = pt_xyz.x;
                                            pt.y = pt_xyz.y;
                                            pt.z = pt_xyz.z;
    //                                        grasp_cloud->points.push_back(pt);
               #endif

                                            Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                            v = cropped - center;
                                            double along_rest_axis_Dist = rest_dir.dot(v);

                                            if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                            {
                                                //project points on a plane with normal along rest dir
                                                temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

               #if(VISOPTION_SURFACE)
                                                pt.r = 255;
                                                pt.g = 0;
                                                pt.b = 255;
                                                grasp_cloud->points.push_back(pt);

                                                pt2.x = temp_restDir(0);
                                                pt2.y = temp_restDir(1);
                                                pt2.z = temp_restDir(2);
               #endif


                                                // project on the plane parallel to normal _dir
                                                v = temp_restDir - center;
                                                double along_axis_Dist = curv_axis.dot(v);
                                                temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                                //calculate the distance of the projected points
                                                temp_axisDir = temp_axisDir - center;

                                                if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                                {
        #if(VISOPTION_SURFACE)
                                                    pt.r = 0;
                                                    pt.g = 255;
                                                    pt.b = 255;
                                                    grasp_cloud->points.push_back(pt);
        #endif
                                                    if( (temp_axisDir.norm() > 0.007) )
                                                    {
                                                       dot_product_vector = temp_axisDir;
                                                       dot_product_vector.normalize();
                                                       along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                                       if( along_axis_Dist > 0.7)
                                                       {

                                                           v = temp_restDir - center;
                                                           along_axis_Dist = normal_dir.dot(v);
                                                           temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

    #if(VISOPTION_SURFACE)
                                                       pt2.r = 0;
                                                       pt2.g = 0;
                                                       pt2.b = 255;
                                                      cloudVis->points.push_back(pt2);

                                                       pt2.r = 255;
                                                       pt2.g = 255;
                                                       pt2.b = 0;
                                                       pt2.x = temp_normalDir(0);
                                                       pt2.y = temp_normalDir(1);
                                                       pt2.z = temp_normalDir(2);
                                                       cloudVis2->points.push_back(pt2);
    #endif

                                                           temp_normalDir = temp_normalDir - center;
                                                           std::pair<float,int> temp_dist;
                                                           temp_dist.first = temp_normalDir.norm();
                                                           temp_dist.second = nn_indices[u];
                                                           dist_from_center_wrong_dir.push_back(temp_dist);

                                                       }
                                                       else if ( along_axis_Dist < -0.7)
                                                       {
               //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                           //project on the plane along axis direction
                                                           v = temp_restDir - center;
                                                           along_axis_Dist = normal_dir.dot(v);
                                                           temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
        #if(VISOPTION_SURFACE)
                                                           pt2.r = 0;
                                                           pt2.g = 0;
                                                           pt2.b = 255;
                                                          cloudVis->points.push_back(pt2);

                                                           pt2.r = 200;
                                                           pt2.g = 100;
                                                           pt2.b = 0;
                                                           pt2.x = temp_normalDir(0);
                                                           pt2.y = temp_normalDir(1);
                                                           pt2.z = temp_normalDir(2);
                                                           cloudVis2->points.push_back(pt2);
        #endif
                                                           temp_normalDir = temp_normalDir - center;

                                                           std::pair<float,int> temp_dist;
                                                           temp_dist.first = temp_normalDir.norm();
                                                           temp_dist.second = nn_indices[u];
                                                           dist_from_center_right_dir.push_back(temp_dist);

                                                       }
                                                    }
                                                    else
                                                    {
                                                        no_point_surface ++;

                                                        v = temp_restDir - center;
                                                        along_axis_Dist = normal_dir.dot(v);
                                                        temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

        //                                                pt2.r = 0;
        //                                                pt2.g = 255;
        //                                                pt2.b = 0;
        //                                                pt2.x = temp_normalDir(0);
        //                                                pt2.y = temp_normalDir(1);
        //                                                pt2.z = temp_normalDir(2);
        //                                                cloudVis2->points.push_back(pt2);

                                                        temp_normalDir = temp_normalDir - center;
                                                        std::pair<float,int> temp_dist;
                                                        temp_dist.first = temp_normalDir.norm();
                                                        temp_dist.second = nn_indices[u];
                                                        dist_from_center_right_dir.push_back( temp_dist );
               #if(VISOPTION_SURFACE)
                                                        pt2.r = 255;
                                                        pt2.g = 0;
                                                        pt2.b = 0;
                                                        cloudVis->points.push_back(pt2);
               #endif
                                                    }

                                                }

                                            }

                                   }


                                   if(  no_point_surface > num_sample  )
                                   {

    //                                   if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                                       if(1)
                                       {
                                            if( dist_from_center_right_dir.size() > num_sample )
                                            {
                                                std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );
                                                std::sort( dist_from_center_wrong_dir.begin(), dist_from_center_wrong_dir.end() );

                                                //check how many number of point is there in the gap and set shell radius to be r
                                                int startPos = -1, prev_start = 0;
                                                int num_of_points_in_gap_positive_points = 0;
                                                int num_of_points_in_gap_negative_points = 0;
        //                                        bool isHandleFound = false;
                                                for( float r = rad - 0.01 ; r <= maxHandAperture; r += 0.001 )
                                                {
                                                    num_of_points_in_gap_positive_points = 0;
                                                    num_of_points_in_gap_negative_points = 0;
                                                    startPos = -1;
                                                    for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                                     {

                                                         if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                         {
                                                             if( startPos == -1 )
                                                                 startPos = c_loop;

                                                             if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                                num_of_points_in_gap_negative_points++;
                                                             else
                                                                 num_of_points_in_gap_positive_points++;

                                                         }

                                                     }

                                                    int num_of_point_wrong_dir = 0;
                                                    for( int g_count = 0; g_count < dist_from_center_wrong_dir.size(); g_count++ )
                                                    {

                                                        if( ( dist_from_center_wrong_dir[g_count].first <= ( r +  handle_gap )  ) )
                                                            num_of_point_wrong_dir++;

                                                    }


        #if(VISOPTION_SURFACE)
                                              std::cout << " num_of_point_wrong_dir = " << num_of_point_wrong_dir << std::endl;
                                              std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                              std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
        #endif
                                                        if( startPos != -1)
                                                          prev_start = startPos ;

                                                        // break from the loop is the handle contains negative points
                                                        if( num_of_points_in_gap_negative_points > point_neg )
                                                        {
                                                            std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                           break;
                                                        }

                                                    if(  (num_of_points_in_gap_positive_points > num_smallest)
                                                         || (num_of_points_in_gap_negative_points > num_smallest)
                                                         || (num_of_point_wrong_dir > num_smallest) )
                                                        continue;

                                                     if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                                     {
                                                         shell.setRadius(r);
                                                         final_shell = shell;
                                                         is_handle_found = true ;
                                                         break;
                                                     }

                                                     //break from the loop if there is no positive points in gap
                                                     if( num_of_points_in_gap_positive_points == 0 )
                                                        break;


                                                 }


                                            }
                                            else
                                            {
                               #if(VISOPTION_SURFACE)
                                           std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                               #endif

                                            }

                                        }
                                       else
                                       {

                               #if(VISOPTION_SURFACE)
                                           std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                               #endif
                                       }
                                   }
                                   else
                                   {
                           #if(VISOPTION_SURFACE)
                                       std::cout << " not enough samples to be a handle... " << std::endl;
                           #endif
                                   }


  //                               if( is_handle_found)
                                   {
               #if(VISOPTION_SURFACE)
                                       std::cout << " no_point_surface " << no_point_surface << std::endl;
                                       std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                       std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                       std::cout << " no_point_surface " << no_point_surface << std::endl;
                                       std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                       std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                       Eigen::Vector3d center_vis = final_shell.getCentroid();
                                       Eigen::Vector3d normal_vis = final_shell.getNormal();
                                       pcl::PointXYZ p3,p4;
                                       p3.x = center_vis(0);
                                       p3.y = center_vis(1);
                                       p3.z = center_vis(2);

                                       p4.x = center_vis(0) + normal_vis(0) * 0.05;
                                       p4.y = center_vis(1) + normal_vis(1) * 0.05;
                                       p4.z = center_vis(2) + normal_vis(2) * 0.05;

                                       pcl::visualization::PCLVisualizer viewer("handle");
                                       viewer.addPointCloud(ptCloud_RGB);
                                       viewer.addPointCloud(grasp_cloud, "c2");
                                       viewer.addLine(p3, p4, 0, 255, 0);
                                           viewer.addPointCloud(cloudVis, "c1");
                                           viewer.addPointCloud(cloudVis2, "c3");
                                       std::cout << " close the window cloud_viewer to continue......." << std::endl;
                                       while(!viewer.wasStopped())
                                           viewer.spinOnce();
               #endif
                                        break;
                                   }
                           }


                       }


                       //increment
                       other_dir++;

                       //save handle
                       if( is_handle_found )
                       {
    #if(VISOPTION_SURFACE)
                           std::cout << " handle found .. " << std::endl;
    #endif
                           box_grasp.push_back(final_shell);
                           break;
                       }
                   }

           }

            }

        }

        return box_grasp;

}


std::vector<CylindricalShell> SurfacePatch::
grasp_localization_without_box_final( const std::vector<PointCloudIndividual> &segRegion,
                                const PointCloudXYZ::Ptr &ptCloud,
                                const PointCloudRGB::Ptr &ptCloud_RGB,
                                const std::vector<int> segnum)
{
    std::vector<CylindricalShell> box_grasp;
    //check inputs
    if( ( ptCloud->points.size() == 0 ) || (segRegion.size() == 0) )
    {
        std::cout << " in function grasp_localization_box (ptCloud->points.size() == 0 ) || (segRegion.size() == 0).." << std::endl;
        box_grasp.resize(0);
        return box_grasp;
    }


    Eigen::Vector3f axis;
    pcl::PointXYZ center1;
    const int points_in_gap_allowed = this->points_in_gap_allowed;
    int num_smallest = points_in_gap_allowed;
    const int point_neg = this->point_neg ; // 20;
    const int num_sample = 30;
    const double handle_gap = this->handle_gap;
    float finger_width = this->finger_width;  // original + error_to_be allowed
    const float finger_length_min = 0.015 ;
    const float finger_length_max = FINGER_LENGTH_MAX;



        //fitted box parameters
        Eigen::Vector3d curv_axis, center, center_box, normal_dir, rest_dir ;
        Eigen::Vector3d dot_product_vector;

        //set parameter for shell
        CylindricalShell shell, final_shell;
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
        tree->setInputCloud(ptCloud);
        std::vector<int> nn_indices;
        std::vector<float> nn_dists;

        for( int w = 0 ; w < segnum.size(); w++ )
        {

   #if(VISOPTION_SURFACE)
           std::cout << " processing segment " << segnum[w] << std::endl;
   #endif

           int processing_seg_num = segnum[w] ;
           center_box = segRegion[processing_seg_num].getrepresentative_position();
           normal_dir = segRegion[processing_seg_num].getrepresentative_normal();
           Eigen::Vector3d axis_length_1 =  segRegion[processing_seg_num].getcut_axis();
           Eigen::Matrix3f axis_seg = segRegion[processing_seg_num].get_axis() ;
           std::vector<int> nnIndices = segRegion[processing_seg_num].getNNIndices();

           float rad_curv = segRegion[processing_seg_num].get_curvature();
           Eigen::Vector3d var = segRegion[processing_seg_num].get_seg_var();
           float var_s = sqrt( (var(0) * var(0)) + (var(1) * var(1)) + (var(2) * var(2))  );

           bool isCylinder ;
           //decide whether to fit cylinder or box based on curvature and var
           {
               if( rad_curv < 0.05 )
               {
                   isCylinder = true;
               }
               else if( rad_curv < 0.1 )
               {
                   if( var_s > 0.1 )
                       isCylinder = true;
                   else
                       isCylinder = false;
               }
               else
               {
                   isCylinder = false;
               }
           }

           if( (segRegion[processing_seg_num].get_use_the_segment()) && (!isCylinder) )
           {

#if(VISOPTION_SURFACE)
               std::cout << normal_dir(2) << " size = " << nnIndices.size() << std::endl;
#endif

               if( ( normal_dir(2) < -this->thr_direction )  && ( nnIndices.size() > 100 ) )
               {
                    std::cout << " processing_seg_num = " << processing_seg_num << std::endl;

                   int other_dir = 0;
                   bool is_handle_found = false;
                   for( int l = 1; l >= 0; l-- )
                   {

                       const double axis_length = axis_length_1(other_dir); // major axis dir / length
                       axis = axis_seg.col( l ) ; //set current axis
                       const float rad = (float)axis_length_1(l);  // minor axis dir / length

                       curv_axis(0) = (double)axis(0);
                       curv_axis(1) = (double)axis(1);
                       curv_axis(2) = (double)axis(2);
                       rest_dir = normal_dir.cross(curv_axis);

//                       const double maxHandAperture = this->maxHandAperture; //rad + 0.015;

                       double maxHandAperture;
                       if( this->maxHandAperture > ( rad + 0.02 ) )
                           maxHandAperture = rad + 0.02;
                       else
                           maxHandAperture = this->maxHandAperture;

                       double outer_sample_radius = ( maxHandAperture + handle_gap ) * ( maxHandAperture + handle_gap) + ( finger_length_max * finger_length_max ) ;
                       outer_sample_radius = sqrt(outer_sample_radius);

       #if(VISOPTION_SURFACE)
                       std::cout << " current axis = " << l << " axis_length = " << other_dir << std::endl;
                       std::cout << " axis_length " << axis_length << " finger_width " << finger_width << " diff " << (axis_length - finger_width/2) <<  std::endl;
       #endif
                       for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                       {

                           center(0) = center_box(0) - ( rest_dir(0) * g);
                           center(1) = center_box(1) - ( rest_dir(1) * g);
                           center(2) = center_box(2) - ( rest_dir(2) * g);

                           center1.x = center(0) ;
                           center1.y = center(1) ;
                           center1.z = center(2) ;

       #if(VISOPTION_SURFACE)
                           std::cout << " center = " << center << std::endl;
        #endif
                           shell.setRadius( (double)rad );
                           shell.setCentroid( center );  //set center
                           shell.setCurvatureAxis( curv_axis );
                           shell.setExtent( finger_width );
                           shell.setNormal( normal_dir );

                           tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );


                               int no_point_surface = 0;
                               std::vector<std::pair<float,int> > dist_from_center_right_dir;
                               std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                               Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                               pcl::PointXYZRGB pt, pt2;

                               for( int u = 0; u < nn_indices.size(); u++ )
                               {
                                        PointXYZ pt_xyz;
                                        pt_xyz = ptCloud->points[nn_indices[u]];
           #if(VISOPTION_SURFACE)
                                        pt.r = 255;
                                        pt.g = 255;
                                        pt.b = 0;
                                        pt.x = pt_xyz.x;
                                        pt.y = pt_xyz.y;
                                        pt.z = pt_xyz.z;
//                                        grasp_cloud->points.push_back(pt);
           #endif

                                        Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                        v = cropped - center;
                                        double along_rest_axis_Dist = rest_dir.dot(v);

                                        if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                        {
                                            //project points on a plane with normal along rest dir
                                            temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

           #if(VISOPTION_SURFACE)
                                            pt.r = 255;
                                            pt.g = 0;
                                            pt.b = 255;
                                            grasp_cloud->points.push_back(pt);

                                            pt2.x = temp_restDir(0);
                                            pt2.y = temp_restDir(1);
                                            pt2.z = temp_restDir(2);
           #endif


                                            // project on the plane parallel to normal _dir
                                            v = temp_restDir - center;
                                            double along_axis_Dist = curv_axis.dot(v);
                                            temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                            //calculate the distance of the projected points
                                            temp_axisDir = temp_axisDir - center;

                                            if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                            {
    #if(VISOPTION_SURFACE)
                                                pt.r = 0;
                                                pt.g = 255;
                                                pt.b = 255;
                                                grasp_cloud->points.push_back(pt);
    #endif
                                                if( (temp_axisDir.norm() > 0.007) )
                                                {
                                                   dot_product_vector = temp_axisDir;
                                                   dot_product_vector.normalize();
                                                   along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                                   if( along_axis_Dist > 0.7)
                                                   {

                                                       v = temp_restDir - center;
                                                       along_axis_Dist = normal_dir.dot(v);
                                                       temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

#if(VISOPTION_SURFACE)
                                                   pt2.r = 0;
                                                   pt2.g = 0;
                                                   pt2.b = 255;
                                                  cloudVis->points.push_back(pt2);

                                                   pt2.r = 255;
                                                   pt2.g = 255;
                                                   pt2.b = 0;
                                                   pt2.x = temp_normalDir(0);
                                                   pt2.y = temp_normalDir(1);
                                                   pt2.z = temp_normalDir(2);
                                                   cloudVis2->points.push_back(pt2);
#endif

                                                       temp_normalDir = temp_normalDir - center;
                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_normalDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_wrong_dir.push_back(temp_dist);

                                                   }
                                                   else if ( along_axis_Dist < -0.7)
                                                   {
           //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                       //project on the plane along axis direction
                                                       v = temp_restDir - center;
                                                       along_axis_Dist = normal_dir.dot(v);
                                                       temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
    #if(VISOPTION_SURFACE)
                                                       pt2.r = 0;
                                                       pt2.g = 0;
                                                       pt2.b = 255;
                                                      cloudVis->points.push_back(pt2);

                                                       pt2.r = 200;
                                                       pt2.g = 100;
                                                       pt2.b = 0;
                                                       pt2.x = temp_normalDir(0);
                                                       pt2.y = temp_normalDir(1);
                                                       pt2.z = temp_normalDir(2);
                                                       cloudVis2->points.push_back(pt2);
    #endif
                                                       temp_normalDir = temp_normalDir - center;

                                                       std::pair<float,int> temp_dist;
                                                       temp_dist.first = temp_normalDir.norm();
                                                       temp_dist.second = nn_indices[u];
                                                       dist_from_center_right_dir.push_back(temp_dist);

                                                   }
                                                }
                                                else
                                                {
                                                    no_point_surface ++;

                                                    v = temp_restDir - center;
                                                    along_axis_Dist = normal_dir.dot(v);
                                                    temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

    //                                                pt2.r = 0;
    //                                                pt2.g = 255;
    //                                                pt2.b = 0;
    //                                                pt2.x = temp_normalDir(0);
    //                                                pt2.y = temp_normalDir(1);
    //                                                pt2.z = temp_normalDir(2);
    //                                                cloudVis2->points.push_back(pt2);

                                                    temp_normalDir = temp_normalDir - center;
                                                    std::pair<float,int> temp_dist;
                                                    temp_dist.first = temp_normalDir.norm();
                                                    temp_dist.second = nn_indices[u];
                                                    dist_from_center_right_dir.push_back( temp_dist );
           #if(VISOPTION_SURFACE)
                                                    pt2.r = 255;
                                                    pt2.g = 0;
                                                    pt2.b = 0;
                                                    cloudVis->points.push_back(pt2);
           #endif
                                                }

                                            }

                                        }

                               }


                               if(  no_point_surface > num_sample  )
                               {

//                                   if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                                   if(1)
                                   {
                                        if( dist_from_center_right_dir.size() > num_sample )
                                        {
                                            std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );
                                            std::sort( dist_from_center_wrong_dir.begin(), dist_from_center_wrong_dir.end() );

                                            //check how many number of point is there in the gap and set shell radius to be r
                                            int startPos = -1, prev_start = 0;
                                            int num_of_points_in_gap_positive_points = 0;
                                            int num_of_points_in_gap_negative_points = 0;
    //                                        bool isHandleFound = false;
                                            for( float r = rad - 0.01 ; r <= maxHandAperture; r += 0.001 )
                                            {
                                                num_of_points_in_gap_positive_points = 0;
                                                num_of_points_in_gap_negative_points = 0;
                                                startPos = -1;
                                                for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                                 {

                                                     if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                     {
                                                         if( startPos == -1 )
                                                             startPos = c_loop;

                                                         if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                            num_of_points_in_gap_negative_points++;
                                                         else
                                                             num_of_points_in_gap_positive_points++;

                                                     }

                                                 }

                                                int num_of_point_wrong_dir = 0;
                                                for( int g_count = 0; g_count < dist_from_center_wrong_dir.size(); g_count++ )
                                                {

                                                    if( ( dist_from_center_wrong_dir[g_count].first <= ( r +  handle_gap )  ) )
                                                        num_of_point_wrong_dir++;

                                                }


    #if(VISOPTION_SURFACE)
                                          std::cout << " num_of_point_wrong_dir = " << num_of_point_wrong_dir << std::endl;
                                          std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                          std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
    #endif
                                                    if( startPos != -1)
                                                      prev_start = startPos ;

                                                    // break from the loop is the handle contains negative points
                                                    if( num_of_points_in_gap_negative_points > point_neg )
                                                    {
                                                        std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                       break;
                                                    }

                                                if(  (num_of_points_in_gap_positive_points > num_smallest)
                                                     || (num_of_points_in_gap_negative_points > num_smallest)
                                                     || (num_of_point_wrong_dir > num_smallest) )
                                                    continue;

                                                 if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                                 {
                                                     shell.setRadius(r);
                                                     final_shell = shell;
                                                     is_handle_found = true ;
                                                     break;
                                                 }

                                                 //break from the loop if there is no positive points in gap
                                                 if( num_of_points_in_gap_positive_points == 0 )
                                                    break;


                                             }


                                        }
                                        else
                                        {
                           #if(VISOPTION_SURFACE)
                                       std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                           #endif

                                        }

                                    }
                                   else
                                   {

                           #if(VISOPTION_SURFACE)
                                       std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                           #endif
                                   }
                               }
                               else
                               {
                       #if(VISOPTION_SURFACE)
                                   std::cout << " not enough samples to be a handle... " << std::endl;
                       #endif
                               }

//                               if( is_handle_found)
                               {
           #if(VISOPTION_SURFACE)
                                   std::cout << is_handle_found << " no_point_surface " << no_point_surface << std::endl;
                                   std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                   std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                   Eigen::Vector3d center_vis = final_shell.getCentroid();
                                   Eigen::Vector3d normal_vis = final_shell.getNormal();
                                   pcl::PointXYZ p3,p4;
                                   p3.x = center_vis(0);
                                   p3.y = center_vis(1);
                                   p3.z = center_vis(2);

                                   p4.x = center_vis(0) + normal_vis(0) * 0.05;
                                   p4.y = center_vis(1) + normal_vis(1) * 0.05;
                                   p4.z = center_vis(2) + normal_vis(2) * 0.05;

                                   pcl::visualization::PCLVisualizer viewer("handle");
                                   viewer.addPointCloud(ptCloud_RGB);
                                   viewer.addPointCloud(grasp_cloud, "c2");
                                   viewer.addLine(p3, p4, 0, 255, 0);
                                       viewer.addPointCloud(cloudVis, "c1");
                                       viewer.addPointCloud(cloudVis2, "c3");
                                   std::cout << " close the window cloud_viewer to continue......." << std::endl;
                                   while(!viewer.wasStopped())
                                       viewer.spinOnce();

           #endif
                                    break;
                               }

                       }

                       if ( !is_handle_found )
                       {
    #if(VISOPTION_SURFACE)
                           std::cout << " checking in the opposite direction ..." << std::endl;
    #endif
                           for( double g = 0.0; g <= (axis_length - finger_width/2.0); g+= 0.01)
                           {

                               center(0) = center_box(0) + ( rest_dir(0) * g);
                               center(1) = center_box(1) + ( rest_dir(1) * g);
                               center(2) = center_box(2) + ( rest_dir(2) * g);

                               center1.x = center(0) ;
                               center1.y = center(1) ;
                               center1.z = center(2) ;
               #if(VISOPTION_SURFACE)
                               std::cout << " center = " << center << std::endl;
               #endif
                               shell.setRadius( (double)rad );
                               shell.setCentroid( center );  //set center
                               shell.setCurvatureAxis( curv_axis );
                               shell.setExtent( finger_width );
                               shell.setNormal( normal_dir );

                               tree->radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );
    //                           int num_smallest = 20;

                                   int no_point_surface = 0;
                                   std::vector<std::pair<float,int> > dist_from_center_right_dir;
                                   std::vector<std::pair<float,int> > dist_from_center_wrong_dir;
                                   Eigen::Vector3d v, temp_restDir, temp_axisDir, temp_normalDir;

                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud_1( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr grasp_cloud_2( new pcl::PointCloud<pcl::PointXYZRGB>() );
                                   pcl::PointXYZRGB pt, pt2;

                                   for( int u = 0; u < nn_indices.size(); u++ )
                                   {
                                            PointXYZ pt_xyz;
                                            pt_xyz = ptCloud->points[nn_indices[u]];
               #if(VISOPTION_SURFACE)
                                            pt.r = 255;
                                            pt.g = 255;
                                            pt.b = 0;
                                            pt.x = pt_xyz.x;
                                            pt.y = pt_xyz.y;
                                            pt.z = pt_xyz.z;
    //                                        grasp_cloud->points.push_back(pt);
               #endif

                                            Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                            v = cropped - center;
                                            double along_rest_axis_Dist = rest_dir.dot(v);

                                            if( fabs(along_rest_axis_Dist) < finger_width / 2.0 )
                                            {
                                                //project points on a plane with normal along rest dir
                                                temp_restDir = cropped - (along_rest_axis_Dist * rest_dir);

               #if(VISOPTION_SURFACE)
                                                pt.r = 255;
                                                pt.g = 0;
                                                pt.b = 255;
                                                grasp_cloud->points.push_back(pt);

                                                pt2.x = temp_restDir(0);
                                                pt2.y = temp_restDir(1);
                                                pt2.z = temp_restDir(2);
               #endif


                                                // project on the plane parallel to normal _dir
                                                v = temp_restDir - center;
                                                double along_axis_Dist = curv_axis.dot(v);
                                                temp_axisDir = temp_restDir - (along_axis_Dist * curv_axis);

                                                //calculate the distance of the projected points
                                                temp_axisDir = temp_axisDir - center;

                                                if(  temp_axisDir.norm() < finger_length_min /*finger_length_max*/ )
                                                {
        #if(VISOPTION_SURFACE)
                                                    pt.r = 0;
                                                    pt.g = 255;
                                                    pt.b = 255;
                                                    grasp_cloud->points.push_back(pt);
        #endif
                                                    if( (temp_axisDir.norm() > 0.007) )
                                                    {
                                                       dot_product_vector = temp_axisDir;
                                                       dot_product_vector.normalize();
                                                       along_axis_Dist = normal_dir.dot(dot_product_vector) ;

                                                       if( along_axis_Dist > 0.7)
                                                       {

                                                           v = temp_restDir - center;
                                                           along_axis_Dist = normal_dir.dot(v);
                                                           temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

    #if(VISOPTION_SURFACE)
                                                       pt2.r = 0;
                                                       pt2.g = 0;
                                                       pt2.b = 255;
                                                      cloudVis->points.push_back(pt2);

                                                       pt2.r = 255;
                                                       pt2.g = 255;
                                                       pt2.b = 0;
                                                       pt2.x = temp_normalDir(0);
                                                       pt2.y = temp_normalDir(1);
                                                       pt2.z = temp_normalDir(2);
                                                       cloudVis2->points.push_back(pt2);
    #endif

                                                           temp_normalDir = temp_normalDir - center;
                                                           std::pair<float,int> temp_dist;
                                                           temp_dist.first = temp_normalDir.norm();
                                                           temp_dist.second = nn_indices[u];
                                                           dist_from_center_wrong_dir.push_back(temp_dist);

                                                       }
                                                       else if ( along_axis_Dist < -0.7)
                                                       {
               //                                            std::cout << " along_axis_Dist right direction = " << along_axis_Dist << std::endl;
                                                           //project on the plane along axis direction
                                                           v = temp_restDir - center;
                                                           along_axis_Dist = normal_dir.dot(v);
                                                           temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);
        #if(VISOPTION_SURFACE)
                                                           pt2.r = 0;
                                                           pt2.g = 0;
                                                           pt2.b = 255;
                                                          cloudVis->points.push_back(pt2);

                                                           pt2.r = 200;
                                                           pt2.g = 100;
                                                           pt2.b = 0;
                                                           pt2.x = temp_normalDir(0);
                                                           pt2.y = temp_normalDir(1);
                                                           pt2.z = temp_normalDir(2);
                                                           cloudVis2->points.push_back(pt2);
        #endif
                                                           temp_normalDir = temp_normalDir - center;

                                                           std::pair<float,int> temp_dist;
                                                           temp_dist.first = temp_normalDir.norm();
                                                           temp_dist.second = nn_indices[u];
                                                           dist_from_center_right_dir.push_back(temp_dist);

                                                       }
                                                    }
                                                    else
                                                    {
                                                        no_point_surface ++;

                                                        v = temp_restDir - center;
                                                        along_axis_Dist = normal_dir.dot(v);
                                                        temp_normalDir = temp_restDir - (along_axis_Dist * normal_dir);

        //                                                pt2.r = 0;
        //                                                pt2.g = 255;
        //                                                pt2.b = 0;
        //                                                pt2.x = temp_normalDir(0);
        //                                                pt2.y = temp_normalDir(1);
        //                                                pt2.z = temp_normalDir(2);
        //                                                cloudVis2->points.push_back(pt2);

                                                        temp_normalDir = temp_normalDir - center;
                                                        std::pair<float,int> temp_dist;
                                                        temp_dist.first = temp_normalDir.norm();
                                                        temp_dist.second = nn_indices[u];
                                                        dist_from_center_right_dir.push_back( temp_dist );
               #if(VISOPTION_SURFACE)
                                                        pt2.r = 255;
                                                        pt2.g = 0;
                                                        pt2.b = 0;
                                                        cloudVis->points.push_back(pt2);
               #endif
                                                    }

                                                }

                                            }

                                   }


                                   if(  no_point_surface > num_sample  )
                                   {

    //                                   if( dist_from_center_wrong_dir.size() <= points_in_gap_allowed )
                                       if(1)
                                       {
                                            if( dist_from_center_right_dir.size() > num_sample )
                                            {
                                                std::sort( dist_from_center_right_dir.begin(), dist_from_center_right_dir.end() );
                                                std::sort( dist_from_center_wrong_dir.begin(), dist_from_center_wrong_dir.end() );

                                                //check how many number of point is there in the gap and set shell radius to be r
                                                int startPos = -1, prev_start = 0;
                                                int num_of_points_in_gap_positive_points = 0;
                                                int num_of_points_in_gap_negative_points = 0;
        //                                        bool isHandleFound = false;
                                                for( float r = rad - 0.01 ; r <= maxHandAperture; r += 0.001 )
                                                {
                                                    num_of_points_in_gap_positive_points = 0;
                                                    num_of_points_in_gap_negative_points = 0;
                                                    startPos = -1;
                                                    for( int c_loop = prev_start; c_loop < dist_from_center_right_dir.size(); c_loop++ )
                                                     {

                                                         if( ( dist_from_center_right_dir[c_loop].first > r ) && ( dist_from_center_right_dir[c_loop].first <= ( r +  handle_gap )  ) )
                                                         {
                                                             if( startPos == -1 )
                                                                 startPos = c_loop;

                                                             if( point_labels_rg_[dist_from_center_right_dir[c_loop].second] == -1 )
                                                                num_of_points_in_gap_negative_points++;
                                                             else
                                                                 num_of_points_in_gap_positive_points++;

                                                         }

                                                     }

                                                    int num_of_point_wrong_dir = 0;
                                                    for( int g_count = 0; g_count < dist_from_center_wrong_dir.size(); g_count++ )
                                                    {

                                                        if(  ( dist_from_center_wrong_dir[g_count].first <= ( r +  handle_gap )  ) )
                                                            num_of_point_wrong_dir++;

                                                    }


        #if(VISOPTION_SURFACE)
                                              std::cout << " num_of_point_wrong_dir = " << num_of_point_wrong_dir << std::endl;
                                              std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                                              std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
        #endif
                                                        if( startPos != -1)
                                                          prev_start = startPos ;

                                                        // break from the loop is the handle contains negative points
                                                        if( num_of_points_in_gap_negative_points > point_neg )
                                                        {
                                                            std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                                                           break;
                                                        }

                                                    if(  (num_of_points_in_gap_positive_points > num_smallest)
                                                         || (num_of_points_in_gap_negative_points > num_smallest)
                                                         || (num_of_point_wrong_dir > num_smallest) )
                                                        continue;

                                                     if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                                                     {
                                                         shell.setRadius(r);
                                                         final_shell = shell;
                                                         is_handle_found = true ;
                                                         break;
                                                     }

                                                     //break from the loop if there is no positive points in gap
                                                     if( num_of_points_in_gap_positive_points == 0 )
                                                        break;


                                                 }


                                            }
                                            else
                                            {
                               #if(VISOPTION_SURFACE)
                                           std::cout << " surface is not graspable .. " << dist_from_center_right_dir.size() << std::endl;
                               #endif

                                            }

                                        }
                                       else
                                       {

                               #if(VISOPTION_SURFACE)
                                           std::cout << " not convex surface to be a handle... "<< dist_from_center_wrong_dir.size() << std::endl;
                               #endif
                                       }
                                   }
                                   else
                                   {
                           #if(VISOPTION_SURFACE)
                                       std::cout << " not enough samples to be a handle... " << std::endl;
                           #endif
                                   }

//                                   if( is_handle_found)
                                   {
               #if(VISOPTION_SURFACE)
                                       std::cout << is_handle_found << " no_point_surface " << no_point_surface << std::endl;
                                       std::cout << " no_point in the wrong direction " << dist_from_center_wrong_dir.size() << std::endl;
                                       std::cout << " no_point in right dirctioin " << dist_from_center_right_dir.size() << std::endl;
                                       Eigen::Vector3d center_vis = final_shell.getCentroid();
                                       Eigen::Vector3d normal_vis = final_shell.getNormal();
                                       pcl::PointXYZ p3,p4;
                                       p3.x = center_vis(0);
                                       p3.y = center_vis(1);
                                       p3.z = center_vis(2);

                                       p4.x = center_vis(0) + normal_vis(0) * 0.05;
                                       p4.y = center_vis(1) + normal_vis(1) * 0.05;
                                       p4.z = center_vis(2) + normal_vis(2) * 0.05;

                                       pcl::visualization::PCLVisualizer viewer("handle");
                                       viewer.addPointCloud(ptCloud_RGB);
                                       viewer.addPointCloud(grasp_cloud, "c2");
                                       viewer.addLine(p3, p4, 0, 255, 0);
                                           viewer.addPointCloud(cloudVis, "c1");
                                           viewer.addPointCloud(cloudVis2, "c3");
                                       std::cout << " close the window cloud_viewer to continue......." << std::endl;
                                       while(!viewer.wasStopped())
                                           viewer.spinOnce();

               #endif
                                        break;
                                   }
                           }


                       }


                       //increment
                       other_dir++;

                       //save handle
                       if( is_handle_found )
                       {
    #if(VISOPTION_SURFACE)
                           std::cout << " handle found .. " << std::endl;
    #endif
                           box_grasp.push_back(final_shell);
                           break;
                       }
                   }

               }

           }

        }

           std::cout << " number of grasp region found in the segment on which box is fitted.. " << box_grasp.size() << std::endl;
           std::cout << std::endl << " checking in  other segments" << std::endl;

        return box_grasp;

}




CylindricalShell SurfacePatch::
grasp_localization_cylinder_bestfit( const std::vector<PointCloudIndividual> &segRegion,
                         const PointCloudXYZ::Ptr &ptCloud,
                         Cylinder_Param cylind )
{
    int points_in_gap_allowed = this->points_in_gap_allowed;
    float finger_width = this->finger_width; // original + error_to_be allowed
    Eigen::Vector3f axis_array = cylind.axis;
    double rad = (double)cylind.radius;
    PointXYZ center1 = cylind.center;
    float axis_length = cylind.axis_length;

    Eigen::Vector3d axis, center;
    axis(0) = axis_array(0);
    axis(1) = axis_array(1);
    axis(2) = axis_array(2);

    center(0) = center1.x; // center1 contains origin
    center(1) = center1.y;
    center(2) = center1.z;

    double maxHandAperture = rad + this->maxHandAperture;
    double handle_gap = this->handle_gap;
    double outer_sample_radius = 1.0 * (maxHandAperture + handle_gap);
    pcl::KdTreeFLANN<pcl::PointXYZ> tree;
    tree.setInputCloud(ptCloud);
    std::vector<int> nn_indices;
    std::vector<float> nn_dists;


    CylindricalShell shell, final_shell;
    shell.setRadius(rad);
    shell.setCurvatureAxis(axis);
    shell.setExtent(finger_width);

    final_shell.setRadius(-1.0);
    final_shell.setExtent(-1.0);

    // save the point best fit
    PointXYZ center_g;
    int num_smallest = 10;
    bool isfound = false;

    for( float g = 0.0; g <= (axis_length/2.5); g+= rad/4.0)
    {
        center(0) = center1.x - ( axis_array(0) * g);
        center(1) = center1.y - ( axis_array(1) * g);
        center(2) = center1.z - ( axis_array(2) * g);

        center_g.x = center(0);
        center_g.y = center(1);
        center_g.z = center(2);

        shell.setCentroid(center); // set center
        tree.radiusSearch( center_g, outer_sample_radius, nn_indices, nn_dists );

        std::vector<int> indices_to_be_tested;
        std::vector<float> dist_from_center;
        Eigen::Vector3d v, temp;
        for( int u = 0; u < nn_indices.size(); u++ )
        {
            Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
            v = cropped - center;
            double along_axis_Dist = axis.dot(v);
            if( fabs(along_axis_Dist) < finger_width / 2 )
            {
                indices_to_be_tested.push_back(nn_indices[u]);

                //project points on a plane with normal dir aloong cylinder axis
                temp = cropped - (along_axis_Dist * axis);

                //calculate the distance of the projected points
                temp = temp - center;

                dist_from_center.push_back(temp.norm());

            }
        }

        std::sort( dist_from_center.begin(), dist_from_center.end() );

        if( dist_from_center.size() > 20 )
        {
            //check how many number of point is there in the gap and set shell radius to be r
            int startPos = 0, endPos = 0, prev_start = 0;
            int num_of_points_in_gap ;
            for( float r = rad; r <= maxHandAperture; r += 0.001 )
            {
                 startPos = 0;
                 for( int c_loop = prev_start; c_loop < dist_from_center.size(); c_loop++ )
                 {
                     //taking care of initial search
                     if( startPos == 0 )
                     {
                         if( dist_from_center[c_loop] > r )
                             startPos = c_loop;
                     }
                     {
                         if( dist_from_center[c_loop] <= ( r +  handle_gap )  )
                             endPos = c_loop;
                     }

                 }
                 num_of_points_in_gap = endPos - startPos + 1;

                 if ( num_of_points_in_gap < num_smallest )
                 {
                     num_smallest = num_of_points_in_gap;
                     isfound = true;
                     shell.setRadius(r);
                     final_shell = shell;
                 }

                 if( num_of_points_in_gap <= points_in_gap_allowed )
                 {
                     shell.setRadius(r);
                     break;
                 }

                 prev_start = startPos ;
             }

//            std::cout << " num_of_points_in_gap = " << num_of_points_in_gap ;

            if( num_of_points_in_gap <= points_in_gap_allowed )
                break;

        }

    }

     return final_shell;

}


CylindricalShell SurfacePatch::
grasp_localization_cylinder_bestfit( const std::vector<PointCloudIndividual> &segRegion,
                                     const PointCloudXYZ::Ptr &ptCloud,
                                     const std::vector<int> localize_seg,
                                     Cylinder_Param cylind )
{
    int points_in_gap_allowed = this->points_in_gap_allowed;
    const float finger_length_min = 0.015 ;
    int num_smallest = points_in_gap_allowed;
    const int point_neg = this->point_neg; //20;
    float finger_width = this->finger_width; // original + error_to_be allowed
    Eigen::Vector3f axis_array = cylind.axis;
    double rad = (double)cylind.radius;
    PointXYZ center1 = cylind.center;
    float axis_length = cylind.axis_length;

    Eigen::Vector3d axis, center;
    axis(0) = axis_array(0);
    axis(1) = axis_array(1);
    axis(2) = axis_array(2);

    center(0) = center1.x; // center1 contains origin
    center(1) = center1.y;
    center(2) = center1.z;

    // the surface normal direction
    Eigen::Vector3d normal_seg;
    bool isNormalFound = false;
    float thr_dot = 0.01;
//    do
//    {
//        for(int n = 0; n < localize_seg.size(); n++ )
//        {
//            normal_seg = segRegion[localize_seg[n]].getrepresentative_normal();
//            double dot = normal_seg.dot(axis);
//            dot = abs(dot);

//            if( dot < thr_dot )
//            {
//                isNormalFound = true;
//                break;
//            }
//        }
//        if( !isNormalFound )
//        {
//#if(VISOPTION_SURFACE)
//            std::cout << " unable to proceed... normal direction is not found..at threshold " << thr_dot << std::endl;
//#endif
//        }

//        thr_dot = thr_dot + 0.01;

//    }while( !isNormalFound && (thr_dot < 0.4) );

    Eigen::MatrixXd seg_normals(localize_seg.size(), 3);
    Eigen::Vector3d seg_normal;
    int total_points = 0;
    isNormalFound = true;
    for(int n = 0; n < localize_seg.size(); n++ )
    {
        PointCloudIndividual obj = segRegion[localize_seg[n]];

//        seg_normal = obj.getrepresentative_normal();
//        std::cout << localize_seg[n] << " normal = " << seg_normal << std::endl;

        int points = obj.getNNIndices().size();
        seg_normals.row(n) = points * ( obj.getrepresentative_normal());
        total_points = total_points + points ;
    }

    // save the variables
    normal_seg(0) = (seg_normals.col(0).sum()) / (double)total_points;
    normal_seg(1) = (seg_normals.col(1).sum()) / (double)total_points;
    normal_seg(2) = (seg_normals.col(2).sum()) / (double)total_points;
    normal_seg.normalize();


    double maxHandAperture = this->maxHandAperture ;//rad + this->maxHandAperture;
    double handle_gap = this->handle_gap;
    double outer_sample_radius = 1.0 * (maxHandAperture + handle_gap);
    pcl::KdTreeFLANN<pcl::PointXYZ> tree;
    tree.setInputCloud(ptCloud);
    std::vector<int> nn_indices;
    std::vector<float> nn_dists;


    CylindricalShell shell, final_shell;
    shell.setRadius(rad);
    shell.setExtent(finger_width);
    shell.setNormal(normal_seg);

    //set axis
    Eigen::Vector3d width_axis;
    width_axis = axis.cross( normal_seg);
    shell.setCurvatureAxis( width_axis );

    final_shell.setRadius(-1.0);
    final_shell.setExtent(-1.0);

    if( isNormalFound )
    {
        // save the point best fit
        PointXYZ center_g;
        bool isfound = false;

        for( float g = 0.0; g <= (axis_length/2.5); g+= rad/4.0)
        {
            center(0) = center1.x - ( axis_array(0) * g);
            center(1) = center1.y - ( axis_array(1) * g);
            center(2) = center1.z - ( axis_array(2) * g);

            center_g.x = center(0);
            center_g.y = center(1);
            center_g.z = center(2);

            shell.setCentroid(center); // set center
            tree.radiusSearch( center_g, outer_sample_radius, nn_indices, nn_dists );

            pcl::PointCloud<pcl::PointXYZRGB>::Ptr visualize_cld( new pcl::PointCloud<pcl::PointXYZRGB>() );
            pcl::PointXYZRGB pt_rgb;

            std::vector<std::pair<float,int> > dist_from_center;
            Eigen::Vector3d v, temp;
            for( int u = 0; u < nn_indices.size(); u++ )
            {
                Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                v = cropped - center;
                double along_axis_Dist = axis.dot(v);
                double along_axis_Dist_normal = normal_seg.dot(v);
                if( fabs(along_axis_Dist) < finger_width / 2  &&  along_axis_Dist_normal > (-finger_length_min) )
                {
                    pt_rgb.r = 255;
                    pt_rgb.g = 0;
                    pt_rgb.b = 0;
                    pt_rgb.x = cropped(0);
                    pt_rgb.y = cropped(1);
                    pt_rgb.z = cropped(2);
                    visualize_cld->points.push_back(pt_rgb);

                    //project points on a plane with normal dir alond surface normal direction
                    temp = cropped - (along_axis_Dist_normal * normal_seg);

                    //calculate the distance of the projected points
                    temp = temp - center;

                    std::pair<float,int> temp_dist;
                    temp_dist.first = temp.norm();
                    temp_dist.second = nn_indices[u];
                    dist_from_center.push_back( temp_dist );

                }
            }

    //        pcl::visualization::PCLVisualizer viewer2("cylinder");
    //        viewer2.addPointCloud(ptCloud);
    //        viewer2.addPointCloud(visualize_cld, "11");
    //        std::cout << " close the window cloud_viewer to continue......." << std::endl;
    //        while(!viewer2.wasStopped())
    //            viewer2.spinOnce();
    //        std::cout << " dist_from_center.size() = " << dist_from_center.size() << std::endl;

            std::sort( dist_from_center.begin(), dist_from_center.end() );

            if( dist_from_center.size() > 30 )
            {
                //check how many number of point is there in the gap and set shell radius to be r
                int startPos = -1, prev_start = 0;
                int num_of_points_in_gap_positive_points = 0;
                int num_of_points_in_gap_negative_points = 0;
                bool isHandleFound = false;
                for( float r = rad ; r <= maxHandAperture; r += 0.001 )
                {
                    std::cout << " current rad = " << r << std::endl;
                    pt_rgb.r = rand() % 255;
                    pt_rgb.g = rand() % 255;
                    pt_rgb.b = rand() % 255;

                    num_of_points_in_gap_positive_points = 0;
                    num_of_points_in_gap_negative_points = 0;
                    startPos = -1;
                     for( int c_loop = prev_start; c_loop < dist_from_center.size(); c_loop++ )
                     {

                         if( ( dist_from_center[c_loop].first > r ) && ( dist_from_center[c_loop].first <= ( r +  handle_gap )  ) )
                         {
                             if( startPos == -1 )
                                 startPos = c_loop;

                             if( point_labels_rg_[dist_from_center[c_loop].second] == -1 )
                                num_of_points_in_gap_negative_points++;
                             else
                                 num_of_points_in_gap_positive_points++;

                             Eigen::Vector3d cropped_1 = ptCloud->points[dist_from_center[c_loop].second].getVector3fMap().cast<double>();

                             pt_rgb.x = cropped_1(0);
                             pt_rgb.y = cropped_1(1);
                             pt_rgb.z = cropped_1(2);
                             visualize_cld->points.push_back(pt_rgb);
                         }

                     }

                     if( startPos != -1)
                       prev_start = startPos ;

    #if(VISOPTION_SURFACE)
                     std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                     std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
    #endif

                     // break from the loop is the handle contains negative points
                     if( num_of_points_in_gap_negative_points > point_neg )
                        break;

                    if(  (num_of_points_in_gap_positive_points > num_smallest) || (num_of_points_in_gap_negative_points > num_smallest) )
                        continue;

                     if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                     {
                         shell.setRadius(r);
                         final_shell = shell;
                         isHandleFound = true ;
                         break;
                     }

                     //break from the loop if there is no positive points in gap
                     if( num_of_points_in_gap_positive_points == 0 )
                        break;

                 }


                if( isHandleFound )
                {
                    break;
                }

            }

        }

    }

    return final_shell;

}



CylindricalShell SurfacePatch::
grasp_localization_sphere(const std::vector<PointCloudIndividual> &segRegion,
                    const PointCloudXYZ::Ptr &ptCloud,
                     Sphere_Param sph )
{

    int points_in_gap_allowed = this->points_in_gap_allowed;
    float finger_width = this->finger_width; // original + error_to_be allowed
    Eigen::Vector3f axis_array = sph.axis;
    double rad = (double)sph.radius;
    PointXYZ center1 = sph.center;

    Eigen::Vector3d axis, center;
    axis(0) = axis_array(0);
    axis(1) = axis_array(1);
    axis(2) = axis_array(2);

    center(0) = center1.x;
    center(1) = center1.y;
    center(2) = center1.z;

    //compute basis direction
    Eigen::Vector3d e_1, e_2;
    e_1 = axis;
    e_2 = basis_direction_e2( axis, center, ptCloud );


    CylindricalShell shell, final_shell;
    shell.setRadius(rad);
    shell.setCentroid(center);
    shell.setCurvatureAxis(axis);
    shell.setExtent(finger_width);

    double maxHandAperture = rad + this->maxHandAperture;
    double handle_gap = this->handle_gap;
    double outer_sample_radius = 1.0 * (maxHandAperture + handle_gap);
    pcl::KdTreeFLANN<pcl::PointXYZ> tree;
    tree.setInputCloud(ptCloud);
    std::vector<int> nn_indices;
    std::vector<float> nn_dists;
    tree.radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );
    int num_smallest = 20;

    for( int r = 18; r <= 180; r+=18)
    {
        Eigen::Vector3d new_dir;

        double cos_theta = cos( r * PI/180.0 );
        double sin_theta = sin( r * PI/180.0 );
        new_dir(0) = (cos_theta * e_1(0)) + ( sin_theta * e_2(0) );
        new_dir(1) = (cos_theta * e_1(1)) + ( sin_theta * e_2(1) );
        new_dir(2) = (cos_theta * e_1(2)) + ( sin_theta * e_2(2) );
        new_dir.normalize();


        shell.setCurvatureAxis( new_dir ); // set current direction
        std::vector<int> indices_to_be_tested;
        std::vector<float> dist_from_center;
        Eigen::Vector3d v, temp;
        for( int u = 0; u < nn_indices.size(); u++ )
        {
            Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
            v = cropped - center;
            double along_axis_Dist = new_dir.dot(v);
            if( fabs(along_axis_Dist) < finger_width / 2 )
            {
                indices_to_be_tested.push_back(nn_indices[u]);

                //project points on a plane with normal dir aloong cylinder axis
                temp = cropped - (along_axis_Dist * new_dir);

                //calculate the distance of the projected points
                temp = temp - center;

                dist_from_center.push_back(temp.norm());

//            pcl::PointXYZ pt_xyz = ptCloud->points[nn_indices[u]];
//            //        pt_rgb.x = pt_xyz.x;
//            //        pt_rgb.y = pt_xyz.y;
//            //        pt_rgb.z = pt_xyz.z;
//            //        cloud_vis->points.push_back(pt_rgb);
//            pt_rgb.r = 0;
//            pt_rgb.g = 0;
//            pt_rgb.b = 255;
//            pt_rgb.x = cropped(0);//pt_xyz.x;
//            pt_rgb.y = cropped(1);//pt_xyz.y;
//            pt_rgb.z = cropped(2);//pt_xyz.z;
//            cloud_vis->points.push_back(pt_rgb);

            }
        }


        std::sort( dist_from_center.begin(), dist_from_center.end() );

        //check how many number of point is there in the gap and set shell radius to be r
        int startPos = 0, endPos = 0, prev_start = 0;
        int num_of_points_in_gap ;
        for( float r = rad ; r <= maxHandAperture; r += 0.001 )
        {
             startPos = 0;
             for( int c_loop = prev_start; c_loop < dist_from_center.size(); c_loop++ )
             {
                 //taking care of initial search
                 if( startPos == 0 )
                 {
                     if( dist_from_center[c_loop] > r )
                         startPos = c_loop;
                 }
                 {
                     if( dist_from_center[c_loop] <= ( r +  handle_gap )  )
                         endPos = c_loop;
                 }

             }
             num_of_points_in_gap = endPos - startPos + 1;
             if ( num_of_points_in_gap < num_smallest )
             {
                 num_smallest = num_of_points_in_gap;
                 shell.setRadius(r);
                 final_shell = shell;
             }

             if( num_of_points_in_gap <= points_in_gap_allowed )
             {
                 shell.setRadius(r);
                 break;
             }

             prev_start = startPos ;
         }


        if( num_of_points_in_gap <= points_in_gap_allowed )
            break;

    }

     return final_shell;

}


CylindricalShell SurfacePatch::
grasp_localization_sphere( const std::vector<PointCloudIndividual> &segRegion,
                           const PointCloudXYZ::Ptr &ptCloud,
                           const std::vector<int> localize_seg,
                           Sphere_Param sph )
{
    const float finger_length_min = 0.02 ;
    int points_in_gap_allowed = this->points_in_gap_allowed;
    int num_smallest = points_in_gap_allowed;
    const int point_neg = this->point_neg; //20;
    float finger_width = this->finger_width; // original + error_to_be allowed
    Eigen::Vector3f axis_array = sph.axis;
    double rad = (double)sph.radius;
    PointXYZ center1 = sph.center;

    Eigen::Vector3d axis, center;
    axis(0) = 0.0; // axis_array(0);
    axis(1) = 1.0; //axis_array(1);
    axis(2) = 0.0; //axis_array(2);

    center(0) = center1.x;
    center(1) = center1.y;
    center(2) = center1.z;

    //compute basis direction
    Eigen::Vector3d e_1, e_2;
    e_1 = axis;
    e_2 = basis_direction_e2( axis, center, ptCloud );

    // the surface normal direction
    Eigen::Vector3d normal_seg;
    bool isNormalFound = false;
    float thr_dot = 0.01;
//    do
//    {
//        for(int n = 0; n < localize_seg.size(); n++ )
//        {
//            normal_seg = segRegion[localize_seg[n]].getrepresentative_normal();
//            double dot = normal_seg.dot(axis);
//            dot = abs(dot);

//            if( dot < thr_dot )
//            {
//                isNormalFound = true;
//                break;
//            }
//        }
//        if( !isNormalFound )
//        {
//#if(VISOPTION_SURFACE)
//            std::cout << " unable to proceed... normal direction is not found..at threshold " << thr_dot << std::endl;
//#endif

//        }

//        thr_dot = thr_dot + 0.01;

//    }while( !isNormalFound  && thr_dot < 0.4);

    Eigen::MatrixXd seg_normals(localize_seg.size(), 3);
    Eigen::Vector3d seg_normal;
    int total_points = 0;
    isNormalFound = true;
    for(int n = 0; n < localize_seg.size(); n++ )
    {
        PointCloudIndividual obj = segRegion[localize_seg[n]];

//        seg_normal = obj.getrepresentative_normal();
//        std::cout << localize_seg[n] << " normal = " << seg_normal << std::endl;

        int points = obj.getNNIndices().size();
        seg_normals.row(n) = points * ( obj.getrepresentative_normal());
        total_points = total_points + points ;
    }

    // save the variables
    normal_seg(0) = (seg_normals.col(0).sum()) / (double)total_points;
    normal_seg(1) = (seg_normals.col(1).sum()) / (double)total_points;
    normal_seg(2) = (seg_normals.col(2).sum()) / (double)total_points;
    normal_seg.normalize();


    //set parameter for shell
    CylindricalShell shell, final_shell;
    shell.setRadius(rad);
    shell.setCentroid(center);
    shell.setExtent(finger_width);
    shell.setNormal(normal_seg);

    final_shell.setRadius(-1.0);
    final_shell.setExtent(-1.0);

    if( isNormalFound )
    {

        double maxHandAperture = this->maxHandAperture; //rad + this->maxHandAperture;
        double handle_gap = this->handle_gap;
        double outer_sample_radius = 1.0 * ( maxHandAperture + handle_gap );
        pcl::KdTreeFLANN<pcl::PointXYZ> tree;
        tree.setInputCloud(ptCloud);
        std::vector<int> nn_indices;
        std::vector<float> nn_dists;
        tree.radiusSearch( center1, outer_sample_radius, nn_indices, nn_dists );


        for( int r = 0; r <= 90; r+=10)
        {
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr visualize_cld( new pcl::PointCloud<pcl::PointXYZRGB>() );
            pcl::PointXYZRGB pt_rgb;

            Eigen::Vector3d new_dir;
            double cos_theta = cos( r * PI/180.0 );
            double sin_theta = sin( r * PI/180.0 );
            new_dir(0) = (cos_theta * e_1(0)) + ( sin_theta * e_2(0) );
            new_dir(1) = (cos_theta * e_1(1)) + ( sin_theta * e_2(1) );
            new_dir(2) = (cos_theta * e_1(2)) + ( sin_theta * e_2(2) );
            new_dir.normalize();

            Eigen::Vector3d width_axis;
            width_axis = new_dir.cross( normal_seg);
            shell.setCurvatureAxis( width_axis ); // set current direction ( robot hand orientation )

            std::vector<std::pair<float,int> > dist_from_center;
            Eigen::Vector3d v, temp;
            for( int u = 0; u < nn_indices.size(); u++ )
            {
                Eigen::Vector3d cropped = ptCloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                v = cropped - center;
                double along_axis_Dist = new_dir.dot(v);   // finger width along axis dir which is perpendicular to robot hand orientation
                double along_axis_Dist_normal = normal_seg.dot(v);
                if( fabs(along_axis_Dist) < finger_width / 2  &&  along_axis_Dist_normal > (-finger_length_min) )
                {
#if(VISOPTION_SURFACE)
                    pt_rgb.r = 255;
                    pt_rgb.g = 0;
                    pt_rgb.b = 0;
                    pt_rgb.x = cropped(0);
                    pt_rgb.y = cropped(1);
                    pt_rgb.z = cropped(2);
                    visualize_cld->points.push_back(pt_rgb);
#endif
                    //project points on a plane with normal dir alond surface normal direction
                    temp = cropped - (along_axis_Dist_normal * normal_seg);

                    //calculate the distance of the projected points
                    temp = temp - center;

                    std::pair<float,int> temp_dist;
                    temp_dist.first = temp.norm();
                    temp_dist.second = nn_indices[u];
                    dist_from_center.push_back( temp_dist );

                }
            }


            std::sort( dist_from_center.begin(), dist_from_center.end() );

            if( dist_from_center.size() > 30 )
            {
                //check how many number of point is there in the gap and set shell radius to be r
                int startPos = -1, prev_start = 0;
                int num_of_points_in_gap_positive_points = 0;
                int num_of_points_in_gap_negative_points = 0;
                bool isHandleFound = false;
                for( float r = rad ; r <= maxHandAperture; r += 0.001 )
                {
                    pt_rgb.r = rand() % 255;
                    pt_rgb.g = rand() % 255;
                    pt_rgb.b = rand() % 255;

                    num_of_points_in_gap_positive_points = 0;
                    num_of_points_in_gap_negative_points = 0;
                    startPos = -1;
                     for( int c_loop = prev_start; c_loop < dist_from_center.size(); c_loop++ )
                     {

                         if( ( dist_from_center[c_loop].first > r ) && ( dist_from_center[c_loop].first <= ( r +  handle_gap )  ) )
                         {
                             if( startPos == -1 )
                                 startPos = c_loop;

                             if( point_labels_rg_[dist_from_center[c_loop].second] == -1 )
                                num_of_points_in_gap_negative_points++;
                             else
                                 num_of_points_in_gap_positive_points++;

                             Eigen::Vector3d cropped_1 = ptCloud->points[dist_from_center[c_loop].second].getVector3fMap().cast<double>();

                             pt_rgb.x = cropped_1(0);
                             pt_rgb.y = cropped_1(1);
                             pt_rgb.z = cropped_1(2);
                             visualize_cld->points.push_back(pt_rgb);
                         }

                     }

                     if( startPos != -1)
                       prev_start = startPos ;

    #if(VISOPTION_SURFACE)
                     std::cout << " num_of_points_in_gap_negative_points " << num_of_points_in_gap_negative_points << std::endl;
                     std::cout << num_smallest << " num_of_points_in_gap_all_points " << num_of_points_in_gap_positive_points << std::endl;
    #endif
                     if( startPos != -1)
                        prev_start = startPos ;

                     // break from the loop is the handle contains negative points
                     if( num_of_points_in_gap_negative_points > point_neg )
                     {
                         std::cout << " num_of_points_in_gap_negative_points = "<< num_of_points_in_gap_negative_points << std::endl;
                         break;
                     }

                     if(  (num_of_points_in_gap_positive_points > num_smallest) || (num_of_points_in_gap_negative_points > num_smallest) )
                        continue;

                     if( abs( num_of_points_in_gap_negative_points - num_of_points_in_gap_positive_points ) <= num_smallest )
                     {
                        shell.setRadius(r);
                        final_shell = shell;
                        isHandleFound = true ;
                        break;
                     }

                     //break from the loop if there is no positive points in gap
                     if( num_of_points_in_gap_positive_points == 0 )
                         break;

                 }


                if( isHandleFound )
                {
                    break;
                }

            }


        }

    }
     return final_shell;

}


Eigen::Vector3d SurfacePatch::
basis_direction_e2( const Eigen::Vector3d vec,
                    const Eigen::Vector3d point,
                    const PointCloudXYZ::Ptr &ptCloud)
{

    double k ;
    float dot_product = vec.dot(point);
    k = dot_product;

    Eigen::Vector3d normal_vec;
    normal_vec(0) = vec(0) * k - point(0);
    normal_vec(1) = vec(1) * k - point(1);
    normal_vec(2) = vec(2) * k - point(2);
    normal_vec.normalize();

    //vec -- corresponds to axis vector
    //normal_vec -- corresponds to axis vector
    //cross_vec -- product vector
    Eigen::Vector3d cross_vec = vec.cross(normal_vec);
    cross_vec.normalize();


    return cross_vec;

}


bool SurfacePatch::
visualize_primitives_handle(   const std::vector<Cylinder_Param> cylinder,
                               const std::vector<Sphere_Param> sphere,
                               const std::vector<Box_Param> box,
                               const PointCloudRGB::Ptr &f_cloud,
                               const std::vector<CylindricalShell> shells  )
{

    int count = 0;
    pcl::visualization::PCLVisualizer viewer("fitted_primitives");
//    viewer.setBackgroundColor (1.0, 1.0, 1.0);
//    viewer.addCoordinateSystem(0.1);

//    if( cylinder.size() > 0 )
    {
        for( int c = 0; c < cylinder.size(); c++ )
        {
            Cylinder_Param cylin = cylinder[c];
            Eigen::Vector3f axis_array = cylin.axis;
            float cut = cylin.axis_length;
            float rad = cylin.radius;

            if( c == 1 )
            {
                cut = cut * 0.8;
            }


              PointXYZ center = cylin.center;

              pcl::ModelCoefficients cylinder_coeff;
              cylinder_coeff.values.resize (7);
              cylinder_coeff.values[0] = center.x - (axis_array(0) * cut) ;
              cylinder_coeff.values[1] = center.y - (axis_array(1) * cut);
              cylinder_coeff.values[2] = center.z - (axis_array(2) * cut);

              cylinder_coeff.values[3] = 2 * (axis_array(0) * cut);
              cylinder_coeff.values[4] = 2 * (axis_array(1) * cut);
              cylinder_coeff.values[5] = 2 * (axis_array(2) * cut);

              cylinder_coeff.values[6] = rad ;

              std::stringstream ss;
              ss << count;
              count++;

            viewer.addCylinder( cylinder_coeff , ss.str() );
            viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_COLOR, 0.0, 0.0, 1.0, ss.str());

        }


    }

//    if( sphere.size() > 0 )
    {
        for( int s = 0; s < sphere.size(); s++ )
        {
            Sphere_Param sphere_fit;
            sphere_fit = sphere[s];

            std::stringstream ss;
            ss << count;
            count++;

            viewer.addSphere( sphere_fit.center, sphere_fit.radius, 255, 0, 0, ss.str());
            viewer.setShapeRenderingProperties
            (pcl::visualization::PCL_VISUALIZER_REPRESENTATION,
             pcl::visualization::PCL_VISUALIZER_REPRESENTATION_WIREFRAME,
               ss.str() );
        }
    }

//    if( box.size() > 0 )
    {
        for( int b = 0; b < box.size(); b++ )
        {
            Box_Param box_fit = box[b];

            //fitting a box
            Eigen::Matrix3f eigDx = box_fit.eigDx;
            eigDx.col(2) = eigDx.col(0).cross(eigDx.col(1));

            // final transform
            const Eigen::Quaternionf qfinal(eigDx);
            const Eigen::Vector3f tfinal = box_fit.mean_diag;

             Eigen::Vector3d axis_length = box_fit.axis_length ;

             std::stringstream ss;
             ss << count;
             count++;

             viewer.addCube(tfinal, qfinal, 2*axis_length(0), 2*axis_length(1), 2*axis_length(2), ss.str() ); //minor, major, normal
             viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_COLOR, 0.5,0.7, 0.0, ss.str());
             viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, ss.str());
        }
    }


    //visualize handles
        pcl::KdTreeFLANN<pcl::PointXYZRGB> tree;
        tree.setInputCloud(f_cloud);
        std::vector<int> nn_indices;
        std::vector<float> nn_dists;

        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_vis( new pcl::PointCloud<pcl::PointXYZRGB>());
        pcl::PointXYZRGB pt_rgb;
        pt_rgb.r = 0;
        pt_rgb.g = 255;
        pt_rgb.b = 255;

        pcl::PointXYZ pt1,pt2,pt3;

        for( int s = 0; s < shells.size(); s++ )
        {
            CylindricalShell shell = shells[s];

            if( shell.getRadius() > 0.0 )
            {
                Eigen::Vector3d center = shell.getCentroid();
                Eigen::Vector3d normal = shell.getNormal();
                Eigen::Vector3d axis = shell.getCurvatureAxis();
                axis = axis.cross(normal);
                axis.normalize();
                Eigen::Vector3d v;
                double shell_extent = shell.getExtent();

                pt1.x = center(0);
                pt1.y = center(1);
                pt1.z = center(2);

                pt2.x = pt1.x + ( normal(0) * (shell.getRadius() + 0.07) );
                pt2.y = pt1.y + ( normal(1) * (shell.getRadius() + 0.07));
                pt2.z = pt1.z + ( normal(2) * (shell.getRadius() + 0.07) );

                pt3.x = pt1.x + ( axis(0) * (shell.getRadius() + 0.07) );
                pt3.y = pt1.y + ( axis(1) * (shell.getRadius() + 0.07) );
                pt3.z = pt1.z + ( axis(2) * (shell.getRadius() + 0.07) );

                pt_rgb.x = center(0);
                pt_rgb.y = center(1);
                pt_rgb.z = center(2);

                tree.radiusSearch(pt_rgb, shell.getRadius() + 0.01, nn_indices, nn_dists);
                std::cout << shell.getRadius() << " nn_indices.size() = " << nn_indices.size() << std::endl;

                            for( int u = 0; u < nn_indices.size(); u++ )
                            {
                                Eigen::Vector3d cropped = f_cloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                v = cropped - center;
                                Eigen::Vector3d axis_robot = axis.cross(normal);
                                double along_axis_Dist = axis_robot.dot(v);
                                if( fabs(along_axis_Dist) < shell_extent / 2.0 )
                                {
                                    pcl::PointXYZRGB pt_rgb1 = f_cloud->points[nn_indices[u]];
                                    pt_rgb1.r = pt_rgb.r;
                                    pt_rgb1.g = pt_rgb.g;
                                    pt_rgb1.b = pt_rgb.b;
                                    cloud_vis->points.push_back(pt_rgb1);
                                }
                            }
                stringstream ss ;
                ss << count;
                viewer.addLine( pt1, pt2, 0, 0, 255, ss.str());
                viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, ss.str());

                count++;

                ss << count;
                viewer.addLine( pt1, pt3, 255, 0, 0, ss.str());
                viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, ss.str());

            }

        }

    // add cylinder
    viewer.addPointCloud(f_cloud);
    viewer.addPointCloud(cloud_vis, "cloud1");
    std::cout << " close the window cloud_viewer to continue......." << std::endl;
    while(!viewer.wasStopped())
        viewer.spinOnce();
    viewer.close();

    return true;

}


bool SurfacePatch::
visualize_primitives_all(   const std::vector<Cylinder_Param> cylinder,
                            const std::vector<Sphere_Param> sphere,
                            const std::vector<Box_Param> box,
                            const PointCloudRGB::Ptr &f_cloud  )
{

    int count = 0;
    pcl::visualization::PCLVisualizer viewer("fitted_primitives");
//    viewer.setBackgroundColor (1.0, 1.0, 1.0);
//    viewer.addCoordinateSystem(0.1);

//    if( cylinder.size() > 0 )
    {
        for( int c = 0; c < cylinder.size(); c++ )
        {
            Cylinder_Param cylin = cylinder[c];
            Eigen::Vector3f axis_array = cylin.axis;
            float cut = cylin.axis_length;
            float rad = cylin.radius;

            if( c == 1 )
            {
                cut = cut * 0.8;
            }


              PointXYZ center = cylin.center;

              pcl::ModelCoefficients cylinder_coeff;
              cylinder_coeff.values.resize (7);
              cylinder_coeff.values[0] = center.x - (axis_array(0) * cut) ;
              cylinder_coeff.values[1] = center.y - (axis_array(1) * cut);
              cylinder_coeff.values[2] = center.z - (axis_array(2) * cut);

              cylinder_coeff.values[3] = 2 * (axis_array(0) * cut);
              cylinder_coeff.values[4] = 2 * (axis_array(1) * cut);
              cylinder_coeff.values[5] = 2 * (axis_array(2) * cut);

              cylinder_coeff.values[6] = rad ;

              std::stringstream ss;
              ss << count;
              count++;

            viewer.addCylinder( cylinder_coeff , ss.str() );
            viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_COLOR, 0.0, 0.0, 1.0, ss.str());

        }


    }

//    if( sphere.size() > 0 )
    {
        for( int s = 0; s < sphere.size(); s++ )
        {
            Sphere_Param sphere_fit;
            sphere_fit = sphere[s];

            std::stringstream ss;
            ss << count;
            count++;

            viewer.addSphere( sphere_fit.center, sphere_fit.radius, 255, 0, 0, ss.str());
            viewer.setShapeRenderingProperties
            (pcl::visualization::PCL_VISUALIZER_REPRESENTATION,
             pcl::visualization::PCL_VISUALIZER_REPRESENTATION_WIREFRAME,
               ss.str() );
        }
    }

//    if( box.size() > 0 )
    {
        for( int b = 0; b < box.size(); b++ )
        {
            Box_Param box_fit = box[b];

            //fitting a box
            Eigen::Matrix3f eigDx = box_fit.eigDx;
            eigDx.col(2) = eigDx.col(0).cross(eigDx.col(1));

            // final transform
            const Eigen::Quaternionf qfinal(eigDx);
            const Eigen::Vector3f tfinal = box_fit.mean_diag;

             Eigen::Vector3d axis_length = box_fit.axis_length ;

             std::stringstream ss;
             ss << count;
             count++;

             viewer.addCube(tfinal, qfinal, 2*axis_length(0), 2*axis_length(1), 2*axis_length(2), ss.str() ); //minor, major, normal
             viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_COLOR, 0.0,1.0, 0.0, ss.str());
             viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, ss.str());
        }
    }


    // add cylinder
    viewer.addPointCloud(f_cloud);
    std::cout << " close the window cloud_viewer to continue......." << std::endl;
    while(!viewer.wasStopped())
        viewer.spinOnce();
    viewer.close();

    return true;

}

// have to check the axis length
bool SurfacePatch::
fit_cylinder_sphere( std::vector<PointCloudIndividual> &segRegion,
                 std::vector<int> &segnum,
                 const PointCloudXYZ::Ptr &f_cloud,
                 const float diff_radius,
                 std::vector<Cylinder_Param> &cylin_final,
                 std::vector<Sphere_Param> &sphere_final,
                 std::vector<int> &localize_seg)
{
    //error check
    if( (segRegion.size() == 0) || (segnum.size() == 0) || (f_cloud->points.size() == 0 ) )
    {
        std::cout << " in fit_cylinder_sphere function ( (segRegion.size() == 0) || (segnum.size() == 0) || (f_cloud->points.size() == 0 ) ) " << std::endl;
        return false;
    }

    std::vector<Cylinder_Param> cylin;
    std::vector<int> check_validity;
    std::vector<int> one_plane( segRegion.size(), -1 );
    std::vector<int> check_segment_on_which_fitted;

    // finding maximum sized segment
    std::pair<int,int> big_segment_final;
    big_segment_final.first = 0;
    big_segment_final.second = -1;
    float curvature_cutoff_high = 0.1;

    for( int cls_seg = 0; cls_seg < segnum.size(); cls_seg++ )
    {
        PointCloudIndividual obj = segRegion[segnum[cls_seg]];
        float radious = obj.get_curvature();
//        Eigen::Vector3d var = obj.get_seg_var();
//        float var_s = sqrt( (var(0) * var(0)) + (var(1) * var(1)) + (var(2) * var(2))  );

        if( radious > curvature_cutoff_high )
            continue;

        std::vector<int> neigh_reg = obj.get_neighbour();
        std::vector<int> neigh;
        //check which neighbours are in the same object
        for( int n_count = 0; n_count < neigh_reg.size(); n_count++ )
        {
            for( int ii = 0; ii < segnum.size(); ii++ )
            {
                if( ( neigh_reg[n_count] == segnum[ii] ) )
                {
                    neigh.push_back( neigh_reg[n_count] );
                    break;
                }

            }
        }

        if( one_plane[segnum[cls_seg]] == -1 )
            one_plane[segnum[cls_seg]] = segnum[cls_seg];  // assign the segment number

        for( int n = 0 ; n < neigh.size(); n++ )
        {
            float rad_nei = segRegion[neigh[n]].get_curvature();

            if( abs( radious - rad_nei ) < diff_radius)
            {
                if( one_plane[neigh[n]] == -1 )
                {
                 one_plane[neigh[n]] = segnum[cls_seg];    // assign the segment number to the neighbour to be merged

                }

            }
        }



    }

    vector<int> segment_num(one_plane);
    sort( segment_num.begin(), segment_num.end() );
    segment_num.erase( unique( segment_num.begin(), segment_num.end() ), segment_num.end() );


    for( int r = 0; r < segment_num.size(); r++ )
    {
        int current_processing_num = segment_num[r];

        PointCloudXYZ::Ptr cloud_pca( new PointCloudXYZ() );
        PointXYZ middle_point;
        middle_point.x = 0.0;
        middle_point.y = 0.0;
        middle_point.z = 0.0;

        if( current_processing_num != -1 )
        {

        std::vector<int> indexFinal;
        std::pair<int,int> big_segment;
        int no_segment_to_be_merged = 0;
        float radius;

        big_segment.first = 0;
        big_segment.second = -1;
        for( int o = 0; o < one_plane.size(); o++ )
        {
            if( one_plane[o] == current_processing_num )
            {
                no_segment_to_be_merged++ ;
                std::vector<int> nnIndices = segRegion[o].getNNIndices();
                std::vector<int> nnIndices_unorganized = segRegion[o].getNNIndices_unorganized();
                float rad_curv = segRegion[o].get_curvature();

                pcl::Normal pt_normal;
                PointXYZ pt_xyz;

                for( int index = 0; index < nnIndices_unorganized.size(); index++ )
                {
                    //save the indices
                    indexFinal.push_back( nnIndices[index] );

                    pt_xyz = f_cloud->points[ nnIndices[index] ];
                    pt_normal = cloud_normals->points[nnIndices_unorganized[index]];

                    pt_xyz.x = pt_xyz.x - (pt_normal.normal_x * rad_curv) ;
                    pt_xyz.y = pt_xyz.y - (pt_normal.normal_y * rad_curv) ;
                    pt_xyz.z = pt_xyz.z - (pt_normal.normal_z * rad_curv) ;

                    cloud_pca->points.push_back(pt_xyz);

                    middle_point.x = middle_point.x + pt_xyz.x;
                    middle_point.y = middle_point.y + pt_xyz.y;
                    middle_point.z = middle_point.z + pt_xyz.z;

                }

                //check biggest segment to use for
                if( nnIndices.size() > big_segment.first )
                {
                    big_segment.first = nnIndices.size();
                    big_segment.second = o;
                    radius = rad_curv;
                }

            }
        }

//---- decide whether to fit  cylinder and sphere --------------------------


        //pca on  point cloud
        pcl::PCA<pcl::PointXYZ> pca;
        pca.setInputCloud ( cloud_pca );
        Eigen::Matrix3f eigen_vecs_center = pca.getEigenVectors();
        Eigen::Vector3f major_axis_center = eigen_vecs_center.col(0);
        major_axis_center.normalize();
        eigen_vecs_center.col(0) = major_axis_center;
        Eigen::Vector3f eigen_val_center = pca.getEigenValues();
        Eigen::Vector3f v;

        float center_major_length = 0.0;
        double thr = 0.99;
        PointXYZ pt_xyz;
        middle_point.x = middle_point.x / (float)indexFinal.size() ;
        middle_point.y = middle_point.y / (float)indexFinal.size() ;
        middle_point.z = middle_point.z / (float)indexFinal.size() ;

        //loop to get major axis length of the center point
        while( center_major_length == 0.0)
        {

            int index_major_pos = -1;
            int index_major_neg = -1;
            double major_pos = 0.0;
            double major_neg = 0.0;

            for( int i = 0; i < cloud_pca->points.size(); i++ )
            {
                // accessing indx w.r.t idx not original point cloud indx
                pt_xyz = cloud_pca->points[i];

                // position vector w.r.t center
                // v = point-orig
                v(0) = pt_xyz.x - middle_point.x;
                v(1) = pt_xyz.y - middle_point.y;
                v(2) = pt_xyz.z - middle_point.z;

                //calculate the norm

                double dist = v.norm();
                v.normalize();

                {
                    double major_angle = ( major_axis_center(0) * v(0) ) + ( major_axis_center(1) * v(1) ) + ( major_axis_center(2) * v(2) ) ;

                    // check angle with major axis
                    if( major_angle > thr )
                    {
                        if( dist > major_pos )
                        {
                            major_pos = dist;
                            index_major_pos = i;
                        }
                    }
                    else if ( major_angle < (-thr) )
                    {
                        if( dist > major_neg )
                        {
                            major_neg = dist;
                            index_major_neg =i;
                        }
                    }

                } //end of center_major_length

            }//end of for


            if( (index_major_pos != -1) && (index_major_neg != -1))
            {
                 center_major_length = major_pos;
                 if( center_major_length > major_neg ) //take minimun of two axis direction
                     center_major_length = major_neg;
            }

            thr = thr - 0.02;


        } //end of while


        PointCloudIndividual obj = segRegion[big_segment.second];
        Eigen::Vector3d re_normal = obj.getrepresentative_normal();
        Eigen::Vector3d pt_on_plane = obj.getrepresentative_position();
        float small_d = obj.get_small_diff();
        Eigen::Vector3d cut = obj.getcut_axis();
        Eigen::Matrix3f eigen_vecs = obj.get_axis();
        int axis_cylinder = -1 ;

        Eigen::Vector3f major_axis = eigen_vecs_center.col(0) ;

        //check whether major axis and center major axis isin same direction
        for( int u = 0; u < 2; u++ )
        {
            major_axis_center = eigen_vecs.col(u);
            float d_p = ( major_axis(0) * major_axis_center(0) ) + ( major_axis(1) * major_axis_center(1) ) + ( major_axis(2) * major_axis_center(2) );

            if( abs(d_p) > 0.8 )
                axis_cylinder = u;
        }

         //--------------code for making decision--------------------------------
        bool isCylinder = false;
        if( axis_cylinder != -1  )
        {

            float factor_eigen_center = eigen_val_center(0) / eigen_val_center(1);
            float float_length_cmp = cut(axis_cylinder) / center_major_length ;
            if( float_length_cmp > 1.0 )
                float_length_cmp = 1 / float_length_cmp ;


            if( factor_eigen_center > 3.0 )
            {
              isCylinder = true;
            }
            else if( factor_eigen_center > 1.3 )
            {
                if( float_length_cmp > 0.8 )
                    isCylinder = true;
                else
                    isCylinder = false;
            }
            else
            {
                isCylinder = false;
            }

//            if( factor_eigen_center > 1.3 )
//            {
//                if( float_length_cmp > 0.8 )
//                    isCylinder = true;
//                else
//                    isCylinder = false;
//            }
//            else
//            {
//                isCylinder = false;
//            }
            std::cout << " factor_eigen_center = " << factor_eigen_center << std::endl;
            std::cout << " float_length_cmp = " << float_length_cmp << std::endl;

         }
            else
                isCylinder = false;

        std::cout << " axis_cylinder = " << axis_cylinder << std::endl;
//---------------------------------------------------------------------------------------------

        if( no_segment_to_be_merged > 1 )
        {
            double curv = curvatureEstimation(f_cloud, indexFinal);
            radius = 1 / ( float )curv;
        }


        if( isCylinder )
        {
            if( radius < 0.07 )
                radius = 0.85 * radius;
            small_d = radius - small_d ;

            PointXYZ re_point;
            re_point.x = pt_on_plane(0) - (re_normal(0) * small_d) ;
            re_point.y = pt_on_plane(1) - (re_normal(1) * small_d) ;
            re_point.z = pt_on_plane(2) - (re_normal(2) * small_d) ;


            Cylinder_Param cylinder_fix;

                 cylinder_fix.radius = radius;
                 cylinder_fix.center = re_point;
                 cylinder_fix.axis = major_axis;//eigen_vecs.col(axis_cylinder);
                 cylinder_fix.axis_length = cut(axis_cylinder);


             cylin.push_back( cylinder_fix );
             check_validity.push_back(isCylinder);
             check_segment_on_which_fitted.push_back(current_processing_num);

                if( indexFinal.size() > big_segment_final.first )
                {
                    big_segment_final.first = indexFinal.size() ;
                    big_segment_final.second = cylin.size() - 1 ;
                }
        }
        else
        {

            Cylinder_Param cylinder_fix;
            cylinder_fix.radius = radius;
            cylinder_fix.center = middle_point;
            cylinder_fix.axis = eigen_vecs.col(0);
            cylinder_fix.axis_length = -1.0;

            cylin.push_back( cylinder_fix );
            check_validity.push_back(isCylinder);
            check_segment_on_which_fitted.push_back(current_processing_num);

            if( indexFinal.size() > big_segment_final.first )
            {
                big_segment_final.first = indexFinal.size() ;
                big_segment_final.second = cylin.size() - 1 ;
            }

        }


        }

    }



    cylin_final.resize(0);
    sphere_final.resize(0);
    if( big_segment_final.second != -1 )
    {
        if(  check_validity[big_segment_final.second] == 1 )
        {
            cylin_final.push_back( cylin[big_segment_final.second] );
        }
        else if ( check_validity[big_segment_final.second] == 0 )
        {
            Cylinder_Param cylinder_fix = cylin[big_segment_final.second] ;
            Sphere_Param sphere;
            sphere.center = cylinder_fix.center;
            sphere.radius = cylinder_fix.radius;
            sphere.axis = cylinder_fix.axis;
            sphere_final.push_back(sphere);
        }

        //------------------------------------------------------------------------------------

        int seg_num_processing = check_segment_on_which_fitted[big_segment_final.second];
        for( int o = 0; o < one_plane.size(); o++ )
        {
            if( one_plane[o] == seg_num_processing )
            {
                localize_seg.push_back(o);
            }
        }

    }


    return true;
}



std::vector<int> SurfacePatch::
classifySegment_gmm_color_curv( std::vector<PointCloudIndividual> &segRegion,
                      const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloud_rgb,
                      std::string obj_cls )
{
    std::vector<int> seg_num ;
    float thr_segment = 0.3;
    float thr_color_curv = 0.3;

     if( ( segRegion.size() == 0 ) && ( pt_cloud_rgb->points.size() == 0 ) )
     {
         seg_num.resize(0);
         return seg_num;
     }
     else
     {
         string model_file = this->model_path + obj_cls;
         std::cout << " model_file " << model_file << std::endl;

         //define models
         EM fgModel;
         EM bgModel;
         EM fgModel_curv;
         EM bgModel_curv;

         {
            FileStorage fs_bg;
            string model_bg = model_file + "/bgModel_color_curv.txt";
            fs_bg.open(model_bg.c_str(), FileStorage::READ);

            if( !fs_bg.isOpened() )
            {
                cout << "Can't open bg model File" << endl;
                exit(0);
            }

            const FileNode& fn_bg = fs_bg["StatModel.EM"];
            bgModel.read(fn_bg);
            fs_bg.release();

            FileStorage fs_fg;
            string model_fg = model_file + "/fgModel_color_curv.txt";
            fs_fg.open(model_fg.c_str(), FileStorage::READ);

            if( !fs_fg.isOpened() )
            {
                cout << "Can't open fg model File" << endl;
                exit(0);
            }

            const FileNode& fn_fg = fs_fg["StatModel.EM"];
            fgModel.read(fn_fg);
            fs_fg.release();

          }


         // ******************************************************************************************
         for(int s_count = 0; s_count < segRegion.size(); s_count++)
         {
             PointCloudIndividual seg = segRegion[s_count];

             if( seg.get_use_the_segment() )
             {
                 std::vector<int> idx = seg.getNNIndices();
                 float curv = seg.get_curvature();
                 if( curv > 0.1 )
                    curv = 0.1;
                 curv = curv * 1000; // scale 1 cm = 10 meter
                 curv = (int) ( curv );

                 pcl::PointXYZRGB pt_rgb;
                 int count_pos = 0;
                 std::vector<float> gmm_color(idx.size());
                 for ( int i_count = 0 ; i_count < idx.size(); i_count++ )
                 {
                     pt_rgb = pt_cloud_rgb->points[idx[i_count]];

                     cv::Mat temp( 1,3,CV_64FC1 );
                     cv::Mat temp_img( 1,3,CV_8UC3 );
                     temp_img.at<Vec3b>(0,0)[0] =  pt_rgb.b;
                     temp_img.at<Vec3b>(0,0)[1] =  pt_rgb.g;
                     temp_img.at<Vec3b>(0,0)[2] =  pt_rgb.r;
                     cvtColor(temp_img,temp_img,CV_BGR2HSV);

//                     temp.at<double>(0,0) = 100*double(temp_img.at<Vec3b>(0,0)[0])/180.0;
//                     temp.at<double>(0,1) = 100*double(temp_img.at<Vec3b>(0,0)[1])/255.0;

                     temp.at<double>(0,0) = 100*double(temp_img.at<Vec3b>(0,0)[0])/180.0;
                     temp.at<double>(0,1) = 100*double(temp_img.at<Vec3b>(0,0)[1])/255.0;
                     temp.at<double>(0,2) = (double)curv;

                     cv::Mat probs_fg, probs_bg;
                     Vec2d out_fg = fgModel.predict(temp,probs_fg);
                     Vec2d out_bg = bgModel.predict(temp,probs_bg);


                     double likelihood_fg = std::exp(out_fg[0]);
                     double likelihood_bg = std::exp(out_bg[0]);

                     float prob_fg = (float)likelihood_fg /(likelihood_fg + likelihood_bg);

                     //save gmm_color
                     gmm_color[i_count] = prob_fg ;

                     if( prob_fg > thr_color_curv )
                         count_pos++ ;

                  }

                 std::sort( gmm_color.begin(), gmm_color.end() );
                 //set gmm score
//                 segRegion[s_count].set_gmm_curv( 1.0 );
                 segRegion[s_count].set_gmm_color(gmm_color);

                 float gn = count_pos / (float)idx.size() ;
                 if( ( gn > thr_segment ) /*( prob_fg_curv > 0.4 )*/ )
                      seg_num.push_back(s_count);

            }

         }

      return seg_num;
      }

}




bool SurfacePatch::
showCloud_gmm( const std::vector<PointCloudIndividual> &segRegion,
           const pcl::PointCloud<pcl::PointXYZ>::Ptr &f_cloud,
           std::vector<int> segnum)
{
        std::vector<bool> isIncluded(segRegion.size(), false);
        for( int i = 0; i < segnum.size(); i++)
            isIncluded[segnum[i]] = true;

       pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
       cloudVis->points.resize(0);
       pcl::visualization::PCLVisualizer viewer( "viewer");
       pcl::PointXYZRGB pt;
       for( int i = 0; i< segRegion.size(); i++ )
       {
           if( isIncluded[i] )
           {
               pt.r = 255;
               pt.g = 0;
               pt.b = 0;
           }
           else
           {
               pt.r = 0;
               pt.g = 0;
               pt.b = 255;
           }

           int index = i;
           PointCloudIndividual obj = segRegion[index];

           std::vector<int> idx = obj.getNNIndices();
           Eigen::Vector3d re_pos = obj.getrepresentative_position();
           for ( int j = 0 ; j < idx.size(); j++ )
           {
            PointXYZ ptrgb;
            ptrgb = f_cloud->points[idx[j]];
            pt.x = ptrgb.x;
            pt.y = ptrgb.y;
            pt.z = ptrgb.z;

            cloudVis->points.push_back(pt);
           }

//           std::vector<int> bndry = obj.get_neighbour();
//           std::cout << index << "bndry.size()" << bndry.size() << std::endl;
//            for( int n = 0; n < bndry.size(); n++ )
//           {
//               std::cout << bndry[n] << " ";
//           }
//            std::cout << " \n next boundary " << std::endl;

        stringstream ss;
        ss << index;
        pcl::PointXYZ pt_xyz;
        pt_xyz.x = re_pos(0);
        pt_xyz.y = re_pos(1);
        pt_xyz.z = re_pos(2);

//        viewer.addText3D ( ss.str(), pt_xyz, 0.01) ;

       }


       viewer.addPointCloud(cloudVis);
       while( !viewer.wasStopped() )
       viewer.spinOnce();

  return true;
}


bool SurfacePatch::
showCloud( const std::vector<PointCloudIndividual> &segRegion,
           const pcl::PointCloud<pcl::PointXYZ>::Ptr &f_cloud,
           const std::vector<int> segnum)
{
    std::cout << " showCloud( ) " << std::endl;
        if( ( segRegion.size() == 0 ) || ( segnum.size() == 0))
            return false;

       pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
       pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudNeg_1( new pcl::PointCloud<pcl::PointXYZRGB>() );

       cloudVis->points.resize(0);
       pcl::visualization::PCLVisualizer viewer( "viewer");
       pcl::PointXYZRGB pt;
       for( int i = 0; i< segnum.size(); i++ )
       {
           int index = segnum[i];
           PointCloudIndividual obj = segRegion[index];
           pt.r = std::rand() % 255;
           pt.g = std::rand() % 255;
           pt.b = std::rand() % 255;
           std::vector<int> idx = obj.getNNIndices();
           Eigen::Vector3d re_pos = obj.getrepresentative_position();
           for ( int j = 0 ; j < idx.size(); j++ )
           {
            PointXYZ ptrgb;
            ptrgb = f_cloud->points[idx[j]];
            pt.x = ptrgb.x;
            pt.y = ptrgb.y;
            pt.z = ptrgb.z;

            cloudVis->points.push_back(pt);

           }



//           std::vector<int> bndry = obj.get_neighbour();
//           std::cout << index << "bndry.size()" << bndry.size() << std::endl;
//            for( int n = 0; n < bndry.size(); n++ )
//           {
//               std::cout << bndry[n] << " ";
//           }
//            std::cout << " \n next boundary " << std::endl;

        stringstream ss;
        ss << index;
        pcl::PointXYZ pt_xyz;
        pt_xyz.x = re_pos(0);
        pt_xyz.y = re_pos(1);
        pt_xyz.z = re_pos(2);

        viewer.addText3D ( ss.str(), pt_xyz, 0.01) ;

       }


       viewer.addPointCloud(cloudVis);
//       viewer.addPointCloud( cloudNeg_1 , "i1");
       while( !viewer.wasStopped() )
       viewer.spinOnce();
       viewer.close();


////       {
////           // testing occlution points
//           pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudNeg( new pcl::PointCloud<pcl::PointXYZRGB>() );

//           for( int ii = 0; ii < point_labels_rg_.size(); ii++ )
//           {
//               pcl::PointXYZ pt_x = f_cloud->points[ii];
//               pcl::PointXYZRGB pt_rgb;
//               pt_rgb.x = pt_x.x;
//               pt_rgb.y = pt_x.y;
//               pt_rgb.z = pt_x.z;

//               if( pcl::isFinite(pt_x) )
//               {
////                   std::cout << point_labels_rg_[ii] << " , ";
//                   if( point_labels_rg_[ii] != -1 )
//                   {
//                       pt_rgb.r = 0;
//                       pt_rgb.g = 0;
//                       pt_rgb.b = 255;

//                       cloudNeg->points.push_back( pt_rgb );
//                   }
//                   else
//                   {
//                       pt_rgb.r = 255;
//                       pt_rgb.g = 0;
//                       pt_rgb.b = 0;

//                       cloudNeg->points.push_back( pt_rgb );

//                   }
//               }
//           }


//           pcl::visualization::PCLVisualizer viewer1( "viewer");
//           viewer1.addPointCloud(cloudNeg);
//           while( !viewer1.wasStopped() )
//                viewer1.spinOnce();
//           viewer1.close();

////       }

  return true;
}


bool SurfacePatch::
showCloud( const std::vector<PointCloudIndividual> &segRegion,
           const pcl::PointCloud<pcl::PointXYZ>::Ptr &f_cloud )
{
        pcl::visualization::PCLVisualizer viewer( "viewer");
       pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB>() );
       cloudVis->points.resize(0);
       pcl::PointXYZRGB pt;
       for( int index = 0; index< segRegion.size(); index++ )
       {
           pt.r = std::rand() % 255;
           pt.g = std::rand() % 255;
           pt.b = std::rand() % 255;
           std::vector<int> idx = segRegion[index].getNNIndices();
           for ( int j = 0 ; j < idx.size(); j++ )
           {
            PointXYZ ptrgb;
            ptrgb = f_cloud->points[idx[j]];
            pt.x = ptrgb.x;
            pt.y = ptrgb.y;
            pt.z = ptrgb.z;

            cloudVis->points.push_back(pt);
           }

            Eigen::Vector3d re_pos = segRegion[index].getrepresentative_position();
           stringstream ss;
           ss << index;
           pcl::PointXYZ pt_xyz;
           pt_xyz.x = re_pos(0);
           pt_xyz.y = re_pos(1);
           pt_xyz.z = re_pos(2);

           viewer.addText3D ( ss.str(), pt_xyz, 0.01) ;
       }


       viewer.addPointCloud(cloudVis);
       while( !viewer.wasStopped() )
       viewer.spinOnce();

  return true;
}


bool SurfacePatch::
curvatureEstimation( const PointCloudXYZ::Ptr &cloud,
                         std::vector<PointCloudIndividual> &segRegion )
{
//    std::cout << " curvature estimation ... " << std::endl;
  // set-up estimator
  pcl::CurvatureEstimationTaubin<pcl::PointXYZ, pcl::PointCurvatureTaubin> estimator;
  estimator.setInputCloud(cloud);

  // output dataset
  pcl::PointCloud<pcl::PointCurvatureTaubin>::Ptr cloud_curvature(new pcl::PointCloud<pcl::PointCurvatureTaubin>);
  cloud_curvature->resize(segRegion.size());

  for( int s_count = 0; s_count < segRegion.size(); s_count++ )
  {
      std::vector<int> nnIndices = segRegion[s_count].getNNIndices();
      double curvature = estimator.computeFeature( nnIndices, s_count, *cloud_curvature );
      segRegion[s_count].set_curvature( (float)1/curvature );
  }
  return true;

}


double SurfacePatch::
curvatureEstimation( const PointCloudXYZ::Ptr &cloud,
                        const std::vector<int> &nnIndices )
{
//    printf("Estimating curvature ...\n");

  // set-up estimator
  pcl::CurvatureEstimationTaubin<pcl::PointXYZ, pcl::PointCurvatureTaubin> estimator;
  estimator.setInputCloud(cloud);
  double curvature = estimator.computeFeature( nnIndices );

  return curvature;

}


std::vector<std::vector<int> > SurfacePatch::
region_growing( const PointCloudXYZ::Ptr &pointCloudTemp,
                pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloudRGB,
                std::vector<PointCloudIndividual> &segRegion )
{
    int min_points = 10;
    int K = get_K();
    int num_to_be_boundary_point = get_num_to_be_boundary_point();
    float threshold_merge = get_threshold_merge();

    bool isdense_cloud =  false;

    std::vector<std::vector<int> > indices;
    //check inputs
    if( ( pointCloudTemp->empty() ) && ( pt_cloudRGB->empty() ) )
       indices.resize(0);
    else
    {
        double start_time = omp_get_wtime();
            PointCloudXYZ::Ptr pt_cloud(new PointCloudXYZ());
            pt_cloud->width = pt_cloud->width;
            pt_cloud->height = pt_cloud->height;
            pt_cloud->points.resize ( pt_cloud->width * pt_cloud->height);
            pt_cloud->is_dense = false;
            //copy data
            *pt_cloud = *pointCloudTemp;

            //limit

//            limitWorkspace_bin( pt_cloud);
//            pcl::visualization::PCLVisualizer viewer("object");
//            viewer.addPointCloud(pt_cloud);
//            std::cout << " close the window cloud_viewer to continue......." << std::endl;
//            while(!viewer.wasStopped())
//                viewer.spinOnce();

            cv::Rect rcnn_bbox = get_rcnn_bounding_box();
            workspaceFilter(pt_cloud, pt_cloudRGB, rcnn_bbox);
            std::vector<int> indicesPointCloud;
            pt_cloud->is_dense = false;

            pcl::removeNaNFromPointCloud( *pt_cloud, *pt_cloud, indicesPointCloud );
            if( pointCloudTemp->points.size() == pt_cloud->points.size() )
                isdense_cloud = true;


            if(pt_cloud->points.size() <= min_points )
            {
                cout << "No points in pt cloud" << endl;
                return indices;
            }


               pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
//              pcl::PointCloud <pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud <pcl::Normal>);
              pcl::NormalEstimationOMP<PointXYZ, pcl::Normal> normal_estimator;
              normal_estimator.setSearchMethod (tree);
              normal_estimator.setInputCloud (pt_cloud);
              normal_estimator.setNumberOfThreads(8);
              normal_estimator.setKSearch(50); //40
              normal_estimator.compute (*cloud_normals);


              pcl::RegionGrowing_Modified<PointXYZ, pcl::Normal> reg;

              reg.setMinClusterSize(min_points);
              reg.setMaxClusterSize(1000000);
              reg.setSearchMethod(tree);
              reg.setNumberOfNeighbours(70);
              reg.setInputCloud(pt_cloud);
              reg.setInputNormals(cloud_normals);
              reg.setSmoothnessThreshold( 3.7 / 180.0 * M_PI);
              reg.setSmoothnessThresholdHigh( 17.0 / 180.0 * M_PI );
              reg.setFracOfPointsBoundary(0.4);
              reg.setCurvatureThreshold(2.0);


              std::vector <pcl::PointIndices> clusters_unstructed;
              reg.extract (clusters_unstructed);
              std::vector <pcl::PointIndices> clusters( clusters_unstructed );

              //--------------------------------------------------------
              //sorting the clusters by size;
              std::vector<std::pair<float,int> > cluster_size( clusters_unstructed.size() );
              std::pair<float,int> aPair;
              for( int ii = 0; ii < clusters_unstructed.size(); ii++ )
              {
                 aPair = make_pair( clusters_unstructed[ii].indices.size(),ii);
                 cluster_size[ii] = aPair ;

              }

              std::sort( cluster_size.begin(), cluster_size.end());

              int ix = 0;
              for( int i_dx = cluster_size.size() - 1; i_dx >= 0 ; i_dx-- )
              {
                 aPair = cluster_size[i_dx];
                 clusters[ix] = clusters_unstructed[ aPair.second ];
                 ix++;

              }

              //--------------------------------------------------------
              //don't change the values
              const int r = 0;
              const int g = 0;

              //define the neighbours
              std::vector<bool> belong_rg_clusters(pt_cloudRGB->points.size(),false);
              pcl::PointXYZRGB point_rgb;


              //define the neighbours
              std::vector<std::vector<int> > neighbour_merge;
              std::vector<std::vector<int> > neighbour_matix;
              std::vector<int> tempArray(clusters.size(),0);

              // don't parallize this loop
              for(int cls = 0 ; cls < clusters.size(); cls++ )
              {
                  std::vector<int> idx;
                  std::vector<int> idx_unorganized;

                  int indx ;
                  pcl::PointXYZRGB point_rgb;
                  Eigen::Vector3d re_normal;
                  Eigen::Vector3d re_pos;

                  idx.clear();
                  idx_unorganized.clear();
                  int size_cluster = clusters[cls].indices.size();
                  Eigen::MatrixXd samples_normals(size_cluster, 3);
                  Eigen::MatrixXd samples_cloud(3, size_cluster);
                  PointCloudIndividual obj;

                  //color assigned in blue channel
                  int b = cls;

                 for( int i = 0; i < size_cluster; i++)
                  {
                     // indx corresponds to the indx of the original point cloud (with nan value)
                     // clusters[cls].indices[i] --- indx of the dense point cloud
                     indx = indicesPointCloud[clusters[cls].indices[i]];
                     idx.push_back( indx );

                     //the indices
                     int idx_dense = clusters[cls].indices[i];
                     idx_unorganized.push_back( idx_dense );

                     //for representative position and normal calculation
                     samples_normals.row(i) = cloud_normals->points[idx_dense].getNormalVector3fMap().cast<double>();
                     samples_cloud.col(i) = pt_cloud->points[idx_dense].getVector3fMap().cast<double>();

                     // save which segment belongs to whish segment ... will be used in later stage
                     point_labels_rg_[indx] = b;

                     //include color
                     point_rgb = pt_cloudRGB->points[indx];
                     point_rgb.r = r;
                     point_rgb.g = g;
                     point_rgb.b = b;
                     pt_cloudRGB->points[indx] = point_rgb;

                    belong_rg_clusters[indx] = true;
                  }

                //// save the indices corresponding to different region in an array
                  indices.push_back( idx );
                  obj.setNNIndices(idx);
                  obj.setNNIndices_unorganized( idx_unorganized );

                  // save the variables
                  re_normal(0) = samples_normals.col(0).mean();
                  re_normal(1) = samples_normals.col(1).mean();
                  re_normal(2) = samples_normals.col(2).mean();
                  re_normal.normalize();
                  obj.setrepresentative_normal(re_normal);

                  //set variance
                  Eigen::MatrixXd centered = samples_normals.rowwise() - samples_normals.colwise().mean();
                  Eigen::MatrixXd cov = ( centered.adjoint() * centered ) / double( samples_normals.rows() - 1 );
                  Eigen::Vector3d re_var;
                  re_var(0) = cov(0,0);
                  re_var(1) = cov(1,1);
                  re_var(2) = cov(2,2);
                  obj.set_seg_var(re_var);

                  re_pos(0) = samples_cloud.row(0).mean();
                  re_pos(1) = samples_cloud.row(1).mean();
                  re_pos(2) = samples_cloud.row(2).mean();
                  obj.setrepresentative_position(re_pos);

                  obj.setrepresentative_color(b);


                  //find nearest point to center point which is inside the point cloud
                  PointXYZ center_point ;
                  center_point.x = re_pos(0);
                  center_point.y = re_pos(1);
                  center_point.z = re_pos(2);

                  std::vector<int> pointIdxNKN(1);
                  std::vector<float> pointNKNSD(1);
                  tree->nearestKSearch( center_point, 1, pointIdxNKN, pointNKNSD);
                  center_point = pt_cloud->points[pointIdxNKN[0]];
                  float small_d = ( center_point.x - re_pos(0) ) * ( center_point.x - re_pos(0) ) +
                                  ( center_point.y - re_pos(1) ) * ( center_point.y - re_pos(1) ) +
                                  ( center_point.z - re_pos(2) ) * ( center_point.z - re_pos(2) ) ;
                  small_d = sqrt(small_d);
                  obj.set_small_diff(small_d );

                  // always set default value this to true;
                  obj.set_use_the_segment(true);
                  obj.set_gmm_color_final(0.0);
                  //save one segment
                  segRegion.push_back(obj);

                  //initialize the matrix to zero.. this will be used in the next loop
                  neighbour_matix.push_back(tempArray);

              }


              //assign color on which
              for( int c = 0; c < belong_rg_clusters.size(); c++ )
              {
                  if( !belong_rg_clusters[c] )
                  {
                      point_rgb = pt_cloudRGB->points[c];
                      point_rgb.r = 255;
                      point_rgb.g = 255;
                      point_rgb.b = 255;
                      pt_cloudRGB->points[c] = point_rgb;

//                      point_labels_rg_[c] = -1;

                  }
              }

//            showCloud(segRegion, pt_cloud);

              //get neighbours
              // K nearest neighbor search
              // total 30 neighbours to be taken

               #pragma omp parallel for
               for( int cls_indx = 0; cls_indx < segRegion.size(); cls_indx++ )
               {
                   PointCloudIndividual obj;
                   obj = segRegion[cls_indx];
                   std::vector<int> indices_un =  obj.getNNIndices_unorganized() ;
                   int indx;
                   pcl::PointXYZ searchPoint;
                   std::vector<int> pointIdxNKNSearch(K);
                   std::vector<float> pointNKNSquaredDistance(K);
                   std::vector<int> neighbours;

                   for( int idx = 0 ; idx < indices_un.size() ; idx++ )
                   {
                       // dense cloud indx
                        searchPoint = pt_cloud->points[indices_un[idx]];
                        if ( tree->nearestKSearch(searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0 )
                        {
                            if( !isdense_cloud )
                            {
                                //exchange the indices
                                for( int u = 0 ; u< pointIdxNKNSearch.size(); u++)
                                   {
                                      indx = pointIdxNKNSearch[u] ;
                                      pointIdxNKNSearch[u] = indicesPointCloud[indx];
                                   }
                            }


                            neighbours.clear();
                            neighbours = isBoundary( pointIdxNKNSearch, pointNKNSquaredDistance, pt_cloudRGB, cls_indx , num_to_be_boundary_point );

                            if( neighbours.size() > 0 )
                            {
                                  for( int j = 0 ; j < neighbours.size() ; j++ )
                                  {
                                        neighbour_matix[cls_indx][neighbours[j]] = 1;
                                        neighbour_matix[neighbours[j]][cls_indx] = 1;
                                  }

//                                  for( int n = 0; n < neighbours.size(); n++ )
//                                      std::cout << neighbours[n] << " ,, " ;

//                                  getchar();
//                                  std::cout << std::endl;
                            }

                        }

                   }


               }


//               for( int ii = 0; ii < neighbour_matix.size(); ii++ )
//               {
//                   std::cout << ii << " neighbours = ";
//                   std::vector<int> fff = neighbour_matix[ii];
//                   for( int jj = 0; jj < fff.size(); jj++ )
//                   {
////                       int size = segRegion[fff[jj]].getNNIndices().size();
//                       if( fff[jj]  == 1 )
//                          std::cout << jj << " , " ;
//                   }
//                   std::cout << std::endl;
//               }
//               std::cout << std::endl;
//               std::cout << std::endl;

               neighbour_merge.resize(0);
               std::vector<std::vector<int> > neighbours;
               neighbour_merge = mergeregions( segRegion, neighbour_matix, neighbours, threshold_merge );

//               for( int i = 0; i < neighbour_merge.size(); i++ )
//               {
//                   std::cout << i << " neighbours merge = ";
//                   std::vector<int> fff = neighbour_merge[i];
//                   for( int jj = 0; jj < fff.size(); jj++ )
//                   {
////                       int size = segRegion[fff[jj]].getNNIndices().size();
//                       std::cout << fff[jj] << " = " ;
//                   }
//                   std::cout << std::endl;
//               }

//               std::cout << std::endl;
//               for( int i = 0; i < neighbours.size(); i++ )
//               {
//                   std::cout << i << " neighbours = ";
//                   std::vector<int> fff = neighbours[i];
//                   for( int jj = 0; jj < fff.size(); jj++ )
//                   {
////                       int size = segRegion[fff[jj]].getNNIndices().size();
//                       std::cout << fff[jj] << " = " ;
//                   }
//                   std::cout << std::endl;
//               }


//               showCloud(segRegion, pointCloudTemp);
               //assign the indices properly
               finalRegion_3( segRegion, neighbour_merge, neighbours );

//               for( int ss = 0; ss < segRegion.size(); ss++ )
//               {
//                   PointCloudIndividual objj = segRegion[ss];
//                   std::vector<int> nn = objj.get_neighbour();

//                   std::cout << std::endl << ss << " neigh =  ";
//                   for( int ii = 0; ii < nn.size(); ii++ )
//                       std::cout << nn[ii] << " " ;


//               }

    }
    return indices;

}




bool SurfacePatch::
finalRegion_2(  std::vector<PointCloudIndividual> &segRegion,
                const std::vector<std::vector<int> > neighbour_merge,
                const std::vector<std::vector<int> > &neighbours)
{

        #pragma omp parallel for
        for( int m_count = 0; m_count < neighbour_merge.size(); m_count++)
        {
                std::vector<int> segNumber = neighbour_merge[m_count];

                Eigen::MatrixXd samples( 3, segNumber.size() );
                Eigen::MatrixXd samples_pos( 3, segNumber.size() );
                Eigen::MatrixXd samples_var( 3, segNumber.size() );
                std::vector<int> nnIndx_original;
                std::vector<int> nnIndx_original_unoga;
                Eigen::Vector3d re_normal;

                if( segNumber.size() > 1 )
                {
                    Eigen::Vector3d re_pos;
                    Eigen::Vector3d re_var;
                    nnIndx_original.clear();
                    nnIndx_original_unoga.clear();
                    int no_points = 0;

                    for( int count = 0 ; count < segNumber.size() ; count++ )
                    {
                            std::vector<int> nnIndx;
                            nnIndx = segRegion[segNumber[count]].getNNIndices();

                            std::vector<int> nnIndx_unorga;
                            nnIndx_unorga = segRegion[segNumber[count]].getNNIndices_unorganized();

                            samples.col(count) = nnIndx.size() * ( segRegion[segNumber[count]].getrepresentative_normal() );
                            samples_pos.col(count) = nnIndx.size() * ( segRegion[segNumber[count]].getrepresentative_position() );
                            samples_var.col(count) = ( nnIndx.size() - 1 ) * ( segRegion[segNumber[count]].get_seg_var() );

                            no_points = no_points + nnIndx.size();
                            for(int k=0; k<nnIndx.size(); k++)
                            {
                                nnIndx_original.push_back( nnIndx[k] );
                                nnIndx_original_unoga.push_back( nnIndx_unorga[k] );

                                //update the segment number
                                point_labels_rg_[nnIndx[k]] = segNumber[0];
                            }

                            nnIndx.clear();
                            segRegion[segNumber[count]].setNNIndices(nnIndx);

                    }

                    // calculate final representative normal for a region
                    re_normal(0) = ( samples.row(0).sum() ) / (double)no_points;
                    re_normal(1) = ( samples.row(1).sum() ) / (double)no_points;
                    re_normal(2) = ( samples.row(2).sum() ) / (double)no_points;
                    re_normal.normalize();

                    re_pos(0) = ( samples_pos.row(0).sum() ) / (double)no_points;
                    re_pos(1) = ( samples_pos.row(1).sum() ) / (double)no_points;
                    re_pos(2) = ( samples_pos.row(2).sum() ) / (double)no_points;

                    re_var(0) = ( samples_var.row(0).sum() ) / (double)( no_points - segNumber.size() );
                    re_var(1) = ( samples_var.row(1).sum() ) / (double)( no_points - segNumber.size() );
                    re_var(2) = ( samples_var.row(2).sum() ) / (double)( no_points - segNumber.size() );

                    //update variable
                    segRegion[segNumber[0]].setNNIndices(nnIndx_original);
                    segRegion[segNumber[0]].setNNIndices_unorganized(nnIndx_original_unoga);
                    segRegion[segNumber[0]].setrepresentative_normal(re_normal);
                    segRegion[segNumber[0]].setrepresentative_position(re_pos);
                    segRegion[segNumber[0]].set_seg_var(re_var);
                }
                else if ( segNumber.size() == 1 )
                {
                    nnIndx_original.clear();
                    nnIndx_original = segRegion[segNumber[0]].getNNIndices();
                    re_normal = segRegion[segNumber[0]].getrepresentative_normal();
                }


        }

        //delete the region already merged
        std::vector<int> new_indices( segRegion.size(), -1 );
        int s_count = 0;
        int indx_count = 0;
        do
        {
            std::vector<int> nnIndx = segRegion[s_count].getNNIndices();
            if( nnIndx.size() == 0)
            {
                segRegion.erase(segRegion.begin() + s_count );
            }
            else
            {
                new_indices[indx_count] = s_count;
                s_count++ ;
            }
            indx_count++;

        }while( s_count < segRegion.size());

        //resassign neighbour
        for( int ii = 0; ii < neighbours.size(); ii++ )
        {
            std::vector<int> temp_nei = neighbours[ii];
            int main_seg = temp_nei[0];
//            std::cout << ii << " , " << main_seg << endl;


            if( new_indices[main_seg] != -1)
            {
                std::vector<int> reass_nei;
                main_seg = new_indices[main_seg];
//                reass_nei.push_back(main_seg);
                for( int jj = 1; jj < temp_nei.size(); jj++ )
                {
                    if( new_indices[temp_nei[jj]] != -1 )
                    {
                         reass_nei.push_back( new_indices[temp_nei[jj]]);

                    }
                }

                //assign
                segRegion[main_seg].set_neighbour( reass_nei );

            }
        }


        return true;
}

bool SurfacePatch::
finalRegion_3(  std::vector<PointCloudIndividual> &segRegion,
                const std::vector<std::vector<int> > neighbour_merge,
                const std::vector<std::vector<int> > &neighbours)
{

    if( ( segRegion.size() == 0 ) || ( neighbour_merge.size() == 0 ) )
        return false;

        for( int m_count = 0; m_count < neighbour_merge.size(); m_count++)
        {
                std::vector<int> segNumber = neighbour_merge[m_count];

                Eigen::MatrixXd samples( 3, segNumber.size() );
                Eigen::MatrixXd samples_pos( 3, segNumber.size() );
                Eigen::MatrixXd samples_var( 3, segNumber.size() );
                std::vector<int> nnIndx_original;
                std::vector<int> nnIndx_original_unoga;
                Eigen::Vector3d re_normal;

                if( segNumber.size() > 1 )
                {
                    Eigen::Vector3d re_pos;
                    Eigen::Vector3d re_var;
                    nnIndx_original.clear();
                    nnIndx_original_unoga.clear();
                    int no_points = 0;

                    for( int count = 0 ; count < segNumber.size() ; count++ )
                    {
                            std::vector<int> nnIndx;
                            nnIndx = segRegion[segNumber[count]].getNNIndices();

                            std::vector<int> nnIndx_unorga;
                            nnIndx_unorga = segRegion[segNumber[count]].getNNIndices_unorganized();

                            samples.col(count) = nnIndx.size() * ( segRegion[segNumber[count]].getrepresentative_normal() );
                            samples_pos.col(count) = nnIndx.size() * ( segRegion[segNumber[count]].getrepresentative_position() );
                            samples_var.col(count) = ( nnIndx.size() - 1 ) * ( segRegion[segNumber[count]].get_seg_var() );

                            no_points = no_points + nnIndx.size();
                            for(int k=0; k<nnIndx.size(); k++)
                            {
                                nnIndx_original.push_back( nnIndx[k] );
                                nnIndx_original_unoga.push_back( nnIndx_unorga[k] );

                                //update the segment number
                                point_labels_rg_[nnIndx[k]] = segNumber[0];
                            }

                            nnIndx.clear();
                            segRegion[segNumber[count]].setNNIndices(nnIndx);

                    }

                    // calculate final representative normal for a region
                    re_normal(0) = ( samples.row(0).sum() ) / (double)no_points;
                    re_normal(1) = ( samples.row(1).sum() ) / (double)no_points;
                    re_normal(2) = ( samples.row(2).sum() ) / (double)no_points;
                    re_normal.normalize();

                    re_pos(0) = ( samples_pos.row(0).sum() ) / (double)no_points;
                    re_pos(1) = ( samples_pos.row(1).sum() ) / (double)no_points;
                    re_pos(2) = ( samples_pos.row(2).sum() ) / (double)no_points;

                    re_var(0) = ( samples_var.row(0).sum() ) / (double)( no_points - segNumber.size() );
                    re_var(1) = ( samples_var.row(1).sum() ) / (double)( no_points - segNumber.size() );
                    re_var(2) = ( samples_var.row(2).sum() ) / (double)( no_points - segNumber.size() );

                    //update variable
                    segRegion[segNumber[0]].setNNIndices(nnIndx_original);
                    segRegion[segNumber[0]].setNNIndices_unorganized(nnIndx_original_unoga);
                    segRegion[segNumber[0]].setrepresentative_normal(re_normal);
                    segRegion[segNumber[0]].setrepresentative_position(re_pos);
                    segRegion[segNumber[0]].set_seg_var(re_var);
                }
                else if ( segNumber.size() == 1 )
                {
                    nnIndx_original.clear();
                    nnIndx_original = segRegion[segNumber[0]].getNNIndices();
                    re_normal = segRegion[segNumber[0]].getrepresentative_normal();
                }


        }


        //----------------------------------------------------------------------------------------------------
//        for( int i = 0; i < neighbour_merge.size(); i++ )
//        {
//            std::cout << i << " neighbours merge = ";
//            std::vector<int> fff = neighbour_merge[i];
//            for( int jj = 0; jj < fff.size(); jj++ )
//            {
////                       int size = segRegion[fff[jj]].getNNIndices().size();
//                std::cout << fff[jj] << " = " ;
//            }
//            std::cout << std::endl;
//        }

//        std::cout << std::endl;
//        for( int i = 0; i < neighbours.size(); i++ )
//        {
//            std::cout << i << " neighbours = ";
//            std::vector<int> fff = neighbours[i];
//            for( int jj = 0; jj < fff.size(); jj++ )
//            {
////                       int size = segRegion[fff[jj]].getNNIndices().size();
//                std::cout << fff[jj] << " = " ;
//            }
//            std::cout << std::endl;
//        }

        //---------------------------------------------------------------------------------------------------

        std::vector<int> new_indices_1( segRegion.size(), -1 );
        for( int m_count = 0; m_count < neighbour_merge.size(); m_count++)
        {
                std::vector<int> segNumber = neighbour_merge[m_count];
                new_indices_1[segNumber[0]] = m_count;
                for( int j = 1; j < segNumber.size(); j++ )
                {
                    new_indices_1[segNumber[j]] = m_count;
                }
        }

        if( segRegion.size() > 0 )
        {
            //delete the region already merged
            std::vector<int> new_indices( segRegion.size(), -1 );
            int s_count = 0;
            int indx_count = 0;
            do
            {
                std::vector<int> nnIndx = segRegion[s_count].getNNIndices();
                if( nnIndx.size() == 0)
                {
                    segRegion.erase(segRegion.begin() + s_count );
                }
                else
                {
                    new_indices[indx_count] = s_count;
                    s_count++ ;
                }
                indx_count++;

            }while( s_count < segRegion.size());

            //---------------------------------------------------
//            for( int i = 0; i < new_indices.size(); i++ )
//            {
//                std::cout << i << " " << new_indices_1[i] <<  " .. " << new_indices[i] << "\n";
//            }

//            std::cout << " size after merging " << segRegion.size() << std::endl;
//            std::cout << " neighbours = " << neighbours.size() << std::endl;

//            for( int i = 0; i < neighbours.size(); i++ )
//            {
//                std::cout << i << " neighbours= ";
//                std::vector<int> fff = neighbours[i];
//                for( int jj = 0; jj < fff.size(); jj++ )
//                {
//    //                       int size = segRegion[fff[jj]].getNNIndices().size();
//                    std::cout << fff[jj] << " = " ;
//                }
//                std::cout << std::endl;
//            }
//            getchar();
            //------------------------------------------------------
            if( segRegion.size() >= neighbours.size() )
            {
            //resassign neighbour
            for( int ii = 0; ii < neighbours.size(); ii++ )
            {

                std::vector<int> temp_nei = neighbours[ii];
                int main_seg = temp_nei[0];
//                std::cout << temp_nei.size() << " processing ..." << ii << std::endl;
                if( (new_indices[main_seg] != -1) || (temp_nei.size() > 0))
                {
                    std::vector<int> reass_nei;
                    for( int jj = 1; jj < temp_nei.size(); jj++ )
                    {
//                        std::cout << ii << " seg = " << temp_nei[jj] << " " << new_indices_1[temp_nei[jj]] << std::endl;
                        if( new_indices_1[temp_nei[jj]] != ii )
                        {
                             reass_nei.push_back( new_indices_1[temp_nei[jj]]);
//                             std::cout << " push back != -1 "<< new_indices_1[temp_nei[jj]] << std::endl;

                        }
                    }

                    sort( reass_nei.begin(), reass_nei.end() );
                    reass_nei.erase( unique( reass_nei.begin(), reass_nei.end() ), reass_nei.end() );

//                    std::cout << " main_seg = " << main_seg << std::endl;
//                    for( int y = 0; y < reass_nei.size(); y++ )
//                        std::cout << reass_nei[y] << " , ";
//                    getchar();

                    //assign
                    segRegion[ii].set_neighbour( reass_nei );

                }
            }
            }
        }

        return true;
}

bool SurfacePatch::
pca_axis_normal( std::vector<PointCloudIndividual> &segRegion,
          const PointCloudXYZ::Ptr &pt_cloud )
{

    //check error condition
    if( pt_cloud->points.size() == 0 )
        return false;

    #pragma omp parallel for
    for( int reg = 0; reg < segRegion.size(); reg++ )
    {

        PointCloudIndividual reg_ind = segRegion[reg];
        std::vector<int> nnIndx = reg_ind.getNNIndices();
        Eigen::Vector3d pt_on_plane = reg_ind.getrepresentative_position();


        PointCloudXYZ::Ptr cloud( new PointCloudXYZ() );
        PointXYZ pt_xyz;
        Eigen::Vector3d v;

        for( int idx = 0 ;idx < nnIndx.size(); idx++ )
        {
            pt_xyz = pt_cloud->points[nnIndx[idx]];

            //prepare  cloud for PCA
            // accessing indx w.r.t idx not original point cloud indx
            cloud->points.push_back(pt_xyz);

        }

        //pca on  point cloud
        pcl::PCA<pcl::PointXYZ> pca;
        pca.setInputCloud ( cloud );

        Eigen::Matrix3f eigen_vecs = pca.getEigenVectors();
        Eigen::Vector3f major_axis = eigen_vecs.col(0);
        major_axis.normalize();
        eigen_vecs.col(0) = major_axis;
        Eigen::Vector3f minor_axis = eigen_vecs.col(1);
        minor_axis.normalize();
        eigen_vecs.col(1) = minor_axis;

        float a_cut = 0.0;
        float b_cut = 0.0;

        double thr = 0.99;
        while( (a_cut == 0.0) || (b_cut == 0.0) )
        {

            int index_major_pos = -1;
            int index_major_neg = -1;
            int index_minor_pos = -1;
            int index_minor_neg = -1;
            double major_pos = 0.0;
            double major_neg = 0.0;
            double minor_pos = 0.0;
            double minor_neg = 0.0;

            for( int i = 0; i < nnIndx.size(); i++ )
            {
                // accessing indx w.r.t idx not original point cloud indx
                pt_xyz = cloud->points[i];

                // position vector w.r.t center
                // v = point-orig
                v(0) = pt_xyz.x - pt_on_plane(0);
                v(1) = pt_xyz.y - pt_on_plane(1);
                v(2) = pt_xyz.z - pt_on_plane(2);

                //calculate the norm

                double dist = v.norm();
                v.normalize();

                if( a_cut == 0.0 )
                {
                    double major_angle = ( major_axis(0) * v(0) ) + ( major_axis(1) * v(1) ) + ( major_axis(2) * v(2) ) ;

                    // check angle with major axis
                    if( major_angle > thr )
                    {
                        if( dist > major_pos )
                        {
                            major_pos = dist;
                            index_major_pos = i;
                        }
                    }
                    else if ( major_angle < -thr )
                    {
                        if( dist > major_neg )
                        {
                            major_neg = dist;
                            index_major_neg =i;
                        }
                    }

                } //end of a_cut

                if( b_cut == 0.0 )
                {
                    double minor_angle = ( minor_axis(0) * v(0) ) + ( minor_axis(1) * v(1) ) + ( minor_axis(2) * v(2) ) ;

                    // check angle with minor axis
                    if( minor_angle > thr )
                    {
                        if( dist > minor_pos )
                        {
                            minor_pos = dist;
                            index_minor_pos = i;
                        }
                    }
                    else if ( minor_angle < -thr )
                    {
                        if( dist > minor_neg )
                        {
                            minor_neg = dist;
                            index_minor_neg = i;
                        }
                    }
                } //end of b_cut

            }//end of for

            //a_cut
            if( a_cut == 0.0)
            {
                if( (index_major_pos != -1) && (index_major_neg != -1))
                {
                    a_cut = major_pos ;
                    if( major_neg > a_cut )
                        a_cut = major_neg;

                }
                else if( (index_major_pos == -1) && (index_major_neg != -1))
                    a_cut =  major_neg ;
                else if( (index_major_pos != -1) && (index_major_neg == -1))
                    a_cut =  major_pos ;
            }


            //b_cut
            if( b_cut == 0.0 )
            {
                if( (index_minor_pos != -1) && (index_minor_neg != -1))
                {
                    //b_cut = ( minor_pos + minor_neg ) / 2.0;
                    b_cut = minor_pos;
                    if( minor_neg > b_cut )
                        b_cut = minor_neg;
                }
                else if( (index_minor_pos == -1) && (index_minor_neg != -1))
                    b_cut =  minor_neg ;
                else if( (index_minor_pos != -1) && (index_minor_neg == -1))
                    b_cut =  minor_pos ;
            }

            thr = thr - 0.02;


        } //end of while


        //assign variable
        Eigen::Vector3d cut_axis;
        cut_axis(0) = a_cut;  // major axis
        cut_axis(1) = b_cut;  //minor axis
        cut_axis(2) = 0.0;
        segRegion[reg].set_axis(eigen_vecs);
        segRegion[reg].setcut_axis(cut_axis);

//        Eigen::Vector3f axis_value = pca.getEigenValues();
//        std::cout << " size " << segRegion[reg].getNNIndices().size() << std::endl;
//        std::cout << " eigen values = " << axis_value << std::endl;
//        std::cout << " eigen values factors = " << axis_value(0) / axis_value(1) << std::endl;
//        std::cout << " a_cut " << a_cut << " b_cut " << b_cut << " factor " << a_cut / b_cut << std::endl;

    }


return true;

}


void SurfacePatch::
visualizeRegion_pcl( const std::vector<PointCloudIndividual> &segRegion,
                     const PointCloudXYZ::Ptr &pt_cloud,
                     const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloudrgb)
{

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_vis( new pcl::PointCloud<pcl::PointXYZRGB>());
    pcl::visualization::PCLVisualizer viewer("cloud_viewer");
    viewer.addCoordinateSystem(0.1);

    int count1 = 0;
    for ( int count = 0 ; count < segRegion.size() ; count++  )
    {

        PointCloudIndividual obj = segRegion[count];
        Eigen::Vector3d pt_on_plane = obj.getrepresentative_position();
        Eigen::Vector3d re_normal = obj.getrepresentative_normal();
        Eigen::Vector3d cut = obj.getcut_axis();
        Eigen::Matrix3f eigen_vecs = obj.get_axis();
        Eigen::Vector3f axis_array;
        pcl::PointXYZ center ;
        pcl::PointXYZ pt_xyz2;
        pcl::PointXYZRGB pt_rgb;

        pt_rgb.r = std::rand() % 255;
        pt_rgb.g = std::rand() % 255;
        pt_rgb.b = std::rand() % 255;
        std::vector<int> nnIndices = obj.getNNIndices();
        for( int i = 0; i < nnIndices.size(); i++ )
        {
            pt_xyz2 = pt_cloud->points[nnIndices[i]];
            pt_rgb.x = pt_xyz2.x;
            pt_rgb.y = pt_xyz2.y;
            pt_rgb.z = pt_xyz2.z;

            cloud_vis->points.push_back(pt_rgb);
        }


        std::stringstream ss;
        std::string put_text;

          center.x = pt_on_plane(0);
          center.y = pt_on_plane(1);
          center.z = pt_on_plane(2);

          count1 ++;
          ss << count1;
          put_text = ss.str();

          //major axis
          axis_array = eigen_vecs.col(0);
          pt_xyz2.x  = pt_on_plane(0) + axis_array(0) * cut(0);
          pt_xyz2.y = pt_on_plane(1) + axis_array(1) * cut(0);
          pt_xyz2.z = pt_on_plane(2) + axis_array(2) * cut(0);
          viewer.addLine( center, pt_xyz2, 100, 100, 0, put_text);
          viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, put_text);

          count1 ++;
          ss << count1;
          put_text = ss.str();

          //major axis
          axis_array = eigen_vecs.col(1);
          pt_xyz2.x  = pt_on_plane(0) + axis_array(0) * cut(1);
          pt_xyz2.y = pt_on_plane(1) + axis_array(1) * cut(1);
          pt_xyz2.z = pt_on_plane(2) + axis_array(2) * cut(1);
          viewer.addLine( center, pt_xyz2, 0, 100, 100, put_text);
          viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, put_text);

          count1 ++;
          ss << count1;
          put_text = ss.str();
          pt_xyz2.x = pt_on_plane(0) + re_normal(0) * 0.03;
          pt_xyz2.y = pt_on_plane(1) + re_normal(1) * 0.03;
          pt_xyz2.z = pt_on_plane(2) + re_normal(2) * 0.03;
          viewer.addLine( center, pt_xyz2, 0, 0, 255, put_text);
          viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, put_text);

          axis_array = eigen_vecs.col(0);
          pcl::ModelCoefficients cylinder_coeff;
          cylinder_coeff.values.resize (7);
          cylinder_coeff.values[0] = pt_on_plane(0) - (axis_array(0) * cut(0)) ;
          cylinder_coeff.values[1] = pt_on_plane(1) - (axis_array(1) * cut(0));
          cylinder_coeff.values[2] = pt_on_plane(2) - (axis_array(2) * cut(0));

          cylinder_coeff.values[3] = 2 * (axis_array(0) * cut(0));
          cylinder_coeff.values[4] = 2 * (axis_array(1) * cut(0));
          cylinder_coeff.values[5] = 2 * (axis_array(2) * cut(0));
          cylinder_coeff.values[6] = cut(1);

          ss << count;
          put_text = ss.str();

    }// end of for loop


    viewer.addPointCloud(cloud_vis);
    std::cout << " close the window cloud_viewer to continue......." << std::endl;
    while(!viewer.wasStopped())
        viewer.spinOnce();

    return;

}

bool SurfacePatch::
visualize_primitives_handles(   const std::vector<Cylinder_Param> cylinder,
                                const std::vector<Sphere_Param> sphere,
                                const std::vector<Box_Param> box,
                                const PointCloudRGB::Ptr &f_cloud,
                                const std::vector<CylindricalShell> shells)
{

    int count = 0;
    pcl::visualization::PCLVisualizer viewer("fitted_primitives");

    //visualize handles
        pcl::KdTreeFLANN<pcl::PointXYZRGB> tree;
        tree.setInputCloud(f_cloud);
        std::vector<int> nn_indices;
        std::vector<float> nn_dists;

        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_vis( new pcl::PointCloud<pcl::PointXYZRGB>());
        pcl::PointXYZRGB pt_rgb;
        pt_rgb.r = 0;
        pt_rgb.g = 255;
        pt_rgb.b = 255;

        pcl::PointXYZ pt1,pt2,pt3;

        for( int s = 0; s < shells.size(); s++ )
        {
            CylindricalShell shell = shells[s];

            if( shell.getRadius() > 0.0 )
            {
                Eigen::Vector3d center = shell.getCentroid();
                Eigen::Vector3d normal = shell.getNormal();
                Eigen::Vector3d axis = shell.getCurvatureAxis();
                axis.normalize();
                Eigen::Vector3d v;
                double shell_extent = shell.getExtent();

                pt1.x = center(0);
                pt1.y = center(1);
                pt1.z = center(2);

                pt2.x = pt1.x + ( normal(0) * 0.07 );
                pt2.y = pt1.y + ( normal(1) * 0.07 );
                pt2.z = pt1.z + ( normal(2) * 0.07 );

                pt3.x = pt1.x + ( axis(0) * 0.07 );
                pt3.y = pt1.y + ( axis(1) * 0.07 );
                pt3.z = pt1.z + ( axis(2) * 0.07 );

                pt_rgb.x = center(0);
                pt_rgb.y = center(1);
                pt_rgb.z = center(2);

                tree.radiusSearch(pt_rgb, shell.getRadius() + 0.01, nn_indices, nn_dists);
                std::cout << shell.getRadius() << " nn_indices.size() = " << nn_indices.size() << std::endl;

                            for( int u = 0; u < nn_indices.size(); u++ )
                            {
                                Eigen::Vector3d cropped = f_cloud->points[nn_indices[u]].getVector3fMap().cast<double>();
                                v = cropped - center;
                                Eigen::Vector3d axis_robot = axis.cross(normal);
                                double along_axis_Dist = axis_robot.dot(v);
                                if( fabs(along_axis_Dist) < shell_extent / 2.0 )
                                {
                                    pcl::PointXYZRGB pt_rgb1 = f_cloud->points[nn_indices[u]];
                                    pt_rgb1.r = pt_rgb.r;
                                    pt_rgb1.g = pt_rgb.g;
                                    pt_rgb1.b = pt_rgb.b;
                                    cloud_vis->points.push_back(pt_rgb1);
                                }
                            }
                stringstream ss ;
                ss << count;
                viewer.addLine( pt1, pt2, 0, 0, 255, ss.str());
                viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, ss.str());

                count++;

                ss << count;
                viewer.addLine( pt1, pt3, 255, 0, 0, ss.str());
                viewer.setShapeRenderingProperties( pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, ss.str());

            }

        }
//        std::cout << " cloud size = " << cloud_vis->points.size() << std::endl;
    // add cylinder
    viewer.addPointCloud(f_cloud);
    viewer.addPointCloud(cloud_vis, "cloud1");
    std::cout << " close the window cloud_viewer to continue......." << std::endl;
    while(!viewer.wasStopped())
        viewer.spinOnce();
    viewer.close();

    return true;

}
