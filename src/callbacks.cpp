#include <grasp/callbacks.h>

/* brief -- function to capture RGB image  */
void colorImagecallBack(const sensor_msgs::ImageConstPtr &image, cv::Mat &drawImage, bool &flag)
{

  cv_bridge::CvImagePtr cv_ptr_frames; // kinect frames

  try
  {
    cv_ptr_frames = cv_bridge::toCvCopy(image,"bgr8");
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  drawImage = cv::Mat(cv_ptr_frames->image);
  flag = true;

  return;
}


// function to capture depth image
void depthImagecallBack(const sensor_msgs::ImageConstPtr &image, cv::Mat &drawImage, bool &flag)
{

  cv_bridge::CvImagePtr cv_ptr_frames; // kinect frames

  try
  {
    cv_ptr_frames = cv_bridge::toCvCopy(image,sensor_msgs::image_encodings::TYPE_32FC1);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  drawImage = cv::Mat(cv_ptr_frames->image);
  flag = true;

  return;
}

/* point cloud call back function */
void ptCloudcallBack(const sensor_msgs::PointCloud2::ConstPtr &input, pcl::PointCloud<pcl::PointXYZRGB>::Ptr ptCloud, bool &flag)
{
    std::cout << input->header.frame_id << endl;
    std::cout << "in call back" << endl;
    pcl::fromROSMsg( *input, *ptCloud );

    flag = true;

}


/* point cloud call back function */
void ptCloudcallBackXYZ(const sensor_msgs::PointCloud2::ConstPtr &input, pcl::PointCloud<pcl::PointXYZ>::Ptr ptCloud, bool &flag)
{

//    cout << input->header.frame_id << endl;

    pcl::fromROSMsg( *input, *ptCloud );

    flag = true;



}
