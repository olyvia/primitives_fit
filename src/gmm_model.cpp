

#include <gmm/gmm_model.h>

void SegmentationUsingGMM()

{


    EM bgModel;
    EM fgModel;

    string addr = "/home/olyvia/tcs/dataset/APC/gmm_train/template/";
    string model_path = "/home/olyvia/tcs/dataset/APC/gmm_train/model/";

#if(TRAIN_GMM)

    //***********************************************************************************

    cv::Mat bg_samples;
    cv::Mat fg_samples;
    int start_indx = 291;
    int end_indx = 305;

    // Read background data
    for(int i = start_indx; i < end_indx; i++)
    {


        string image_addr;
        string mask_addr;
        char s_i[200];
        sprintf(s_i,"%d.jpg",i);
        image_addr = addr + s_i ;
        cout << " processing img -- " << s_i << endl;

        sprintf(s_i,"mask_%d.jpg",i);
        mask_addr = addr + s_i ;

        cv::Mat frame = cv::imread(image_addr.c_str());
        cv::Mat frame_mask = cv::imread(mask_addr.c_str(),CV_LOAD_IMAGE_GRAYSCALE);

        cv::cvtColor(frame, frame, CV_BGR2HSV );

        for(int j = 0; j < frame.cols; j++ )
        {
            for(int k = 0; k < frame.rows; k++ )
            {

                //check with mask image
                int value = frame_mask.at<uchar>(k,j);

                if( value > 50 )
                {

                    cv::Mat temp(1,3,CV_64FC1);

                    temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/180.0;
                    temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                    temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                    fg_samples.push_back(temp.row(0));
                }
                else
                {
                    cv::Mat temp(1,3,CV_64FC1);

                    temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/180.0;
                    temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                    temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                    bg_samples.push_back(temp.row(0));
                }


            }

        }

    }


    //***********************************************************************************

    int clusNum = 2;

    // Train background model


    bgModel = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));



    vector<Mat> covsInit;//(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);

    Mat meansInput = Mat::zeros(clusNum, bg_samples.cols, CV_64FC1);

    Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

    Mat out1, out2, out3;


    cout << "Training GMM model" << endl;

    bgModel.trainE(bg_samples, meansInput, covsInit, weightsInput, out1, out2, out3);



    //***********************************************************************************
        // Train foreground model


        int fg_cluster = 5;

        fgModel =  EM(fg_cluster, EM::COV_MAT_DIAGONAL,TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));


        vector<Mat> covsInit_fg;//(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);

        Mat meansInput_fg = Mat::zeros(fg_cluster, fg_samples.cols, CV_64FC1);

        Mat weightsInput_fg = Mat::ones(1, fg_cluster, CV_64FC1)/fg_cluster;

        Mat out1_fg, out2_fg, out3_fg;


        fgModel.trainE(fg_samples,meansInput_fg,covsInit_fg,weightsInput_fg,out1_fg,out2_fg,out3_fg);

     //**************************************************************************************


    // Saving trained Model in a file (Both background and foreground)

    FileStorage fs_bg;
    string model_bg = model_path + "bgModel.txt" ;
    fs_bg.open( model_bg.c_str(), FileStorage::WRITE);

    bgModel.write(fs_bg);


    FileStorage fs_fg;
    string model_fg = model_path + "fgModel.txt";
    fs_fg.open( model_fg.c_str(), FileStorage::WRITE);

    fgModel.write(fs_fg);

    fs_fg.release();

    fs_bg.release();

    // ***********************************************************************************
    #else



        FileStorage fs_bg;
        string model_bg = model_path + "bgModel.txt" ;
        fs_bg.open( model_bg.c_str(), FileStorage::READ);

        if(!fs_bg.isOpened())

        {

            cout << "Can't open Background model File" << endl;

            exit(0);

        }

        const FileNode& fn_bg = fs_bg["StatModel.EM"];

        bgModel.read(fn_bg);





        FileStorage fs_fg;
        string model_fg = model_path + "fgModel.txt";
        fs_fg.open(model_fg.c_str(), FileStorage::READ);

        if(!fs_fg.isOpened())

        {

            cout << "Can't open Foreground model File" << endl;

            exit(0);

        }

        const FileNode& fn_fg = fs_fg["StatModel.EM"];


        fgModel.read(fn_fg);


        fs_fg.release();

        fs_bg.release();


    #endif

        // ******************************************************************************************

        cv::Mat test_image = cv::imread("/home/olyvia/tcs/dataset/APC/original.png");

        cvtColor(test_image,test_image,CV_BGR2HSV);


        cv::Mat segmented_imag = cv::Mat::zeros(test_image.size(),test_image.type());



        for(int i=0;i<test_image.cols;i++)

        {

            for(int j=0;j<test_image.rows;j++)

            {

                cv::Mat temp(1,3,CV_64FC1);


                temp.at<double>(0,0) = 100 * double(test_image.at<Vec3b>(j,i)[0])/180.0;

                temp.at<double>(0,1) = 100 * double(test_image.at<Vec3b>(j,i)[1])/255.0;

                temp.at<double>(0,2) = 100 * double(test_image.at<Vec3b>(j,i)[2])/255.0;


                cv::Mat probs_bg,probs_fg;


                Vec2d out_bg =  bgModel.predict(temp,probs_bg);

                Vec2d out_fg = fgModel.predict(temp,probs_fg);


                double likelihood_bg = std::exp(out_bg[0]);

                double likelihood_fg = std::exp(out_fg[0]);



                double probs = (double)likelihood_fg /(likelihood_bg+likelihood_fg);



                if(probs > 0.55)

                {
                    segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(255,255,255);

                }

                else

                {

                    segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(0,0,0);


                }

            }

        }

        cv::imshow("window" , segmented_imag );
        waitKey(0);



    return;


}



void twoClassSegmentationUsingGMM( string class_1, string class_2, int num_img )
{


    EM c1Model;
    EM c2Model;
    EM bgModel;

    string addr = "/home/olyvia/tcs/dataset/APC/gmm_train/template/";
    string model_path = "/home/olyvia/tcs/dataset/APC/gmm_train/model/";

#if(TRAIN_GMM)

    //***********************************************************************************

    cv::Mat c2_samples;
    cv::Mat c1_samples;
    cv::Mat bg_samples;

 {
    string addr_class = addr + class_1;


    // Read class 1 data
    for(int i = 1; i <= num_img; i++)
    {

        string image_addr;
        string mask_addr;
        char s_i[200];
        sprintf(s_i,"/%d.jpg",i);
        image_addr = addr_class + s_i ;
        cout << " processing img -- " << image_addr << endl;

        sprintf(s_i,"/mask_%d.jpg",i);
        mask_addr = addr_class + s_i ;

        cv::Mat frame = cv::imread( image_addr.c_str() );
        cv::Mat frame_mask = cv::imread(mask_addr.c_str(),CV_LOAD_IMAGE_GRAYSCALE);

//        cv::cvtColor(frame, frame, CV_BGR2HSV );

        for(int j = 0; j < frame.cols; j++ )
        {
            for(int k = 0; k < frame.rows; k++ )
            {

                //check with mask image
                int value = frame_mask.at<uchar>(k,j);

                if( value > 50 )
                {

                    cv::Mat temp(1,3,CV_64FC1);

//                    temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/180.0;
                    temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/255.0;
                    temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                    temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                    c1_samples.push_back(temp.row(0));
                }
                else
                {
                    cv::Mat temp(1,3,CV_64FC1);

//                    temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/180.0;
                    temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/255.0;
                    temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                    temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                    bg_samples.push_back(temp.row(0));
                }



            }

        }

    }


    //***********************************************************************************

    int clusNum = 6;

    c1Model = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

    vector<Mat> covsInit;//(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);

    Mat meansInput = Mat::zeros(clusNum, c1_samples.cols, CV_64FC1);

    Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

    Mat out1, out2, out3;


    cout << "Training GMM model" << endl;

    c1Model.trainE(c1_samples, meansInput, covsInit, weightsInput, out1, out2, out3);

    // ***********************************************************************************

    // Saving trained Model in a file (Both background and foreground)

    FileStorage fs_c1;
    string model_c1 = model_path + "c1Model.txt" ;
    fs_c1.open( model_c1.c_str(), FileStorage::WRITE);

    c1Model.write(fs_c1);

    fs_c1.release();

}


    //gmm for class 2
    {
       string addr_class = addr + class_2;


       // Read class 2 data
       for(int i = 1; i <= num_img; i++)
       {

           string image_addr;
           string mask_addr;
           char s_i[200];
           sprintf(s_i,"/%d.jpg",i);
           image_addr = addr_class + s_i ;
           cout << " processing img -- " << image_addr << endl;

           sprintf(s_i,"/mask_%d.jpg",i);
           mask_addr = addr_class + s_i ;

           cv::Mat frame = cv::imread( image_addr.c_str() );
           cv::Mat frame_mask = cv::imread(mask_addr.c_str(),CV_LOAD_IMAGE_GRAYSCALE);

//           cv::cvtColor(frame, frame, CV_BGR2HSV );

           for(int j = 0; j < frame.cols; j++ )
           {
               for(int k = 0; k < frame.rows; k++ )
               {

                   //check with mask image
                   int value = frame_mask.at<uchar>(k,j);

                   if( value > 50 )
                   {

                       cv::Mat temp(1,3,CV_64FC1);

//                       temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/180.0;
                       temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/255.0;
                       temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                       temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                       c2_samples.push_back(temp.row(0));
                   }
                   else
                   {
                       cv::Mat temp(1,3,CV_64FC1);

   //                    temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/180.0;
                       temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/255.0;
                       temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                       temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                       bg_samples.push_back(temp.row(0));
                   }



               }

           }

       }


       //***********************************************************************************

       int clusNum = 6;

       c2Model = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

       vector<Mat> covsInit;//(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);

       Mat meansInput = Mat::zeros(clusNum, c2_samples.cols, CV_64FC1);

       Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

       Mat out1, out2, out3;


       cout << "Training GMM model" << endl;

       c2Model.trainE(c2_samples, meansInput, covsInit, weightsInput, out1, out2, out3);

       // ***********************************************************************************

       // Saving trained Model in a file (Both background and foreground)

       FileStorage fs_c2;
       string model_c2 = model_path + "c2Model.txt" ;
       fs_c2.open( model_c2.c_str(), FileStorage::WRITE);

       c2Model.write(fs_c2);

       fs_c2.release();

   }

    {
        // train model for background
        //***********************************************************************************

        int clusNum = 2;

        bgModel = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

        vector<Mat> covsInit; //(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);

        Mat meansInput = Mat::zeros(clusNum, bg_samples.cols, CV_64FC1);

        Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

        Mat out1, out2, out3;


        cout << "Training bg_GMM model" << endl;

        bgModel.trainE(bg_samples, meansInput, covsInit, weightsInput, out1, out2, out3);

        // ***********************************************************************************

        // Saving trained Model in a file (Both background and foreground)

        FileStorage fs_bg;
        string model_bg = model_path + "bgModel.txt" ;
        fs_bg.open( model_bg.c_str(), FileStorage::WRITE);

        bgModel.write(fs_bg);

        fs_bg.release();

    }

    // ***********************************************************************************
    #else

        //read from file

        FileStorage fs_c1;
        string model_c1 = model_path + "c1Model.txt" ;
        fs_c1.open( model_c1.c_str(), FileStorage::READ);

        if(!fs_c1.isOpened())

        {

            cout << "Can't open c1  model File" << endl;

            exit(0);

        }

        const FileNode& fn_c1 = fs_c1["StatModel.EM"];

        c1Model.read(fn_c1);





        FileStorage fs_c2;
        string model_c2 = model_path + "c2Model.txt";
        fs_c2.open(model_c2.c_str(), FileStorage::READ);

        if(!fs_c2.isOpened())

        {

            cout << "Can't open c2 model File" << endl;

            exit(0);

        }

        const FileNode& fn_c2 = fs_c2["StatModel.EM"];


        c2Model.read(fn_c2);


        FileStorage fs_bg;
        string model_bg = model_path + "bgModel.txt";
        fs_bg.open(model_bg.c_str(), FileStorage::READ);

        if(!fs_bg.isOpened())

        {

            cout << "Can't open bg model File" << endl;

            exit(0);

        }

        const FileNode& fn_bg = fs_bg["StatModel.EM"];


        bgModel.read(fn_bg);


        fs_c1.release();

        fs_c2.release();

        fs_bg.release();

    #endif


        // ******************************************************************************************

        cv::Mat test_image = cv::imread("/home/olyvia/tcs/dataset/APC/original.png");

//        cvtColor(test_image,test_image,CV_BGR2HSV);


        cv::Mat segmented_imag = cv::Mat::zeros(test_image.size(),test_image.type());


        for( int i=0; i<test_image.cols; i++ )

        {

            for( int j=0; j<test_image.rows; j++)

            {

                cv::Mat temp(1,3,CV_64FC1);


//                temp.at<double>(0,0) = 100 * double(test_image.at<Vec3b>(j,i)[0])/180.0;
                temp.at<double>(0,0) = 100 * double(test_image.at<Vec3b>(j,i)[0])/255.0;
                temp.at<double>(0,1) = 100 * double(test_image.at<Vec3b>(j,i)[1])/255.0;
                temp.at<double>(0,2) = 100 * double(test_image.at<Vec3b>(j,i)[2])/255.0;


                cv::Mat probs_c1, probs_c2, probs_bg;


                Vec2d out_c1 =  c1Model.predict(temp,probs_c1);

                Vec2d out_c2 = c2Model.predict(temp,probs_c2);

                Vec2d out_bg = bgModel.predict(temp,probs_bg);


                double likelihood_c1 = std::exp(out_c1[0]);

                double likelihood_c2 = std::exp(out_c2[0]);

                double likelihood_bg = std::exp(out_bg[0]);


                double prob_c1 = (double)likelihood_c1 /(likelihood_c2+likelihood_c1 + likelihood_bg);
                double prob_c2 = (double)likelihood_c2 /(likelihood_c2+likelihood_c1 + likelihood_bg);
                double prob_bg = (double)likelihood_bg /(likelihood_c2+likelihood_c1 + likelihood_bg);



                if(prob_c1 > 0.55)
                {
                    segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(255,0,255);
//                    cout << " positive " ;
                }
                else if ( prob_c2 > 0.55)
                {
                    segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(0,0,255);
                }

//                cout <<"("<<j<<","<<i<<") = " <<out_c1[0] << " , 2 nd class = " << out_c2[0] << endl;

//                circle( segmented_imag,
//                         Point(j,i),
//                         1.0,
//                         Scalar( 0, 0, 255 ));
//            cv::imshow("window" , segmented_imag );
//            waitKey(100);

            }

        }

        cv::imshow("window" , segmented_imag );
                    waitKey(0);


    return;


}




void SegmentationUsingGMM( string class_1, int start_num, int end_num, int total_number )
{


    EM c1Model;
    EM c2Model;
    EM bgModel;

    string addr = "/home/olyvia/tcs/dataset/APC/unorganized_kinect_v2_raw_images";
    string model_path = "/home/olyvia/tcs/dataset/APC/gmm_train/model1/";


#if(TRAIN_GMM)

    //***********************************************************************************

    cv::Mat c2_samples;
    cv::Mat c1_samples;
    cv::Mat bg_samples;


    string addr_class = addr ;


    // Read class 1 data
    for(int i = 0; i <= total_number; i++)
    {

        string image_addr;
        string mask_addr;
        char s_i[200];
        sprintf(s_i,"/%d.jpg",i);
        image_addr = addr_class + s_i ;
        cout << " processing img -- " << image_addr << endl;

        sprintf(s_i,"/mask_%d.jpg",i);
        mask_addr = addr_class + s_i ;

        cv::Mat frame = cv::imread( image_addr.c_str() );
        cv::Mat frame_mask = cv::imread(mask_addr.c_str(),CV_LOAD_IMAGE_GRAYSCALE);

//        cv::cvtColor(frame, frame, CV_BGR2HSV );
        if( ( i >= start_num) && ( i <= end_num ) )
        {

            for(int j = 0; j < frame.cols; j++ )
            {
                for(int k = 0; k < frame.rows; k++ )
                {

                    //check with mask image
                    int value = frame_mask.at<uchar>(k,j);

                    if( value > 50 )
                    {

                        cv::Mat temp(1,3,CV_64FC1);

                        temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/255.0;
                        temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                        temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                        c1_samples.push_back(temp.row(0));
                    }
                    else
                    {
                        cv::Mat temp(1,3,CV_64FC1);

                        temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/255.0;
                        temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                        temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                        bg_samples.push_back(temp.row(0));
                    }



                }

            }

        } //end of if
        else
        {

            for(int j = 0; j < frame.cols; j++ )
            {
                for(int k = 0; k < frame.rows; k++ )
                {

                    //check with mask image
                    int value = frame_mask.at<uchar>(k,j);

                    if( value > 50 )
                    {

                        cv::Mat temp(1,3,CV_64FC1);

                        temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/255.0;
                        temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                        temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                        c2_samples.push_back(temp.row(0));
                    }

                }

            }
        }

    }




    {
        //***********************************************************************************

        int clusNum = 6;

        c1Model = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

        vector<Mat> covsInit;

        Mat meansInput = Mat::zeros(clusNum, c1_samples.cols, CV_64FC1);

        Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

        Mat out1, out2, out3;


        cout << "Training GMM model with points" <<c1_samples.size() <<  endl;

        c1Model.trainE(c1_samples, meansInput, covsInit, weightsInput, out1, out2, out3);

        // ***********************************************************************************

        // Saving trained Model in a file (Both background and foreground)

        FileStorage fs_c1;
        string model_c1 = model_path + "c1Model.txt" ;
        fs_c1.open( model_c1.c_str(), FileStorage::WRITE);

        c1Model.write(fs_c1);

        fs_c1.release();

    }


    {

      //gmm for class 2

       //***********************************************************************************

       int clusNum = 3;

       c2Model = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

       vector<Mat> covsInit;

       Mat meansInput = Mat::zeros(clusNum, c2_samples.cols, CV_64FC1);

       Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

       Mat out1, out2, out3;


       cout << "Training c2_GMM model with points " << c2_samples.size() << endl;

       c2Model.trainE(c2_samples, meansInput, covsInit, weightsInput, out1, out2, out3);

       // ***********************************************************************************

       // Saving trained Model in a file (Both background and foreground)

       FileStorage fs_c2;
       string model_c2 = model_path + "c2Model.txt" ;
       fs_c2.open( model_c2.c_str(), FileStorage::WRITE);

       c2Model.write(fs_c2);

       fs_c2.release();
    }


    {
        // train model for background
        //***********************************************************************************

        int clusNum = 2;

        bgModel = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

        vector<Mat> covsInit; //(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);

        Mat meansInput = Mat::zeros(clusNum, bg_samples.cols, CV_64FC1);

        Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

        Mat out1, out2, out3;


        cout << "Training bg_GMM model with points " << bg_samples.size() << endl;

        bgModel.trainE(bg_samples, meansInput, covsInit, weightsInput, out1, out2, out3);

        // ***********************************************************************************

        // Saving trained Model in a file (Both background and foreground)

        FileStorage fs_bg;
        string model_bg = model_path + "bgModel.txt" ;
        fs_bg.open( model_bg.c_str(), FileStorage::WRITE);

        bgModel.write(fs_bg);

        fs_bg.release();
    }



    // ***********************************************************************************
    #else

        //read from file

        FileStorage fs_c1;
        string model_c1 = model_path + "c1Model.txt" ;
        fs_c1.open( model_c1.c_str(), FileStorage::READ);

        if(!fs_c1.isOpened())

        {

            cout << "Can't open c1  model File" << endl;

            exit(0);

        }

        const FileNode& fn_c1 = fs_c1["StatModel.EM"];

        c1Model.read(fn_c1);





        FileStorage fs_c2;
        string model_c2 = model_path + "c2Model.txt";
        fs_c2.open(model_c2.c_str(), FileStorage::READ);

        if(!fs_c2.isOpened())

        {

            cout << "Can't open c2 model File" << endl;

            exit(0);

        }

        const FileNode& fn_c2 = fs_c2["StatModel.EM"];


        c2Model.read(fn_c2);


        FileStorage fs_bg;
        string model_bg = model_path + "bgModel.txt";
        fs_bg.open(model_bg.c_str(), FileStorage::READ);

        if(!fs_bg.isOpened())

        {

            cout << "Can't open bg model File" << endl;

            exit(0);

        }

        const FileNode& fn_bg = fs_bg["StatModel.EM"];


        bgModel.read(fn_bg);


        fs_c1.release();

        fs_c2.release();

        fs_bg.release();

    #endif


        // ******************************************************************************************

        cv::Mat test_image = cv::imread("/home/olyvia/tcs/dataset/APC/original.png");

//        cvtColor(test_image,test_image,CV_BGR2HSV);


        cv::Mat segmented_imag = cv::Mat::zeros(test_image.size(),test_image.type());


        for( int i=0; i<test_image.cols; i++ )
        {

            for( int j=0; j<test_image.rows; j++)
            {

                cv::Mat temp(1,3,CV_64FC1);


//                temp.at<double>(0,0) = 100 * double(test_image.at<Vec3b>(j,i)[0])/180.0;
                temp.at<double>(0,0) = 100 * double(test_image.at<Vec3b>(j,i)[0])/255.0;
                temp.at<double>(0,1) = 100 * double(test_image.at<Vec3b>(j,i)[1])/255.0;
                temp.at<double>(0,2) = 100 * double(test_image.at<Vec3b>(j,i)[2])/255.0;


                cv::Mat probs_c1, probs_c2, probs_bg;


                Vec2d out_c1 =  c1Model.predict(temp,probs_c1);

                Vec2d out_c2 = c2Model.predict(temp,probs_c2);

                Vec2d out_bg = bgModel.predict(temp,probs_bg);


                double likelihood_c1 = std::exp(out_c1[0]);

                double likelihood_c2 = std::exp(out_c2[0]);

                double likelihood_bg = std::exp(out_bg[0]);


                double prob_c1 = (double)likelihood_c1 /(likelihood_c2+likelihood_c1 + likelihood_bg);
                double prob_c2 = (double)likelihood_c2 /(likelihood_c2+likelihood_c1 + likelihood_bg);
                double prob_bg = (double)likelihood_bg /(likelihood_c2+likelihood_c1 + likelihood_bg);



                if(prob_c1 > 0.55)
                {
                    segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(255,0,255);
//                    cout << " positive " ;
                }
                else if ( prob_c2 > 0.55)
                {
                    segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(0,0,255);
                }

            }

        }

        cv::imshow("window" , segmented_imag );
                    waitKey(0);


    return;


}



void SegmentationUsingGMM( string class_1,  int total_number )
{


    EM c1Model;
    EM c2Model;
    EM bgModel;

    string addr = "/home/olyvia/tcs/dataset/APC/gmm_train/template/";
    string model_path = "/home/olyvia/tcs/dataset/APC/gmm_train/model/";

    vector<string> files(20);
    files[0] = "barkely_bones";
    files[1] = "bunny_book";
    files[2] = "clorox_brush";
    files[3] = "cloud_bear";
    files[4] = "command_hooks";
    files[5] = "crayola_24_ct";
    files[6] = "easter_sippy_cup";
    files[7] = "elmers_school_glue";
    files[8] = "expo_eraser";
    files[9] = "folgers_coffee";
    files[10] = "glucose_up_botle";
    files[11] = "jane_dvd";
    files[12] = "kleenex_towels";
    files[13] = "kygen_puppies";
    files[14] = "laugh_joke_book";
    files[15] = "pencils";
    files[16] = "safety_plugs";
    files[17] = "scotch_tape";
    files[18] = "staples_card";
    files[19] = "while_lightbulb";

    string cls_modelPath = model_path + class_1;


#if(TRAIN_GMM)

    //***********************************************************************************

    cv::Mat c2_samples;
    cv::Mat c1_samples;
    cv::Mat bg_samples;



    // Read class data
    for(int cls = 0; cls < files.size(); cls++)

    {

        std::cout << cls << " , " << files[cls] << endl;
        string addr_class = addr + files[cls];

        if( class_1.compare(files[cls]) == 0 )
        {
            for(int i = 0; i <= total_number; i++)
            {

                string image_addr;
                string mask_addr;
                char s_i[200];
                sprintf(s_i,"/%d.jpg",i);
                image_addr = addr_class + s_i ;


                sprintf(s_i,"/mask_%d.jpg",i);
                mask_addr = addr_class + s_i ;

                cv::Mat frame = cv::imread( image_addr.c_str() );
                cv::Mat frame_mask = cv::imread(mask_addr.c_str(),CV_LOAD_IMAGE_GRAYSCALE);

                //        cv::cvtColor(frame, frame, CV_BGR2HSV );

                if( frame.data != NULL )
                {
                    cout << " current cls img -- " << image_addr << endl;
                    for(int j = 0; j < frame.cols; j++ )
                    {
                        for(int k = 0; k < frame.rows; k++ )
                        {

                            //check with mask image
                            int value = frame_mask.at<uchar>(k,j);

                            if( value > 50 )
                            {

                                cv::Mat temp(1,3,CV_64FC1);

                                temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/255.0;
                                temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                                temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                                c1_samples.push_back(temp.row(0));
                            }
                            else
                            {
                                cv::Mat temp(1,3,CV_64FC1);

                                temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/255.0;
                                temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                                temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                                bg_samples.push_back(temp.row(0));
                            }

                        }

                    } //end of for

                }  //end of if

            } //end of for

        } //end of if
        else
        {

            for(int i = 0; i <= total_number; i++)
            {

                string image_addr;
                string mask_addr;
                char s_i[200];
                sprintf(s_i,"/%d.jpg",i);
                image_addr = addr_class + s_i ;


                sprintf(s_i,"/mask_%d.jpg",i);
                mask_addr = addr_class + s_i ;

                cv::Mat frame = cv::imread( image_addr.c_str() );
                cv::Mat frame_mask = cv::imread(mask_addr.c_str(),CV_LOAD_IMAGE_GRAYSCALE);

                //        cv::cvtColor(frame, frame, CV_BGR2HSV );

                if( frame.data != NULL )
                {
                    cout << " other img -- " << image_addr << endl;

                    for(int j = 0; j < frame.cols; j++ )
                    {
                        for(int k = 0; k < frame.rows; k++ )
                        {

                            //check with mask image
                            int value = frame_mask.at<uchar>(k,j);

                            if( value > 50 )
                            {

                                cv::Mat temp(1,3,CV_64FC1);

                                temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/255.0;
                                temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                                temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;

                                c2_samples.push_back(temp.row(0));
                            }

                        }

                    }// end of for (image)

                } //end of if

            } //end of for
        }// end of else

    } // end of for( class )

    cout << " c2 samples .." << c2_samples.size() << endl;
    cout << " bg samples .." << bg_samples.size() << endl;
    cout << " c1 samples .." << c1_samples.size() << endl;

    {
        //***********************************************************************************

        int clusNum = 6;

        c1Model = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

        vector<Mat> covsInit;

        Mat meansInput = Mat::zeros(clusNum, c1_samples.cols, CV_64FC1);

        Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

        Mat out1, out2, out3;


        cout << "Training GMM model with points" <<c1_samples.size() <<  endl;

        c1Model.trainE(c1_samples, meansInput, covsInit, weightsInput, out1, out2, out3);

        // ***********************************************************************************

        // Saving trained Model in a file (Both background and foreground)

        FileStorage fs_c1;
        string model_c1 = cls_modelPath + "/c1Model.txt" ;
        fs_c1.open( model_c1.c_str(), FileStorage::WRITE);

        c1Model.write(fs_c1);

        fs_c1.release();

    }


    {

      //gmm for class 2

       //***********************************************************************************

       int clusNum = 3;

       c2Model = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

       vector<Mat> covsInit;

       Mat meansInput = Mat::zeros(clusNum, c2_samples.cols, CV_64FC1);

       Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

       Mat out1, out2, out3;


       cout << "Training c2_GMM model with points " << c2_samples.size() << endl;

       c2Model.trainE(c2_samples, meansInput, covsInit, weightsInput, out1, out2, out3);

       // ***********************************************************************************

       // Saving trained Model in a file (Both background and foreground)

       FileStorage fs_c2;
       string model_c2 = cls_modelPath + "/ocModel.txt" ;
       fs_c2.open( model_c2.c_str(), FileStorage::WRITE);

       c2Model.write(fs_c2);

       fs_c2.release();
    }


    {
        // train model for background
        //***********************************************************************************

        int clusNum = 2;

        bgModel = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

        vector<Mat> covsInit; //(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);

        Mat meansInput = Mat::zeros(clusNum, bg_samples.cols, CV_64FC1);

        Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;

        Mat out1, out2, out3;


        cout << "Training bg_GMM model with points " << bg_samples.size() << endl;

        bgModel.trainE(bg_samples, meansInput, covsInit, weightsInput, out1, out2, out3);

        // ***********************************************************************************

        // Saving trained Model in a file (Both background and foreground)

        FileStorage fs_bg;
        string model_bg = cls_modelPath + "/bgModel.txt" ;
        fs_bg.open( model_bg.c_str(), FileStorage::WRITE);

        bgModel.write(fs_bg);

        fs_bg.release();
    }



    // ***********************************************************************************
    #else

        //read from file

        FileStorage fs_c1;
        string model_c1 = cls_modelPath + "c1Model.txt" ;
        fs_c1.open( model_c1.c_str(), FileStorage::READ);

        if(!fs_c1.isOpened())

        {

            cout << "Can't open c1  model File" << endl;

            exit(0);

        }

        const FileNode& fn_c1 = fs_c1["StatModel.EM"];

        c1Model.read(fn_c1);





        FileStorage fs_c2;
        string model_c2 = cls_modelPath + "c2Model.txt";
        fs_c2.open(model_c2.c_str(), FileStorage::READ);

        if(!fs_c2.isOpened())

        {

            cout << "Can't open c2 model File" << endl;

            exit(0);

        }

        const FileNode& fn_c2 = fs_c2["StatModel.EM"];


        c2Model.read(fn_c2);


        FileStorage fs_bg;
        string model_bg = cls_modelPath + "bgModel.txt";
        fs_bg.open(model_bg.c_str(), FileStorage::READ);

        if(!fs_bg.isOpened())

        {

            cout << "Can't open bg model File" << endl;

            exit(0);

        }

        const FileNode& fn_bg = fs_bg["StatModel.EM"];


        bgModel.read(fn_bg);


        fs_c1.release();

        fs_c2.release();

        fs_bg.release();

    #endif


        // ******************************************************************************************

        cv::Mat test_image = cv::imread("/home/olyvia/tcs/dataset/APC/original.png");

//        cvtColor(test_image,test_image,CV_BGR2HSV);


        cv::Mat segmented_imag = cv::Mat::zeros(test_image.size(),test_image.type());


        for( int i=0; i<test_image.cols; i++ )
        {

            for( int j=0; j<test_image.rows; j++)
            {

                cv::Mat temp(1,3,CV_64FC1);


//                temp.at<double>(0,0) = 100 * double(test_image.at<Vec3b>(j,i)[0])/180.0;
                temp.at<double>(0,0) = 100 * double(test_image.at<Vec3b>(j,i)[0])/255.0;
                temp.at<double>(0,1) = 100 * double(test_image.at<Vec3b>(j,i)[1])/255.0;
                temp.at<double>(0,2) = 100 * double(test_image.at<Vec3b>(j,i)[2])/255.0;


                cv::Mat probs_c1, probs_c2, probs_bg;


                Vec2d out_c1 =  c1Model.predict(temp,probs_c1);

                Vec2d out_c2 = c2Model.predict(temp,probs_c2);

                Vec2d out_bg = bgModel.predict(temp,probs_bg);


                double likelihood_c1 = std::exp(out_c1[0]);

                double likelihood_c2 = std::exp(out_c2[0]);

                double likelihood_bg = std::exp(out_bg[0]);


                double prob_c1 = (double)likelihood_c1 /(likelihood_c2+likelihood_c1 + likelihood_bg);
                double prob_c2 = (double)likelihood_c2 /(likelihood_c2+likelihood_c1 + likelihood_bg);
                double prob_bg = (double)likelihood_bg /(likelihood_c2+likelihood_c1 + likelihood_bg);



                if(prob_c1 > 0.55)
                {
                    segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(255,0,255);
//                    cout << " positive " ;
                }
                else if ( prob_c2 > 0.55)
                {
                    segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(0,0,255);
                }

            }

        }

//        cv::imshow("window" , segmented_imag );
//                    waitKey(0);

   cout << " end of thr function..";
    return;


}

bool classifySegment( std::vector<PointCloudIndividual> &segRegion,
                      const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &pt_cloud_rgb,
                      string class_1,
                      pcl::PointCloud<pcl::PointXYZRGB>::Ptr &vis_cloud_rgb)
{


    //declare EM variables
    EM c1Model;
    EM c2Model;
    EM bgModel;

    {
        string model_path = "/home/olyvia/tcs/dataset/APC/gmm_train/model/";
        string cls_modelPath = model_path + class_1;
        std::cout << cls_modelPath ;
        getchar();
        //read from file

        FileStorage fs_c1;
        string model_c1 = cls_modelPath + "c1Model.txt" ;
        fs_c1.open( model_c1.c_str(), FileStorage::READ);

        if(!fs_c1.isOpened())
        {
            cout << "Can't open c1  model File" << endl;
            exit(0);
        }

        const FileNode& fn_c1 = fs_c1["StatModel.EM"];
        c1Model.read(fn_c1);





        FileStorage fs_c2;
        string model_c2 = cls_modelPath + "c2Model.txt";
        fs_c2.open(model_c2.c_str(), FileStorage::READ);

        if(!fs_c2.isOpened())
        {
            cout << "Can't open c2 model File" << endl;
            exit(0);
        }

        const FileNode& fn_c2 = fs_c2["StatModel.EM"];
        c2Model.read(fn_c2);


        FileStorage fs_bg;
        string model_bg = cls_modelPath + "bgModel.txt";
        fs_bg.open(model_bg.c_str(), FileStorage::READ);

        if(!fs_bg.isOpened())

        {

            cout << "Can't open bg model File" << endl;

            exit(0);

        }

        const FileNode& fn_bg = fs_bg["StatModel.EM"];
        bgModel.read(fn_bg);


        fs_c1.release();
        fs_c2.release();
        fs_bg.release();

    }

    PointCloudIndividual reg_ind = segRegion[0];
    std::vector<int> nnIndx = reg_ind.getNNIndices();

    for( int idx = 0; idx < nnIndx.size(); idx++ )
    {

        cv::Mat temp(1,3,CV_64FC1);

        pcl::PointXYZRGB pt_rgb = pt_cloud_rgb->points[nnIndx[idx]];

        temp.at<double>(0,0) = 100 * double(pt_rgb.b)/255.0;  //double(test_image.at<Vec3b>(j,i)[0])/255.0; //B
        temp.at<double>(0,1) = 100 * double(pt_rgb.g)/255.0;  //double(test_image.at<Vec3b>(j,i)[1])/255.0; //G
        temp.at<double>(0,2) = 100 * double(pt_rgb.r)/255.0;  //double(test_image.at<Vec3b>(j,i)[2])/255.0; //R


        cv::Mat probs_c1, probs_c2, probs_bg;


        Vec2d out_c1 = c1Model.predict(temp,probs_c1);
        Vec2d out_c2 = c2Model.predict(temp,probs_c2);
        Vec2d out_bg = bgModel.predict(temp,probs_bg);


        double likelihood_c1 = std::exp(out_c1[0]);
        double likelihood_c2 = std::exp(out_c2[0]);
        double likelihood_bg = std::exp(out_bg[0]);


        double prob_c1 = (double)likelihood_c1 /(likelihood_c2+likelihood_c1 + likelihood_bg);
        double prob_c2 = (double)likelihood_c2 /(likelihood_c2+likelihood_c1 + likelihood_bg);
        double prob_bg = (double)likelihood_bg /(likelihood_c2+likelihood_c1 + likelihood_bg);



        if(prob_c1 > 0.55)
        {
//            segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(255,0,255);
            vis_cloud_rgb->points.push_back(pt_rgb);
    //                    cout << " positive " ;
        }
//        else if ( prob_c2 > 0.55)
//        {
//            segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(0,0,255);
//        }
    }

    return true;
}
