
#include <grasp/helpers.h>

std::vector<std::vector<int> > RG( const PointCloudXYZ::Ptr &pointCloudTemp,
                                   const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &input_rgb_cloud,
                                   vector<Rect> &patchesFromSurfaceNormals,cv::Mat img)
{
    PointCloudXYZ::Ptr pt_cloud(new PointCloudXYZ());
    std::vector<int> indicesPointCloud;
    pointCloudTemp->is_dense = false;
    pcl::removeNaNFromPointCloud(*pointCloudTemp, *pt_cloud, indicesPointCloud);


    pcl::search::Search<PointXYZ>::Ptr tree = boost::shared_ptr<pcl::search::Search<PointXYZ> > (new pcl::search::KdTree<PointXYZ>);
    pcl::PointCloud <pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud <pcl::Normal>);
    pcl::NormalEstimationOMP<PointXYZ, pcl::Normal> normal_estimator;
    normal_estimator.setSearchMethod (tree);
    normal_estimator.setInputCloud (pt_cloud);
    normal_estimator.setNumberOfThreads(8);
    normal_estimator.setKSearch (50);
    normal_estimator.compute (*cloud_normals);


    pcl::RegionGrowing<PointXYZ, pcl::Normal> reg;

    reg.setMinClusterSize(50);
    reg.setMaxClusterSize(1000000);
    reg.setSearchMethod(tree);
    reg.setNumberOfNeighbours(30);
    reg.setInputCloud(pt_cloud);
    reg.setInputNormals(cloud_normals);
    reg.setSmoothnessThreshold( 4 / 180.0 * M_PI);  //parameter
    reg.setCurvatureThreshold(2.0);

    std::vector <pcl::PointIndices> clusters;
    reg.extract (clusters);

    vector< vector<int> > cluster_indices;
    for( int cls = 0 ; cls < clusters.size() ; cls++ )
    {
        vector<int> temp;

        for( int i = 0; i < clusters[cls].indices.size(); i++)
        {
            int indx = indicesPointCloud[clusters[cls].indices[i]];
            temp.push_back(indx);
        }
        cluster_indices.push_back(temp);
    }


    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++


    //++++++++++++++++++++++++++++++++++++++++++++++++

    //****************************************************



    cv::Mat image_t = cv::Mat::zeros(img.rows,img.cols,CV_8UC3);


    cv::Point pt;
    for( int index = 0; index< cluster_indices.size(); index++ )
    {
        vector<cv::Point> cluster_points;
        std::vector<int> idx = cluster_indices[index];

        for ( int j = 0 ; j < idx.size(); j++ )
        {
            //         PointXYZ ptrgb;
            //         ptrgb = pt_cloud->points[idx[j]];
            pt.x = idx[j] / img.cols;
            pt.y = idx[j] % img.cols;
            pcl::PointXYZRGB ptxyzrgb = input_rgb_cloud->points[idx[j]];

            //            cout << (int)ptxyzrgb.r << "\t" << (int)ptxyzrgb.g << "\t" << (int)ptxyzrgb.b << endl;
            image_t.at<Vec3b>(pt.x,pt.y) = cv::Vec3b((int)ptxyzrgb.b,(int)ptxyzrgb.g,(int)ptxyzrgb.r);
            cluster_points.push_back(cv::Point(pt.y,pt.x));

        }
        //        cout << index << "\t" << cluster_points.size() << endl;
        cv::Rect tmpRect = cv::boundingRect(cluster_points);

        patchesFromSurfaceNormals.push_back(tmpRect);

        cv::rectangle(img,tmpRect,cv::Scalar(255,0,0),2,8,0);

    }

//        cv::imshow("image_t",image_t);
//        cv::imshow("Window from normals",img);
//        cv::waitKey(0);

    cout << "Number of rectangles \t" << patchesFromSurfaceNormals.size() << endl;

    // Surface normal patches display image
    //    cv::Mat img = cv::imread("/home/nishant/Desktop/28Mar/img_0.jpg");

    //    for(int i=0;i<patchesFromSurfaceNormals.size();i++)
    //    {
    //        cv::rectangle(img,patchesFromSurfaceNormals[i],cv::Scalar(255,0,0),2,8,0);
    //    }

    //    cv::imshow("Window from normals",img);
    //    cv::waitKey(0);


    //*********************************************



    return cluster_indices;


}

bool showHandle( const pcl::PointCloud<pcl::PointXYZ>::Ptr ptCloud,
                 const std::vector<std::vector<int> > &indices,
                 pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloudVis)
{


    cloudVis->points.resize(0);
    pcl::PointXYZRGB pt;
    for( int index = 0; index< indices.size(); index++ )
    {
        pt.r = std::rand() % 255;
        pt.g = std::rand() % 255;
        pt.b = std::rand() % 255;
        std::vector<int> idx = indices[index];
        for ( int j = 0 ; j < idx.size(); j++ )
        {
            PointXYZ ptrgb;
            ptrgb = ptCloud->points[idx[j]];
            pt.x = ptrgb.x;
            pt.y = ptrgb.y;
            pt.z = ptrgb.z;

            cloudVis->points.push_back(pt);
        }


    }


    //    // Input point cloud viewer
    //    pcl::visualization::PCLVisualizer inputviewer;
    //    inputviewer.addCoordinateSystem(0,1,0.0,0.0,0.0);
    //    inputviewer.addPointCloud(input_rgb_cloud,"inputCloud");


    //    pcl::visualization::PCLVisualizer viewer;
    //    viewer.addCoordinateSystem(0.1,0.0,0.0,0.0);
    //    if(!cloudVis->empty())
    //    {
    //        viewer.addPointCloud(cloudVis, "cloud vis");
    //        while(!viewer.wasStopped())
    //        {
    //            viewer.spinOnce();
    //            //                    inputviewer.spinOnce();
    //            //                    pcl::toROSMsg(*cloudVis, pc2msg);

    //            //                    pc2msg.header.stamp = ros::Time::now();
    //            //                    pc2msg.header.frame_id = "/camera_depth_optical_frame";
    //            //                    pub.publish(pc2msg);  //pt_cloud
    //        }
    //        viewer.close();
    //    }
    //    else
    //        cout << "No points in the cloud" << endl;

    std::cout << " cloudVis = " << cloudVis->points.size() << std::endl;

    return true;
}
/*!
      @brief Finds a random color for a given seed, defualt seed id current timestamp in nanoseconds.
      @param[in]    Input seed for random number generator.
      @return       Random Scalar color value.
      */
Scalar getRandColor(uint64_t seed )
{
    timespec t1;
    clock_gettime(0,&t1);
    //   RNG randNum(t1.tv_nsec);
    RNG randNum(seed);
    Scalar randColor;

    int t = randNum.uniform(0,256);
    for(int k=0; k<3; k++)
    {
        t = randNum.uniform(0,256);
        randColor[k] = t;
    }

    return randColor;
}

void showHistogram(Mat& img,vector<double>& array)
{
    int bins = 256;             // number of bins
    int nc = img.channels();    // number of channels
    vector<Mat> hist(nc);       // array for storing the histograms
    vector<Mat> canvas(nc);     // images for displaying the histogram
    int hmax[3] = {0,0,0};      // peak value for each histogram

    // The rest of the code will be placed here
    for (int i = 0; i < hist.size(); i++)
        hist[i] = Mat::zeros(1, bins, CV_32SC1);


    for (int i = 0; i < img.rows; i++)
    {
        for (int j = 0; j < img.cols; j++)
        {
            for (int k = 0; k < nc; k++)
            {
                uchar val = nc == 1 ? img.at<uchar>(i,j) : img.at<Vec3b>(i,j)[k];
                hist[k].at<int>(val) += 1;
            }
        }
    }


    for (int i = 0; i < nc; i++)
    {
        for (int j = 0; j < bins-1; j++)
            hmax[i] = hist[i].at<int>(j) > hmax[i] ? hist[i].at<int>(j) : hmax[i];
    }

    for(int i=0;i<bins-1;i++)
        array.push_back(hist[0].at<int>(i));
    //    const char* wname[3] = { "Hue", "Sat", "Value" };

    const char* wname[3] = { "blue", "green", "red" };
    Scalar colors[3] = { Scalar(255,0,0), Scalar(0,255,0), Scalar(0,0,255) };

    for (int i = 0; i < nc; i++)
    {
        canvas[i] = Mat::ones(125, bins, CV_8UC3);

        for (int j = 0, rows = canvas[i].rows; j < bins-1; j++)
        {
            line(
                        canvas[i],
                        Point(j, rows),
                        Point(j, rows - (hist[i].at<int>(j) * rows/hmax[i])),
                        nc == 1 ? Scalar(200,200,200) : colors[i],
                        1, 8, 0
                        );
        }

        imshow(nc == 1 ? "value" : wname[i], canvas[i]);
    }
}

void FindBlobs(const cv::Mat &binary, std::vector < std::vector<cv::Point2i> > &blobs)
{
    blobs.clear();

    // Fill the label_image with the blobs
    // 0  - background
    // 1  - unlabelled foreground
    // 2+ - labelled foreground

    cv::Mat label_image;
    binary.convertTo(label_image, CV_32SC1);

    int label_count = 2; // starts at 2 because 0,1 are used already

    for(int y=0; y < label_image.rows; y++) {
        int *row = (int*)label_image.ptr(y);
        for(int x=0; x < label_image.cols; x++) {
            if(row[x] != 1) {
                continue;
            }

            cv::Rect rect;
            cv::floodFill(label_image, cv::Point(x,y), label_count, &rect, 0, 0, 4);

            std::vector <cv::Point2i> blob;

            for(int i=rect.y; i < (rect.y+rect.height); i++) {
                int *row2 = (int*)label_image.ptr(i);
                for(int j=rect.x; j < (rect.x+rect.width); j++) {
                    if(row2[j] != label_count) {
                        continue;
                    }

                    blob.push_back(cv::Point2i(j,i));
                }
            }

            blobs.push_back(blob);

            label_count++;
        }
    }
}

#if(MRF_MAP)

/*!
    @brief Function for finding the factors for a given adjacenecy matrix of a graph
    @param[in,out] factorFile   stores the factors in particular format in this file
    @param[in] adjMat           adjacency matrix of the graph
    @param[in] candidates       candidates for the nodes of graph
    @param[out] edgePotentials  stores edge potentials for each possible edge, in order to calculate the error of the selected edge
*/
void getFactors(ofstream& factorFile,vector<double> true_angle, Mat_<int> adjMat, vector< vector<Point2f> > candidates,
                vector< Mat_<float> >& edgePotentials)
{
    // Iterate over the adjacency matrix (upper triangualar part only)
    for(uint i=0; i<adjMat.rows; i++)
        for(uint j=i+1; j<adjMat.cols; j++)
        {
            // only if there is a connection between two nodes
            if(adjMat[i][j]!=-1)
            {
                cout << "Inside -------------------------------- \t" << endl;
                Mat_<float> truthTable(candidates[i].size(),candidates[j].size(),0.0);
                // for this particular case, the required angle between nodes is -90
                //                float theta1 = -90;
                float theta1 = true_angle[i];


                factorFile << 2 << endl; // Number of variables in the factor
                factorFile << i << " " << j << endl; // Indicies of those variables
                factorFile << candidates[i].size() << " " << candidates[j].size() << endl; // Cardinality of each variable
                factorFile <<  candidates[i].size() * candidates[j].size() << endl; // Non Zero states

                int factorCounter=0;

                // Iterate over all the possible candidates for given pair of nodes
                for(int m=0; m<candidates[j].size(); m++)
                {
                    //                    candidateError[j][m] = 0;
                    for(int l=0; l<candidates[i].size(); l++)
                    {

                        //                        candidateError[i][l] = 0;
                        {

                            // Find the angle between candidate nodes
                            float theta2 = /*float(180 * 7.0/22.0) **/
                                    ( atan2( (candidates[i][l].y - candidates[j][m].y), (candidates[i][l].x - candidates[j][m].x) ) );

                            //                            if(isnan(theta2))
                            //                                theta2 = -90;
                            cout << "Thetas \t" << theta2 << "\t" << theta1 <<
                                    "\t" << candidates[i][l].y << "\t" << candidates[j][m].y
                                 << "\t" << candidates[i][l].x << "\t" << candidates[j][m].x << endl;
                            // Calcualte edge potential
                            float psi = exp(- 5*abs(theta1-theta2)/100 );

                            // if it is too less, punish further, possibly an outlier
                            if(psi < 0.6)
                            {
                                psi = 0.0001;
                            }

                            factorFile << factorCounter << " " << psi << endl; // factor state index and value

                            truthTable(l,m) = psi;

                            //                            cout << i << "\t" << j << "\t" << l << "\t" << m << "\t" <<
                            //                                    theta2 << "\t" << psi << "\t" << abs(theta1-theta2) << endl;

                            factorCounter++;
                        }
                    }
                }
                edgePotentials.push_back(truthTable);

                factorFile << "\n" << endl;
            }
        }
}

///*!
//    @brief Finds graph matching based on MAP solution using BP for a given MRF model of an undirected graph
//    @param[in] adjMat   adjacency matrix of the graph (n x n, n is number of nodes of graph)
//    @param[in] candidates   observed candidates for each node of graph
//    @param[out] outpoints   selected candidates
//    @param[out] partsFound  flags vector for nodes found or not found
//    @param[out] matchIndex  indicies of selected candidates
//*/
//void findMatch_MRF_MAP(Mat_<int> adjMat, vector<double> trueAngle,
//                       vector< vector<Point2f> > candidates,
//                       vector<Point2f>& outPoints,
//                       vector<int>& partsFound,
//                       vector<int>& matchIndex)
//{


//    // File to store the factors in a particular format
//    ofstream factorFile;
//    factorFile.open("factor.fg");

//    factorFile << adjMat.rows-1 << "\n" << endl;

//    vector< Mat_<float> > edgePotentials;

//    // Function for calculating factors
//    getFactors(factorFile,trueAngle,adjMat,candidates,edgePotentials);

//    cout << "Factors reading done \t" << endl;
//    // Reading factors from the file (written above)
//    FactorGraph fg;
//    fg.ReadFromFile("factor.fg");

//    cout << fg.nrEdges() << endl;

//    // Set some constants
//    size_t maxiter = 10000;
//    Real   tol = 1e-9;
//    size_t verb = 1;

//    // Store the constants in a PropertySet object
//    PropertySet opts;
//    opts.set("maxiter",maxiter);  // Maximum number of iterations
//    opts.set("tol",tol);          // Tolerance for convergence
//    opts.set("verbose",verb);     // Verbosity (amount of output generated)

//    // Construct a BP (belief propagation) object from the FactorGraph fg
//    // using the parameters specified by opts and two additional properties,
//    // specifying the type of updates the BP algorithm should perform and
//    // whether they should be done in the real or in the logdomain
//    //
//    // Note that inference is set to MAXPROD, which means that the object
//    // will perform the max-product algorithm instead of the sum-product algorithm
//    BP mp(fg, opts("updates",string("SEQMAX"))("logdomain",false)("inference",string("MAXPROD"))("damping",string("0.1")));
//    // Initialize max-product algorithm
//    mp.init();
//    // Run max-product algorithm
//    mp.run();
//    // Calculate joint state of all variables that has maximum probability
//    // based on the max-product result

//    vector<size_t> mpstate = mp.findMaximum();

//    double prob = (double)std::exp(fg.logScore(mpstate))/std::exp(mp.logZ());
//    cout << "Probability \t" << prob << endl;

//    //    cout << "belief \t" << mp.beliefs() << endl;
//    //    cout << mp.logZ() << endl;
//    //    cout << mp.beliefV(0) << endl;
//    cout << fg.logScore(mpstate) << "\t" << mp.logScore(mpstate) << endl;
//    cout << "Log Z value \t" << mp.logZ() << "\t" << mp.logScore(mpstate) << endl;
//    //    exit(0);
//    cout << "Mpstate values \t" << mpstate[0] << "\t" << mpstate[1] << "\t" << mpstate[2] << endl;

//    for( size_t i = 0; i < mpstate.size(); i++ )
//    {
//        cout << mpstate[i] << "\t" << candidates[i][mpstate[i]] << endl;
//        // Store the output points selected by MAP solution (mpstate stores the selected candidate index)
//        outPoints.push_back(candidates[i][mpstate[i]]);
//        matchIndex.push_back(mpstate[i]);
//    }

//    partsFound = vector<int>(adjMat.rows-1,1);
//    // Check if selected edgePotential is in range
//    for(int k=0; k<adjMat.rows-1; k++)
//    {
//        if(edgePotentials[k](mpstate[k],mpstate[k+1]) < 0.1)
//        {
//            partsFound[k] = -1;
//        }
//    }
//}

#endif


void getConnectedRegions(Mat color_image,vector<cv::Point2f> &patch_centers,
                         vector<cv::Scalar> &patch_colors,vector<double> &patch_area)
{
    patch_centers.clear();
    patch_area.clear();
    patch_colors.clear();

    cv::Mat display_image = color_image.clone();

    Mat c_img, g_img, mask, dst, result;
    bool all_complete = false;
    color_image.copyTo(c_img);
    cvtColor(color_image, g_img, COLOR_BGR2GRAY);
    mask.create(color_image.rows+2, color_image.cols+2, CV_8UC1);
    mask = Scalar::all(0);
    namedWindow( "image", 0 );

    int low_diff = 55, up_diff = 55;
    int ffillMode = 1, useColor = 1, connect_bar = 0;
    int connectivity = 4;
    int newMaskVal = 255;
    createTrackbar( "lo_diff", "image", &low_diff, 255, 0 );
    createTrackbar( "up_diff", "image", &up_diff, 255, 0 );
    createTrackbar( "Mode", "image", &ffillMode, 2, 0 );
    createTrackbar( "Image_format", "image", &useColor, 1, 0 );
    createTrackbar( "Connectivity", "image", &connect_bar, 1, 0 );


    vector< vector<Point> > inliers;
    Mat scan_matrix(color_image.rows, color_image.cols, CV_8UC1, Scalar::all(0));

    int scale_x = 0, scale_y = 0;
    double t = ros::Time::now().toSec();
    while(!all_complete)
    {
        Point seed;
        bool got_seed = false;

        for(int i = scale_x; i < color_image.rows; i++)
        {
            for(int j = scale_y; j < color_image.cols; j++)
            {
                if(mask.at<uchar>(i+1,j+1)==0 && scan_matrix.at<uchar>(i,j)==0)
                {
                    seed = Point (j,i);// seed idx to referred as[col,row]
                    got_seed = true;
                    scan_matrix.at<uchar>(i,j)=255;

                    if(i>scale_x)
                        scale_y = 0;
                    else
                        scale_y = j;
                    scale_x = i;

                    break;
                }
            }
            if(got_seed)
            {
                break;
            }
            else
            {
                scale_y = 0;
                if(i>=color_image.rows-1)
                {
                    cout << "[Row:Col][" << i << "," << color_image.cols << "]\nCompleted image scan" << endl;
                    all_complete = true;
                    break;
                }
            }
        }

        if(!all_complete)
        {
            int lo = ffillMode == 0 ? 0 : low_diff;
            int up = ffillMode == 0 ? 0 : up_diff;
            int flags = connectivity + (newMaskVal << 8) +
                    (ffillMode == 1 ? FLOODFILL_FIXED_RANGE : 0);
            int b = (unsigned)theRNG() & 255;
            int g = (unsigned)theRNG() & 255;
            int r = (unsigned)theRNG() & 255;
            Rect ccomp;
            Scalar newVal = useColor ? Scalar(b, g, r) : Scalar(r*0.299 + g*0.587 + b*0.114);
            dst = useColor ? c_img : g_img;
            int area;
            threshold(mask, mask, 1, 128, THRESH_BINARY);
            area = floodFill(dst, mask, seed, newVal, &ccomp, Scalar(lo, lo, lo),
                             Scalar(up, up, up), flags);

            if(area>0)
            {
                vector<Point> locations;
                Mat r_above = (mask==newMaskVal);
                findNonZero(r_above,locations);
                for(int i = 0; i < locations.size(); i++)
                {
                    locations[i].x -= 1;// column index of pixel
                    locations[i].y -= 1;// row index of pixel
                }

                inliers.push_back(locations);
            }
        }
    }
    cout << "Time: " << ros::Time::now().toSec() - t << endl;
    if(!dst.empty())
    {
        imshow("image", dst);
        waitKey(1);
    }
    cout << "Processed floodfill for entire image" << endl;
    cout << "Got " << inliers.size() << " regions" << endl;


    vector<double> patchAreas;
    vector<cv::Point2f> patchCenters;
    vector<cv::Scalar> patchColors;
    vector<int> patchIndex;

    for(int i=0;i<inliers.size();i++)
    {
        cv::Rect mRect = boundingRect(inliers[i]);
        if(mRect.area() > 500)
        {
            patchIndex.push_back(i);
            patchCenters.push_back(cv::Point2f(mRect.tl().x+mRect.width/2,mRect.tl().y+mRect.height/2));
            patchAreas.push_back(mRect.area());
            int R=0,G=0,B=0;

            for(int j=0;j<inliers[i].size();j++)
            {
                Vec3b intensity = color_image.at<Vec3b>(inliers[i][j].y,inliers[i][j].x);
                R += intensity.val[0];
                G += intensity.val[1];
                B += intensity.val[2];
            }

            int avg_r=0,avg_b=0,avg_g=0;
            avg_r = (int)R / inliers[i].size();
            avg_b = (int)B / inliers[i].size();
            avg_g = (int)G / inliers[i].size();


            patchColors.push_back(cv::Scalar(avg_r,avg_g,avg_b));


        }
    }

    cout << "Sizes of each patch vectors \t" << patchAreas.size() << "\t" <<
            patchCenters.size() << "\t" << patchColors.size() << "\t" << patchIndex.size() << endl;


    for(int i=0;i<patchCenters.size();i++)
    {

        for(int j=0;j<inliers[patchIndex[i]].size();j++)
        {
            cv::circle(display_image,inliers[patchIndex[i]][j],1,cv::Scalar(0,0,0),1,8,0);
        }
        cv::circle(display_image,patchCenters[i],3,cv::Scalar(255,255,255),-1,8,0);

        //        cout << patchColors[i] << endl;
        patch_centers.push_back(patchCenters[i]);
        patch_colors.push_back(patchColors[i]);
        patch_area.push_back(patchAreas[i]);
        //        cv::imshow("display_image",display_image);
        //        cv::waitKey(0);
        cout << patchCenters[i] << "\t" << patchColors[i] << "\t" << patchAreas[i] << endl;
    }

    cv::imshow("display_image",display_image);
    cv::waitKey(0);
    //    exit(0);

    //    waitKey(0);
    return;
}
void createHSHistogram(cv::Mat img,cv::MatND& hist,cv::Mat mask)
{
    Mat src, hsv;
    //        if( argc != 2 || !(src=imread(argv[1], 1)).data )
    //            return -1;

    src = img.clone();
    cvtColor(src, hsv, CV_BGR2HSV);

    // Quantize the hue to 30 levels
    // and the saturation to 32 levels
    int hbins = 50, sbins = 32;
    int histSize[] = {hbins, sbins};
    // hue varies from 0 to 179, see cvtColor
    float hranges[] = { 0, 180 };
    // saturation varies from 0 (black-gray-white) to
    // 255 (pure spectrum color)
    float sranges[] = { 0, 256 };
    const float* ranges[] = { hranges, sranges };
    // we compute the histogram from the 0-th and 1-st channels
    int channels[] = {0, 1};

    if(mask.data == NULL)
    {
        calcHist( &hsv, 1, channels, Mat(), // do not use mask
                  hist, 2, histSize, ranges,
                  true, // the histogram is uniform
                  false );
        cout << " not using mask " << endl;
    }
    else
    {
        calcHist( &hsv, 1, channels, mask, //  use mask
                  hist, 2, histSize, ranges,
                  true, // the histogram is uniform
                  false );
        cout << " using mask " << endl;
    }

    cv::normalize(hist,hist,1.0,0.0,cv::NORM_L1);

    //    cout << hist.rows << "\t" << hist.cols  << endl;
    //    double maxVal=0;
    //    minMaxLoc(hist, 0, &maxVal, 0, 0);

    //    cout << hist << endl;

    //    int scale = 10;
    //    Mat histImg = Mat::zeros(sbins*scale, hbins*10, CV_8UC3);

    //    for( int h = 0; h < hbins; h++ )
    //    {
    //        for( int s = 0; s < sbins; s++ )
    //        {
    //            float binVal = hist.at<float>(h, s);
    //            int intensity = cvRound(binVal*255/maxVal);
    //            cout << intensity << "\t" ;
    //            rectangle( histImg, Point(h*scale, s*scale),
    //                       Point( (h+1)*scale - 1, (s+1)*scale - 1),
    //                       Scalar::all(intensity),
    //                       CV_FILLED );
    //        }
    //        cout << endl;
    //    }

    //    namedWindow( "Source", 1 );
    //    imshow( "Source", src );

    //    namedWindow( "H-S Histogram", 1 );
    //    imshow( "H-S Histogram", histImg );
    //    waitKey();
    return;

}
template<typename T>
void findMaxOfArray(vector<T> array,int &maxIndex,T &maxValue)
{

    maxValue = 0;
    for(int i=0;i<array.size();i++)
    {
        if(array[i] > maxValue)
        {
            maxIndex = i;
            maxValue = array[i];
        }
    }


    return;
}



template <typename _Tp>
void OLBP_(const Mat& src, Mat& dst,const Mat mask) {
    dst = Mat::zeros(src.rows-2, src.cols-2, CV_8UC1);
    for(int i=1;i<src.rows-1;i++) {
        for(int j=1;j<src.cols-1;j++) {
            _Tp center = src.at<_Tp>(i,j);
            unsigned char code = 0;

            if(mask.data != NULL)
            {
                if(mask.at<_Tp>(i,j) > 200)
                {
                    code |= (src.at<_Tp>(i-1,j-1) > center) << 7;
                    code |= (src.at<_Tp>(i-1,j) > center) << 6;
                    code |= (src.at<_Tp>(i-1,j+1) > center) << 5;
                    code |= (src.at<_Tp>(i,j+1) > center) << 4;
                    code |= (src.at<_Tp>(i+1,j+1) > center) << 3;
                    code |= (src.at<_Tp>(i+1,j) > center) << 2;
                    code |= (src.at<_Tp>(i+1,j-1) > center) << 1;
                    code |= (src.at<_Tp>(i,j-1) > center) << 0;
                }
                dst.at<unsigned char>(i-1,j-1) = code;
            }
            else
            {
                code |= (src.at<_Tp>(i-1,j-1) > center) << 7;
                code |= (src.at<_Tp>(i-1,j) > center) << 6;
                code |= (src.at<_Tp>(i-1,j+1) > center) << 5;
                code |= (src.at<_Tp>(i,j+1) > center) << 4;
                code |= (src.at<_Tp>(i+1,j+1) > center) << 3;
                code |= (src.at<_Tp>(i+1,j) > center) << 2;
                code |= (src.at<_Tp>(i+1,j-1) > center) << 1;
                code |= (src.at<_Tp>(i,j-1) > center) << 0;
                dst.at<unsigned char>(i-1,j-1) = code;

            }
        }
    }

    return;
}


void lbp_histogram(cv::Mat lbp_img,cv::Mat &hist)
{
    hist = cv::Mat::zeros(1,256,CV_32F);


    for(int i=0;i<lbp_img.rows;i++)
    {
        for(int j=0;j<lbp_img.cols;j++)
        {
            hist.at<float>(0,(int)(lbp_img.at<uchar>(i,j)))+=1;
        }
    }

    cv::normalize(hist,hist,1.0,0.0,cv::NORM_L1);

    return;

}

void createH_Histogram(cv::Mat img, cv::MatND &hist,cv::Mat mask)
{
//    hist = cv::MatND::zeros(1,183,CV_8UC1);



    vector<float> hist_temp(183,0);

    cv::cvtColor(img,img,CV_BGR2HSV);

    for(int  j=0;j<img.cols;j++)
    {
        for(int i=0;i<img.rows;i++)
        {

            if(mask.data != NULL)
            {
                if(mask.at<uchar>(i,j) > 200)
                {
                    int H = img.at<cv::Vec3b>(i,j)[0];
                    int S = img.at<cv::Vec3b>(i,j)[1];
                    int V = img.at<cv::Vec3b>(i,j)[2];
//                    cout << "With mask\t" << i << "\t" << j << "\t" << H << "\t" << S << "\t" << V << endl;
                    if(S > 90 && V > 20)
                        hist_temp[H] += 1;
                    else if(V > 200)
                            hist_temp[180] +=1;
                    else if(V < 200 && V > 50)
                            hist_temp[181] +=1;
                    else if(V < 50)
                            hist_temp[182] += 1;


                }
            }
            else
            {
                int H = img.at<cv::Vec3b>(i,j)[0];
                int S = img.at<cv::Vec3b>(i,j)[1];
                int V = img.at<cv::Vec3b>(i,j)[2];
//                cout << "Without mask \t" << i << "\t" << j << "\t" << H << "\t" << S << "\t" << V << endl;
//                cout << hist << endl;
                if(S > 90 && V > 20)
                    hist_temp[H] += 1;
                else if(V > 200)
                        hist_temp[180] +=1;
                else if(V < 200 && V > 50)
                        hist_temp[181] +=1;
                else if(V < 50)
                        hist_temp[182] += 1;



            }
        }
    }

    for(int i=0;i<hist_temp.size();i++)
        hist.push_back(hist_temp[i]);

    cv::transpose(hist,hist);

    cv::normalize(hist,hist,1.0,0.0,cv::NORM_L1);

    return;
}

void createHSVHistogram(cv::Mat img, cv::MatND &hist,cv::Mat mask)
{
    Mat src, hsv;
    //        if( argc != 2 || !(src=imread(argv[1], 1)).data )
    //            return -1;

    src = img.clone();
    cvtColor(src, hsv, CV_BGR2HSV);

    // Quantize the hue to 30 levels
    // and the saturation to 32 levels
    int hbins = 50, sbins = 32,vbins = 32;
    int histSize[] = {hbins, sbins,vbins};
    // hue varies from 0 to 179, see cvtColor
    float hranges[] = { 0, 180 };
    // saturation varies from 0 (black-gray-white) to
    // 255 (pure spectrum color)
    float sranges[] = { 0, 256 };
    float vranges[] = {0, 256};
    const float* ranges[] = { hranges, sranges, vranges };
    // we compute the histogram from the 0-th and 1-st channels
    int channels[] = {0, 1, 2};

    if(mask.data == NULL)
    {
        calcHist( &hsv, 1, channels, Mat(), // do not use mask
                  hist, 2, histSize, ranges,
                  true, // the histogram is uniform
                  false );
    }
    else
    {
        calcHist( &hsv, 1, channels, mask, //  use mask
                  hist, 2, histSize, ranges,
                  true, // the histogram is uniform
                  false );
    }

    cv::normalize(hist,hist,1.0,0.0,cv::NORM_L1);

    return;
}


void createRGBHistogram(cv::Mat img, cv::MatND &hist,cv::Mat mask)
{
    Mat src, hsv;
    //        if( argc != 2 || !(src=imread(argv[1], 1)).data )
    //            return -1;

    src = img.clone();
    cvtColor(src, hsv, CV_BGR2HSV);

    // Quantize the hue to 30 levels
    // and the saturation to 32 levels
    int bbins = 32, gbins = 32,rbins = 32;
    int histSize[] = {bbins, gbins, rbins};
    // hue varies from 0 to 179, see cvtColor
    float branges[] = { 0, 256 };
    // saturation varies from 0 (black-gray-white) to
    // 255 (pure spectrum color)
    float granges[] = { 0, 256 };
    float rranges[] = {0, 256};
    const float* ranges[] = { branges, granges, rranges };
    // we compute the histogram from the 0-th to 2-nd channels
    int channels[] = {0, 1, 2};

    if(mask.data == NULL)
    {
        calcHist( &hsv, 1, channels, Mat(), // do not use mask
                  hist, 3, histSize, ranges,
                  true, // the histogram is uniform
                  false );
    }
    else
    {
        calcHist( &hsv, 1, channels, mask, //  use mask
                  hist, 3, histSize, ranges,
                  true, // the histogram is uniform
                  false );
    }


    cv::normalize(hist,hist,1.0,0.0,cv::NORM_L1);

    return;
}

//template<typename T>
//void findMinOfArray1(vector<T> array,int &minIndex,T &minValue)

void findMinOfArray( vector<double> array, int &minIndex, double &minValue)
{

    minValue = 1000;
    for( int i=0;i<array.size();i++ )
    {
        if(array[i] < minValue)
        {
            minValue = array[i];
            minIndex = i;
        }
    }

    return;
}
