#include <grasp/ptCloudIndividual.h>
#include "grasp/SurfacePatch.h"
//#include <grasp/object_detection.h>


using namespace std;

void listFile(string path, vector<string> &files)
{
    DIR *pDIR;
    struct dirent *entry;
    if( pDIR=opendir(path.c_str()) ){
        while(entry = readdir(pDIR)){
            if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 )
            {
//                cout << entry->d_name << "\n";
                string str = path + "/" + entry->d_name;
                files.push_back(str);

            }
        }
        closedir(pDIR);
    }
}


int getdetectionBox( std::string txt_path,
                     std::vector<Rect> &bbox,
                     std::vector<std::string> &object )
{

    string file = txt_path;
    std::string line;

    ifstream myReadFile;
    myReadFile.open(file.c_str());

    std::getline(myReadFile, line, '\n');

    while( std::getline(myReadFile, line, '\n') )
    {

        size_t pos = 0;
        size_t prev_loc = 0;
        pos = line.find_first_of(" ");
        string item = line.substr(0,pos);
        object.push_back(item);
        prev_loc = pos;

        std::getline(myReadFile, line, '\n');
        pos = 0;
        prev_loc = 0;
        pos = line.find_first_of(" ");
        item = line.substr(0,pos);
        cout << " score = " << item;

        std::getline(myReadFile, line, '\n');
        std::vector<string> param;

            pos = 0;
            prev_loc = 0;
            pos = line.find_first_of(" ");
            item = line.substr(0,pos);
            param.push_back(item);
            prev_loc = pos;
            string p_str = line;

            while( pos != string::npos )
            {
                p_str = p_str.substr( pos + 1,string::npos);
                pos = p_str.find_first_of(" ");
                item = p_str.substr(0,pos);
                param.push_back(item);
            }


            Rect box;
            stringstream convert_x(param[0]);
            convert_x >> box.x ;
            stringstream convert_y(param[1]);
            convert_y >> box.y ;
            stringstream convert_w(param[2]);
            convert_w >> box.width ;          // not width.. max x position
            stringstream convert_h(param[3]);
            convert_h >> box.height ;         // not height.. max y position

            //push back
            bbox.push_back(box);


    }

    return 0;

}

bool checkClsName( string cls )
{

    vector<string> files(20);
    files[0] = "barkely_bones";
    files[1] = "bunny_book";
    files[2] = "clorox_brush";
    files[3] = "cloud_bear";
    files[4] = "command_hooks";
    files[5] = "crayola_24_ct";
    files[6] = "easter_sippy_cup";
    files[7] = "elmers_school_glue";
    files[8] = "expo_eraser";
    files[9] = "folgers_coffee";
    files[10] = "glucose_up_botle";
    files[11] = "jane_dvd";
    files[12] = "kleenex_towels";
    files[13] = "kygen_puppies";
    files[14] = "laugh_joke_book";
    files[15] = "pencils";
    files[16] = "safety_plugs";
    files[17] = "scotch_tape";
    files[18] = "staples_card";
    files[19] = "while_lightbulb";

    for( int i = 0; i < files.size() ; i++)
    {
        if( strcmp(cls.c_str(), files[i].c_str()) == 0 )
           return true;
    }
    return false;
}


int main(int argc, char** argv)
{

//    std::string path = "/home/olyvia/tcs/dataset/APC/new_test_n";
//    vector<string> files;
//    listFile( path, files);


    // initialize ROS
    ros::init(argc, argv, "object_apc");
    ros::NodeHandle node;

    //declare an object
    SurfacePatch region;

        ros::Publisher pub_org = node.advertise<sensor_msgs::PointCloud2>("/ptCloudorg", 100);
        sensor_msgs::PointCloud2 pc2msg1;

    Object_detection *obj_det = new Object_detection();

    while( ros::ok()  )

    {

      for( int f = 70; f < 74; f++ )
      {
           stringstream ss;
           ss << f;
           string file_num = ss.str();

            string path = "/home/olyvia/tcs/dataset/APC/new_objects_database/";
            string box_path = path + "output_files/img_" +file_num + ".txt";
            string pcd_path = path + "pcd/pc_" +file_num + ".pcd";

             std::vector<Rect> bbox;
             std::vector<string> class1;
             getdetectionBox( box_path, bbox, class1 );


             std::vector<Cylinder_Param> cylind;
             std::vector<Box_Param> box;

             pcl::PointCloud<pcl::PointXYZRGB>::Ptr visCloud ( new pcl::PointCloud<pcl::PointXYZRGB> ());
             if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(pcd_path, *visCloud) == -1)
             {
               std::cerr << "Couldn't read pcd file" << std::endl;
               return (-1);
             }


             std::cout << pcd_path << " bbox.size() " << bbox.size() << std::endl;
       for( int box_num = 0; box_num < bbox.size(); box_num++ )
       {
           if( checkClsName( class1[box_num] ) )
           {
               std::cout << " processing for class " << class1[box_num] << std::endl;
                   pcl::PointCloud<pcl::PointXYZ>::Ptr ptCloud_file( new pcl::PointCloud<pcl::PointXYZ> ());
                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB_file( new pcl::PointCloud<pcl::PointXYZRGB> ());
                   pcl::PointCloud<pcl::PointXYZ>::Ptr ptCloud( new pcl::PointCloud<pcl::PointXYZ> ());
                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB( new pcl::PointCloud<pcl::PointXYZRGB> ());
                   pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudvis( new pcl::PointCloud<pcl::PointXYZRGB> ());

                   {
                     // load point cloud from PCD file
                     if (pcl::io::loadPCDFile<pcl::PointXYZ>(pcd_path, *(ptCloud_file)) == -1)
                     {
                       std::cerr << "Couldn't read pcd file" << std::endl;
                       return (-1);
                     }
                     if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(pcd_path, *cloudRGB_file) == -1)
                     {
                       std::cerr << "Couldn't read pcd file" << std::endl;
                       return (-1);
                     }

                     if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(pcd_path, *cloudvis) == -1)
                     {
                       std::cerr << "Couldn't read pcd file" << std::endl;
                       return (-1);
                     }
                     std::cout << " cloudvis size = " << cloudvis->points.size() << std::endl;

                   }


                region.workspaceFilter(ptCloud_file, cloudRGB_file, bbox[box_num], ptCloud, cloudRGB );

                pcl::visualization::PCLVisualizer viewer("cloudVis1");
                viewer.addCoordinateSystem(0.1);
                viewer.addPointCloud( cloudRGB );
                while(!viewer.wasStopped())
                    viewer.spinOnce();

                double start_time = omp_get_wtime();
                std::vector<Cylinder_Param> cylind_temp;
                std::vector<Box_Param> box_temp;
                region.extract_region_main(ptCloud, cloudRGB, node, class1[box_num], cylind_temp, box_temp );

                if( cylind_temp.size() > 0 )
                {
                    Cylinder_Param cylinder_ind;
                    for( int c = 0; c < cylind_temp.size(); c++ )
                    {
                        cylinder_ind = cylind_temp[c];
                        cylind.push_back(cylinder_ind);
                    }
                }
                if( box_temp.size() > 0 )
                {
                    Box_Param box_ind;
                    for( int b = 0; b < box_temp.size(); b++ )
                    {
                        box_ind = box_temp[b];
                        box.push_back(box_ind);
                    }
                }


                printf("time taken by regionGrowingPtCloud in %.3f sec.\n", omp_get_wtime() - start_time);
                std::cout << " cylinder size = " << cylind.size() << std::endl;
                std::cout << " box size = " << box.size() << std::endl;

                region.visualize_primitives( cylind, box, visCloud );
           }

       }



////        cloudVis1->points.resize(0);
////        region.showHandle( indices , ptCloud, cloudVis1 );
////        std::cout << " cloudVis1 size = " << cloudVis1->points.size() << std::endl;

////        std::cout << " cloudVis size = " << cloudVis->points.size() << std::endl;
////        std::cout << " rgb = " << cloudRGB->points.size() << std::endl;
////        std::cout << " indices size = " << indices.size() << std::endl;

////        pub_nor.publish(surface_normals);
////       //        pub[0].publish(boundary_lines);
////       pcl::toROSMsg(*cloudVis, pc2msg);
////       pc2msg.header.stamp = ros::Time::now();
////       pc2msg.header.frame_id = "/camera_depth_optical_frame";
////       pub_cloud.publish(pc2msg);  //pt_cloud


//        pcl::toROSMsg(*cloudRGB, pc2msg1);
//        pc2msg1.header.stamp = ros::Time::now();
//        pc2msg1.header.frame_id = "/camera_depth_optical_frame";
//        pub_org.publish(pc2msg1);  //pt_cloudReg



        getchar();

    }


    }



  return (0);
}
