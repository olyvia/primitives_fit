#include <grasp/object_detection.h>

Object_detection::Object_detection()
{
    //    ros::Time::init();

    obj_detected_mat = vector< vector<int> >(101,vector<int>(int(NO_OF_MASK_TEMPLATES),0));

    multiple_tempHists = vector< vector<cv::MatND> >(int(NO_OF_MASK_TEMPLATES),
                                                     vector<cv::MatND>(4));
    multiple_lbp_temp_hist = vector< vector<cv::MatND> >(int(NO_OF_MASK_TEMPLATES),
                                                     vector<cv::MatND>(4));


    template_address = vector< vector<string> >(int(NO_OF_MASK_TEMPLATES),
                                                vector<string>(4));



    pub = nh.advertise<sensor_msgs::PointCloud2>("/ptcloud",1);

//    string video_addr = ros::package::getPath("apc_obj_detection").append("/data/output_video.avi");


//    writer.open(video_addr.c_str(),CV_FOURCC('M','J','P','G'),30,cv::Size(960,480),true);

//    cv::initModule_nonfree();

//#if(ORB)
//    featuredetector = cv::FeatureDetector::create("FAST");
//    descriptorExtractor = cv::DescriptorExtractor::create("ORB");
//#elif(SURF)
//    featuredetector = cv::FeatureDetector::create("SURF");
//    descriptorExtractor = cv::DescriptorExtractor::create("SURF");
//#endif

    //    ROI = cv::Rect(90,150,360,170);
    //    //    ROI = cv::Rect(0,0,640,480);


    ////    bk_train_image_addr = ros::package::getPath("apc_obj_detection").append("/data/bin4_bg.jpg");
    //    test_image_rgb_addr = ros::package::getPath("apc_obj_detection").append("/data/dataset/img_1.jpg");
    //    test_image_pcd_addr = ros::package::getPath("apc_obj_detection").append("/data/dataset/pc_1.pcd");

    ////    bk_train_img = cv::imread(bk_train_image_addr.c_str());
    //    test_image_orig = cv::imread(test_image_rgb_addr.c_str());

    ////    bk_train_img = bk_train_img(ROI).clone();
    //    test_image_orig = test_image_orig(ROI).clone();


    for(int i=0;i<int(NO_OF_TEMPLATES);i++)
    {
        colorVector.push_back(getRandColor(ros::Time::now().toNSec()));
    }

    objectKeypoint = vector < vector<cv::KeyPoint> >(int(NO_OF_TEMPLATES));


//    emObject_Model = vector<cv::EM>(int(NO_OF_GMM_OBJECT_TEMPLATES));

//    emObject_mask_model =vector<cv::EM>(int(NO_OF_MASK_TEMPLATES));
}

Object_detection::~Object_detection()
{

}


void Object_detection::readObjectsInEachBin()
{

    string gtruth_addr = ros::package::getPath("apc_obj_detection").
            append("/data/gtruth.txt");

    ifstream truthFile;
    truthFile.open(gtruth_addr.c_str());
    if(!truthFile.is_open())
    {
        cerr << "Error opening gturhtFile" << endl;
        exit(0);
    }

    vector<string> obj_names(int(NO_OF_OBJECT_TEMPLATES));
    for(int i=0;i<int(NO_OF_OBJECT_TEMPLATES);i++)
    {
        truthFile >> obj_names[i];
    }


    vector< vector<int> > gtruthMatrix;

    for(int i=0;i<101;i++)
    {
        vector<int> temp(int(NO_OF_OBJECT_TEMPLATES),0);
        for(int j=0;j<int(NO_OF_OBJECT_TEMPLATES);j++)
        {
            truthFile >> temp[j];
        }

        gtruthMatrix.push_back(temp);
    }

    for(int i=0;i<gtruthMatrix.size();i++)
    {
        vector<int> temp;
        for(int j=0;j<gtruthMatrix[i].size();j++)
        {
            if(gtruthMatrix[i][j] == 1)
                temp.push_back(j);

        }

        obj_bin_status.push_back(temp);

    }



    return;
}

//void Object_detection::doLBPHistogramMatchingUsingTemplates()
//{
//    // Remove patches from the list which has very small area
//    vector<cv::Rect> tempPatchList(patchesFromSurfaceNormals.begin(),patchesFromSurfaceNormals.end());

//    cout << "Number of rectangle before  \t" << patchesFromSurfaceNormals.size() << endl;

//    patchesFromSurfaceNormals.clear();


//    for(int i=-0;i<tempPatchList.size();i++)
//    {
//        if(tempPatchList[i].area() > 1000)
//            patchesFromSurfaceNormals.push_back(tempPatchList[i]);

//    }

//    cv::Mat display_image = test_image_orig.clone();
//    cv::Mat out_panel = cv::Mat(480,320,display_image.type(),cv::Scalar(255,255,255));
//    hconcat(display_image,out_panel,display_image);


//    int obj_counter = 0;

//    for(int i=0;i<patchesFromSurfaceNormals.size();i++)
//    {

//        cv::Mat img = cv::imread(test_image_rgb_addr.c_str());
//        cv::Mat patchImg = img(patchesFromSurfaceNormals[i]);

//        cv::Mat mask;
//        cv::Mat lbp_img;
//        cv::Mat hist;
//        OLBP_<unsigned char>(patchImg,lbp_img,mask);
//        lbp_histogram(lbp_img,hist);

//        vector<double> matchingVector;
//        for(int j=0;j<multiple_lbp_temp_hist.size();j++)
//        {
//            for(int k=0;k<multiple_lbp_temp_hist[j].size();k++)
//            {
//                double matching = cv::compareHist(hist,multiple_lbp_temp_hist[j][k],3);
//                matchingVector.push_back(matching);
//            }
//        }


//        int minIndex = 0;
//        double minValue = 0;
//        findMinOfArray(matchingVector,minIndex,minValue);

////        cout << minValue << endl;
////        exit(0);

//        if(minValue < 0.10)
//        {
//            int obj_id = minIndex/multiple_tempHists[0].size();

//            cout << "Object is present \t" << obj_id << endl;

//            cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(255,0,0),2,8,0);
//            char s[200];
//            sprintf(s,"%d",obj_id);
//            cv::Point centre;
//            centre.x = patchesFromSurfaceNormals[i].tl().x+patchesFromSurfaceNormals[i].width/2;
//            centre.y = patchesFromSurfaceNormals[i].tl().y+patchesFromSurfaceNormals[i].height/2;
//            cv::putText(display_image,s,centre,1,1,cv::Scalar(0,0,255),2,8,0);

//    #if(WRITE_PRODUCT_DETAILS)

//            // Read product Names from the file
//            ifstream productNameFile;
//            string productFileAddr = ros::package::getPath("apc_obj_detection").append("/data/hist_templates/pNames.txt");
//            productNameFile.open(productFileAddr.c_str());
//            if(!productNameFile.is_open())
//            {
//                cerr << "Error opening product File" << endl;
//                exit(0);
//            }

//            vector<string> productNames;
//            for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//            {
//                string temp;
//                productNameFile >> temp;
//                char str[200];
//                sprintf(str,"(%d)",i);
//                temp.append(str);
//                productNames.push_back(temp);
//            }
//            productNameFile.close();

//            cv::Point obj_name_pos;
//            obj_name_pos.x = 640;
//            obj_name_pos.y = 30+obj_counter*60;
//            cv::putText(display_image,productNames[obj_id],obj_name_pos,1,1,cv::Scalar(0,0,255),2,8,0);
//            obj_counter++;
//    #endif

//        }
//    }

//    writer.write(display_image);

//    return;
//}

//void Object_detection::initLBPHistogramForTemplates()
//{

//    for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//    {
//        for(int j=0;j<4;j++)
//        {

//            cout << i << "\t " << j << endl;
//            string addr = ros::package::getPath("apc_obj_detection").append("/data/hist_templates/");
//            string mask_addr = ros::package::getPath("apc_obj_detection").append("/data/hist_templates/");

//            char s[200];
//            sprintf(s,"%d/%d.JPG",i,j+1);
//            addr.append(s);
//            char a[200];
//            sprintf(a,"%d/mask_%d.jpg",i,j+1);
//            mask_addr.append(a);

//            cv::Mat rgb_img = cv::imread(addr.c_str());
//            cv::Mat mask_img = cv::imread(mask_addr.c_str(),CV_8UC1);

//            if(rgb_img.data == NULL )
//            {
//                addr.clear();
//                addr = ros::package::getPath("apc_obj_detection").append("/data/hist_templates/");

//                char sa[200];
//                sprintf(sa,"%d/%d.jpg",i,j+1);
//                addr.append(sa);

//                cout << addr.c_str() << endl;
//                rgb_img = cv::imread(addr.c_str());

//                if(rgb_img.data == NULL)
//                {
//                    cout << "Image data not present " << endl;
//                    exit(0);
//                }
//            }

//            if(mask_img.data == NULL)
//            {
//                cout << i << "\t" << j << "\t" <<  "No mask present " << endl;
//            }

//            template_address[i][j].assign(addr.begin(),addr.end());


//            cv::Mat lbp_img;
//            OLBP_<unsigned char>(rgb_img,lbp_img,mask_img);

//            lbp_histogram(lbp_img,multiple_lbp_temp_hist[i][j]);

//            //            if(i==6)
//            //            {
//            //                cout << multiple_tempHists[i][j] << endl;
//            //            }

//            //            cv::Mat mask;
//            //            cv::MatND im;
//            //            createHSHistogram(rgb_img,im,mask_img);

//            //            cout << compareHist(multiple_tempHists[i][j],im,3);

//            //            exit(0);


//        }
//    }
//}

//void Object_detection::doObjectMatchingUsingShapes()
//{
//    // Remove patches from the list which has very small area
//    vector<cv::Rect> tempPatchList(patchesFromSurfaceNormals.begin(),patchesFromSurfaceNormals.end());

//    cout << "Number of rectangle before  \t" << patchesFromSurfaceNormals.size() << endl;

//    patchesFromSurfaceNormals.clear();


//    for(int i=-0;i<tempPatchList.size();i++)
//    {
//        if(tempPatchList[i].area() > 1000)
//            patchesFromSurfaceNormals.push_back(tempPatchList[i]);

//    }

//    cv::Mat display_image = test_image_orig.clone();
//    cv::Mat out_panel = cv::Mat(480,320,display_image.type(),cv::Scalar(255,255,255));
//    hconcat(display_image,out_panel,display_image);


//    int obj_counter = 0;

//    for(int i=0;i<patchesFromSurfaceNormals.size();i++)
//    {

//        cv::Mat img = cv::imread(test_image_rgb_addr.c_str());
//        cv::Mat patchImg = img(patchesFromSurfaceNormals[i]);

//        cv::Mat contour_output;

//        cv::cvtColor(patchImg,contour_output,CV_BGR2GRAY);
//        vector< vector<cv::Point> > contours;
//        cv::findContours(contour_output,contours,CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

//        double maxArea = 0.0;
//        int maxIndex = 0;
//        for (size_t idx = 0; idx < contours.size(); idx++) {
//            if(cv::contourArea(contours[idx]) > maxArea)
//            {
//                maxArea = cv::contourArea(contours[idx]);
//                maxIndex = idx;
//                //                    cout << idx << "\t Contour area \t" << cv::contourArea(contours[idx]);
//                //                    cv::drawContours(contourImage, contours, idx, colors[idx % 3]);
//            }
//        }

//        vector<double> matchingVector;
//        int count = 0;
//        for(int j=0;j<int(NO_OF_MASK_TEMPLATES);j++)
//        {

//            for(int k=0;k<4;k++)
//            {
//                double matching = cv::matchShapes(templates_contours[count],contours[maxIndex],CV_CONTOURS_MATCH_I2,0);
//                count++;
//                matchingVector.push_back(matching);
//                cout << matching << "\t";
//            }
//        }
//        cout << endl;
//        int max_ind=0;
//        double max_value = 0.0;
//        // Finding maxMatching template
//        findMinOfArray(matchingVector,max_ind,max_value);

//        if(max_value > 0.25)
//            continue;

//        cout << "MAx index \t" << max_ind << endl;

//        int obj_id = max_ind/4;

//        cout << "Object is present \t" << obj_id << endl;

//        cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(255,0,0),2,8,0);
//        char s[200];
//        sprintf(s,"%d",obj_id);
//        cv::Point centre;
//        centre.x = patchesFromSurfaceNormals[i].tl().x+patchesFromSurfaceNormals[i].width/2;
//        centre.y = patchesFromSurfaceNormals[i].tl().y+patchesFromSurfaceNormals[i].height/2;
//        cv::putText(display_image,s,centre,1,1,cv::Scalar(0,0,255),2,8,0);

//#if(WRITE_PRODUCT_DETAILS)

//        // Read product Names from the file
//        ifstream productNameFile;
//        string productFileAddr = ros::package::getPath("apc_obj_detection").append("/data/hist_templates/pNames.txt");
//        productNameFile.open(productFileAddr.c_str());
//        if(!productNameFile.is_open())
//        {
//            cerr << "Error opening product File" << endl;
//            exit(0);
//        }

//        vector<string> productNames;
//        for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//        {
//            string temp;
//            productNameFile >> temp;
//            char str[200];
//            sprintf(str,"(%d)",i);
//            temp.append(str);
//            productNames.push_back(temp);
//        }
//        productNameFile.close();

//        cv::Point obj_name_pos;
//        obj_name_pos.x = 640;
//        obj_name_pos.y = 30+obj_counter*60;
//        cv::putText(display_image,productNames[obj_id],obj_name_pos,1,1,cv::Scalar(0,0,255),2,8,0);
//        obj_counter++;
//#endif

//    }

//    writer.write(display_image);

//    return;
//}


//void Object_detection::initContoursForTemplates()
//{
//    for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//    {
//        for(int j=0;j<4;j++)
//        {

//            string addr = ros::package::getPath("apc_obj_detection").append("/data/hist_templates/");
//            string mask_addr = ros::package::getPath("apc_obj_detection").append("/data/hist_templates/");

//            char s[200];
//            sprintf(s,"%d/%d.JPG",i,j+1);
//            addr.append(s);
//            char a[200];
//            sprintf(a,"%d/mask_%d.jpg",i,j+1);
//            mask_addr.append(a);

//            cv::Mat rgb_img = cv::imread(addr.c_str());
//            cv::Mat mask_img = cv::imread(mask_addr.c_str());

//            //            cv::resize(rgb_img,rgb_img,cv::Size(rgb_img.cols/8,rgb_img.rows/8));
//            //            cv::resize(mask_img,mask_img,cv::Size(rgb_img.cols/8,rgb_img.rows/8));

//            if(rgb_img.data == NULL )
//            {
//                cout << "Image data not present " << endl;
//                exit(0);
//            }

//            if(mask_img.data == NULL)
//            {
//                cout << i << "\t" << j << "\t" <<  "No mask present " << endl;
//            }

//            template_address[i][j].assign(addr.begin(),addr.end());

//            cv::Mat contour_output;

//            //            cv::imshow("Orig,img",rgb_img);


//            for(int l=0;l<rgb_img.cols;l++)
//            {
//                for(int k=0;k<rgb_img.rows;k++)
//                {

//                    if(mask_img.at<Vec3b>(k,l)[0] < 100)
//                    {
//                        rgb_img.at<Vec3b>(k,l)[0] = 0;
//                        rgb_img.at<Vec3b>(k,l)[1] = 0;
//                        rgb_img.at<Vec3b>(k,l)[2] = 0;

//                    }
//                }
//            }

//            cv::cvtColor(rgb_img,contour_output,CV_BGR2GRAY);


//            //             cv::imshow("rgb,img",rgb_img);
//            //             cv::waitKey(0);

//            //            for(int l=0;l<rgb_img.cols;l++)
//            //            {
//            //                for(int k=0;k<rgb_img.rows;k++)
//            //                {
//            //                    if(mask_img.at<Vec3b>(k,l)[0] > 200)
//            //                    {

//            //                        cv::Mat temp(1,3,CV_64FC1);
//            //                        temp.at<double>(0,0) = 100*double(rgb_img.at<Vec3b>(k,l)[0])/180.0;
//            //                        temp.at<double>(0,1) = 100*double(rgb_img.at<Vec3b>(k,l)[1])/255.0;
//            //                        temp.at<double>(0,2) = 100*double(rgb_img.at<Vec3b>(k,l)[2])/255.0;











//            vector< vector<cv::Point> > contours;
//            cv::findContours(contour_output,contours,CV_RETR_LIST, CV_CHAIN_APPROX_NONE);


//            //            cv::Mat contourImage(rgb_img.size(), CV_8UC3, cv::Scalar(0,0,0));
//            //            cv::Scalar colors[3];
//            //            colors[0] = cv::Scalar(255, 0, 0);
//            //            colors[1] = cv::Scalar(0, 255, 0);
//            //            colors[2] = cv::Scalar(0, 0, 255);
//            //            for (size_t idx = 0; idx < contours.size(); idx++) {
//            //                if(cv::contourArea(contours[idx]) > 1000)
//            //                {
//            //                    cout << idx << "\t Contour area \t" << cv::contourArea(contours[idx]);
//            //                    cv::drawContours(contourImage, contours, idx, colors[idx % 3]);
//            //                }
//            //            }

//            //            cv::imshow("Original image",contour_output);
//            //            cv::imshow("Contour image",contourImage);
//            //            cv::waitKey(0);


//            double maxArea = 0.0;
//            int maxIndex = 0;

//            //            cout << "Contour size \t" << contours.size() << endl;

//            for (size_t idx = 0; idx < contours.size(); idx++)
//            {
//                if(cv::contourArea(contours[idx]) > maxArea)
//                {
//                    maxArea = cv::contourArea(contours[idx]);
//                    maxIndex = idx;
//                    //                    cout << idx << "\t Contour area \t" << cv::contourArea(contours[idx]) << endl;
//                    //                    cv::drawContours(contourImage, contours, idx, colors[idx % 3]);
//                }
//            }

//            templates_contours.push_back(contours[maxIndex]);

//        }
//    }

//    //    exit(0);
//    //    cout << templates_contours.size() << endl;
//    //    exit(0);


//}

//void Object_detection::findObjectUsingSVMwithLBP()
//{
//    // Remove patches from the list which has very small area
//    vector<cv::Rect> tempPatchList(patchesFromSurfaceNormals.begin(),patchesFromSurfaceNormals.end());

//    cout << "Number of rectangle before  \t" << patchesFromSurfaceNormals.size() << endl;

//    patchesFromSurfaceNormals.clear();


//    for(int i=-0;i<tempPatchList.size();i++)
//    {
//        if(tempPatchList[i].area() > 1000)
//            patchesFromSurfaceNormals.push_back(tempPatchList[i]);

//    }

//    cv::Mat display_image = test_image_orig.clone();
//    cv::Mat out_panel = cv::Mat(480,320,display_image.type(),cv::Scalar(255,255,255));
//    hconcat(display_image,out_panel,display_image);


//    int obj_counter = 0;

//    for(int i=0;i<patchesFromSurfaceNormals.size();i++)
//    {

//        cv::Mat img = cv::imread(test_image_rgb_addr.c_str(),CV_8UC1);
//        cv::Mat patchImg = img(patchesFromSurfaceNormals[i]);
//        cv::Mat lbp_img,hist;
//        OLBP_<unsigned char>(patchImg,lbp_img,cv::Mat());

//        //        cv::imshow("patchImg",patchImg);
//        //        cv::imshow("lbp_img",lbp_img);
//        //        cv::waitKey(0);

//        lbp_histogram(lbp_img,hist);

//        int obj_id = -1;
//        for(int j=0;j<int(NO_OF_MASK_TEMPLATES);j++)
//        {
//            int response = svm[j].predict(hist);

//            cout << response << "\t";
//            if(response == 1)
//            {
//                obj_id = j;
//                break;
//            }
//        }

//        cout << endl;

//        //        continue;

//        cout << "Object id \t" << obj_id << endl;

//        if(obj_id == -1)
//        {
//            cout << "No object is present in this patch" << endl;
//            continue;
//        }

//        if(obj_detected_mat[imgNumber][obj_id] == 0)
//            obj_detected_mat[imgNumber][obj_id] = 1;

//        cout << "Object is present \t" << obj_id << endl;

//        cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(255,0,0),2,8,0);
//        char s[200];
//        sprintf(s,"%d",obj_id);
//        cv::Point centre;
//        centre.x = patchesFromSurfaceNormals[i].tl().x+patchesFromSurfaceNormals[i].width/2;
//        centre.y = patchesFromSurfaceNormals[i].tl().y+patchesFromSurfaceNormals[i].height/2;
//        cv::putText(display_image,s,centre,1,1,cv::Scalar(0,0,255),2,8,0);

//#if(WRITE_PRODUCT_DETAILS)

//        // Read product Names from the file
//        ifstream productNameFile;
//        string productFileAddr = ros::package::getPath("apc_obj_detection").append("/data/hist_templates/pNames.txt");
//        productNameFile.open(productFileAddr.c_str());
//        if(!productNameFile.is_open())
//        {
//            cerr << "Error opening product File" << endl;
//            exit(0);
//        }

//        vector<string> productNames;
//        for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//        {
//            string temp;
//            productNameFile >> temp;
//            char str[200];
//            sprintf(str,"(%d)",i);
//            temp.append(str);
//            productNames.push_back(temp);
//        }
//        productNameFile.close();

//        cv::Point obj_name_pos;
//        obj_name_pos.x = 640;
//        obj_name_pos.y = 30+obj_counter*60;
//        cv::putText(display_image,productNames[obj_id],obj_name_pos,1,1,cv::Scalar(0,0,255),2,8,0);
//        obj_counter++;
//#endif
//    }

//    writer.write(display_image);

//}

//void Object_detection::trainSVMUsingMaskTemplatesWithLBP()
//{
//    vector<int> img_count_templates(int(NO_OF_MASK_TEMPLATES),0);
//    img_count_templates[0] = 62;
//    img_count_templates[1] = 65;
//    img_count_templates[2] = 65;
//    img_count_templates[3] = 57;
//    img_count_templates[4] = 45;

//    std::string datasetAddress = ros::package::getPath("apc_obj_detection").append("/data/mask_data/");

//    vector<cv::Mat> data_temp(int(NO_OF_MASK_TEMPLATES));

//    int no_of_training_samples = cv::sum(img_count_templates)[0];
//    Mat trainData;
//    Mat labels;


//    for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//    {
//        for(int j=0;j<img_count_templates[i];j+=10)
//        {
//            char s[200];
//            sprintf(s,"%d/%d.JPG",i,j+1);

//            string rgb_image_addr;
//            rgb_image_addr.assign(datasetAddress.begin(),datasetAddress.end());
//            rgb_image_addr.append(s);

//            string mask_image_add;
//            mask_image_add.assign(datasetAddress.begin(),datasetAddress.end());
//            char a[200];
//            sprintf(a,"%d/mask_%d.jpg",i,j+1);
//            mask_image_add.append(a);

//            // Read the rgb and mask image
//            cv::Mat rgb_img = cv::imread(rgb_image_addr.c_str(),CV_8UC1);
//            cv::Mat mask_img = cv::imread(mask_image_add.c_str(),CV_8UC1);

//            //            cv::resize(rgb_img,rgb_img,cv::Size(rgb_img.cols/2,rgb_img.rows/2));
//            //            cv::resize(mask_img,mask_img,cv::Size(rgb_img.cols/2,rgb_img.rows/2));

//            cv::Mat lbp_img,hist;
//            OLBP_<unsigned char>(rgb_img,lbp_img,mask_img);

//            //            cv::imshow("src_img",rgb_img);
//            //            cv::imshow("lbp_img",lbp_img);
//            //            cv::waitKey(0);

//            lbp_histogram(lbp_img,hist);

//            //            cout << hist << endl;
//            //            exit(0);

//            //            data_temp[i].push_back(hist.row((0)));

//            trainData.push_back(hist.row(0));
//            labels.push_back(i);
//        }
//    }



//    /* SVM training
//    */

//    //------------------------ 2. Set up the support vector machines parameters --------------------
//    CvSVMParams params;
//    params.svm_type    = SVM::C_SVC;
//    params.C           = 0.1;
//    params.kernel_type = SVM::RBF;
//    params.term_crit   = TermCriteria(CV_TERMCRIT_ITER, (int)1e7, 1e-6);
//    params.gamma = 16;

//    //------------------------ 3. Train the svm ----------------------------------------------------
//    cout << "Starting training process" << endl;

//    for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//    {
//        cout << "Training template no. \t" << i << endl;
//        cv::Mat train_data;
//        cv::Mat train_labels;

//        for(int j=0;j<trainData.rows;j++)
//        {
//            train_data.push_back(trainData.row(j));

//            if(labels.at<int>(j,0) == i)
//                train_labels.push_back(1);
//            else
//                train_labels.push_back(0);
//        }

//        svm[i].train_auto(train_data, train_labels, Mat(), Mat(), params);


//    }

//    cout << "Finished training process" << endl;

//    return;
//}

void Object_detection::doHistMatchingMaskedMultipleTemplates()
{

    // Remove patches from the list which has very small area
    vector<cv::Rect> tempPatchList(patchesFromSurfaceNormals.begin(),patchesFromSurfaceNormals.end());

    cout << "Number of rectangle before  \t" << patchesFromSurfaceNormals.size() << endl;

    patchesFromSurfaceNormals.clear();


    for(int i=-0;i<tempPatchList.size();i++)
    {
        if(tempPatchList[i].area() > 1200)
            patchesFromSurfaceNormals.push_back(tempPatchList[i]);

    }

    cv::Mat display_image = test_image_orig.clone();
    cv::Mat out_panel = cv::Mat(480,320,display_image.type(),cv::Scalar(255,255,255));
    hconcat(display_image,out_panel,display_image);


    int obj_counter = 0;
    for(int i=0;i<patchesFromSurfaceNormals.size();i++)
    {
        cv::Mat img = test_image_orig.clone();
        cv::Mat patchImg = img(patchesFromSurfaceNormals[i]);

        cv::MatND patchHist;
        cv::Mat mask;
#if(HS_HIST)
        createHSHistogram(patchImg,patchHist,mask);
#else
        createH_Histogram(patchImg,patchHist,mask);
#endif
        vector<double> histCompVector;

        for(int j=0;j<obj_bin_status[imgNumber].size();j++)
        {
            for(int k=0;k<multiple_tempHists[obj_bin_status[imgNumber][j]].size();k++)
            {

                double value = cv::compareHist(patchHist,multiple_tempHists[j][k],3);
                histCompVector.push_back(value);
                //                cout << "Hist matching \t" << value << endl;
                //                cv::Mat img = cv::imread(template_address[j][k].c_str());
                //                cv::resize(img,img,cv::Size(img.cols/8,img.rows/8));
                //                cv::imshow("template",img);
                //                cv::imshow("patch",patchImg);
                //                cv::waitKey(0);

            }
        }


        // Find maximum of histogram matching
        double minValue = 0;
        int minIndex = 0;

//        for(int m=0;m<histCompVector.size();m++)
//            cout << histCompVector[m] << "\t";
//        cout << endl;

        findMinOfArray(histCompVector,minIndex,minValue);

        int obj_id = -1;


        int count = 0;
        // finding obj_id
        for(int j=0;j<obj_bin_status[imgNumber].size();j++)
        {
            count+=multiple_tempHists[obj_bin_status[imgNumber][j]].size();

            if(minIndex <= count)
            {
                obj_id = obj_bin_status[imgNumber][j];
                break;
            }
        }

        cout << "Object id \t" << obj_id << "\t" << minIndex << "\t" <<
                minValue << endl;

        if(minValue < 0.90 && obj_id >= 0)
        {


//            obj_id = minIndex/multiple_tempHists[0].size();

            if(obj_detected_mat[imgNumber][obj_id] == 0)
                obj_detected_mat[imgNumber][obj_id] = 1;

            cout << "Object is present \t" << obj_id << endl;

            cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(255,0,0),2,8,0);
            char s[200];
            sprintf(s,"%d",obj_id);
            cv::Point centre;
            centre.x = patchesFromSurfaceNormals[i].tl().x+patchesFromSurfaceNormals[i].width/2;
            centre.y = patchesFromSurfaceNormals[i].tl().y+patchesFromSurfaceNormals[i].height/2;
            cv::putText(display_image,s,centre,1,1,cv::Scalar(0,0,255),2,8,0);

#if(WRITE_PRODUCT_DETAILS)

            // Read product Names from the file
            ifstream productNameFile;
            string productFileAddr = ros::package::getPath("apc_obj_detection").append("/data/hist_templates/pNames.txt");
            productNameFile.open(productFileAddr.c_str());
            if(!productNameFile.is_open())
            {
                cerr << "Error opening product File" << endl;
                exit(0);
            }

            vector<string> productNames;
            for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
            {
                string temp;
                productNameFile >> temp;
                char str[200];
                sprintf(str,"(%d)",i);
                temp.append(str);
                productNames.push_back(temp);
            }
            productNameFile.close();

            cv::Point obj_name_pos;
            obj_name_pos.x = 640;
            obj_name_pos.y = 30+obj_counter*60;
            cv::putText(display_image,productNames[obj_id],obj_name_pos,1,1,cv::Scalar(0,0,255),2,8,0);
            obj_counter++;
#endif

//            cv::imshow("patchImg",patchImg);
//            int j_index = obj_id%multiple_tempHists[0].size();
//            cv::imshow("template",cv::imread(template_address[obj_id][j_index]));
//            cv::waitKey(0);
        }
        else
        {
            cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(0,255,0),2,8,0);

        }

    }

    writer.write(display_image);


    return;
}


void Object_detection::doHistMatchingMaskedMultipleTemplates(MatND test_hist)
{

        vector<double> histCompVector;

        for(int j = 0; j < multiple_tempHists.size(); j++)
        {
            for(int k = 0; k < multiple_tempHists[j].size(); k++)
            {

                double value = cv::compareHist( test_hist, multiple_tempHists[j][k], 3);
                histCompVector.push_back(value);
                                cout << "Hist matching \t" << value << endl;
                                cv::Mat img = cv::imread(template_address[j][k].c_str());
                                cv::resize(img,img,cv::Size(img.cols/8,img.rows/8));
                                cv::imshow("template",img);
//                                cv::imshow("patch",patchImg);
                                cv::waitKey(10);
                                getchar();


            }
        }



        // Find maximum of histogram matching
        double minValue = 0;
        int minIndex = 0;

//        for(int m=0;m<histCompVector.size();m++)
//            cout << histCompVector[m] << "\t";
//        cout << endl;

        findMinOfArray(histCompVector,minIndex,minValue);

        int obj_id = -1;

        cout <<  "\t" << minIndex << "\t" <<
                minValue << endl;


//        int count = 0;
//        // finding obj_id
//        for(int j=0;j<obj_bin_status[imgNumber].size();j++)
//        {
//            count+=multiple_tempHists[obj_bin_status[imgNumber][j]].size();

//            if(minIndex <= count)
//            {
//                obj_id = obj_bin_status[imgNumber][j];
//                break;
//            }
//        }

//        cout << "Object id \t" << obj_id << "\t" << minIndex << "\t" <<
//                minValue << endl;

//        if(minValue < 0.90 && obj_id >= 0)
//        {


////            obj_id = minIndex/multiple_tempHists[0].size();

//            if(obj_detected_mat[imgNumber][obj_id] == 0)
//                obj_detected_mat[imgNumber][obj_id] = 1;

//            cout << "Object is present \t" << obj_id << endl;

//            cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(255,0,0),2,8,0);
//            char s[200];
//            sprintf(s,"%d",obj_id);
//            cv::Point centre;
//            centre.x = patchesFromSurfaceNormals[i].tl().x+patchesFromSurfaceNormals[i].width/2;
//            centre.y = patchesFromSurfaceNormals[i].tl().y+patchesFromSurfaceNormals[i].height/2;
//            cv::putText(display_image,s,centre,1,1,cv::Scalar(0,0,255),2,8,0);

//#if(WRITE_PRODUCT_DETAILS)

//            // Read product Names from the file
//            ifstream productNameFile;
//            string productFileAddr = ros::package::getPath("apc_obj_detection").append("/data/hist_templates/pNames.txt");
//            productNameFile.open(productFileAddr.c_str());
//            if(!productNameFile.is_open())
//            {
//                cerr << "Error opening product File" << endl;
//                exit(0);
//            }

//            vector<string> productNames;
//            for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//            {
//                string temp;
//                productNameFile >> temp;
//                char str[200];
//                sprintf(str,"(%d)",i);
//                temp.append(str);
//                productNames.push_back(temp);
//            }
//            productNameFile.close();

//            cv::Point obj_name_pos;
//            obj_name_pos.x = 640;
//            obj_name_pos.y = 30+obj_counter*60;
//            cv::putText(display_image,productNames[obj_id],obj_name_pos,1,1,cv::Scalar(0,0,255),2,8,0);
//            obj_counter++;
//#endif

////            cv::imshow("patchImg",patchImg);
////            int j_index = obj_id%multiple_tempHists[0].size();
////            cv::imshow("template",cv::imread(template_address[obj_id][j_index]));
////            cv::waitKey(0);
//        }
//        else
//        {
//            cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(0,255,0),2,8,0);

//        }

//    writer.write(display_image);


    return;
}





void Object_detection::initHistWithMultipleTemplates()
{

    for(int i=0; i< int(NO_OF_MASK_TEMPLATES); i++)
    {
        for(int j=0;j<4;j++)
        {

            cout << i << "\t " << j << endl;
            string addr = "/home/olyvia/tcs/dataset/APC/data/hist_templates/" ;
            string mask_addr = "/home/olyvia/tcs/dataset/APC/data/hist_templates/" ;

            char s[200];
            sprintf(s,"%d/%d.JPG", i, j+1);
            addr.append(s);
            char a[200];
            sprintf(a,"%d/mask_%d.jpg", i, j+1);
            mask_addr.append(a);

            cv::Mat rgb_img = cv::imread(addr.c_str());
            cv::Mat mask_img = cv::imread(mask_addr.c_str(),CV_8UC1);

            if(rgb_img.data == NULL )
            {
                addr.clear();
                addr = "/home/olyvia/tcs/dataset/APC/data/hist_templates/" ;

                char sa[200];
                sprintf(sa,"%d/%d.jpg",i,j+1);
                addr.append(sa);

                cout << addr.c_str() << endl;
                rgb_img = cv::imread(addr.c_str());

                if(rgb_img.data == NULL)
                {
                    cout << "Image data not present " << endl;
                    exit(0);
                }
            }

            if( mask_img.data == NULL )
            {
//                cout << i << "\t" << j << "\t" <<  "No mask present " << endl;
            }

            template_address[i][j].assign(addr.begin(),addr.end());


            //            cout << mask_img << endl;
            //            cout << mask_img.channels() << "\t" << mask_img.type() << endl;
            //            exit(0);
#if(HS_HIST)
            createHSHistogram(rgb_img,multiple_tempHists[i][j],mask_img);
//             createRGBHistogram(rgb_img,multiple_tempHists[i][j],mask_img);
#else
            createH_Histogram(rgb_img,multiple_tempHists[i][j],mask_img);
#endif
            //            if(i==6)
            //            {
            //                cout << multiple_tempHists[i][j] << endl;
            //            }

//                        cv::Mat mask;
//                        cv::MatND im;
//                        createHSHistogram(rgb_img,im,mask_img);

//                        cout << "endl" << compareHist(multiple_tempHists[i][j],im,3);

            //            exit(0);



        }

    }

    return;
}

//void Object_detection::initGMMUsingMaskTemplates()
//{

//#if(TRAIN_GMM)

//    vector<int> img_count_templates(int(NO_OF_MASK_TEMPLATES),0);
//    img_count_templates[0] = 62;
//    img_count_templates[1] = 65;
//    img_count_templates[2] = 65;
//    img_count_templates[3] = 57;
//    img_count_templates[4] = 45;

//    std::string datasetAddress = ros::package::getPath("apc_obj_detection").append("/data/mask_data/");

//    for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//    {

//        cv::Mat obj_samples;

//        for(int j=0;j<img_count_templates[i];j++)
//        {
//            char s[200];
//            sprintf(s,"%d/%d.JPG",i,j+1);

//            string rgb_image_addr;
//            rgb_image_addr.assign(datasetAddress.begin(),datasetAddress.end());
//            rgb_image_addr.append(s);

//            string mask_image_add;
//            mask_image_add.assign(datasetAddress.begin(),datasetAddress.end());
//            char a[200];
//            sprintf(a,"%d/mask_%d.jpg",i,j+1);
//            mask_image_add.append(a);



//            // Read the rgb and mask image
//            cv::Mat rgb_img = cv::imread(rgb_image_addr.c_str());
//            cv::Mat mask_img = cv::imread(mask_image_add.c_str());

//            cv::imshow("img orig",rgb_img);


//            cv::resize(rgb_img,rgb_img,cv::Size(rgb_img.cols/8,rgb_img.rows/8));
//            cv::resize(mask_img,mask_img,cv::Size(rgb_img.cols/8,rgb_img.rows/8));

//            //            cv::imshow("img_red",rgb_img);
//            //            cv::waitKey(0);


//            cvtColor(rgb_img,rgb_img,CV_BGR2HSV);

//            for(int l=0;l<rgb_img.cols;l++)
//            {
//                for(int k=0;k<rgb_img.rows;k++)
//                {
//                    if(mask_img.at<Vec3b>(k,l)[0] > 200)
//                    {

//                        cv::Mat temp(1,3,CV_64FC1);
//                        temp.at<double>(0,0) = 100*double(rgb_img.at<Vec3b>(k,l)[0])/180.0;
//                        temp.at<double>(0,1) = 100*double(rgb_img.at<Vec3b>(k,l)[1])/255.0;
//                        temp.at<double>(0,2) = 100*double(rgb_img.at<Vec3b>(k,l)[2])/255.0;


//                        obj_samples.push_back(temp.row(0));
//                        //                            obj_seg_img.at<Vec3b>(k,l) = cv::Vec3b(255,255,255);


//                    }

//                }
//            }
//        }

//        //***********************************************************************************
//        int clusNum = 3;
//        // Train background model
//        emObject_mask_model[i] = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

//        vector<Mat> covsInit;//(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);
//        Mat meansInput = Mat::zeros(clusNum, obj_samples.cols, CV_64FC1);
//        Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;
//        Mat out1, out2, out3;

//        cout << "Training GMM model for Object No." << i <<  endl;
//        emObject_mask_model[i] .trainE(obj_samples, meansInput, covsInit, weightsInput, out1, out2, out3);


//        string model_path = ros::package::getPath("apc_obj_detection").append("/data/models/");

//        // Saving trained Model in a file (Object Model)
//        FileStorage fs_obj;
//        char model_name[200];
//        sprintf(model_name,"obj_mask_model%d.txt",i);
//        model_path.append(model_name);
//        fs_obj.open(model_path.c_str(),FileStorage::WRITE);
//        emObject_mask_model[i].write(fs_obj);
//    }

//#else


//    for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//    {
//        string model_path = ros::package::getPath("apc_obj_detection").append("/data/models/");


//        FileStorage fs_obj;
//        char model_name[200];
//        sprintf(model_name,"obj_mask_model%d.txt",i);

//        model_path.append(model_name);
//        fs_obj.open(model_path.c_str(),FileStorage::READ);

//        if(!fs_obj.isOpened())
//        {
//            cout << model_path.c_str() << endl;
//            cout << "Can't open Background model File for " << endl;
//            exit(0);
//        }
//        const FileNode& fn_obj = fs_obj["StatModel.EM"];
//        emObject_mask_model[i].read(fn_obj);
//        fs_obj.release();
//        //        cout << emObject_Model[i].isTrained() << endl;

//    }

//#endif

//    return;
//}


//void Object_detection::doObjectMatchingMaskedGMM()
//{

//    // Remove patches from the list which has very small area
//    vector<cv::Rect> tempPatchList(patchesFromSurfaceNormals.begin(),patchesFromSurfaceNormals.end());

//    cout << "Number of rectangle before  \t" << patchesFromSurfaceNormals.size() << endl;

//    patchesFromSurfaceNormals.clear();


//    for(int i=-0;i<tempPatchList.size();i++)
//    {
//        if(tempPatchList[i].area() > 1000)
//            patchesFromSurfaceNormals.push_back(tempPatchList[i]);

//    }

//    cout << "Number of rectangle \t" << patchesFromSurfaceNormals.size() << endl;

//    cv::Mat display_image = test_image_orig.clone();
//    cv::Mat out_panel = cv::Mat(480,320,display_image.type(),cv::Scalar(255,255,255));
//    hconcat(display_image,out_panel,display_image);



//    int obj_counter = 0;
//    for(int i=0;i<patchesFromSurfaceNormals.size();i++)
//    {
//        cv::Mat img = test_image_orig.clone();
//        cv::Mat patchImg = img(patchesFromSurfaceNormals[i]);

//        vector<int> pixeLCountForEachTemplate(int(NO_OF_MASK_TEMPLATES),0);


//        cvtColor(patchImg,patchImg,CV_BGR2HSV);

//        for(int j=0;j<patchImg.cols;j++)
//        {
//            for(int k=0;k<patchImg.rows;k++)
//            {
//                cv::Mat temp(1,3,CV_64FC1);
//                temp.at<double>(0,0) = 100*double(patchImg.at<Vec3b>(k,j)[0])/180.0;
//                temp.at<double>(0,1) = 100*double(patchImg.at<Vec3b>(k,j)[1])/255.0;
//                temp.at<double>(0,2) = 100*double(patchImg.at<Vec3b>(k,j)[2])/255.0;

//                vector<cv::Mat> probs(int(NO_OF_MASK_TEMPLATES));
//                vector<cv::Vec2d> out(int(NO_OF_MASK_TEMPLATES));
//                vector<double> likelihood(int(NO_OF_MASK_TEMPLATES),0.0);

//                for(int l=0;l<int(NO_OF_MASK_TEMPLATES);l++)
//                {
//                    out[l] = emObject_mask_model[l].predict(temp,probs[l]);
//                    likelihood[l] = std::exp(out[l][0]);
//                }



//                vector<double> prob(int(NO_OF_MASK_TEMPLATES),0);
//                for(int l=0;l<int(NO_OF_MASK_TEMPLATES);l++)
//                {
//                    prob[l] = (double)likelihood[l]/cv::sum(likelihood)[0];
//                    //                    cout << out[l][0] << "\t" <<  likelihood[l] << "\t" << out[l][1] << "\t" << prob[l] << endl;
//                }
//                cout << endl;

//                int maxIndex = 0;
//                double maxValue = 0.0;
//                findMaxOfArray(prob,maxIndex,maxValue);
//                //                cout << i << "\t" << j << "\t" << maxIndex << "\t" << maxValue << endl;
//                pixeLCountForEachTemplate[maxIndex] += 1;
//            }
//        }


//        int maxMatchIdx = 0;
//        int maxMatchValue = 0;
//        findMaxOfArray(pixeLCountForEachTemplate,maxMatchIdx,maxMatchValue);

//        double percent_match = (double)maxMatchValue/(patchImg.rows*patchImg.cols);
//        cout << "Percentage match \t" << percent_match << endl;
//        if(percent_match > 0.60)
//        {
//            // Object present
//            cout << "Object is present \t" << maxMatchIdx << endl;

//            cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(255,0,0),2,8,0);
//            char s[200];
//            sprintf(s,"%d",maxMatchIdx);
//            cv::Point centre;
//            centre.x = patchesFromSurfaceNormals[i].tl().x+patchesFromSurfaceNormals[i].width/2;
//            centre.y = patchesFromSurfaceNormals[i].tl().y+patchesFromSurfaceNormals[i].height/2;
//            cv::putText(display_image,s,centre,1,1,cv::Scalar(0,0,255),2,8,0);

//#if(WRITE_PRODUCT_DETAILS)

//            // Read product Names from the file
//            ifstream productNameFile;
//            string productFileAddr = ros::package::getPath("apc_obj_detection").append("/data/models/pNames.txt");
//            productNameFile.open(productFileAddr.c_str());
//            if(!productNameFile.is_open())
//            {
//                cerr << "Error opening product File" << endl;
//                exit(0);
//            }

//            vector<string> productNames;
//            for(int i=0;i<int(NO_OF_MASK_TEMPLATES);i++)
//            {
//                string temp;
//                productNameFile >> temp;
//                char str[200];
//                sprintf(str,"(%d)",i);
//                temp.append(str);
//                productNames.push_back(temp);
//            }
//            productNameFile.close();

//            cv::Point obj_name_pos;
//            obj_name_pos.x = 640;
//            obj_name_pos.y = 30+obj_counter*60;
//            cv::putText(display_image,productNames[maxMatchIdx],obj_name_pos,1,1,cv::Scalar(0,0,255),2,8,0);
//            obj_counter++;
//#endif


//        }
//        else
//        {
//            // Object Not present
//            cout <<  "Object is not present" << endl;
//            cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(0,255,0),2,8,0);
//        }

//        //        cv::imshow("dis_img",display_image);
//        //        cv::waitKey(0);
//    }

//    writer.write(display_image);
//    //    cv::imshow("dis_img",display_image);
//    //    cv::waitKey(0);

//    return;
//}

//void Object_detection::doObjectMatchingUsingGMM()
//{

//    // Remove patches from the list which has very small area
//    vector<cv::Rect> tempPatchList(patchesFromSurfaceNormals.begin(),patchesFromSurfaceNormals.end());

//    cout << "Number of rectangle before  \t" << patchesFromSurfaceNormals.size() << endl;

//    patchesFromSurfaceNormals.clear();


//    for(int i=-0;i<tempPatchList.size();i++)
//    {
//        if(tempPatchList[i].area() > 1000)
//            patchesFromSurfaceNormals.push_back(tempPatchList[i]);

//    }

//    cout << "Number of rectangle \t" << patchesFromSurfaceNormals.size() << endl;

//    cv::Mat display_image = test_image_orig.clone();
//    cv::Mat out_panel = cv::Mat(480,320,display_image.type(),cv::Scalar(255,255,255));
//    hconcat(display_image,out_panel,display_image);



//    int obj_counter = 0;
//    for(int i=0;i<patchesFromSurfaceNormals.size();i++)
//    {
//        cv::Mat img = test_image_orig.clone();
//        cv::Mat patchImg = img(patchesFromSurfaceNormals[i]);

//        vector<int> pixeLCountForEachTemplate(int(NO_OF_GMM_OBJECT_TEMPLATES),0);


//        cvtColor(patchImg,patchImg,CV_BGR2HSV);

//        for(int j=0;j<patchImg.cols;j++)
//        {
//            for(int k=0;k<patchImg.rows;k++)
//            {
//                cv::Mat temp(1,3,CV_64FC1);
//                temp.at<double>(0,0) = 100*double(patchImg.at<Vec3b>(k,j)[0])/180.0;
//                temp.at<double>(0,1) = 100*double(patchImg.at<Vec3b>(k,j)[1])/255.0;
//                temp.at<double>(0,2) = 100*double(patchImg.at<Vec3b>(k,j)[2])/255.0;

//                vector<cv::Mat> probs(int(NO_OF_GMM_OBJECT_TEMPLATES));
//                vector<cv::Vec2d> out(int(NO_OF_GMM_OBJECT_TEMPLATES));
//                vector<double> likelihood(int(NO_OF_GMM_OBJECT_TEMPLATES),0.0);

//                for(int l=0;l<int(NO_OF_GMM_OBJECT_TEMPLATES);l++)
//                {
//                    out[l] = emObject_Model[l].predict(temp,probs[l]);
//                    likelihood[l] = std::exp(out[l][0]);
//                }



//                vector<double> prob(int(NO_OF_GMM_OBJECT_TEMPLATES),0);
//                for(int l=0;l<int(NO_OF_GMM_OBJECT_TEMPLATES);l++)
//                {
//                    prob[l] = (double)likelihood[l]/cv::sum(likelihood)[0];
//                    cout << out[l][0] << "\t" <<  likelihood[l] << "\t" << out[l][1] << "\t" << prob[l] << endl;


//                }
//                cout << endl;

//                int maxIndex = 0;
//                double maxValue = 0.0;
//                findMaxOfArray(prob,maxIndex,maxValue);
//                //                cout << i << "\t" << j << "\t" << maxIndex << "\t" << maxValue << endl;
//                pixeLCountForEachTemplate[maxIndex] += 1;
//            }
//        }


//        int maxMatchIdx = 0;
//        int maxMatchValue = 0;
//        findMaxOfArray(pixeLCountForEachTemplate,maxMatchIdx,maxMatchValue);

//        double percent_match = (double)maxMatchValue/(patchImg.rows*patchImg.cols);
//        cout << "Percentage match \t" << percent_match << endl;
//        if(percent_match > 0.40)
//        {
//            // Object present
//            cout << "Object is present \t" << maxMatchIdx << endl;

//            cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(255,0,0),2,8,0);
//            char s[200];
//            sprintf(s,"%d",maxMatchIdx);
//            cv::Point centre;
//            centre.x = patchesFromSurfaceNormals[i].tl().x+patchesFromSurfaceNormals[i].width/2;
//            centre.y = patchesFromSurfaceNormals[i].tl().y+patchesFromSurfaceNormals[i].height/2;
//            cv::putText(display_image,s,centre,1,1,cv::Scalar(0,0,255),2,8,0);

//#if(WRITE_PRODUCT_DETAILS)

//            // Read product Names from the file
//            ifstream productNameFile;
//            string productFileAddr = ros::package::getPath("apc_obj_detection").append("/data/pNames.txt");
//            productNameFile.open(productFileAddr.c_str());
//            if(!productNameFile.is_open())
//            {
//                cerr << "Error opening product File" << endl;
//                exit(0);
//            }

//            vector<string> productNames;
//            for(int i=0;i<int(NO_OF_GMM_OBJECT_TEMPLATES);i++)
//            {
//                string temp;
//                productNameFile >> temp;
//                char str[200];
//                sprintf(str,"(%d)",i);
//                temp.append(str);
//                productNames.push_back(temp);
//            }
//            productNameFile.close();

//            cv::Point obj_name_pos;
//            obj_name_pos.x = 640;
//            obj_name_pos.y = 30+obj_counter*60;
//            cv::putText(display_image,productNames[maxMatchIdx],obj_name_pos,1,1,cv::Scalar(0,0,255),2,8,0);
//            obj_counter++;
//#endif


//        }
//        else
//        {
//            // Object Not present
//            cout <<  "Object is not present" << endl;
//            cv::rectangle(display_image,patchesFromSurfaceNormals[i],cv::Scalar(0,255,0),2,8,0);
//        }

//        //        cv::imshow("dis_img",display_image);
//        //        cv::waitKey(0);
//    }

//    writer.write(display_image);
//    //    cv::imshow("dis_img",display_image);
//    //    cv::waitKey(0);




//    return;
//}

//void Object_detection::initTemplateHistogramUsingGMM()
//{

//#if(TRAIN_GMM)
//    vector<int> templateImagesCount(17);
//    templateImagesCount[0] = 21;
//    templateImagesCount[1] = 21;
//    templateImagesCount[2] = 21;

//    templateImagesCount[3] = 11;
//    templateImagesCount[4] = 16;
//    templateImagesCount[5] = 14;

//    templateImagesCount[6] = 17;
//    templateImagesCount[7] = 20;
//    templateImagesCount[8] = 18;

//    templateImagesCount[9] = 16;
//    templateImagesCount[10] = 16;
//    templateImagesCount[11] = 18;
//    templateImagesCount[12] = 21;

//    templateImagesCount[13] = 22;
//    templateImagesCount[14] = 17;
//    templateImagesCount[15] = 8;

//    templateImagesCount[16] = 13;

//    EM tempBg,tempFg;

//    FileStorage fs_bg;
//    fs_bg.open("bgModel.txt",FileStorage::READ);
//    if(!fs_bg.isOpened())
//    {
//        cout << "Can't open Background model File" << endl;
//        exit(0);
//    }
//    const FileNode& fn_bg = fs_bg["StatModel.EM"];
//    tempBg.read(fn_bg);



//    FileStorage fs_fg;
//    fs_fg.open("fgModel.txt",FileStorage::READ);
//    if(!fs_fg.isOpened())
//    {
//        cout << "Can't open Foreground model File" << endl;
//        exit(0);
//    }
//    const FileNode& fn_fg = fs_fg["StatModel.EM"];

//    tempFg.read(fn_fg);




//    // Read the training data from 17 Templates
//    int counter = 0;

//    for(int i=0;i<templateImagesCount.size();i++)
//    {

//        cv::Mat obj_samples;

//        for(int j=0;j<templateImagesCount[i];j++)
//        {

//            if(counter ==2 || counter==42 || counter == 87)
//            {
//                counter++;
//                continue;
//            }
//            // Read object training data

//            string obj_addr = ros::package::getPath("apc_obj_detection").append("/data/");

//            char s[200];
//            sprintf(s,"rgb_data/train_img/img_%d.jpg",counter);

//            obj_addr.append(s);
//            cv::Mat obj_img = cv::imread(obj_addr.c_str());

//            //            if(obj_img.data == NULL)
//            //            {
//            //                counter++;
//            //                continue;
//            //            }

//            cout << i << "\t" << j << "\t" << obj_addr.c_str() << endl;

//            cv::cvtColor(obj_img,obj_img,CV_BGR2HSV);
//            cv::Mat obj_seg_img = cv::Mat::zeros(obj_img.size(),obj_img.type());


//            for(int l=0;l<obj_img.cols;l++)
//            {
//                for(int k=0;k<obj_img.rows;k++)
//                {
//                    cv::Mat temp(1,3,CV_64FC1);
//                    temp.at<double>(0,0) = 100*double(obj_img.at<Vec3b>(k,l)[0])/180.0;
//                    temp.at<double>(0,1) = 100*double(obj_img.at<Vec3b>(k,l)[1])/255.0;
//                    temp.at<double>(0,2) = 100*double(obj_img.at<Vec3b>(k,l)[2])/255.0;

//                    // Check if temp belongs to foreground or background
//                    cv::Mat probs_bg,probs_fg;

//                    Vec2d out_bg =  tempBg.predict(temp,probs_bg);
//                    Vec2d out_fg = tempFg.predict(temp,probs_fg);

//                    double likelihood_bg = std::exp(out_bg[0]);
//                    double likelihood_fg = std::exp(out_fg[0]);


//                    double probs = (double)likelihood_fg/(likelihood_bg+likelihood_fg);
//                    //                    cout << probs << endl;
//                    if(probs > 0.6)
//                    {
//                        obj_samples.push_back(temp.row(0));
//                        obj_seg_img.at<Vec3b>(k,l) = cv::Vec3b(255,255,255);

//                    }
//                    else
//                    {
//                        obj_seg_img.at<Vec3b>(k,l) = cv::Vec3b(0,0,0);

//                    }

//                }
//            }

//            //            cv::imshow("seg_img",obj_seg_img);
//            //            cv::waitKey(0);



//            counter++;
//        }

//        cout << "Templates sizes \t" << i << "\t" << obj_samples.rows << endl;
//        //***********************************************************************************
//        int clusNum = 3;
//        // Train background model
//        emObject_Model[i] = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

//        vector<Mat> covsInit;//(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);
//        Mat meansInput = Mat::zeros(clusNum, obj_samples.cols, CV_64FC1);
//        Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;
//        Mat out1, out2, out3;

//        cout << "Training GMM model for Object No." << i <<  endl;
//        emObject_Model[i] .trainE(obj_samples, meansInput, covsInit, weightsInput, out1, out2, out3);


//        // Saving trained Model in a file (Object Model)
//        FileStorage fs_obj;
//        char model_name[200];
//        sprintf(model_name,"obj_model%d.txt",i);
//        fs_obj.open(model_name,FileStorage::WRITE);
//        emObject_Model[i].write(fs_obj);

//    }



//#else

//    for(int i=0;i<int(NO_OF_GMM_OBJECT_TEMPLATES);i++)
//    {

//        FileStorage fs_obj;
//        char model_name[200];
//        sprintf(model_name,"obj_model%d.txt",i);
//        fs_obj.open(model_name,FileStorage::READ);

//        if(!fs_obj.isOpened())
//        {
//            cout << "Can't open Background model File" << endl;
//            exit(0);
//        }
//        const FileNode& fn_obj = fs_obj["StatModel.EM"];
//        emObject_Model[i].read(fn_obj);
//        fs_obj.release();
//        //        cout << emObject_Model[i].isTrained() << endl;

//    }

//#endif

//}

//void Object_detection::doHistogramMatchingWithTemplates()
//{

//    // Remove patches from the list which has very small area
//    vector<cv::Rect> tempPatchList(patchesFromSurfaceNormals.begin(),patchesFromSurfaceNormals.end());

//    cout << "Number of rectangle before  \t" << patchesFromSurfaceNormals.size() << endl;

//    patchesFromSurfaceNormals.clear();


//    for(int i=-0;i<tempPatchList.size();i++)
//    {
//        if(tempPatchList[i].area() > 1200)
//            patchesFromSurfaceNormals.push_back(tempPatchList[i]);

//    }

//    cout << "Number of rectangle \t" << patchesFromSurfaceNormals.size() << endl;
//    vector<cv::MatND> patchLocalHist(patchesFromSurfaceNormals.size());


//    for(int i=0;i<patchesFromSurfaceNormals.size();i++)
//    {
//        cv::Mat img = test_image_orig.clone();
//        cv::Mat patchImg = img(patchesFromSurfaceNormals[i]);
//        cv::Mat mask;
//#if(HS_HIST)
//        createHSHistogram(patchImg,patchLocalHist[i],mask);
//#else
//        createH_Histogram(patchImg,patchLocalHist[i],mask);
//#endif

//    }

//    vector<int> matchResultIndex;
//    vector<double> matchingResultCoefficient;
//    // Match histogram of templates with patches found in current image
//    for(int i=0;i<patchLocalHist.size();i++)
//    {

//        double maxMatching = 0.0;
//        int maxIndex = 0;
//        for(int j=0;j<templateHistograms.size();j++)
//        {
//            double match_coeff = cv::compareHist(patchLocalHist[i],templateHistograms[j],0);

//            if(match_coeff > maxMatching)
//            {
//                maxMatching = match_coeff;
//                maxIndex = j;
//            }
//        }
//        matchingResultCoefficient.push_back(maxMatching);
//        matchResultIndex.push_back(maxIndex);

//    }


//    cv::Mat img = test_image_orig.clone();
//    cv::Mat out_panel = cv::Mat(480,320,img.type(),cv::Scalar(255,255,255));
//    hconcat(img,out_panel,img);

//    int obj_counter = 0;
//    for(int i=0;i<matchResultIndex.size();i++)
//    {
//        if(matchingResultCoefficient[i] > 0.3)
//        {
//            cv::rectangle(img,patchesFromSurfaceNormals[i],cv::Scalar(255,0,0),2,8,0);
//            char s[200];
//            sprintf(s,"%d",matchResultIndex[i]);
//            cv::Point centre;
//            centre.x = patchesFromSurfaceNormals[i].tl().x+patchesFromSurfaceNormals[i].width/2;
//            centre.y = patchesFromSurfaceNormals[i].tl().y+patchesFromSurfaceNormals[i].height/2;
//            cv::putText(img,s,centre,1,1,cv::Scalar(0,0,255),2,8,0);

//#if(WRITE_PRODUCT_DETAILS)

//            // Read product Names from the file
//            ifstream productNameFile;
//            string productFileAddr = ros::package::getPath("apc_obj_detection").append("/data/Templates//hist_pnames.txt");
//            productNameFile.open(productFileAddr.c_str());
//            if(!productNameFile.is_open())
//            {
//                cerr << "Error opening product File" << endl;
//                exit(0);
//            }

//            vector<string> productNames;
//            for(int i=0;i<int(NO_OF_GMM_OBJECT_TEMPLATES);i++)
//            {
//                string temp;
//                productNameFile >> temp;
//                char str[200];
//                sprintf(str,"(%d)",i);
//                temp.append(str);
//                productNames.push_back(temp);
//            }
//            productNameFile.close();

//            cv::Point obj_name_pos;
//            obj_name_pos.x = 640;
//            obj_name_pos.y = 30+obj_counter*60;
//            cv::putText(img,productNames[matchResultIndex[i]],obj_name_pos,1,2,cv::Scalar(0,0,255),2,8,0);
//            obj_counter++;
//#endif

//        }
//        else
//        {
//            cv::rectangle(img,patchesFromSurfaceNormals[i],cv::Scalar(0,255,0),2,8,0);

//        }
//        cout << i << "\t" << matchResultIndex[i] << "\t" << matchingResultCoefficient[i] << endl;

//        //        cv::imshow("img_show",img);
//        //        cv::waitKey(0);

//        writer.write(img);
//    }



//    return;
//}

void Object_detection::createSegmentUsingSurfaceNormals()
{

    //--------------------------------------------------------------------------------------

    pcl::PointCloud<pcl::PointXYZ>::Ptr ptCloud( new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis( new pcl::PointCloud<pcl::PointXYZRGB> ());
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB( new pcl::PointCloud<pcl::PointXYZRGB> ());
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr input_rgb_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());


    //--------------------------------------------------------------------------------------

    // load point cloud from PCD file
    if (pcl::io::loadPCDFile<pcl::PointXYZ>(test_image_pcd_addr, *ptCloud) == -1)
    {
        std::cerr << "Couldn't read pcd file" << std::endl;
        return ;
    }
    if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(test_image_pcd_addr, *cloudRGB) == -1)
    {
        std::cerr << "Couldn't read pcd file" << std::endl;
        return ;
    }

    if (pcl::io::loadPCDFile<pcl::PointXYZRGB>(test_image_pcd_addr, *input_rgb_cloud) == -1)
    {
        std::cerr << "Couldn't read pcd file" << std::endl;
        return ;
    }

    std::cout << " point cloud size = " << ptCloud->points.size() << std::endl;

    /*
     * Creating pointcloud inputs for ROI
     */

    //------------------------------------------------------------------------------

    pcl::PointCloud<pcl::PointXYZ>::Ptr ptCloud_roi( new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudVis_roi( new pcl::PointCloud<pcl::PointXYZRGB> ());
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB_roi( new pcl::PointCloud<pcl::PointXYZRGB> ());
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr input_rgb_cloud_roi(new pcl::PointCloud<pcl::PointXYZRGB>());

    //--------------------------------------------------------------------------------------

    ptCloud_roi->height = ROI.height;
    ptCloud_roi->width = ROI.width;
    input_rgb_cloud_roi->height = ROI.height;
    input_rgb_cloud_roi->width = ROI.width;


    ptCloud_roi->points.resize(ROI.height*ROI.width);
    input_rgb_cloud_roi->points.resize(ROI.height*ROI.width);
    input_rgb_cloud_roi->is_dense = input_rgb_cloud->is_dense;
    ptCloud_roi->is_dense = ptCloud->is_dense;


    for(int j=ROI.y;j<ROI.height+ROI.y;j++)
    {
        for(int i=ROI.x;i<ROI.width+ROI.x;i++)
        {
            ptCloud_roi->at(i-ROI.x,j-ROI.y) =  ptCloud->at(i,j);
            input_rgb_cloud_roi->at(i-ROI.x,j-ROI.y) = input_rgb_cloud->at(i,j);
        }
    }


    // Input point cloud viewer
    //    pcl::visualization::PCLVisualizer inputviewer("Full cloud");
    //    inputviewer.addCoordinateSystem(0,1,0.0,0.0,0.0);
    //    inputviewer.addPointCloud(input_rgb_cloud,"input full");


    //    pcl::visualization::PCLVisualizer viewer("ROI cloud");
    //    viewer.addCoordinateSystem(0.1,0.0,0.0,0.0);
    //    viewer.addPointCloud(input_rgb_cloud_roi,"roi");

    //    while(!viewer.wasStopped())
    //    {
    //        inputviewer.spinOnce();
    //        viewer.spinOnce();
    //    }

    //    viewer.close();

    //    if(!cloudVis->empty())
    //    {
    //        viewer.addPointCloud(cloudVis, "cloud vis");
    //        while(!viewer.wasStopped())
    //        {
    //            viewer.spinOnce();
    //            //                    inputviewer.spinOnce();
    //            //                    pcl::toROSMsg(*cloudVis, pc2msg);

    //            //                    pc2msg.header.stamp = ros::Time::now();
    //            //                    pc2msg.header.frame_id = "/camera_depth_optical_frame";
    //            //                    pub.publish(pc2msg);  //pt_cloud
    //        }
    //        viewer.close();
    //    }
    //    else
    //        cout << "No points in the cloud" << endl;


    //--------------------------------------------------------------------------------------


    //Remove background points from the input pointcloud

    const float bad_point = std::numeric_limits<float>::quiet_NaN();
    cv::Vec3b bad_color(0,0,0);


    for(int i=0;i<bg_region_points.size();i++)
    {
        //        ptCloud->at(bg_region_points[i].x,bg_region_points[i].y).x = bad_point;
        //        ptCloud->at(bg_region_points[i].x,bg_region_points[i].y).y = bad_point;
        //        ptCloud->at(bg_region_points[i].x,bg_region_points[i].y).z = bad_point;

        ptCloud_roi->at(bg_region_points[i].x,bg_region_points[i].y).x = bad_point;
        ptCloud_roi->at(bg_region_points[i].x,bg_region_points[i].y).y = bad_point;
        ptCloud_roi->at(bg_region_points[i].x,bg_region_points[i].y).z = bad_point;
    }


    //-------------------------------------------------------------------------

    patchesFromSurfaceNormals.clear();
    cv::Mat img = test_image_orig.clone();
    std::vector<std::vector<int> > indices;
    //    indices = RG(ptCloud,input_rgb_cloud,patchesFromSurfaceNormals,img);
    indices = RG(ptCloud_roi,input_rgb_cloud_roi,patchesFromSurfaceNormals,img);

    //    showHandle( ptCloud, indices , cloudVis );

    //    ros::NodeHandle node;
    //    ros::Publisher pub_cloud = node.advertise<sensor_msgs::PointCloud2>("/ptCloud", 1);
    //    sensor_msgs::PointCloud2 pc2msg;

    //    while( ros::ok()  )
    //    {
    //        pcl::toROSMsg(*cloudVis, pc2msg);
    //        pc2msg.header.stamp = ros::Time::now();
    //        pc2msg.header.frame_id = "/camera_depth_optical_frame";
    //        pub_cloud.publish(pc2msg);  //pt_cloud


    //    }
    return;
}

//void Object_detection::initColorTemplates()
//{
//    for(int i=0;i<int(NO_OF_TEMPLATES);i++)
//    {
//        std::string path = ros::package::getPath("apc_obj_detection").append("/data/Templates/");
//        std::ostringstream oss;
//        oss << path << i << ".png";


//        cv::Mat img = cv::imread(oss.str());

//        //        cout << img.rows << "\t" <<  img.cols << endl;

//        //        vector<cv::Point2f> tempCenters;
//        //        vector<cv::Scalar> tempColor;
//        //        vector<double> tempArea;
//        //        vector<double> tempAngle;

//        //        // Call function
//        //        getConnectedRegions(img,tempCenters,tempColor,tempArea);
//        //        vector<int> index;

//        //        //        for(int j=0;j<tempArea.size();j++)
//        //        //            cout << tempArea[j]  << endl;


//        //        cv::sortIdx(tempArea,index,CV_SORT_DESCENDING+CV_SORT_EVERY_ROW);

//        //        vector<cv::Point2f> tCenters;
//        //        vector<cv::Scalar> tColor;
//        //        for(int j=0;j<int(NO_OF_PATCHES_TEMPLATES);j++)
//        //        {
//        //            cout << i << "\t" << tempCenters[index[j]] << endl;
//        //            tCenters.push_back(tempCenters[index[j]]);
//        //            tColor.push_back(tempColor[index[j]]);
//        //            cv::circle(img,tempCenters[index[j]],5,cv::Scalar(0,0,0),-1,8,0);
//        //            cv::imshow("img_color",img);
//        //            cv::waitKey(0);
//        //        }

//        //        double angle1 = atan2((double)(tCenters[0].y-tCenters[1].y),(tCenters[0].x-tCenters[1].x));
//        //        double angle2 = atan2((double)(tCenters[1].y-tCenters[2].y),(tCenters[1].x-tCenters[2].x));

//        //        cout << angle1 << "\t" << angle2 << "\t" << (angle2-angle1) << endl;

//        //        tempAngle.push_back(angle1);
//        //        tempAngle.push_back(angle2);
//        //        //        for(int j=0;j<tempArea.size();j++)
//        //        //            cout << tempArea[index[j]]  << endl;
//        //        templatePatchCenters.push_back(tCenters);
//        //        templatePatchColor.push_back(tColor);
//        //        templatePatchAngle.push_back(tempAngle);



//        cv::MatND tempHist;
//        cv::Mat mask;
//        // Pushing HS histogram in the vector
//#if(HS_HIST)
//        createHSHistogram(img,tempHist,mask);
//#else
//        createH_Histogram(img,tempHist,mask);
//#endif
//        templateHistograms.push_back(tempHist);

//    }



//    return;
//}

//void Object_detection::findContours()
//{
//    cv::Mat img = test_image_orig.clone();
//    cv::Mat output = cv::Mat::zeros(img.size(), CV_8UC3);

//    //    img = img(segmented_region_rect);

//    cv::Mat binary_img;
//    cvtColor(img,binary_img,CV_BGR2GRAY);

//    cv::threshold(binary_img,binary_img,100,255,CV_THRESH_BINARY);

//    std::vector<std::vector<cv::Point> > contours;
//    cv::Mat contourOutput = binary_img.clone();
//    cv::findContours( contourOutput, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE );

//    cout << "Numeber of contours \t" << contours.size() << endl;
//    //Draw the contours
//    cv::Mat contourImage(img.size(), CV_8UC3, cv::Scalar(0,0,0));
//    cv::Scalar colors[3];
//    colors[0] = cv::Scalar(255, 0, 0);
//    colors[1] = cv::Scalar(0, 255, 0);
//    colors[2] = cv::Scalar(0, 0, 255);
//    for (size_t idx = 0; idx < contours.size(); idx++) {
//        if(cv::contourArea(contours[idx]) > 1000)
//        {
//            cout << idx << "\t Contour area \t" << cv::contourArea(contours[idx]);
//            cv::drawContours(contourImage, contours, idx, colors[idx % 3]);
//        }
//    }

//    cv::imshow("Original image",img);
//    cv::imshow("Contour image",contourImage);
//    cv::waitKey(0);

//    return;
//}



//void Object_detection::findObjectsUsingPointFeatures(cv::Mat queryframe)
//{

//    vector<int> neigbourResultVector(int(NO_OF_TEMPLATES),0);

//    //Vector constaining matching descriptors indices
//    vector<int> matchingDescriptorIndices;

//    vector<int> matchInd;
//    cv::Mat currentImgDesc;
//    vector<cv::KeyPoint> currentKeyp;

//    cv::Mat currentImg = queryframe.clone();

//    cv::Mat mask = cv::Mat::zeros(currentImg.size(),CV_8U);
//    cv::Mat roi(mask,segmented_region_rect);
//    roi = Scalar(255,255,255);

//    featuredetector->detect(currentImg,currentKeyp,mask);

//    descriptorExtractor->compute(currentImg,currentKeyp,currentImgDesc);


//    for(int descCount = 0;descCount < currentImgDesc.rows;descCount++)
//    {
//        // Matrices storing nearest neighbour index and their respective distances from they query descriptor
//        cv::Mat indices,dists;
//        // Number of nearest neighbours
//        int knn = 2;
//        // Initialsing search parameters for kdtree search
//        const cv::flann::SearchParams& params=cv::flann::SearchParams(128);
//        // Searching kdtree for nearest neighbour for each feature
//        index_tree.knnSearch(cv::Mat(currentImgDesc.row(descCount)),indices,dists,knn,params);

//        // Calculating distance ratio to decide whether the nearest neighbour is correct match or not
//        float dist_ratio = float(dists.at<float>(0)/dists.at<float>(1));

//        //Condition to check whether nearest neighbour obtained through kdtree is good or not
//        if(dist_ratio < DISTANCE_RATIO)
//        {

//            matchingDescriptorIndices.push_back(indices.at<int>(0));
//            matchInd.push_back(descCount);

//            // Storing number of times same descriptor is obtained as a nearest neighbour in image
//            //            descriptorFrequency[indices.at<int>(0)]+=1;

//            //            matchedDescriptors++;

//            // Showing matched descriptors on an image
//            cv::circle(currentImg,cv::Point(currentKeyp[descCount].pt.x,currentKeyp[descCount].pt.y),2,
//                       colorVector[descriptorNodeTable.at(indices.at<int>(0)).at(0)],2,8,0);

//            objectKeypoint[descriptorNodeTable.at(indices.at<int>(0)).at(0)].push_back(currentKeyp[descCount]);



//            // Find all the nodes where this feature is present
//            for(int j=0;j<descriptorNodeTable.at(indices.at<int>(0)).size();j++)
//            {
//                // Extract the node which contains this feature increment the number of features found in a particular node
//                neigbourResultVector[descriptorNodeTable.at(indices.at<int>(0)).at(j)] += 1;

//            }

//        }

//    }

//    for(int i=0;i<neigbourResultVector.size();i++)
//        cout << i << "\t" << neigbourResultVector[i] << endl;


//    cv::imshow("matched desc image",currentImg);
//    cv::waitKey(0);

//    return;
//}

//void Object_detection::segmentForeGround()
//{
//    // Background subtractor
//    cv::Ptr<cv::BackgroundSubtractorMOG> pMog = new cv::BackgroundSubtractorMOG();
//    cv::Ptr<cv::BackgroundSubtractorMOG2> pMog2 = new cv::BackgroundSubtractorMOG2();
//    cv::Ptr<cv::BackgroundSubtractorGMG> pGMG = new cv::BackgroundSubtractorGMG();

//    pMog2->setDouble("varThreshold",1300);

//    // Foreground Mask generated after background subtraction
//    cv::Mat fgMaskMog2,fgMaskMog,fgMaskGMG;

//    pMog2->setInt("nmixtures",2);
//    pMog2->setDouble("varThresholdGen",9);

//    //    pMog2->setDouble("backgroundRatio",0.9);



//    //Training Background

//    cout << "Training background \t" << endl;
//    for(int i=6;i<int(7);i++)
//    {

//        string addr = ros::package::getPath("apc_obj_detection");
//        char s[200];
//        sprintf(s,"/data/bg/bin_img_%d.jpg",i);
//        addr.append(s);
//        cout << addr.c_str() << endl;

//        cv::Mat frame = cv::imread(addr.c_str());
//        //        cv::imshow("img",frame);
//        //        cv::waitKey(0);
//        frame = frame(ROI);
//        cv::cvtColor(frame,frame,CV_BGR2HSV);
//        pMog2->operator ()(frame,fgMaskMog2);


//        // training Frame
//        //    cv::Mat frame = bk_train_img.clone();
//        //    cv::cvtColor(frame,frame,CV_BGR2HSV);

//        //    pMog2->operator ()(frame,fgMaskMog2);
//        //    pMog->operator ()(frame,fgMaskMog);
//        //    pGMG->operator ()(frame,fgMaskGMG);

//    }

//    cv::imshow("for",fgMaskMog2);
//    cv::waitKey(0);



//    cv::Mat test_frame = test_image_orig.clone();

//    segmented_image = cv::Mat::zeros(test_frame.size(),CV_8UC3);

//    cv::cvtColor(test_frame,test_frame,CV_BGR2HSV);


//    pMog2->operator ()(test_frame,fgMaskMog2);
//    //    pMog->operator ()(test_frame,fgMaskMog);
//    //    pGMG->operator ()(test_frame,fgMaskGMG);


//    // Get background image
//    cv::Mat bak_img;
//    pMog2->getBackgroundImage(bak_img);

//    cv::medianBlur(fgMaskMog2,fgMaskMog2,41);
//    //    cv::GaussianBlur(fgMaskMog2,fgMaskMog2,cv::Size(1,3),0.5,0.5);
//    //        cv::blur(fgMaskMog2,fgMaskMog2,cv::Size(3,3));
//    // Find the pixels in original image

//    //    int dilation_elem = 0;
//    //    int dilation_size = 1;
//    //    int dilation_type;
//    //      if( dilation_elem == 0 ){ dilation_type = MORPH_RECT; }
//    //      else if( dilation_elem == 1 ){ dilation_type = MORPH_CROSS; }
//    //      else if( dilation_elem == 2) { dilation_type = MORPH_ELLIPSE; }

//    //      Mat element = getStructuringElement( dilation_type,
//    //                                           Size( 2*dilation_size + 1, 2*dilation_size+1 ),
//    //                                           Point( dilation_size, dilation_size ) );
//    //      /// Apply the dilation operation
//    //      dilate( fgMaskMog2, fgMaskMog2, element );

//    object_region_points.clear();
//    bg_region_points.clear();


//    for(int i=0;i<fgMaskMog2.rows;i++)
//    {
//        for(int j=0;j<fgMaskMog2.cols;j++)
//        {
//            cv::Scalar intensity = fgMaskMog2.at<uchar>(i,j);

//            if(intensity.val[0] > 0)
//            {
//                //                segmented_image.at<cv::Vec3b>(i+ROI.y,j+ROI.x).val[0] = 255;
//                //                segmented_image.at<cv::Vec3b>(i+ROI.y,j+ROI.x).val[1] = 255;
//                //                segmented_image.at<cv::Vec3b>(i+ROI.y,j+ROI.x).val[2] = 255;
//                //                object_region_points.push_back(cv::Point2f(j+ROI.x,i+ROI.y));

//                segmented_image.at<cv::Vec3b>(i,j).val[0] = 255;
//                segmented_image.at<cv::Vec3b>(i,j).val[1] = 255;
//                segmented_image.at<cv::Vec3b>(i,j).val[2] = 255;
//                object_region_points.push_back(cv::Point2f(j,i));
//            }
//            else
//            {

//                //                bg_region_points.push_back(cv::Point2f(j+ROI.x,i+ROI.y));
//                bg_region_points.push_back(cv::Point2f(j,i));

//            }
//        }
//    }


//    segmented_region_rect = cv::boundingRect(object_region_points);

//    cv::Mat img_s = test_image_orig.clone();

//    //    cv::imshow("Back_img",bak_img);
//    cv::imshow("Orig",img_s);
//    //    cv::imshow("Rack",frame);

//    cv::imshow("test",test_frame);
//    cv::imshow("fg Mask Mog2",fgMaskMog2);
//    //    cv::imshow("fg Mask Mog",fgMaskMog);
//    //    cv::imshow("fg Mask GMG",fgMaskGMG);
//    cv::imshow("segmented image",segmented_image);
//    cv::waitKey(0);



//    return;
//}

void Object_detection::backgroundSegmentationUsingGMM()
{

#if(TRAIN_GMM)
    //***********************************************************************************

    cv::Mat bg_samples;
    // Read background data

    for(int i=0;i<int(NO_OF_BG_TEMPLATES);i++)
    {
        string addr = ros::package::getPath("apc_obj_detection");
        char s[200];
        sprintf(s,"/data/bg/bin_img_%d.jpg",i);
        addr.append(s);
        cout << addr.c_str() << endl;

        cv::Mat frame = cv::imread(addr.c_str());
        cv::cvtColor(frame,frame,CV_BGR2HSV);

        for(int j=0;j<frame.cols;j++)
        {
            for(int k=0;k<frame.rows;k++)
            {
                cv::Mat temp(1,3,CV_64FC1);
                temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/180.0;
                temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;
                bg_samples.push_back(temp.row(0));

            }
        }

    }

    //***********************************************************************************


    cv::Mat fg_samples;
    // Read foreground data

    for(int i=0;i<int(NO_OF_TEMPLATES);i++)
    {
        std::string path = ros::package::getPath("apc_obj_detection").append("/data/Templates/");
        std::ostringstream oss;
        oss << path << i << ".png";


        cv::Mat frame = cv::imread(oss.str());
        cv::cvtColor(frame,frame,CV_BGR2HSV);

        for(int j=0;j<frame.cols;j++)
        {
            for(int k=0;k<frame.rows;k++)
            {
                cv::Mat temp(1,3,CV_64FC1);
                temp.at<double>(0,0) = 100*double(frame.at<Vec3b>(k,j)[0])/180.0;
                temp.at<double>(0,1) = 100*double(frame.at<Vec3b>(k,j)[1])/255.0;
                temp.at<double>(0,2) = 100*double(frame.at<Vec3b>(k,j)[2])/255.0;
                fg_samples.push_back(temp.row(0));

            }
        }

    }

    //***********************************************************************************
    int clusNum = 2;
    // Train background model
    bgModel = EM(clusNum, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));


    vector<Mat> covsInit;//(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);
    Mat meansInput = Mat::zeros(clusNum, bg_samples.cols, CV_64FC1);
    Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;
    Mat out1, out2, out3;

    cout << "Training GMM model" << endl;
    bgModel.trainE(bg_samples, meansInput, covsInit, weightsInput, out1, out2, out3);


    //***********************************************************************************

    // Train foreground model

    int fg_cluster = 5;
    fgModel =  EM(fg_cluster, EM::COV_MAT_DIAGONAL,TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));

    vector<Mat> covsInit_fg;//(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);
    Mat meansInput_fg = Mat::zeros(fg_cluster, fg_samples.cols, CV_64FC1);
    Mat weightsInput_fg = Mat::ones(1, fg_cluster, CV_64FC1)/fg_cluster;
    Mat out1_fg, out2_fg, out3_fg;

    fgModel.trainE(fg_samples,meansInput_fg,covsInit_fg,weightsInput_fg,out1_fg,out2_fg,out3_fg);


    //***********************************************************************************

    // Saving trained Model in a file (Both background and foreground)
    FileStorage fs_bg;
    fs_bg.open("bgModel.txt",FileStorage::WRITE);
    bgModel.write(fs_bg);

    FileStorage fs_fg;
    fs_fg.open("fgModel.txt",FileStorage::WRITE);
    fgModel.write(fs_fg);




#else


    FileStorage fs_bg;
    fs_bg.open("bgModel.txt",FileStorage::READ);
    if(!fs_bg.isOpened())
    {
        cout << "Can't open Background model File" << endl;
        exit(0);
    }
    const FileNode& fn_bg = fs_bg["StatModel.EM"];
    bgModel.read(fn_bg);



    FileStorage fs_fg;
    fs_fg.open("fgModel.txt",FileStorage::READ);
    if(!fs_fg.isOpened())
    {
        cout << "Can't open Foreground model File" << endl;
        exit(0);
    }
    const FileNode& fn_fg = fs_fg["StatModel.EM"];

    fgModel.read(fn_fg);

    fs_fg.release();
    fs_bg.release();

#endif


    cv::Mat test_image = test_image_orig.clone();
    cv::Mat orig_img = test_image_orig.clone();
    cvtColor(test_image,test_image,CV_BGR2HSV);

    cv::Mat segmented_imag = cv::Mat::zeros(test_image.size(),test_image.type());

    for(int i=0;i<test_image.cols;i++)
    {
        for(int j=0;j<test_image.rows;j++)
        {
            cv::Mat temp(1,3,CV_64FC1);

            temp.at<double>(0,0) = 100*double(test_image.at<Vec3b>(j,i)[0])/180.0;
            temp.at<double>(0,1) = 100*double(test_image.at<Vec3b>(j,i)[1])/255.0;
            temp.at<double>(0,2) = 100*double(test_image.at<Vec3b>(j,i)[2])/255.0;

            cv::Mat probs_bg,probs_fg;

            Vec2d out_bg =  bgModel.predict(temp,probs_bg);
            Vec2d out_fg = fgModel.predict(temp,probs_fg);

            double likelihood_bg = std::exp(out_bg[0]);
            double likelihood_fg = std::exp(out_fg[0]);


            double probs = (double)likelihood_fg/(likelihood_bg+likelihood_fg);

            //            cout << "prob_bg \t" << probs << endl;

            if(probs > 0.6)
            {
                segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(255,255,255);

            }
            else
            {
                segmented_imag.at<Vec3b>(j,i) = cv::Vec3b(0,0,0);

            }



        }
    }


    cv::Mat blur_media_seg;
    cv::medianBlur(segmented_imag,blur_media_seg,9);



//    cv::imshow("Original image",orig_img);
//    cv::imshow("eros",blur_media_seg);
//    cv::imshow("seg image",segmented_imag);
//    cv::waitKey(0);

    object_region_points.clear();
    bg_region_points.clear();


    for(int i=0;i<blur_media_seg.cols;i++)
    {
        for(int j=0;j<blur_media_seg.rows;j++)
        {
            if(blur_media_seg.at<Vec3b>(j,i)[0] > 250)
            {
                object_region_points.push_back(cv::Point2f(i,j));

            }
            else
            {
                bg_region_points.push_back(cv::Point2f(i,j));

            }
        }
    }


    return;

}

////void Object_detection::initFeatureTemplates()
////{
////    // Template descriptors array
////    vector<cv::Mat> template_desc_array;
////    vector< vector<cv::KeyPoint> > template_keyp_array;
////    cv::Mat featureDataset;



////    for(int i=0;i<int(NO_OF_TEMPLATES);i++)
////    {
////        std::string path = ros::package::getPath("apc_obj_detection").append("/data/Templates/");
////        std::ostringstream oss;
////        oss << path << i << ".png";
////        cv::Mat img = cv::imread(oss.str());

////        cv::Mat tempDesc;
////        vector<cv::KeyPoint> tempKey;

////        featuredetector->detect(img,tempKey);
////        descriptorExtractor->compute(img,tempKey,tempDesc);

////        cout << tempDesc.rows << "\t" << tempDesc.cols << endl;

////        template_desc_array.push_back(tempDesc);
////        template_keyp_array.push_back(tempKey);


////        vector<int> tempVector;
////        tempVector.push_back(i);
////        for(int j=0;j<tempDesc.rows;j++)
////        {
////            featureDataset.push_back(tempDesc.row(j));

////            descriptorNodeTable.insert(std::pair<int,vector<int> >(featureDataset.rows-1,tempVector));
////        }

////        //        cv::imshow("img",img);
////        //        cv::waitKey(0);
////    }


////    cout << featureDataset.rows << "\t" << descriptorNodeTable.size() << endl;

////    /*
////     Initialse a kdtree parameters
////     */
////#if(ORB)
////    cv::flann::LshIndexParams lshparams(20,20,2);
////#elif(SURF)
////    cv::flann::KDTreeIndexParams kdparams(4);
////#endif

////    cv::Mat data_set;
////    /*
////     Initialse a kdtree and build it with the dataset constructed through templates
////     */
////#if(ORB)
////    index_tree.build(featureDataset,lshparams,cvflann::FLANN_DIST_HAMMING);
////#elif(SURF)
////    index_tree.build(featureDataset,data_set,kdparams,cvflann::FLANN_DIST_L2);
////#endif

////    cout << "K-D tree built" << endl;

////    return;
////}

//void Object_detection::findColorPathcesInImage(Mat img)
//{

//    cv::Mat tempImg = img.clone();
//    patchStatusInImage.clear();
//    patchCentersInImage.clear();
//    patchColorInImage.clear();
//    patchAreaInImage.clear();

//    // Insert segmented data in pathCentersInImage vector and patchStatusInImage to the size
//    // template
//    img = img(segmented_region_rect);
//    getConnectedRegions(tempImg,patchCentersInImage,patchColorInImage,patchAreaInImage);

//    cout << "Selected regions for MRF matching \t" << patchCentersInImage.size() << endl;

//    for(int i=0;i<patchCentersInImage.size();i++)
//        patchStatusInImage.push_back(false);

//    //    cv::waitKey(0);

//    return;
//}


////void Object_detection::objectGraphMatchingMRF(cv::Mat img)
////{
////    vector<bool> objectsPresent(int(NO_OF_TEMPLATES),false);

////    // Find the candidates for each node of each template
////    for(int i=0;i<int(NO_OF_TEMPLATES);i++)
////    {
////        cout << "Number of pathces in the template \t" << templatePatchColor[i].size() << "\t" << patchColorInImage.size() << endl;
////        vector< vector<cv::Point2f> > candidates(templatePatchColor[i].size());
////        vector< vector<int> > candidates_index(templatePatchColor[i].size());

////        for(int j=0;j<templatePatchColor[i].size();j++)
////        {
////            // Compare template patch for each color and find out candidates for each
////            // color node in the template
////            for(int k=0;k<patchColorInImage.size();k++)
////            {
////                if(!patchStatusInImage[k])
////                {
////                    //                    cout << patchColorInImage[k].val[0] << "\t" << patchColorInImage[k].val[1]
////                    //                                                           << "\t" << patchColorInImage[k].val[2] << endl;
////                    //                    cout << patchColorInImage[k] << "\t" << templatePatchColor[i][j] << endl;
////                    //                    cout << j << "\t" << k << "\t" << norm(patchColorInImage[k]-templatePatchColor[i][j]) << endl;
////                    // Compare patches and insert candidates into a vector
////                    if(norm(patchColorInImage[k]-templatePatchColor[i][j]) < 60)
////                    {
////                        candidates[j].push_back(patchCentersInImage[k]);
////                        candidates_index[j].push_back(k);
////                    }
////                }
////            }
////        }

////        // Go with MRF
////        bool use_mrf = true;

////        // Check if any node in the template does not have candidates
////        for(int j=0;j<candidates.size();j++)
////        {
////            if(candidates[j].size() == 0)
////                use_mrf = false;
////        }


////        //        Show candidate matrix
////        for(int j=0;j<templatePatchColor[i].size();j++)
////        {
////            for(int k=0;k<candidates[j].size();k++)
////            {
////                cout << candidates[j][k] << "\t";
////            }
////            cout << endl;
////        }

////        //        exit(0);

////        if(use_mrf)
////        {

////            // Create adjacency matrix
////            Mat_<int> adjMatrix(templatePatchColor[i].size(),templatePatchColor[i].size(),-1);
////            for(int j=0;j<adjMatrix.rows-1;j++)
////            {
////                adjMatrix[j][j+1]=1;
////            }



////            vector<Point2f> outPts;
////            vector<int> partsAvailable;
////            vector<int> matchIdx;

////            //        vector<double> t_patchAngle;
////            //        t_patchAngle.assign(templatePatchAngle[i].begin(),templatePatchAngle[i].end);

////            // Do MRF_MAP for each template (Whether the object is present or not)
////            findMatch_MRF_MAP(adjMatrix,templatePatchAngle[i],candidates,
////                              outPts,partsAvailable,matchIdx);


////            cout << "Matched points \t" << endl;
////            cout << templatePatchCenters[i][0] << "\t" << outPts[0] << endl;
////            cout << templatePatchCenters[i][1] << "\t" << outPts[1] << endl;
////            cout << templatePatchCenters[i][2] << "\t" << outPts[2] << endl;

////            cout << "Color values of matched points \t" << endl;
////            cout << templatePatchColor[i][0] << "\t" << patchColorInImage[candidates_index[0][matchIdx[0]]] << "\t" << matchIdx[0] << endl;
////            cout << templatePatchColor[i][1] << "\t" << patchColorInImage[candidates_index[1][matchIdx[1]]] << "\t" << matchIdx[1] <<  endl;
////            cout << templatePatchColor[i][2] << "\t" << patchColorInImage[candidates_index[2][matchIdx[2]]] << "\t" << matchIdx[2] << endl;


////            bool object_present = true;
////            // Check whether this template is available or not. If object is available
////            // remove the corresponding patches from the patchCentersINImage Vector
////            for(int j=0;j<templatePatchColor[i].size();j++)
////            {
////                if(!partsAvailable[j])
////                {
////                    object_present = false;
////                    break;
////                }
////            }

////            cout << "Whether all nodes are present in the object \t" << object_present << endl;

////            if(object_present)
////                objectsPresent[i] = true;

////            if(object_present)
////            {
////                // Update the patch Status after finding objects
////                for(int j=0;j<templatePatchColor[i].size();j++)
////                {

////                }
////            }

////            cv::Mat tempimg = img /*= img(segmented_region_rect)*/;
////            for(int j=0;j<templatePatchColor[i].size();j++)
////            {
////                //            cout << outPts[0] << "\t" << outPts[1] << "\t" << outPts[2] << endl;
////                cv::circle(tempimg,outPts[j],4,cv::Scalar(255,0,0),-1,8,0);
////            }

////            // Draw line between nodes
////            for(int j=0;j<templatePatchColor[i].size()-1;j++)
////            {
////                cv::line(tempimg,outPts[j],outPts[j+1],cv::Scalar(255,0,0),1,8,0);
////            }

////            cv::imshow("im",tempimg);
////            cv::waitKey(0);
////            cout << "Check data \t" << templatePatchColor[i].size() << endl;

////        }
////        //        exit(0);

////    }

////    return;
////}

void Object_detection::generateAccuracyResults()
{
    string gtruth_addr = ros::package::getPath("apc_obj_detection").
            append("/data/gtruth.txt");

    ifstream truthFile;
    truthFile.open(gtruth_addr.c_str());
    if(!truthFile.is_open())
    {
        cerr << "Error opening gturhtFile" << endl;
        exit(0);
    }

    vector<string> obj_names(int(NO_OF_OBJECT_TEMPLATES));
    for(int i=0;i<int(NO_OF_OBJECT_TEMPLATES);i++)
    {
        truthFile >> obj_names[i];
    }

    vector< vector<int> > gtruthMatrix;
    for(int i=0;i<101;i++)
    {
        vector<int> temp(int(NO_OF_OBJECT_TEMPLATES),0);
        for(int j=0;j<int(NO_OF_OBJECT_TEMPLATES);j++)
        {
            truthFile >> temp[j];
        }

        gtruthMatrix.push_back(temp);
    }

    //    for(int i=0;i<int(NO_OF_OBJECT_TEMPLATES);i++)
    //        cout << obj_names[i] << "\t";
    //    cout << endl;

    //    for(int i=0;i<gtruthMatrix.size();i++)
    //    {
    //        for(int j=0;j<gtruthMatrix[i].size();j++)
    //        {
    //            cout << gtruthMatrix[i][j] << "\t" ;
    //        }
    //        cout << endl;
    //    }

    int truePositives =0;
    int trueNegatives = 0;
    int falsePositives = 0;
    int falseNegatives = 0;

    for(int i=0;i<gtruthMatrix.size();i++)
    {
        for(int j=0;j<gtruthMatrix[i].size();j++)
        {
            if(obj_detected_mat[i][j] == 1)
            {
                if(obj_detected_mat[i][j] == gtruthMatrix[i][j])
                {
                    truePositives++;
                }
                else
                {
                    falsePositives++;
                }
            }
            else
            {
                if(obj_detected_mat[i][j] == gtruthMatrix[i][j])
                {
                    trueNegatives++;
                }
                else
                {
                    falseNegatives++;
                }
            }
        }
    }

    cout << "True positives \t" << truePositives << endl;
    cout << "True Negatives \t" << trueNegatives << endl;
    cout << "False positives \t" << falsePositives << endl;
    cout << "False negatives \t" << falseNegatives  << endl;
    if((truePositives+falsePositives) > 0)
    {
        cout << "Precision \t" << (double)(truePositives)/((truePositives+falsePositives));
    }
    if((truePositives+falseNegatives) > 0)
    {
        cout << "Recall \t" << (double)(truePositives)/((truePositives+falseNegatives)) << endl;
    }
    return;

}


