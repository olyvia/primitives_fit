#include "primitives.h"

Primitives::Primitives()
{
    cloudRGB_ptr = boost::shared_ptr <pcl::PointCloud <pcl::PointXYZRGB> > (new pcl::PointCloud<pcl::PointXYZRGB> ());
    cloudXYZ_ptr = boost::shared_ptr <pcl::PointCloud <pcl::PointXYZ> > (new pcl::PointCloud<pcl::PointXYZ> ());

    cylinder_marker = node.advertise<visualization_msgs::MarkerArray>("/fit_primitives/cylinder_marker",10);
//    axis_marker = node.advertise<visualization_msgs::Marker>("/axis_marker",10);
//    normal_marker = node.advertise<visualization_msgs::Marker>("/normal_marker",10);

    axis_markerarray = node.advertise<visualization_msgs::MarkerArray>("/fit_primitives/axis_marker",10);
    normal_markerarray = node.advertise<visualization_msgs::MarkerArray>("/fit_primitives/normal_marker",10);
    sphere_markerarray = node.advertise<visualization_msgs::MarkerArray>("/fit_primitives/sphere_marker",10);

    bin_corners_markerarray = node.advertise<visualization_msgs::MarkerArray>("/fit_primitives/bin_corners_marker",10);


}

Primitives::~Primitives()
{
}
