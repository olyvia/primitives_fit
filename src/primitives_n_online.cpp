#include <fit_primitives/GraspPose.h>
#include <primitives.h>
#include <ros/package.h>
#include <pcl/search/organized.h>

using namespace std;
using namespace cv;
//using namespace pcl;

string obj_names[22] ={  "barbie_book","black_ball","brown_plastic_cup","camlin_color_pencils","care_mate_swipes",
                         "cleaning_brush","cloth_clips","devi_coffee",
                         "dove_soap","feeding_bottle","fevicol",
                         "garnet_bulb","green_battery","nivea_deo",
                         "patanjali_toothpaste","realsense_box","red_green_ball",
                         "scissors","skipping_rope","socks","tissue_paper","cotton_balls" };

//int pixel[2];
Vec2i top, bottom;
//bool new_right_pt = false, new_left_pt = false;
bool got_cord = false;
void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
    MouseParams* mp = (MouseParams*)userdata;

    if  ( event == EVENT_LBUTTONDOWN )
    {
//        mp->top(0) = y;
//        mp->top(1) = x;
        cout << "Left button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
//        new_left_pt = true;

        if(mp->get_tl)
        {
            mp->tl(0) = y;
            mp->tl(1) = x;
            mp->get_tl = false;
        }
        else if(mp->get_tr)
        {
            mp->tr(0) = y;
            mp->tr(1) = x;
            mp->get_tr = false;
        }
        else if(mp->get_bl)
        {
            mp->bl(0) = y;
            mp->bl(1) = x;
            mp->get_bl = false;
        }
        else if(mp->get_br)
        {
            mp->br(0) = y;
            mp->br(1) = x;
            mp->get_br = false;
        }
        else if(mp->get_rcnn_bbox)
        {
            mp->top(0) = y;
            mp->top(1) = x;
        }
    }
    else if  ( event == EVENT_RBUTTONDOWN )
    {
//        mp->bottom(0) = y;
//        mp->bottom(1) = x;
        cout << "Right button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
//        mp->got_cord = true;
//        new_right_pt = true;

        if(mp->get_rcnn_bbox)
        {
            mp->bottom(0) = y;
            mp->bottom(1) = x;
            mp->get_rcnn_bbox = false;
            mp->got_cord = true;
        }

    }
//    else if  ( event == EVENT_MBUTTONDOWN )
//    {
//        //          cout << "Middle button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
//        cout << "Saving the image: "
//             << "[" << mp->top[0] << " " << mp->top[1] << "] ["  << mp->bottom[0] << " " << mp->bottom[1] << "]" << endl;
//        //          got_cord = true;
//    }
    //     else if ( event == EVENT_MOUSEMOVE )
    //     {
    //          cout << "Mouse move over the window - position (" << x << ", " << y << ")" << endl;

    //     }
}

tf::StampedTransform getTransform(string src, string trg)
{
    tf::TransformListener transform_listener;
    tf::StampedTransform transform;

    transform_listener.waitForTransform(trg, src, ros::Time(0), ros::Duration(3));
    transform_listener.lookupTransform(trg, src, ros::Time(0), transform);

    return transform;
}

bool find2DCentroidPixels(pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_ptr, cv::Vec2i &pixel, Eigen::Vector3d point3d)
{
    pcl::search::OrganizedNeighbor<pcl::PointXYZ>::Ptr organized_search(
                new pcl::search::OrganizedNeighbor<pcl::PointXYZ>());
    organized_search->setInputCloud(cloud_ptr);

    //  At present it is organized point cloud
    //    pcl::KdTreeFLANN<pcl::PointXYZ>::Ptr kdtree(new pcl::KdTreeFLANN<pcl::PointXYZ>());
    //    kdtree->setInputCloud(cloud);

    //    if(cloud->isOrganized())
    //    {
    //        cout << "Organized" << endl;

    //    }
    //    else
    //    {
    //        cout << "Non-Organized" << endl;
    //    }

    pcl::PointXYZ searchPoint;

    searchPoint.x = point3d(0);
    searchPoint.y = point3d(1);
    searchPoint.z = point3d(2);

    // Neighbors within radius search
    std::vector<int> pointIdx;
    std::vector<float> pointSquaredDistance;

    float radius = 0.01;

    std::cout << "Neighbors within radius search at (" << searchPoint.x
              << " " << searchPoint.y
              << " " << searchPoint.z
              << ") with radius=" << radius << std::endl;
    int K = 2;
    int n_neighbours = 0;
    //        if(cloud->isOrganized())
    {
        //            n_neighbours = organized_search->radiusSearch(searchPoint, radius, pointIdx, pointSquaredDistance);
        n_neighbours = organized_search->nearestKSearch(searchPoint, K, pointIdx, pointSquaredDistance);
        n_neighbours = pointIdx.size();
    }
    //        else
    //        {
    //            n_neighbours = kdtree->radiusSearch (searchPoint, radius, pointIdx, pointSquaredDistance);
    //            n_neighbours = pointIdx.size();
    //        }



    if(n_neighbours>0)
    {
        int avg_row = 0, avg_col = 0;
        for(int j=0; j<n_neighbours; j++)
        {
            int idx = pointIdx[j];
            int row = idx/cloud_ptr->width;
            int col = idx%cloud_ptr->width;

            avg_row += row;
            avg_col += col;
        }

        pixel(0) = avg_col/n_neighbours;
        pixel(1) = avg_row/n_neighbours;

        return true;
    }
    else
        return false;
}

bool point2Dto3D(Vec2i pixel, PointXYZ &point3d, pcl::PointCloud<pcl::PointXYZRGB>::Ptr &ref_cloud)
{
    int width = ref_cloud->width, height = ref_cloud->height;
    int row = pixel[0], col = pixel[1];

//    cout << "Kernel: " ;
    for(int kernel=0; kernel<25; kernel++)
    {
        bool is_nan = true;
        double sum_x = 0, sum_y = 0, sum_z = 0;
        int count = 0;
//        cout << kernel << " ";
        for(int n=row-kernel; n<=row+kernel; n++)
        {
            for(int m=col-kernel; m<=col+kernel; m++)
            {
                if(n >=0 && n < height && m >= 0 && m < width)
                {
                    PointXYZRGB& pt = ref_cloud->at(n*width+m);
                    if(!isnan(pt.x))
                    {
                        sum_x += pt.x;
                        sum_y += pt.y;
                        sum_z += pt.z;
                        count++;
                        is_nan = false;
                    }
                }
            }
        }
        if(!is_nan)
        {
            point3d.x = sum_x/count;
            point3d.y = sum_y/count;
            point3d.z = sum_z/count;
            return true;
        }
        else
            continue;
//            return false;// could not get any valid point within a kernel of size 10*10
    }
//    cout << endl;
    return false;

}


void kinectPCCallback(const sensor_msgs::PointCloud2::ConstPtr &input, pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr,
                      bool &copy, bool &process_cloud)
{
    if(copy)
    {
        pcl::fromROSMsg(*input, *cloud_ptr);
        if(!cloud_ptr->empty())
        {
            copy = false;
//            cout << "Copied cloud in kinect call back " << endl;
//            cout << "frame: " << input->header.frame_id << endl;
            process_cloud = true;
        }
    }
    return;
}

void imageCallback(const sensor_msgs::ImageConstPtr image_ptr, cv::Mat &image, bool &get_img)
{
    if(get_img)
    {
        cv_bridge::CvImagePtr cv_image_ptr;
        try
        {
            cv_image_ptr = cv_bridge::toCvCopy(image_ptr,"bgr8");
        }
        catch(cv_bridge::Exception &e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }
        image=cv::Mat(cv_image_ptr->image);
        get_img = false;
    }
    return;
}

bool graspPoseCallback(fit_primitives::GraspPose::Request &req, fit_primitives::GraspPose::Response &res, SurfacePatch &surf_patch, Primitives &primitives_obj)
{
    static int pc_num = 0;

    //--------------------------------------------------------------------
//    cv::namedWindow("RCNN_BBOX");
//    setMouseCallback("RCNN_BBOX", CallBackFunc, (void*) &primitives_obj.mp);  /* change  */
    //------------------------------------------------------------------------
    double time = ros::Time::now().toSec();
    primitives_obj.get_img = true;

    // get the image of bin
    primitives_obj.src_image.release();

    while(ros::ok() && primitives_obj.get_img)
    {
        ros::spinOnce();
    }
    cout << "Got the image" << endl;

    ros::Rate rate(10);

//    primitives_obj.mp.get_tl = true;
//    while(ros::ok() && primitives_obj.mp.get_tl)
//    {
//        cv::imshow("RCNN_BBOX", primitives_obj.src_image);// wait to get bounding box of object
//        cv::waitKey(1);
//        ros::spinOnce();
//        cout << "Left click on top left corner of bin" << endl;
//        rate.sleep();
//    }
//    cout << "Got the top left bin corner" << endl;

//    primitives_obj.mp.get_tr = true;
//    while(ros::ok() && primitives_obj.mp.get_tr)
//    {
//        cv::imshow("RCNN_BBOX", primitives_obj.src_image);// wait to get bounding box of object
//        cv::waitKey(1);
//        ros::spinOnce();
//        cout << "Left click on top right corner of bin" << endl;
//        rate.sleep();
//    }
//    cout << "Got the top right bin corner" << endl;

//    primitives_obj.mp.get_bl = true;
//    while(ros::ok() && primitives_obj.mp.get_bl)
//    {
//        cv::imshow("RCNN_BBOX", primitives_obj.src_image);// wait to get bounding box of object
//        cv::waitKey(1);
//        ros::spinOnce();
//        cout << "Left click on bottom left corner of bin" << endl;
//        rate.sleep();
//    }
//    cout << "Got the bottom left bin corner" << endl;

//    primitives_obj.mp.get_br = true;
//    while(ros::ok() && primitives_obj.mp.get_br)
//    {
//        cv::imshow("RCNN_BBOX", primitives_obj.src_image);// wait to get bounding box of object
//        cv::waitKey(1);
//        ros::spinOnce();
//        cout << "Left click on bottom right corner of bin" << endl;
//        rate.sleep();
//    }
//    cout << "Got the bottom right bin corner" << endl;

    if(!req.roi_provided.data)
    {
        primitives_obj.mp.get_rcnn_bbox = true;
        while(ros::ok() && primitives_obj.mp.get_rcnn_bbox)
        {
            cv::imshow("RCNN_BBOX", primitives_obj.src_image);// wait to get bounding box of object
            cv::waitKey(1);
            ros::spinOnce();
            cout << "Left click on top left corner of rcnn bbox" << endl;
            cout << "Left click on bottom right corner of rcnn bbox" << endl;
            rate.sleep();
        }
        cout << "Got the rcnn bounding box" << endl;
    }
    else
    {
        primitives_obj.mp.top(0) = req.roi.data[0]; // column of top left corner in ROI
        primitives_obj.mp.top(1) = req.roi.data[1]; // row of top left corner in ROI
        primitives_obj.mp.bottom(0) = req.roi.data[2]; // column of bottom right corner in ROI
        primitives_obj.mp.bottom(1) = req.roi.data[3]; // row of bottom right corner in ROI
    }

//    cv::rectangle(primitives_obj.src_image, cv::Point (primitives_obj.mp.tl(1), primitives_obj.mp.tl(0)),
//                  cv::Point (primitives_obj.mp.br(1), primitives_obj.mp.br(0)), 4);

    //-----------------------------------------------------------------------------------------
//    cv::rectangle(primitives_obj.src_image, cv::Point (primitives_obj.mp.top(1), primitives_obj.mp.top(0)),
//                  cv::Point (primitives_obj.mp.bottom(1), primitives_obj.mp.bottom(0)), cv::Scalar(0,0,255), 2);
//    cv::imshow("RCNN_BBOX", primitives_obj.src_image);
//    waitKey(1);
    //-----------------------------------------------------------------------------------------
//    cv::destroyWindow("RCNN_BBOX");


    while( ros::ok()  )
    {
        ros::spinOnce();

//        if(!primitives_obj.mp.got_cord)
//        {
//            if(!primitives_obj.src_image.empty())
//            {
////                if(new_left_pt)
////                {
////                    cv::circle(primitives_obj.src_image, Point (primitives_obj.mp.top(1), primitives_obj.mp.top(0)), 8, Scalar( 0, 0, 255 ), -1, 8);
////                    new_left_pt = false;
////                }
////                else if(new_right_pt)
////                {
////                    cv::circle(primitives_obj.src_image, Point (primitives_obj.mp.bottom(1), primitives_obj.mp.bottom(0)), 10, Scalar( 0, 255, 255 ), -1, 8);
////                    new_right_pt = false;
////                    cv::imshow("RCNN_BBOX", primitives_obj.src_image);// wait to get bounding box of object
////                    cv::waitKey(0);
////                }

//                cv::imshow("RCNN_BBOX", primitives_obj.src_image);// wait to get bounding box of object
//                cv::waitKey(1);
//            }
////            cout << "Getting coordinates" << endl;
//        }

//        if(primitives_obj.mp.got_cord && !primitives_obj.process_pc)
        if(!primitives_obj.process_pc)
        {
            cout << "Get pt cloud " << endl;
            primitives_obj.get_cloud = true;
        }



        if(primitives_obj.process_pc)
        {
            primitives_obj.mp.got_cord = false;
            primitives_obj.process_pc = false;

            cout << "Processing pt cloud " << endl;
            pcl::PointXYZ pcl_point;
//            // tl bin corner
//            point2Dto3D(primitives_obj.mp.tl, pcl_point, primitives_obj.cloudRGB_ptr);
//            primitives_obj.bin_corners.push_back(pcl_point);
//            std::cout << pcl_point.x << " " << pcl_point.y << " " << pcl_point.z << endl;
//            // bl bin corner
//            point2Dto3D(primitives_obj.mp.bl, pcl_point, primitives_obj.cloudRGB_ptr);
//            primitives_obj.bin_corners.push_back(pcl_point);
//            std::cout << pcl_point.x << " " << pcl_point.y << " " << pcl_point.z << endl;
//            // br bin corner
//            point2Dto3D(primitives_obj.mp.br, pcl_point, primitives_obj.cloudRGB_ptr);
//            primitives_obj.bin_corners.push_back(pcl_point);
//            std::cout << pcl_point.x << " " << pcl_point.y << " " << pcl_point.z << endl;
//            // tr bin corner
//            point2Dto3D(primitives_obj.mp.tr, pcl_point, primitives_obj.cloudRGB_ptr);
//            primitives_obj.bin_corners.push_back(pcl_point);
//            std::cout << pcl_point.x << " " << pcl_point.y << " " << pcl_point.z << endl;

            tf::StampedTransform transform = getTransform("/world", "/camera_rgb_optical_frame");
            tf::Vector3 vector3_pt, tf_vector3_pt;
            std::vector<tf::Vector3> bin_corner_markers;

            primitives_obj.bin_corners.clear();
            // top left bin corner
            vector3_pt.setX(req.tl_corner.x);
            vector3_pt.setY(req.tl_corner.y);
            vector3_pt.setZ(req.tl_corner.z);

            tf_vector3_pt = transform * vector3_pt;
            bin_corner_markers.push_back(tf_vector3_pt);

            pcl_point.x = tf_vector3_pt.getX();
            pcl_point.y = tf_vector3_pt.getY();
            pcl_point.z = tf_vector3_pt.getZ();

            primitives_obj.bin_corners.push_back(pcl_point);
            //******************

            // bottom left bin corner
            vector3_pt.setX(req.bl_corner.x);
            vector3_pt.setY(req.bl_corner.y);
            vector3_pt.setZ(req.bl_corner.z);

            tf_vector3_pt = transform * vector3_pt;
            bin_corner_markers.push_back(tf_vector3_pt);

            pcl_point.x = tf_vector3_pt.getX();
            pcl_point.y = tf_vector3_pt.getY();
            pcl_point.z = tf_vector3_pt.getZ();

            primitives_obj.bin_corners.push_back(pcl_point);
            //******************

            // bottom right bin corner
            vector3_pt.setX(req.br_corner.x);
            vector3_pt.setY(req.br_corner.y);
            vector3_pt.setZ(req.br_corner.z);

            tf_vector3_pt = transform * vector3_pt;
            bin_corner_markers.push_back(tf_vector3_pt);

            pcl_point.x = tf_vector3_pt.getX();
            pcl_point.y = tf_vector3_pt.getY();
            pcl_point.z = tf_vector3_pt.getZ();

            primitives_obj.bin_corners.push_back(pcl_point);
            //******************

            //top right bin corner
            vector3_pt.setX(req.tr_corner.x);
            vector3_pt.setY(req.tr_corner.y);
            vector3_pt.setZ(req.tr_corner.z);

            tf_vector3_pt = transform * vector3_pt;
            bin_corner_markers.push_back(tf_vector3_pt);

            pcl_point.x = tf_vector3_pt.getX();
            pcl_point.y = tf_vector3_pt.getY();
            pcl_point.z = tf_vector3_pt.getZ();

            primitives_obj.bin_corners.push_back(pcl_point);
            //******************

            double bin_corner_color[3] = {1.0, 0.0, 0.0};
//            addSphereMarkerArray(bin_corner_markers, primitives_obj.bin_corners_markerarray, "/camera_rgb_optical_frame", 0.04, bin_corner_color);

            cout << "Got rcnn bbox: " << ros::Time::now().toSec()-time << endl;
            time = ros::Time::now().toSec();

            cout << "Got coordinates" << endl;

            cout << "obj name: " << obj_names[req.obj_id.data] << endl;
            pcl::copyPointCloud(*primitives_obj.cloudRGB_ptr, *primitives_obj.cloudXYZ_ptr);

            //apply workspace filter here----
            Rect bbox;
            bbox.x=primitives_obj.mp.top(1);//349;//439;
            bbox.y=primitives_obj.mp.top(0);//252;//356;/camera/rgb/image_raw
            bbox.width=primitives_obj.mp.bottom(1);//445;//490;
            bbox.height=primitives_obj.mp.bottom(0);//341;//410;
            cout << bbox.x << " " << bbox.y << " " << bbox.width << " " << bbox.height << endl;

//            Rect bbox_bin;
//            bbox_bin.x = 110;
//            bbox_bin.y = 145;
//            bbox_bin.width = 385;
//            bbox_bin.height = 286;
//            cv::rectangle(primitives_obj.src_image, cv::Point (bbox.y, bbox.x),
//                          cv::Point (bbox.width, bbox.height), 4);
//            cv::imshow("RCNN_BBOX", primitives_obj.src_image);
//            waitKey(0);

//            surf_patch.workspaceFilter(primitives_obj.cloudXYZ_ptr, primitives_obj.cloudRGB_ptr, bbox_bin);
            surf_patch.limitWorkspace(primitives_obj.cloudXYZ_ptr, primitives_obj.cloudRGB_ptr );
            surf_patch.set_boundary_pcl( primitives_obj.bin_corners );
            surf_patch.set_rcnn_bounding_box(bbox);
//            surf_patch.remove_walls_bin( primitives_obj.cloudXYZ_ptr, primitives_obj.bin_corners , 0.025 );

//            pcl::visualization::PCLVisualizer viewer( "input_grasping_cloud");
//            viewer.addPointCloud(primitives_obj.cloudRGB_ptr);
//            pcl::PointXYZ pt_1;
//            pt_1 = primitives_obj.bin_corners[0];
//            viewer.addSphere( pt_1, 0.01, 255, 0, 0, "1");
//            pt_1 = primitives_obj.bin_corners[1];
//            viewer.addSphere( pt_1, 0.01, 0, 255, 0, "2");
//            pt_1 = primitives_obj.bin_corners[2];
//            viewer.addSphere( pt_1, 0.01, 0, 0, 255, "3");
//            pt_1 = primitives_obj.bin_corners[3];
//            viewer.addSphere( pt_1, 0.01, 255, 255, 0, "4");
//            while( !viewer.wasStopped() )
//            viewer.spinOnce();
//            viewer.close();


            // save the pcd and image
            string folder = ros::package::getPath("fit_primitives");
            folder.append("/data");
            char file_num[50];
            sprintf(file_num,"_%d_%d_%d_%d", bbox.x, bbox.y, bbox.width, bbox.height);
//            cout << file_num << endl;
            string pc_file = folder, img_file_rcnn = folder, img_file_grasp_region = folder;

            pc_file.append("/cloud");
            pc_file.append(file_num);
            pc_file.append(".pcd");
            cout << pc_file << endl;
            img_file_rcnn.append("/img");
            img_file_rcnn.append(file_num);

            img_file_rcnn.append("RCNN.jpg");

            img_file_grasp_region.append("/img");
            img_file_grasp_region.append(file_num);

            img_file_grasp_region.append("grasp.jpg");

            cout << img_file_rcnn << endl;

            pcl::io::savePCDFileASCII(pc_file, *primitives_obj.cloudRGB_ptr);
            cv::imwrite(img_file_rcnn, primitives_obj.src_image);

            //-----------------------------------------------
//            for(int i=0; i<22; i++)
//                cout << i << " " << obj_names[i] << endl;

//            cout << "Enter the object id: ";

//            int obj_id ;
//            cin >> obj_id;
//            getchar();
            //----------------------------------------------------------------
            int obj_id = req.obj_id.data; /* changes made */

            std::vector<CylindricalShell> shells;
            std::string current_cls = obj_names[obj_id];
            cout << "--- Object to pick: " << current_cls << endl;
            shells = surf_patch.extract_region_main( primitives_obj.cloudXYZ_ptr, primitives_obj.cloudRGB_ptr, current_cls );


            if(shells.size() > 0)
            {
                CylindricalShell shell;
                shell = shells[0];
                double radius = 2.5*shell.getRadius();
                //            cout << "Radius: " << radius << endl;
                vector<tf::Vector3> centroids;
                tf::Vector3 c(shell.getCentroid()[0], shell.getCentroid()[1], shell.getCentroid()[2]);
                centroids.push_back(c);

                tf::Vector3 normal(shell.getNormal()[0], shell.getNormal()[1], shell.getNormal()[2]);
                tf::Vector3 axis(shell.getCurvatureAxis()[0], shell.getCurvatureAxis()[1], shell.getCurvatureAxis()[2]);

                tf::Vector3 end_pt_normal = getEndpt(normal, centroids[0]);
                tf::Vector3 end_pt_axis = getEndpt(axis, centroids[0]);

                vector<tf::Vector3> end_pts;
                end_pts.push_back(end_pt_normal);
                double normal_color[3] = {0.0, 0.0, 1.0};
                addArrowMarkerArray(centroids, end_pts, primitives_obj.normal_markerarray, "normal", normal_color);

                end_pts.clear();
                end_pts.push_back(end_pt_axis);
                double axis_color[3] = {1.0, 0.0, 0.0};

                double centroid_color[3] = {1.0, 1.0, 0.0};
                addArrowMarkerArray(centroids, end_pts, primitives_obj.axis_markerarray, "axis", axis_color);
//                addSphereMarkerArray(centroids, primitives_obj.sphere_markerarray, "/camera_rgb_optical_frame", radius, centroid_color);

                cout << "Found grasp location" << endl;
                cout << shell.getCentroid() << endl;
                cout << shell.getCurvatureAxis() << endl;
                cout << shell.getNormal() << endl;
                cout << shell.getNormal().cross(shell.getCurvatureAxis()) << endl;

//                cv::Vec2i pixel_2d;
//                if(find2DCentroidPixels(primitives_obj.cloudXYZ_ptr, pixel_2d, shell.getCentroid()))
//                    cv::circle(primitives_obj.src_image, cv::Point (pixel_2d(0), pixel_2d(1)), 8, Scalar( 0, 255, 0 ), 5, 8);

//                cv::imwrite(img_file_grasp_region, primitives_obj.src_image);


                if(c.getX() < 0.00001 && c.getY() < 0.00001 && c.getZ() < 0.00001)
                    return false;
                else
                {
                    res.centroid.x = c.getX();  res.centroid.y = c.getY();  res.centroid.z = c.getZ();
                    res.axis.x = axis.getX();   res.axis.y = axis.getY();   res.axis.z = axis.getZ();
                    res.normal.x = normal.getX();   res.normal.y = normal.getY();   res.normal.z = normal.getZ();

                    cout << "Time for grasp pose detection: " << ros::Time::now().toSec()-time << endl;
                    return true;
                }
            }
            else
            {
                cout << "No shells found" << endl;
                return false;
            }
        }

    }

}

int main(int argc, char** argv)
{
    // initialize ROS
    ros::init(argc, argv, "object_node");
    ros::NodeHandle nh;

    //declare an object
    SurfacePatch region;
    Primitives primitives;

    ros::Subscriber sub_cloud = nh.subscribe<sensor_msgs::PointCloud2>
            ("/camera/depth_registered/points", 1, boost::bind(kinectPCCallback, _1, boost::ref(primitives.cloudRGB_ptr),
                                                    boost::ref(primitives.get_cloud), boost::ref(primitives.process_pc)));

    ros::Subscriber subimage = nh.subscribe<sensor_msgs::Image>
            ("/camera/rgb/image_color",1,boost::bind(imageCallback,_1,boost::ref(primitives.src_image), boost::ref(primitives.get_img)));

    ros::ServiceServer service_grasp_pose = nh.advertiseService<fit_primitives::GraspPose::Request, fit_primitives::GraspPose::Response>
            ("/fit_primitives/grasp_pose", boost::bind(graspPoseCallback, _1, _2, boost::ref(region), boost::ref(primitives)));

    while(ros::ok())
        ros::spinOnce();



    return (0);
}

