#include <publisher.h>

void addCylinderMarker(double *centroid, geometry_msgs::Quaternion &q, double &cyl_radius, double *color, visualization_msgs::Marker &marker, std::string &frame_id, int id)
{
//    static int id = 0;

    marker.header.frame_id = frame_id;
    marker.header.stamp = ros::Time::now();
    marker.ns = "marker_cylinder";
    marker.id = id;
    marker.type = visualization_msgs::Marker::CYLINDER;
    marker.action = visualization_msgs::Marker::ADD;

    marker.pose.position.x = centroid[0];
    marker.pose.position.y = centroid[1];
    marker.pose.position.z = centroid[2];
//    marker.pose.orientation.x = q.x;
//    marker.pose.orientation.y = q.y;
//    marker.pose.orientation.z = q.z;
//    marker.pose.orientation.w = q.w;
    marker.scale.x = cyl_radius;
    marker.scale.y = cyl_radius;
    marker.scale.z = 2*cyl_radius;

    marker.color.r = color[0];
    marker.color.g = color[1];
    marker.color.b = color[2];
    marker.color.a = 0.5;

//    marker.lifetime = ros::Duration((double) MARKERS_DURATION);

//    id++;
}

void addLineMarker(string &ns, vector<tf::Vector3> &start_pt, vector<tf::Vector3> &end_pt,
                   double *color, visualization_msgs::Marker &marker, std::string &frame_id, int id)
{
    marker.header.frame_id = frame_id;
    marker.header.stamp = ros::Time::now();

    marker.ns = ns;
    marker.id = id;
    marker.type = visualization_msgs::Marker::LINE_LIST;
    marker.action = visualization_msgs::Marker::ADD;

    for(int n_i = 0; n_i<start_pt.size(); n_i++)
    {
        geometry_msgs::Point pt;
        pt.x = start_pt[n_i].getX(); pt.y = start_pt[n_i].getY(); pt.z = start_pt[n_i].getZ();

        marker.points.push_back(pt);// start_pt

        pt.x = end_pt[n_i].getX(); pt.y = end_pt[n_i].getY(); pt.z = end_pt[n_i].getZ();
        marker.points.push_back(pt);// end_pt
    }

    marker.scale.x = 0.025;

    marker.color.r = color[0];
    marker.color.g = color[1];
    marker.color.b = color[2];
    marker.color.a = 1.0;

//    marker.lifetime = ros::Duration((double) MARKERS_DURATION);
}

void addArrowMarker(tf::Vector3 &start_pt, tf::Vector3 &end_pt, double *color, visualization_msgs::Marker &marker,
                    std::string &frame_id, int id)
{
    marker.header.frame_id = frame_id;
    marker.header.stamp = ros::Time::now();

    marker.ns = "orient_vector";
    marker.id = id;
    marker.type = visualization_msgs::Marker::ARROW;
    marker.action = visualization_msgs::Marker::ADD;

    geometry_msgs::Point pt;
    pt.x = start_pt.getX(); pt.y = start_pt.getY(); pt.z = start_pt.getZ();

    marker.points.push_back(pt);// start_pt

    pt.x = end_pt.getX(); pt.y = end_pt.getY(); pt.z = end_pt.getZ();
    marker.points.push_back(pt);// end_pt


    marker.scale.x = 0.0075;
    marker.scale.y = 0.01;

    marker.color.r = color[0];
    marker.color.g = color[1];
    marker.color.b = color[2];
    marker.color.a = 1.0;

//    marker.lifetime = ros::Duration((double) MARKERS_DURATION);
}

// Function to accept rotation matrix and return Quaternion
geometry_msgs::Quaternion rotationMatrixtoQuaternion(tf::Matrix3x3 &matrix)
{
    tf::Quaternion q;
    geometry_msgs::Quaternion gm_qt;

    matrix.getRotation(q);
    gm_qt.x = q.getX();
    gm_qt.y = q.getY();
    gm_qt.z = q.getZ();
    gm_qt.w = q.getW();

    return gm_qt;

}

// Function to get the end point a vector at distance 7cm from the start point
tf::Vector3 getEndpt(tf::Vector3 vector, tf::Vector3 &start_pt)
{
    vector *= 0.2;
    tf::Vector3 end_pt(vector);
    end_pt += start_pt;

    return end_pt;
}

void addCylinderMarkerArray(vector<tf::Vector3> &centroids, vector<tf::Matrix3x3> orientation, double &cyl_radius, ros::Publisher &marker_pub)
{
    std::cout << "Publishing Markers" << std::endl;
    visualization_msgs::MarkerArray markerArray;

    visualization_msgs::Marker cyl_marker;

    double color[3] = {0.0, 1.0, 0.0};

    double centroid[3];

    cout << "ref color: cyan" << endl;
    cout << "Putting markers: " << centroids.size() << endl;
    for(int j=0; j<centroids.size(); j++)
    {
        tf::Vector3 v = centroids[j];
        centroid[0] = v.getX(); centroid[1] = v.getY(); centroid[2] = v.getZ();
        string frame = "/camera_rgb_optical_frame";
        geometry_msgs::Quaternion quat = rotationMatrixtoQuaternion(orientation[j]);

        cout << j+1 << ": " << v.getX() << " " << v.getY() << " " << v.getZ() << endl;

        addCylinderMarker(centroid, quat, cyl_radius, color, cyl_marker, frame, j);
        markerArray.markers.push_back(cyl_marker);
    }

    while(marker_pub.getNumSubscribers() < 1)
    {
        ros::Duration(3).sleep();
        cout << "Put subscriber for topic: cylinder_marker"  << endl;
    }

    marker_pub.publish(markerArray);

    return;
}

void addLineMarkerArray(vector<tf::Vector3> &start_points, vector<tf::Vector3> &end_points, ros::Publisher &marker_pub,
                        string ns, double *line_color)
{
    static int id = 0;
    std::cout << "Publishing Markers: " << ns << " lines" << std::endl;

    //    visualization_msgs::MarkerArray markerArray;
    visualization_msgs::Marker line_marker;

    string frame = "/camera_rgb_optical_frame";
    addLineMarker(ns, start_points, end_points, line_color, line_marker, frame, id++);

    while(marker_pub.getNumSubscribers() < 1)
    {
        ros::Duration(3).sleep();
        cout << "Put subscriber for topic: " << ns << "_marker"  << endl;
    }

    marker_pub.publish(line_marker);

    return;
}

void addArrowMarkerArray(vector<tf::Vector3> &start_points, vector<tf::Vector3> &end_points, ros::Publisher &marker_pub,
                        string ns, double *line_color)
{
    static int id = 0;
    std::cout << "Publishing Markers: " << ns << " lines" << std::endl;

    visualization_msgs::MarkerArray markerArray;
    visualization_msgs::Marker arrow_marker;

    string frame = "/camera_rgb_optical_frame";

    for(int n_i = 0; n_i < start_points.size(); n_i++)
    {
        addArrowMarker(start_points[n_i], end_points[n_i], line_color, arrow_marker, frame, id);
        markerArray.markers.push_back(arrow_marker);
    }
    while(marker_pub.getNumSubscribers() < 1)
    {
        ros::Duration(3).sleep();
        cout << "Put subscriber for topic: " << ns << "_marker"  << endl;
    }

    marker_pub.publish(markerArray);

    return;
}

void addSphereMarkerArray(vector<tf::Vector3> &points, ros::Publisher &marker_pub, string frame_id, double radius, double *color)
{
    std::cout << "Publishing Markers" << std::endl;
    visualization_msgs::MarkerArray markerArray;

    visualization_msgs::Marker sphere_marker;

//    double color[3] = {color[0], color[1], color[2]};

    double centroid[3];

    cout << "ref color: red" << endl;
    for(int j=0; j<points.size(); j++)
    {
        tf::Vector3 v = points[j];
        centroid[0] = v.getX(); centroid[1] = v.getY(); centroid[2] = v.getZ();
        string frame = frame_id;

        addSphereMarker(centroid,color,sphere_marker,frame, j, radius);
        markerArray.markers.push_back(sphere_marker);
    }

    while(marker_pub.getNumSubscribers() < 1)
    {
        ros::Duration(3).sleep();

        cout << "Put subscriber for topic: " << marker_pub.getTopic()  << endl;
    }

    marker_pub.publish(markerArray);

    return;
}

void addSphereMarker(double *centroid, double *color, visualization_msgs::Marker &marker, std::string &frame_id, int id, double radius)
{
    marker.header.frame_id = frame_id;
    marker.header.stamp = ros::Time::now();
    marker.ns = "marker";
    marker.id = id;
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.action = visualization_msgs::Marker::ADD;

    marker.pose.position.x = centroid[0];
    marker.pose.position.y = centroid[1];
    marker.pose.position.z = centroid[2];
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
//    radius = 0.05;
    marker.scale.x = radius;
    marker.scale.y = radius;
    marker.scale.z = radius;

    marker.color.r = color[0];
    marker.color.g = color[1];
    marker.color.b = color[2];
    marker.color.a = 1.0;

//    marker.lifetime = ros::Duration((double) MARKERS_DURATION);

//    id++;
}


